<?php
/**
 * @author Marat Komarov <bassguitarrer@gmail.com>
 */

require_once ('Cruiser/NSTree/Exception.php');

require_once ('Zend/Db/Table.php');

class Cruiser_NSTree_Table extends Zend_Db_Table 
{
	protected $_level;
	
	protected $_left;
	
	protected $_right;
	
	protected $_dataForeign;

	
	public function __construct ($config)
	{
		$this->_name 		= $config['name'];
		$this->_primary 	= $config['primary'];
		$this->_left 		= $config['left'];
		$this->_right 		= $config['right'];
		$this->_level 		= $config['level'];
		$this->_dataForeign = $config['dataForeign'];
		
		parent::__construct($config);
	}
	
	/**
	 * ������� ��������� ����� ����. 
	 * 
	 * ��������� ��� primary, left, right, level, dataForeign?
	 *
	 * @param int $id
	 * @param int $axis
	 * @param array $params
	 * @return Zend_Db_Select
	 */
	public function makeSelect ($id, $axis, $params=array()) 
	{
		$select = $this->_db->select();

		$info = $this->info();
		$cols = array();
		foreach (array_keys($info['cols']) as $column) {
			$cols[] = "s1.$column AS $column";
		}
		
       	$select->from($this->_name . ' AS s1', join(', ', $cols));
        if ($axis != Cruiser_NSTree::AXIS_SELF) {
        	$select->from($this->_name . ' AS s2', null);
        }
        
        
    	$indentCond = 's%d.%s = ?';
    	$indentValue = $id ? $id : '1';
    	$indentCond = sprintf($indentCond, 
    						  $axis == Cruiser_NSTree::AXIS_SELF ? '1' : '2', 
    						  $id ? $this->_primary : $this->_left
							  );
    	
		$select->where($indentCond, $indentValue);
	
							  
        switch ($axis) {
            case Cruiser_NSTree::AXIS_CHILD:
            case Cruiser_NSTree::AXIS_CHILD_OR_SELF:
            case Cruiser_NSTree::AXIS_LEAF:
            case Cruiser_NSTree::AXIS_DESCENDANT:
            case Cruiser_NSTree::AXIS_DESCENDANT_OR_SELF:
            
                if ($axis == Cruiser_NSTree::AXIS_CHILD) {
                	$select->where("s1.{$this->_level} = s2.{$this->_level} + 1");
                }
                
                if ($axis == Cruiser_NSTree::AXIS_CHILD_OR_SELF) {
                	$select->where("s1.{$this->_level} - s2.{$this->_level} <= ?", "1");
                }
                
                if ($axis == Cruiser_NSTree::AXIS_LEAF) {
                	$select->where("s1.{$this->_left} = s1.{$this->_right} - 1");
                }
                
                if ($axis == Cruiser_NSTree::AXIS_DESCENDANT_OR_SELF ||
                	$axis == Cruiser_NSTree::AXIS_CHILD_OR_SELF) {
                	$select->where("(s1.{$this->_left} BETWEEN s2.{$this->_left} AND s2.{$this->_right})");
                }
                else {
                    $select->where("s1.{$this->_left} > s2.{$this->_left} AND s1.{$this->_right} < s2.{$this->_right}");
                }
                
                break;
            
            case Cruiser_NSTree::AXIS_PARENT:
            	$select->where("s1.{$this->_level} = s2.{$this->_level} - 1");
            	// ������ ��� 
                
            case Cruiser_NSTree::AXIS_ANCESTOR:
            case Cruiser_NSTree::AXIS_ANCESTOR_OR_SELF:
                if ($axis == Cruiser_NSTree::AXIS_ANCESTOR_OR_SELF) {
                	$select->where("s1.{$this->_left} <= s2.{$this->_left}");
                	$select->where("s1.{$this->_right} >= s2.{$this->_right}");
                }
                else {
                	$select->where("s1.{$this->_left} < s2.{$this->_left}");
                	$select->where("s1.{$this->_right} > s2.{$this->_right}");
                }
                
                break;
            
            case Cruiser_NSTree::AXIS_FOLLOWING_SIBLING:
            case Cruiser_NSTree::AXIS_PRECENDING_SIBLING:
                if ($info = $this->getParentInfo($id)) {
                	$select->where("s2.{$this->_level} = s1.{$this->_level}");
                	$select->where("s1.{$this->_left} > ?", $info[$this->_left]);
                	$select->where("s1.{$this->_right} < ?", $info[$this->_right]);
                	
                    if ($axis == Cruiser_NSTree::AXIS_FOLLOWING_SIBLING) {
                    	$select->where("s1.{$this->_left} > s2.{$this->_right}");
                    }
                    else {
                    	$select->where("s1.{$this->_right} < s2.{$this->_left}");
                    }
                }
                else {
                	throw new Cruiser_NSTree_Exception('parent node was not found');
                	return;
                }
                
                break;
			
        }
        
        // ������������ �������������� ��������� ...
        if ($params["depth"] && 
        	in_array( $axis, array(Cruiser_NSTree::AXIS_ANCESTOR, 
        	                       Cruiser_NSTree::AXIS_ANCESTOR_OR_SELF, 
        				 		   Cruiser_NSTree::AXIS_DESCENDANT, 
        				 		   Cruiser_NSTree::AXIS_DESCENDANT_OR_SELF))) {
        
      		$depth = abs($params["depth"]);
      		
      		if (in_array ($axis, array(Cruiser_NSTree::AXIS_DESCENDANT, 
      								   Cruiser_NSTree::AXIS_DESCENDANT_OR_SELF))) {
      			
      			$select->where("s1.{$this->_level} <= s2.{$this->_level} + ?", $depth);
			}
			else {
				$select->where("s1.{$this->_level} >= s2.{$this->_level} - ?", $depth);
			}
        }
        elseif (isset($params['slice1']) && $params['slice2']) {
        	$slice1 = abs($params["slice1"]);
        	$slice2 = abs($params["slice2"]);
        	
        	if ($slice2 < $slice1) {
        		throw new Cruiser_NSTree_Exception('�������� slice2 ������ ���� ������ slice1');
        	}
        	else {
        		$ax_desc = in_array ($axis, array(Cruiser_NStree::AXIS_DESCENDANT, 
											 	  Cruiser_NSTree::AXIS_DESCENDANT_OR_SELF));
				$select->where("( "
					   . "s1.{$this->_level} BETWEEN "
					   . "s2.{$this->_level} " . ($ax_desc ? '+'.$slice1 : '-'.$slice2) . " AND "
					   . "s2.{$this->_level} " . ($ax_desc ? '+'.$slice2 : '-'.$slice1) 
					   . " )"
					   );
        	}
        }

        $select->order("s1.{$this->_left}");
        
        return $select;
	}
	
	/**
	 * @param int $id
	 * @return array
	 */
	public function getNodeInfo ($id)
	{
		$select = $this->makeSelect($id, Cruiser_NSTree::AXIS_SELF);
		return $this->_db->fetchRow($select->__toString());
	}
	
	/**
	 * @param int $id
	 * @return array
	 */
	public function getParentInfo ($id)
	{
		$select = $this->makeSelect($id, Cruiser_NSTree::AXIS_PARENT);		
		return $this->_db->fetchRow($select->__toString());
	}
	
	/**
	 * ����������� ����� � ������ ��� ��������� ����
	 *
	 * @param int $id
	 * @return int ���������� ���� ������ ����
	 */
	public function allocChild ($id, $pos)
	{
		$p = $this->getNodeInfo($id);
		
		if ($pos == Cruiser_NSTree::AT_BEGIN) {
			$v = $p[$this->_left];
			$left = $p[$this->_left] + 1;
			$right = $p[$this->_left] + 2;
		}
		elseif ($pos == Cruiser_NSTree::AT_END) {
			$v = $p[$this->_right];
			$left = $p[$this->_right];
			$right = $p[$this->_right] + 1;
		}
		else {
			throw new Cruiser_NSTree_Exception("invalid node position '$pos'");
		}		
		
		$sql = "
			UPDATE {$this->_name}
	        SET 
	        	left  = 
					CASE 
						WHEN left > :v THEN left + 2
						ELSE left
					END,
				right = 
					CASE
						WHEN right >= :v THEN right + 2
						ELSE right
					END
        	WHERE 
        		right >= :v
        ";
		$sql = $this->_inflectQuery($sql);
		$this->_db->query($sql, array("v" => $v));
		
		$columns = array(
			$this->_left 	=> $left, 
			$this->_right	=> $right, 
			$this->_level	=> $p[$this->_level] + 1
		);
		return $this->insert($columns);
	}
	
	/**
	 * ����������� ����� � ������ ��� ��������� ����
	 *
	 * @param int $id
	 * @param int $pos Cruiser_NSTree::BEFORE, Cruiser_NSTree::AFTER
	 * @return int
	 */
	function allocSibling ($id, $pos)
	{
		$n = $this->getNodeInfo($id);
		
		if ($pos == Cruiser_NSTree::BEFORE) {
			$v = $n[$this->_left];
			$left = $n[$this->_left];
			$right = $n[$this->_left] + 1;
		}
		elseif ($pos == Cruiser_NSTree::AFTER) {
			$v = $n[$this->_right];
			$left = $n[$this->_right] + 1;
			$right = $n[$this->_right] + 2;
		}
		else {
			throw new Cruiser_NSTree_Exception("invalid node position '$pos'");
		}
		
		$sql = "
 			UPDATE {$this->_name}
		    SET
		    	left = 
		    		CASE 
		    		WHEN left >= :v THEN 
		    			left + 2
		    		ELSE 
		    			left
		    		END,
		    	right =
		    		CASE 
	    			WHEN right >= :v THEN 
	    				right + 2
	    			ELSE 
	    				right
		    		END
		    WHERE 
		    	right > :v
		";
		$sql = $this->_inflectQuery($sql);
		$this->_db->query($sql, array('v' => $v));
		
		$columns = array(
			$this->_left	=> $left,
			$this->_right	=> $right,
			$this->_level	=> $n[$this->_level]
		);
		return $this->insert($columns);
		
	}
	
	/**
	 * ����������� ���� � ������ ��������
	 *
	 * @param int $id
	 * @param int $newParentId
	 * @return bool true ��� ������� ��������
	 * @throws Cruiser_NSTree_Exception
	 */	
	public function replaceNode ($id, $newParentId)
	{
		$n = $this->getNodeInfo($id);
		$p = $this->getNodeInfo($newParentId);

		if ($n[$this->_primary] == $p[$this->_primary]) {
			throw new Cruiser_NSTree_Exception('node can\'t be replaced to itself');
		}
		
		$holders = array(
			'L_n' => $n[$this->_left],
			'R_n' => $n[$this->_right],
			'V_n' => $n[$this->_level],
			'L_p' => $p[$this->_left],
			'R_p' => $p[$this->_right],
			'V_p' => $p[$this->_level]
		);
		
		if ($p[$this->_left] < $n[$this->_left] && 
			$p[$this->_right] > $n[$this->_right]) {
			// intersection
			$sql = "
                UPDATE {$this->_name}
                SET 
                	level = 
                		CASE
                		WHEN left BETWEEN :L_n AND :R_n THEN	
                			level - (:V_n - :V_p - 1)
                		ELSE 
                			level
                		END, 
                    right = 
                    	CASE
                    	WHEN right BETWEEN :R_n+1 AND :R_p-1 THEN	
                    		right - (:R_n -:L_n + 1)
                    	ELSE
                    		CASE
                    		WHEN left BETWEEN :L_n AND :R_n THEN	
                    			right + (:R_p - :R_n - 1)
                    		ELSE 
                    			right
                    		END
                    	END, 
                    left = 
                    	CASE
                    	WHEN left BETWEEN :R_n+1 AND :R_p-1 THEN 
                    		left - (:R_n - :L_n + 1)
                    	ELSE 
                    		CASE
                    		WHEN left BETWEEN :L_n AND :R_n THEN	
                    			left + (:R_p - :R_n - 1)
                    		ELSE 
                    			left
                    		END
                    	END
                WHERE 
                	left BETWEEN :L_p+1 AND :R_p-1 
			";
		}
		elseif ($n[$this->_left] > $p[$this->_right]) {
			// after
            $sql =  "
                UPDATE {$this->_name}
                SET 
                	level = 
                		CASE
                		WHEN left BETWEEN :L_n AND :R_n THEN	
                			level - (:V_n - :V_p - 1)
                		ELSE 
                			level
                		END, 
                    left = 
                    	CASE
                    	WHEN left BETWEEN :R_p AND (:L_n-1) THEN
                    		left + (:R_n-:L_n+1)
                    	ELSE
                    		CASE
                    		WHEN left BETWEEN :L_n AND :R_n THEN
                    			left-(:L_n-:R_p)
                    		ELSE
                            	left
                            END
						END,
                    right = 
                    	CASE
                    	WHEN right BETWEEN :R_p AND :L_n THEN
                        	right + (:R_n - :L_n +1)
                        ELSE
                        	CASE
                        	WHEN right BETWEEN :L_n AND :R_n THEN
	                            right-(:L_n-:R_p)
							ELSE
                            	right
                            END
						END
                WHERE 
                	left BETWEEN :L_p AND :R_n 
                		OR
                	right BETWEEN :L_p AND :R_n
            ";
		}
		elseif ($n[$this->_right] < $p[$this->_left]) {
			// before
            $sql = "
                UPDATE {$this->_name}
                SET
                    level = 
                		CASE
                		WHEN left BETWEEN :L_n AND :R_n THEN	
                			level - (:V_n - :V_p - 1)
                		ELSE 
                			level
                		END, 
                    left = 
                    	CASE
                    	WHEN left BETWEEN :R_n AND :R_p THEN
                        	left - (:R_n - :L_n + 1)
                        ELSE
                        	CASE
                        	WHEN left BETWEEN :L_n AND :R_n THEN
                            	left+(:R_p-1-:R_n)
                            ELSE
                            	left
                            END
                        END,
                    right = 
                    	CASE
                    	WHEN right BETWEEN (:R_n+1) AND (:R_p-1) THEN
                        	right-(:R_n-:L_n+1)
                        ELSE
                        	CASE
                        	WHEN right BETWEEN :L_n AND :R_n THEN
                            	right+(:R_p-1-:R_n)
                            ELSE
                            	right
                            END
                        END
                WHERE 
                    left BETWEEN :L_n AND :R_p 
                    	OR 
                    right BETWEEN :L_n AND :R_p 
            ";
		}
		else {
			throw new Cruiser_NSTree_Exception('node can\'t be placed here');
		}
		
		$sql = $this->_inflectQuery($sql);
		$this->_db->query($sql, $holders);
		
	}
	
	/**
	 * ����������� ���� ����� ����������
	 *
	 * @param int $id
	 * @param int $beforeId
	 * @return bool
	 * @throws Cruiser_NSTree_Exception
	 */
	public function replaceBefore ($id, $beforeId)
	{
		$n = $this->getNodeInfo($id);
		$b = $this->getNodeInfo($beforeId);
		
		if ($b[$this->_left] == 1) {
			throw new Cruiser_NSTree_Exception('node can\'t be placed before root');
		}
		
		$placeholders = array(
			'L_n' => $n[$this->_left],
			'R_n' => $n[$this->_right],
			'V_n' => $n[$this->_level],
			'L_b' => $b[$this->_left],
			'R_b' => $b[$this->_right],
			'V_b' => $b[$this->_level]
		);
		
		if ($n[$this->_left] > $b[$this->_left] && 
			$n[$this->_right] < $b[$this->_right]) {
			// intersection
			$sql = "
				UPDATE $this->_name
				SET
					left =
						CASE
						WHEN left BETWEEN :L_n AND :R_n THEN
							left - (:L_n - :L_b) 
						WHEN left BETWEEN :L_b AND :L_n-1 THEN
							left + (:R_n - :L_n + 1)
						ELSE
							left
						END,
					right =
						CASE
						WHEN left BETWEEN :L_n AND :R_n THEN
							right - (:L_n - :L_b)
						WHEN right BETWEEN :L_b AND :L_n-1 THEN
							right + (:R_n - :L_n + 1)
						ELSE
							right
						END,
					level = 
						CASE
						WHEN left BETWEEN :L_n AND :R_n THEN
							level - (:V_n - :V_b)
						ELSE
							level
						END
				WHERE
					left BETWEEN :L_b AND :R_b
			";
		}
		elseif ($n[$this->_right] < $b[$this->_left]) {
			// before
			$sql = "
				UPDATE $this->_name
				SET
					left =  
						CASE
						WHEN left BETWEEN :L_n AND :R_n THEN
							left + (:L_b - :R_n - 1)
						WHEN left BETWEEN :R_n+1 AND :L_b-1 THEN
							left - (:R_n - :L_n + 1)
						ELSE
							left
						END,
					right = 
						CASE 
						WHEN left BETWEEN :L_n AND :R_n THEN
							right + (:L_b - :R_n - 1)
						WHEN right BETWEEN :R_n+1 AND :L_b-1 THEN
							right - (:R_n - :L_n + 1)
						ELSE
							right
						END,
					level = 
						CASE
						WHEN left BETWEEN :L_n AND :R_n THEN
							level - (:V_n - :V_b)
						ELSE
							level
						END
				WHERE
					left BETWEEN :L_n AND :L_b-1 
						OR
					right BETWEEN :L_n AND :L_b-1
			";
		}
		elseif ($n[$this->_left] > $b[$this->_right]) {
			// after
			$sql = "
				UPDATE $this->_name
				SET
					left = 
						CASE
						WHEN left BETWEEN :L_n AND :R_n THEN
							left - (:L_n - :L_b)
						WHEN left BETWEEN :L_b AND :L_n-1 THEN
							left + (:R_n - :L_n + 1)
						ELSE
							left
						END,
					right =
						CASE
						WHEN left BETWEEN :L_n AND :R_n THEN
							right - (:L_n - :L_b)
						WHEN right BETWEEN :L_b AND :L_n-1 THEN
							right + (:R_n - :L_n + 1)
						ELSE
							right
						END,
					level = 
						CASE
						WHEN left BETWEEN :L_n AND :R_n THEN
							level - (:V_n - :V_b)
						ELSE
							level
						END
				WHERE
					left BETWEEN :L_b AND :R_n
						OR
					right BETWEEN :L_b AND :R_n
			";
		}
		else {
			throw new Cruiser_NSTree_Exception('node can\'t be placed here');
		}
		
		$sql = $this->_inflectQuery($sql);
		$this->_db->query($sql, $placeholders);
	}
	
	/**
	 * �������� ����
	 *
	 * @param int $id
	 * @param bool $removeChilds
	 * @return bool
	 */
	function deleteNode ($id, $removeChilds)
	{
		$n = $this->getNodeInfo($id);
		
		$holders = array(
			'L' => $n[$this->_left],
			'R' => $n[$this->_right]
		);
		
		if ($removeChilds) {
			// delete node and his children
			$sql = "DELETE FROM $this->_name "
			     . "WHERE $this->_left BETWEEN :L AND :R";
			$this->_db->query($sql, $holders);
			
            // clear blank spaces in a tree
            $holders['D'] = $n[$this->_right] - $n[$this->_left] + 1;
            $sql = "
                UPDATE {$this->_name}
                SET 
                    left = 
                    	CASE
                    	WHEN left > :L THEN 
                    		left - :D
                    	ELSE 
                    		left
                    	END,
                    right = 
                    	CASE
                    	WHEN right > :L THEN 
                    		right - :D
                    	ELSE 
                    		right
                    	END
                WHERE 
                	right > :R
            ";
            $sql = $this->_inflectQuery($sql);
            return $this->_db->query($sql, $holders);
		}
		else {
			// delete node structure
			$where = $this->_db->quoteInto("$this->_primary = ?", $n[$this->_primary]);
			$this->delete($where);
			
			// replace children to node parent
			$sql = "
		        UPDATE {$this->_name}
		        SET
		            left = 
		            	CASE 
		            	WHEN left BETWEEN :L AND :R THEN 
		            		left - 1
		            	WHEN left > :R THEN
		            		left - 2
		            	ELSE 
		            		left
		            	END,
		            right = 
		            	CASE
		            	WHEN right BETWEEN :L AND :R-1 THEN 
		            		right - 1
		            	WHEN right >= :R THEN
		            		right - 2
		            	ELSE 
		            		right
		            	END,
		            level = 
		            	CASE
		            	WHEN left BETWEEN :L AND :R THEN 
		            		level-1
		            	ELSE 
		            		level
		            	END
		        WHERE right > :L
			";
			$sql = $this->_inflectQuery($sql);
			return $this->_db->query($sql, $holders);
		}
	}
	
	protected function _inflectQuery ($sql)
	{
		$regexp = '/[^\:](left|right|level|primary|dataForeign)/';
		return preg_replace_callback($regexp, array($this, '_inflectCallback'), $sql);
		
	}
	
	protected function _inflectCallback ($matches)
	{
		$column = "_{$matches[1]}";
		return str_replace($matches[1], $this->$column, $matches[0]);
	}
	
}
