<?php
/**
 * @author Marat Komarov <bassguitarrer@gmail.com>
 */

require_once ('Zend/Db/Table.php');

class Cruiser_NSTree_Table2 extends Zend_Db_Table 
{
	public function __construct($config) 
	{
		$this->_name = $config['name'];
		$this->_primary = $config['primary'];
		parent::__construct($config);
	}
}

?>