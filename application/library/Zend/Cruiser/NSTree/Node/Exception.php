<?php
/**
 * @author Marat Komarov <bassguitarrer@gmail.com>
 */

require_once ('Cruiser/NSTree/Exception.php');

class Cruiser_NSTree_Node_Exception extends Cruiser_NSTree_Exception 
{}