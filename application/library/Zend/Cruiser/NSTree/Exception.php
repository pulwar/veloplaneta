<?php
/**
 * @author Marat Komarov <bassguitarrer@gmail.com>
 */

require_once ("Zend/Exception.php");

class Cruiser_NSTree_Exception extends Zend_Exception {}
