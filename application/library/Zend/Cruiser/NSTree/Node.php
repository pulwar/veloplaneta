<?php
/**
 * @author Marat Komarov <bassguitarrer@gmail.com>
 */

require_once ('Zend/Db/Table/Row.php');

require_once ('Cruiser/NStree/Node/Exception.php');

class Cruiser_NSTree_Node extends Zend_Db_Table_Row
{
	protected $_struct;
	
	public function __construct($config) 
	{
		$this->_struct = $config['struct'];
		parent::__construct($config);
	}
	
	
	public function getPrimary ()
	{
		return $this->_struct['primary'];
	}
	
	public function getDataPrimary ()
	{
		$info = $this->_table->info();
		return $info['primary'];
	}
	
	public function getStructure()
	{
		return $this->_struct;
	}
	
	public function getLevel ()
	{
		return $this->_struct['level'];
	}
	
	function isRoot () 
	{
		return $this->getLevel() == 0;
	}
	
	function isLeaf () 
	{
		return !$this->hasChildren();
	}
	
	function hasChildren () 
	{
		return $this->_struct['right'] - $this->_struct['left'] > 1;
	}
	
	// ��������� ��������� Zend_Db_Table_Row
	
	public function save ()
	{
		if (!isset($this->_struct)) {
			/**
			 * @todo ������� ������� �����
			 */
			throw new Cruiser_NSTree_Node_Exception('�������� save() ��� ����� �������������� � ������. '
					  . '�������������� �������� ���� appendChild() ��� insertSibling()');
		}
		
		parent::save();
	}
}
?>