<?php
/**
 * @author Marat Komarov <bassguitarrer@gmail.com>
 */

require_once ('Cruiser/NSTree/Node.php');

class Cruiser_NSTree_Nodeset implements RecursiveIterator
{
    protected $_nodes;
    
    protected $_db;
    
    protected $_table;
	
 	/**
     * Iterator pointer.
     */
    protected $_pointer = 0;
    
    protected $_count;

    protected $_rows = array();   
    
	public function __construct($config = array())
    {
    	$this->_nodes = $config['nodes'];
    	$this->_db = $config['db'];
    	$this->_table = $config['table'];
    	
    	$this->_count = count($this->_nodes);
    }
	
	/**
     * Return the current element.
     * Similar to the current() function for arrays in PHP
     * 
     * @return mixed current element from the collection
     */
    public function current()
    {
        // is the pointer at a valid position?
        if (! $this->valid()) {
            return false;
        }
        
        // do we already have a row object for this position?
        if (empty($this->_rows[$this->_pointer])) {
            $node = $this->_nodes[$this->_pointer];

        	// create a row object
            $this->_rows[$this->_pointer] = new Cruiser_NSTree_Node(array(
                'db'    => $this->_db,
                'table' => $this->_table,
                'struct'=> $node->struct,                
                'data'  => $node->data
            ));
        }
		
		// return the row object
		return $this->_rows[$this->_pointer];
    }
    
    public function key ()
    {
    	return $this->_pointer;
    }
    
    public function next ()
    {
    	return ++$this->_pointer;
    }
    
    public function count()
    {
        return $this->_count;
    }    
    
    public function exists()
    {
        return $this->_count > 0;
    }    
    
    public function valid ()
    {
    	return $this->_pointer < $this->count();
    }
    
    public function rewind ()
    {
    	 $this->_pointer = 0;
    }
    
    public function hasChildren ()
    {
    	return !empty($this->_nodes[$this->_pointer]->children);
    }
    
    public function getChildren ()
    {
    	return new Cruiser_NSTree_Nodeset(array(
    		'db' => $this->_db,
    		'table' => $this->_table,
    		'nodes' => $this->_nodes[$this->_pointer]->children
    	));
    }
}