<?php
/**
 * @author Marat Komarov <bassguitarrer@gmail.com>
 */


/** Zend */
//require_once ('Zend.php');

/** Cruiser_NSTree_Exception */
require_once ('Cruiser/NSTree/Exception.php');

/** Cruiser_NSTree_Table */
require_once ('Cruiser/NSTree/Table.php');

/** Cruier_NSTree_Nodeset */
require_once ('Cruiser/NSTree/Nodeset.php');

class Cruiser_NSTree
{
	/**
	 * Axises 
	 */
	const AXIS_SELF 				= 0x0000;		
	const AXIS_DESCENDANT 			= 0x0001;
	const AXIS_CHILD 				= 0x0002; 
	const AXIS_ANCESTOR 			= 0x0003;
	const AXIS_PARENT 				= 0x0004;
	const AXIS_FOLLOWING_SIBLING 	= 0x0005;
	const AXIS_PRECENDING_SIBLING	= 0x0006;
	const AXIS_LEAF 				= 0x0007;	
	const AXIS_DESCENDANT_OR_SELF	= 0x0008;
	const AXIS_ANCESTOR_OR_SELF 	= 0x0009;
	const AXIS_CHILD_OR_SELF 		= 0x0010;
	
	static public $AXES = array(
			self::AXIS_SELF, self::AXIS_DESCENDANT, self::AXIS_CHILD,
			self::AXIS_ANCESTOR, self::AXIS_PARENT,
			self::AXIS_FOLLOWING_SIBLING, self::AXIS_PRECENDING_SIBLING,
			self::AXIS_DESCENDANT_OR_SELF, self::AXIS_ANCESTOR_OR_SELF,
			self::AXIS_LEAF, self::AXIS_CHILD_OR_SELF
		  );
	
	/**
	 * Tree types
	 */
	const TYPE_SINGLE	= "single";
	const TYPE_DOUBLE	= "double";
	const TYPE_NETWORK	= "network";
	
	static public $TYPES = array(self::TYPE_SINGLE, self::TYPE_DOUBLE, self::TYPE_NETWORK);
	
	const BEFORE 	= 1;
	const AFTER 	= 2;
	const AT_BEGIN 	= 3;
	const AT_END 	= 4;
	
	/**
     * Default Zend_Db_Adapter object.
     *
     * @var Zend_Db_Adapter
     */
	static protected $_defaultDb;
	
	/**
	 * Zend_Db_Adapter object.
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $_db;
	
	/**
	 * Names of structure columns
	 *
	 * @var string
	 */
	protected $_primary,
			  $_left,
			  $_right,
			  $_level,
			  $_dataPrimary,
			  $_dataForeign;
	
	protected $_systemColumns = array();
			  
	/**
	 * Names of tables
	 *
	 * @var string
	 */
	protected $_tableName,
			  $_tableName2;

			  
	/**
	 * Tree type
	 * 
	 * 3 types defined 'single', 'double', 'network'
	 * 
	 * 	single - tree structure and data are stored in ONE table
	 * 
	 *  double - structure and data strored in SEPARATE tables
	 *  
	 *  network -same as double, but one data record can be binded to several nodes
	 *
	 * @var string
	 */
	protected $_type = self::TYPE_SINGLE;	
	
	/**
	 * Structure table. 
	 *
	 * @var Cruiser_NSTree_Table
	 */
	protected $_table;
	
	/**
	 * Slave table
	 *
	 * @var Cruiser_NSTree_Table2
	 */
	protected $_table2;
	
	/**
	 * Boolean flag, that two tables are enabled
	 *
	 * @var bool
	 */
	protected $_2tables = false;
	
	/**
	 * Data table.
	 * For 'single' tree it is $_table, 
	 * For 'double' and 'network' trees it's $_table2
	 *
	 * @var Zend_Db_Table
	 */
	protected $_dataTable;
	

	public function __construct ($config) 
	{
		if (! empty($config['db'])) {
            // convenience variable
            $db = $config['db'];

            // use an object from the registry?
            if (is_string($db)) {
                $db = Zend::registry($db);
            }

            // save the connection
            $this->_db = $db;
        }
		
        $this->_primary = $config['primary'];
        $this->_left 	= $config['left'];
        $this->_right	= $config['right'];
        $this->_level	= $config['level'];
        $this->_systemColumns = array(
        	$this->_primary => 'primary',
        	$this->_left => 'left',
        	$this->_right => 'right',
        	$this->_level => 'level'
        );

        $this->_tableName = $config['table'];
        
        $this->_type	= $config['type'];
        $this->_2tables	= in_array($config['type'], 
        						   array(self::TYPE_DOUBLE , self::TYPE_NETWORK));
        if ($this->_2tables) {
        	$this->_dataPrimary = $config['dataPrimary'];
        	$this->_dataForeign = $config['dataForeign'];
        	
        	if ($this->_dataPrimary == $this->_primary) {
        		throw new Cruiser_NSTree_Exception('���������� ����� � ������� ��������� � � ������� ������ ������ ����� ������ �����. ����� ���� �����������');
        	}
        	
        	$this->_tableName2  = $config['table2'];
			$this->_systemColumns[$this->_dataForeign] = 'dataForeign';
        }
        		
        $this->_setup();
	}
	
	
	protected function _setup ()
	{
        // get the database adapter
        if (! $this->_db) {
            $this->_db = $this->_getDefaultDbAdapter();
        }
        
        if (! $this->_db instanceof Zend_Db_Adapter_Abstract) {
            throw new Zend_Db_Table_Exception('db object does not implement Zend_Db_Adapter_Abstract');
        }
		
        if (! in_array($this->_type, self::$TYPES)) {
        	throw new Cruiser_NSTree_Exception ('����� �������� ��� ������');
        }
        
        $this->_table = new Cruiser_NSTree_Table(array(
			'name' 		=> $this->_tableName,
			'primary' 	=> $this->_primary,
			'left' 		=> $this->_left,
			'right' 	=> $this->_right,
			'level' 	=> $this->_level,
			'dataForeign'=> $this->_dataForeign,
			'db'		=> $this->_db
        ));
        
        if ($this->_2tables) {
        	Zend::loadClass('Cruiser_NSTree_Table2');
        	$this->_table2 = new Cruiser_NSTree_Table2(array(
        		'name'	=> $this->_tableName2,
        		'primary' => $this->_dataPrimary,
        		'db'	=> $this->_db
        	));
        }
        
        $this->_dataTable = $this->_2tables ? $this->_table2 : $this->_table;
	}
	
	/**
     * Sets the default Zend_Db_Adapter for all Zend_Db_Table objects.
     *
     * @param Zend_Db_Adapter $db A Zend_Db_Adapter object.
     */
    static public final function setDefaultDbAdapter($db)
    {
        // make sure it's a Zend_Db_Adapter
        if (! $db instanceof Zend_Db_Adapter_Abstract) {
            throw new Cruiser_NSTree_Exception ('db object does not extend Zend_Db_Adapter_Abstract');
        }
        self::$_defaultDb = $db;
    }
	
	/**
     * Gets the default Zend_Db_Adapter for all Zend_Db_Table objects.
     *
     */
    protected final function _getDefaultDbAdapter()
    {
        return self::$_defaultDb;
    }
    
    /**
     * Gets the Zend_Db_Adapter for this particular Cruiser_NSTree object.
     * 
     * @return Zend_Db_Adapter
     */
    public final function getDbAdapter()
    {
        return $this->_db;
    }    
    
    /**
     * Clears tree.
     * 
     * Important!!! 
     * 	this method will destroy all data in the tree
     *
     * @param array|Cruiser_NSTree_Node $data
     * @return int|Cruiser_NSTree_Node
     */
    public function clear ($data = array()) 
    {
    	// delete structure .... oops
    	$this->_table->delete('');
		
		if ($this->_2tables) {
			// delete data ....oops
			$this->_table2->delete('');
		}
		
		// create root
		$columns = array(
			$this->_left 	=> 1,
			$this->_right 	=> 2,
			$this->_level 	=> 0
		);
		$id = $this->_table->insert($columns);
		
		// bind data
		return $this->_bindNode($id, $data);
    }
    
	/**
	 * Get nodes selection
	 * 
	 * $params
	 * 	depth int - depth of the selection. will be usefull for 
	 * 				AXIS_DESCENDANT, AXIS_ANCESTOR selects
	 * 
	 *  slice1 int,
	 * 	slice2 int -select data slice, will be usefull for 
	 * 				AXIS_DESCENDANT, AXIS_ANCESTOR selects
	 * 
	 * @param int $contextId 
	 * @param int $axis axis of the selection. see AXIS_* consts
	 * @param array $params 
	 * @return Cruiser_NSTree_NodeSet
	 * @throws Cruiser_NSTree_Exception
	 */
	public function select ($contextId, $axis=self::AXIS_SELF, $params=array())
	{
		$select = $this->_table->makeSelect($contextId, $axis, $params);
		
		if ($this->_2tables) {
			$info = $this->_table2->info();
			$cols = array();
			foreach (array_keys($info['cols']) as $column) {
				$cols[] = "d.$column AS $column";
			}
			$select->from("$this->_tableName2 AS d", join(', ', $cols));
			$select->where("s1.$this->_dataForeign = d.$this->_dataPrimary");
		}
		
		$fetched = $this->_db->fetchAll($select->__toString());
		
		$data = array();
		$structure = array();
		foreach ($fetched as $row) {
			$dataRow = $this->_2tables ? array_diff_key($row, $this->_systemColumns) : $row;
			$structRow = array();
			foreach ($this->_systemColumns as $name => $title) {
				$structRow[$title] = $row[$name];
			}
			
			$structure[] = $structRow;
			$data[] = $dataRow;
		}
		
		
		/**
		 * Convert data
		 */
		
		$pseudoRoot = new stdClass();
		$pseudoRoot->children = array();
		
		$levels = array();
		$levels[$structure[0]['level']-1] = $pseudoRoot;
		
		foreach ($structure as $i => $nodeStruct)
		{
			$node = new stdClass();
			$node->struct = $nodeStruct;
			$node->data = $data[$i];
			$node->children = array();
			
			$parent = $levels[$nodeStruct['level']-1];
			$parent->children[] = $node;
			
			$levels[$nodeStruct['level']] = $node;
		}
		
		return new Cruiser_NSTree_Nodeset(array(
			'db' => $this->_db,
			'table' => $this->_dataTable,
			'nodes' => $pseudoRoot->children
		));
	}
	
	
	/**
	 * Appends the child node to existed node, identified by $parentId
	 * $pos points a position
	 * 	AT_END - at the end of children list
	 *  AT_BEGIN - at the beginig of children list
	 *
	 * @param int $parentId
	 * @param array|Cruiser_NSTree_Node $data
	 * @return Cruiser_NSTree_Node
	 */
	public function appendChild ($parentId, $data, $pos = self::AT_END) 
	{
		$newId = $this->_table->allocChild($parentId, $pos);
		return $this->_bindNode($newId, $data);
	}
	
	/**
	 * Inserts the sibling node for existing node? identified by $refId.
	 * Parameter $pos points a position of insertion
	 * 	BEFORE - before ref node
	 * 	AFTER - after ref node
	 * 
	 * Inserting a node before root will throw exception
	 * 
	 * @param int $id
	 * @param array|Cruiser_NSTree_Node $data
	 * @return Cruiser_NSTree_Node
	 * @throws Cruiser_NSTree_Exception
	 */
	public function insertSibling ($refId, $data, $pos = self::BEFORE)
	{
		$newId = $this->_table->allocSibling($refId, $pos);
		return $this->_bindNode($newId, $data);
	}
	
	/**
	 * Binds data to node structure
	 *
	 * @param int $id
	 * @param array|Cruiser_NSTree_Node $data
	 * @return Cruiser_NSTree_Node 
	 */
	protected function _bindNode ($id, $data) 
	{
		$dataId = null;
		
		if ($data instanceof Cruiser_NSTree_Node) {
			$this->_bindNode($id, $data->toArray());
		}
		
		if (is_array($data) && count($data)) {
			if ($this->_2tables) {
				$dataId = $this->_table2->insert($data);
			} else {
				$this->_table->update(array_diff_key($data, $this->_systemColumns), 
					$this->_db->quoteInto("$this->_primary = ?", $id));
			}
		}
		elseif (is_numeric($data) && $this->_type == self::TYPE_NETWORK) {
			$dataId = $data;
		}
		
		// update data Id
		if ($dataId) {
			$set = array($this->_dataForeign => $dataId);
			$this->_table->update($set, 
							$this->_db->quoteInto($this->_primary . ' = ?', $id));
		}
//		print_r($this);exit;
		return $this->select($id)->current();
	}
	
	/**
	 * Replace node to new parent
	 *
	 * @param int $id
	 * @param int $newParentId
	 * @return bool true on success
	 * @throws Cruiser_NSTree_Exception
	 */
	public function replaceNode ($id, $newParentId)
	{
		$this->_table->replaceNode($id, $newParentId);
	}
	
	/**
	 * Replace node before node identified by $beforeId
	 *
	 * @param int $id
	 * @param int $beforeId
	 * @return bool
	 * @throws Cruiser_NSTree_Exception
	 */
	public function replaceBefore ($id, $beforeId)
	{
		$this->_table->replaceBefore($id, $beforeId);
	}
	
	/**
	 * Delete nodes
	 *
	 * @param int $refId
	 * 
	 * @param bool $deleteChildren if false descendant nodes of $refId 
	 * 							   will be replaced to it's parent node
	 * 
	 * @param bool $deleteData  if true for 'double' tree data will be deleted.
	 * 							For 'network' trees not implemeted yet
	 * @return bool
	 * @throws Cruiser_NSTree_Exception
	 */
	public function deleteNode ($refId, $deleteChildren = true, $deleteData = true) 
	{
		if ($deleteData) {
			
			$n = $this->getNode($refId);
			$str = $n->getStructure();
			$bind = array(
				'L' => $str['left'],
				'R' => $str['right'],
				'I' => $str['primary']
			);
			
			if ($this->_type == self::TYPE_DOUBLE) {
				$sql = "
					DELETE FROM {$this->_tableName2}
					WHERE {$this->_dataPrimary} IN (
						SELECT {$this->_dataForeign} 
						FROM {$this->_tableName}
						WHERE %s
					)
				";
				if ($deleteChildren) {
					$sql = sprintf($sql, "{$this->_left} BETWEEN :L AND :R");
				}
				else {
					$sql = sprintf($sql, "{$this->_primary} = :I");
				}
				
				$this->_db->query($sql, $bind);
			}
			elseif ($this->_type == self::TYPE_NETWORK) {
				throw new Cruiser_NSTree_Exception('delete data in not implemented for network trees.'
						  . ' use $deleteData = false');
			}
		}
		
		return $this->_table->deleteNode($refId, $deleteChildren);
	}
	
	
	/**
	 * Creates new node
	 *
	 * @return Cruiser_NSTree_Node
	 */
	public function createNode ()
	{
		$info = $this->_dataTable->info();
		$keys = array_keys($info['cols']);
        $vals = array_fill(0, count($keys), null);		
		
		$node = new Cruiser_NSTree_Node(array(
			'db' => $this->_db,
			'table' => $this->_dataTable,
			'struct' => null,
			'data' => array_combine($keys, $vals)
		));
		
		return $node;
	}

	/**
	 * Gets the tree root node
	 *
	 * @return Cruiser_NSTree_Node
	 */
	function getRoot ()
	{
		return $this->getNode(null);
	}

	/**
	 * Gets node by identy key
	 *
	 * @param int $id
	 * @return Cruiser_NSTree_Node
	 * @throws Cruiser_NSTree_Exception  Node not found
	 * 		   Zend_Db_Exception 		 Data base errors
	 */
	function getNode ($id) 
	{
		$nodeset = $this->select($id, Cruiser_NSTree::AXIS_SELF);
		if (!$nodeset->count()) {
			throw new Cruiser_NSTree_Exception('node not found');
		}
		return $nodeset->current();
	}

	/**
	 * Gets node path from root
	 *
	 * @param int $id
	 * @return Cruiser_NSTree_Nodeset
	 */
	function getNodePath ($id) 
	{
		return $this->select($id, Cruiser_NSTree::AXIS_ANCESTOR_OR_SELF);
	}
	
	/**
	 * Gets parent node. For root node it will returns NULL
	 *
	 * @param int $id
	 * @return Cruiser_NSTree_Node
	 */
	function getParentNode ($id) 
	{
		$nodeset = $this->select($id, Cruiser_NSTree::AXIS_PARENT);
		return $nodeset->current();
	}
	
	/**
	 * Get child nodes
	 *
	 * @param int $id
	 * @return Cruiser_NSTree_Nodeset
	 */
	function getChildNodes ($id) 
	{
		return $this->select($id, Cruiser_NSTree::AXIS_CHILD);
	}
	
	/**
	 * Gets previous sibling node
	 *
	 * @param int $id
	 * @return Cruiser_NSTree_Node
	 */
	function getNextSibling ($id) 
	{
		$nodeset = $this->select($id, Cruiser_NSTree::AXIS_FOLLOWING_SIBLING);
		return $nodeset->current();
	}
	
	/**
	 * Gets next sibling node
	 *
	 * @param int $id
	 * @return Cruiser_NSTree_Node
	 */
	function getPrevSibling ($id) 
	{
		$nodeset = $this->select($id, Cruiser_NSTree::AXIS_PRECENDING_SIBLING);
		return $nodeset->current();
	}
}
