<?php

class Zend_View_Helper_Partial
{
    const NO_CLEAR = 'NO_CLEAR';
    
    /**
     * View
     *
     * @var Zend_View_Interface
     */
    protected $view = null;
    
    protected $_alwaysUsePrefixDir = true;
    
    protected $_prefixDir = '_partials/';
    
    public function partial($script, $variables = array(), $usePrefixDir = null)
    {
        if ($variables == Xend_View_Helper_Partial::NO_CLEAR || $variables === false) {
            $view = $this->view;
            $variables = array();
        } else {
            $view = clone $this->view;
            $view->clearVars();
        }
        
        foreach ($variables as $name => $value) {
            $view->$name = $value;
        }
        
        if ($usePrefixDir !== null) {
            
            if (is_string($usePrefixDir)) {
                $script = rtrim($usePrefixDir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $script;
            } elseif ($usePrefixDir === true) {
                $script = $this->_prefixDir . $script;
            }
            
        } elseif ($usePrefixDir === null) {
            if ($this->_alwaysUsePrefixDir) {
                $script = $this->_prefixDir . $script;
            }
        }

        $content = $view->render($script);
        $view = null;
        return $content;
    }
    
    public function setView($view)
    {
        $this->view = $view;
        return;
    }
    
    public function alwaysUsePrefixDir($use = null)
    {
        if ($use === true || $use === false) {
            $this->_alwaysUsePrefixDir = $use;
        }
        
        return $this->_alwaysUsePrefixDir;
    }
    
    public function setPrefixDir($dir)
    {
        $this->_prefixDir = rtrim($dir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
    }
    
}