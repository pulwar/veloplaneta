<?php

/**
 * Layout Helper
 *
 */
class Zend_View_Helper_Translate
{
    public function Translate($tr){
    	$request = Zend_Controller_Front::getInstance()->getRequest();
    	$id = $request->getParam('id');
    	if(isset($id))
    	{
    		$page = $this->getPage($id);
    		Loader::loadPublicModel('Lang');
    		$version = $page->version;
    	}
    	if (!isset($version)) $version='ru';
    	$translate = new Zend_Translate('csv', DIR_APPLICATION . 'languages' . DS . $version . ".csv", 'ru');
		$translate->addTranslation(DIR_APPLICATION . 'languages' . DS . $version . ".csv", 'en');
		$translate->addTranslation(DIR_APPLICATION . 'languages' . DS . $version . ".csv", 'de');
		
		return $translate->_($tr, $version);
    }
    
    private function getPage($id){
    	Loader::loadPublicModel('Pages');
    	
    	return Pages::getInstance()->getPage($id);
    }
        
}