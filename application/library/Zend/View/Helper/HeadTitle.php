<?php

/**
 * Layout Helper
 *
 */
class Zend_View_Helper_HeadTitle
{

    /**
     * $_prefix
     *
     * @var string
     */
    protected $_prefix  = null;
    
    /**
     * $_glue
     *
     * @var string
     */
    protected $_glue    = ' ';
    
    /**
     * $_title
     *
     * @var string
     */
    protected $_title   = null;
    
    /**
     * $_postfix
     *
     * @var string
     */
    protected $_postfix = null;
    
    /**
     * headTitle() - Helper Method
     *
     * @param string $title
     * @return Xend_Layout_ViewHelper_HeadTitle
     */
    public function headTitle($title = null, $prefix = null, $postfix = null, $glue = null)
    {
        if ($title) {
            $this->setTitle($title);
        }
        
        if ($prefix) {
            $this->setPrefix($prefix);
        }
        
        if ($postfix) {
            $this->setPostfix($postfix);
        }
        
        if ($glue) {
            $this->setGlue($glue);
        }
        
        return $this;
    }
    
    /**
     * setTitle()
     *
     * @param string $title
     * @return Xend_Layout_ViewHelper_HeadTitle
     */
    public function setTitle($title)
    {
        $this->_title = (string) $title;
        return $this;
    }
    
    /**
     * appendTitle()
     *
     * @param string $title
     * @return Xend_Layout_ViewHelper_HeadTitle
     */
    public function appendTitle($title, $glue = null)
    {
        $glue = ($glue != null) ? $glue : $this->_glue;
        $this->_title = $this->_title . $glue . (string) $title;
        return $this;
    }
    
    /**
     * getTitle()
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }
    
    /**
     * setPrefix()
     *
     * @param string $prefix
     * @return Xend_Layout_ViewHelper_HeadTitle
     */
    public function setPrefix($prefix)
    {
        $this->_prefix = $prefix;
        return $this;
    }
    
    /**
     * getPrefix()
     *
     * @return string
     */
    public function getPrefix()
    {
        return $this->_prefix;
    }

    /**
     * setGlue()
     *
     * @param string $glue
     * @return Xend_Layout_ViewHelper_HeadTitle
     */
    public function setGlue($glue)
    {
        $this->_glue = $glue;
        return $this;
    }
    
    /**
     * getGlue()
     *
     * @return string
     */
    public function getGlue()
    {
        return $this->_glue;
    }
    
    /**
     * setPostfix()
     *
     * @param string $postfix
     * @return Xend_Layout_ViewHelper_HeadTitle
     */
    public function setPostfix($postfix)
    {
        $this->_postfix = $postfix;
        return $this;
    }
    
    /**
     * getPostfix()
     *
     * @return string
     */
    public function getPostfix()
    {
        return $this->_postfix;
    }
    
    /**
     * toString()
     *
     * @return string
     */
    public function toString()
    {
        return $this->__toString();
    }
    
    /**
     * __toString()
     *
     * @return string
     */
    public function __toString()
    {
        $return = '';
        if (($prefix = $this->getPrefix()) != null) {
            $return .= $prefix . $this->_glue;
        }

        $return .= $this->getTitle();

        if (($postfix = $this->getPostfix()) != null) {
            $return .= $this->_glue . $postfix;
        }

        return '<title>' . $return . '</title>';
    }

}