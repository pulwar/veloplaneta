<?php

/**
 * Layout Helper
 *
 */
class Zend_View_Helper_Sitemap
{
    public function Sitemap($lang)
    {
        Loader::loadPublicModel('Pages');
        $root = Pages::getInstance()->getRoot($lang);
        $sitemap = Pages::getInstance()->getSiteMap($lang, $root->id);
        
        return $sitemap;
    }
        
}