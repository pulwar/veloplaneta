<?php
/**
 * @see Zend_Filter_Interface
 */
require_once 'Zend/Filter/Interface.php';


/**
 * @category   Zend
 * @package    Zend_Filter
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Gr_Filter_Quote implements Zend_Filter_Interface
{
    /**
     * Defined by Zend_Filter_Interface
     * @param  string $value
     * @return string
     */
    public function filter($value)
    {	
		return str_replace('"' , '&quot;' ,$value);        
    }
}
