<?php

/**
 * Layout Helper
 * обрезает текст до заданной длинны по пробелам
 *
 *
 */
class Gr_View_Helper_CutTextByWords extends Zend_View_Helper_Abstract {

    /**
     * @var Zend_View
     */
    protected $_view = null ;

    /**
     * Enter description here...
     *
     */
    public function init() {

        $this->_view = Registry::getView() ;


    }

    /**
     * Site menu
     *
     * @param int $type
     * @param array $items
     * @return phtml
     */
    public function cutTextByWords($text, $counttext = 200) {  
    	$text = strip_tags($text);  	
    	$text = (strlen($text) > $counttext)?substr($text, 0, strpos($text,' ', $counttext)).'...':$text;    	
        
        return $text;
    }
}
