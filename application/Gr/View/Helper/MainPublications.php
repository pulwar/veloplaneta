<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_MainPublications extends Zend_View_Helper_Abstract
{
   
	var $_view = null;	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {
		$this->_view = Registry::getView ();		
	}
	
	/**
	 * Get baners for plase
	 */
	public function MainPublications(){
		$this->init();
		$item = Publications::getInstance()->fetchRow("active=1 AND file!=''", 'added DESC');
		$this->_view->item = $item;
		return $this->_view->render('MainPublications.phtml');
	}	
}