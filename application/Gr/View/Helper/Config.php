<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_Config extends Zend_View_Helper_Abstract
{
    /**
	 * @var Zend_View
	 */
	protected $_view = null ;
	

	public function init() {
		
		$this->_view = Registry::getView() ;
		
	}
	
	
	public function Config($name){
       return Configs::getInstance()->getByName($name);
	}	
}