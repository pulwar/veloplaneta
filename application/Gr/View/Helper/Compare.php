<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_Compare extends Zend_View_Helper_Abstract {
	
	/**
	 * Zend_Session_Namespace
	 *
	 * @var Zend_Session_Namespace
	 */
	private $_compareSession = null;
	/**
	 * @var Zend_View
	 */
	protected $_view = null;
	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {
		$this->_view = Registry::getView ();
		$this->_compareSession = new Zend_Session_Namespace ( 'compare' );
	}
	
	/**
	 * Enter description here...
	 *
	 * 
	 * @return html
	 */
	public function Compare() {
		$this->init ();
		if (isset ( $this->_compareSession->items )) {
			$items = $this->_compareSession->items;
			$this->_view->tovars = Catalog_Tovar::getInstance ()->find ( array_keys ( $items ) );
		}
		
		return $this->_view->render ( 'CompareFront.phtml' );
	}

}