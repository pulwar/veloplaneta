<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_Series extends Zend_View_Helper_Abstract
{
    /**
	 * @var Zend_View
	 */
	protected $_view = null ;

    protected $_current_div = null;


	public function init() {

		$this->_view = Registry::getView() ;
		$this->_current_div = Zend_Controller_Front::getInstance()->getParam('div_id');
		//$this->_current_div = 1;

	}

	/**
	 * gets main items  from infoblock
	 */
	public function Series($divId = 0, $currentId=5)
        {
            $this->init();
            $catalog_page_id = 14;
            $items =  Catalog_Division::getInstance()->getChildDivisions($divId);
            
            //$items = Catalog_Division::getInstance()->fetchAll('level=0 AND id_page='.(int)$catalog_page_id, 'sortId');
            $this->_view->items = $items;
            $this->_view->cur_id = $currentId;
            //$this->_view->current_div = $this->_current_div;
            return $this->_view->render( 'Series.phtml' ) ;

	}

}