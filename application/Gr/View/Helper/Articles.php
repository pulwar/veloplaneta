<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_Articles extends Zend_View_Helper_Abstract
{

	/**
	 * @var Zend_View
	 */
	protected $_view = null ;

	/**
	 * Enter description here...
	 *
	 */
	public function init() {

		$this->_view = Registry::getView() ;

	}

	/**
	 * Site menu
	 *
	 * @param string $type
	 * @param string $version
	 * @return phtml
	 */
    public function Articles($type)
    {
        //$type:
        //'columnList'
        //'pageList'
        //'page'
    	$this->init();
        switch ($type) {
            // case 'columnList':
            //     $this->_view->articles = News::getInstance()->getMain('articles', 0);
            //     return $this->_view->render( 'ArticlesColumnList.phtml' ) ;
            //     break;
            case 'pageList':
                return $this->_view->render( 'ArticlesPageList.phtml' ) ;
                break;
            case 'page':
                return $this->_view->render( 'ArticlesPage.phtml' ) ;
                break;
            default:
                break;
        }

    }


}