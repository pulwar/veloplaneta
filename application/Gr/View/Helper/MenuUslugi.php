<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_MenuUslugi extends Zend_View_Helper_Abstract
{
	
	/**
	 * @var Zend_View
	 */
	protected $_view = null ;
	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {
		
		$this->_view = Registry::getView() ;
		
	}
	
	/**
	 * Site menu
	 *
	 * @param string $type
	 * @param string $version
	 * @return phtml
	 */
    public function MenuUslugi($parent_id=172)
    {   //172 - страница услуги
    	$this->init();
    	$current_page_url =substr($_SERVER['REQUEST_URI'], 1);
    	$active[] = $current_page_url;
    	$cur_page = Pages::getInstance()->getPageByParam('path', $current_page_url);
    	if ($cur_page && $cur_page->parentId!=1){// существует родительская страница
    		 $active[] = Pages::getInstance()->find($cur_page->parentId)->current()->path;
    	}
    	   	
    	$items = Pages::getInstance()->fetchAll("parentId=".(int)$parent_id." AND published=1", 'sortId ASC');
    	//print_r($items);
    	$this->_view->items = $items;
    	$this->_view->active = $active;    	
        return $this->_view->render( 'MenuUslugi.phtml' ) ;
    }
    
    
        
}