<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_CatalogBrandsLoop extends Zend_View_Helper_Abstract
{
    /**
	 * @var Zend_View
	 */
	protected $_view = null ;
	
    protected $_current_div = null;
	
    
	public function init() {
		
		$this->_view = Registry::getView() ;		
		
	}
	
	/**
	 * gets main items  from infoblock
	 */
	public function CatalogBrandsLoop()
        {
            $this->init();
            $catalog_page_id = 14;
            $page= Pages::getInstance()->find($catalog_page_id)->current();
            $page_path = '';
            if ($page!=null){
                    $page_path = $page->path;

            }
            unset($page);
            $this->_view->page_path = $page_path;
            $items = Catalog_Division::getInstance()->fetchAll("level=0 AND parent_id=0 AND img!='' ", "RAND()");
            $this->_view->items = $items;
            return $this->_view->render( 'CatalogBrandsLoop.phtml' ) ;
        
	}	
}