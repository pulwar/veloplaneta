<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_MainNews extends Zend_View_Helper_Abstract
{
    /**
	 * @var Zend_View
	 */
	protected $_view = null ;
	

	public function init() {
		
		$this->_view = Registry::getView() ;
		
	}
	
	/**
	 * gets main items  from infoblock
	 */
	public function MainNews($type, $limit=0)
    {
        $this->init();
        $this->_view->main_news = News::getInstance()->getMain($type, $limit);
        return $this->_view->render( 'MainNews.phtml' ) ;
        
	}	
}