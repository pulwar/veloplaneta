<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_SearchResult extends Zend_View_Helper_Abstract {
	
	/**
	 * Zend_Session_Namespace
	 *
	 * @var Zend_Session_Namespace
	 */
	private $_search = null;
	/**
	 * @var Zend_View
	 */
	protected $_view = null;
	/**
	 * Pages Instance
	 */
	protected $_Pages=null;
	/**
	 * News Instance
	 */
	protected $_News=null;
	/**
	 * Catalog_Division Instance
	 */
	protected $_Division=null;
	/**
	 * Catalog_Tovar Instance
	 */
	protected $_Tovar=null;
	
	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {
		$this->_view = Registry::getView ();
		$this->_Pages = Pages::getInstance();
    	$this->_News = News::getInstance();
    	$this->_Division = Portfolio_Divisions::getInstance();
    	$this->_Work = Portfolio_Works::getInstance();
		$this->_search = new Zend_Session_Namespace ( 'search' );
	}
	
	/**
	 * Enter description here...
	 *
	 * 
	 * @return html
	 */
	public function SearchResult($search, $result) {
		//$this->init ();
		$search_string='';
		if (isset ( $result )&& count($result)>0) {			
			$path = array(
				'works' => Pages::getInstance()->getPageByParam('template', 'works')->path,
				'publications' => Pages::getInstance()->getPageByParam('template', 'publications')->path,
				'peoples' => Pages::getInstance()->getPageByParam('template', 'peoples')->path
				
			);
			$search_string.='';
				foreach ($result as $key=>$data){ 
					switch ($data['TYPE']){ 						
						
						case 'pages':
							 			
										$search_string .= '
										<div class="s_result">
	         								<p class="link">
	         									<a href="/'.$data['path'].'">'.$data['name'].'</a>
	         								</p>
         								</div>';
							
						break; 
						
						default:	
										if (isset($data['page_id']) && $data['page_id']!=0){
											$url = Pages::getInstance()->find((int)$data['page_id'])->current()->path;
											$search_string .= '
												<div class="s_result">
			         								<p class="link">
			         									<a href="/'.$url.'/item/'.$data['url'].'">'.$data['name'].'</a>
			         								</p>
		         								</div>';
										} else{
								 			$search_string .= '
											<div class="s_result">
		         								<p class="link">
		         									<a href="/'.$path[$data['TYPE']].'/item/'.$data['url'].'">'.$data['name'].'</a>
		         								</p>
	         								</div>';
										}
						break; 
							
					} 
				} 
			$search_string.='';
		} else $search_string ="";
		
		return $search_string;
	}

}