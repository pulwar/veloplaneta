<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_CatalogMenu extends Zend_View_Helper_Abstract
{
    /**
     * @var Zend_View
    */
    protected $_view = null ;

    protected $_current_div = null;

    public function init() {
        $this->_view = Registry::getView() ;
        $this->_current_div = Zend_Controller_Front::getInstance()->getParam('div_id');
        //$this->_current_div = 1;
    }

    /**
     * gets main items  from infoblock
     */
    public function CatalogMenu($season="summer", $type="menu")
    {
        $this->init();
        $catalog_page_id = 14;
        $page = Pages::getInstance()->find($catalog_page_id)->current();
        $this->_view->path = $page->path;
        if ($type == "all") {
            $items =  Catalog_Division::getInstance()->getTree($catalog_page_id, NULL);
        }
        elseif ($type=="sitemap") {
            $items =  Catalog_Division::getInstance()->getTree($catalog_page_id);
        }
        else {
           $items =  Catalog_Division::getInstance()->getTree($catalog_page_id);
        }
        $this->_view->items = $items;
        if ($type=="menu"){
            return $this->_view->render( 'CatalogMenu.phtml' ) ;
        }
         elseif ($type=="sitemap"){
             return $this->_view->render( 'CatalogSitemap.phtml' ) ;
         }
         else
            return $this->_view->render( 'Catalog.phtml' ) ;
    }
}