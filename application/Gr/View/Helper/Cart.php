<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_Cart extends Zend_View_Helper_Abstract
{
	
	/**
	 * @var Zend_View
	 */
	protected $_view = null ;
	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {
		
		$this->_view = Registry::getView() ;
		
	}
	
	/**
	 * Site menu
	 *
	 * @param string $type
	 * @param string $version
	 * @return phtml
	 */
    public function Cart($type)
    {
        //$type
        //'cart'
        //'pageCart'
    	$this->init();
        switch ($type) {
            case 'cart':
                return $this->_view->render( 'Cart.phtml' ) ;
                break;
            case 'pageCart':
                return $this->_view->render( 'PageCart.phtml' ) ;
                break;
            default:
                break;
        }
    }

        
}