<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_News extends Zend_View_Helper_Abstract
{
	
	/**
	 * @var Zend_View
	 */
	protected $_view = null ;
	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {
		
		$this->_view = Registry::getView() ;
		
	}
	
	/**
	 * Site menu
	 *
	 * @param string $type
	 * @param string $version
	 * @return phtml
	 */
    public function News($type)
    {
        //$type:
        //'columnList'
        //'pageList'
        //'page'
    	$this->init();
        switch ($type) {
            case 'columnList':
                $this->_view->news = News::getInstance()->getMain('news', 7);
                return $this->_view->render( 'NewsColumnList.phtml' ) ;
                break;
            case 'pageList':
                return $this->_view->render( 'NewsPageList.phtml' ) ;
                break;
            case 'page':
                return $this->_view->render( 'NewsPage.phtml' ) ;
                break;
            default:
                break;
        }
        
    }

        
}