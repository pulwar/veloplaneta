<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_GetPageByParam extends Zend_View_Helper_Abstract
{
    /**
	 * @var Zend_View
	 */
	//protected $_view = null ;
	

	public function init() {
		
		$this->_view = Registry::getView() ;
		
	}
	
	
	public function GetPageByParam($name , $value, $version = 'ru'){
        $page =  Pages::getInstance()->getPageByParam($name , $value, $version);
       	if($page){
       		return $page;
       	} else return Pages::getInstance()->fetchNew();
        
	}	
}