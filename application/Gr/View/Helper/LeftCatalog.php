<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_LeftCatalog extends Zend_View_Helper_Abstract
{
	
    /**
     * @var Zend_View
     */
    protected $_view = null ;

    /**
     * Enter description here...
     *
     */
    public function init() {

            $this->_view = Registry::getView() ;

    }

    public function getProductsTree($items)
    {
        $catalog_page_id = 14;
        foreach ($items as $item)
        {
            $subitems = Catalog_Division::getInstance()->fetchAll('level=' . $item->level+1 . 'AND id_page='.(int)$catalog_page_id, 'sortId');
            if (count($subitems) == 0) continue;
            else
            {
                $item->subitems = $subitems;
                $item->subitems = getProductsTree($item->subitems);
            }
        }
        return $items;
    }

    public function LeftCatalog()
    {
    	$this->init();
        $catalog_page_id = 14;
        $items = Catalog_Division::getInstance()->fetchAll('level=0 AND id_page='.(int)$catalog_page_id, 'sortId');
        $items = getProductsTree($items);
        foreach ($items as $item)
        {
            echo "<pre>";
            print_r($item->id);
            echo "</pre>";

        }
        exit;
        $this->_view->items = $items;
        return $this->_view->render( 'LeftCatalog.phtml' ) ;
    }        
}