<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_Basket extends Zend_View_Helper_Abstract {
	
	/**
	 * Zend_Session_Namespace
	 *
	 * @var Zend_Session_Namespace
	 */
	private $_basketSession = null;
	/**
	 * @var Zend_View
	 */
	protected $_view = null;
	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {
		$this->_view = Registry::getView ();
		$this->_basketSession = new Zend_Session_Namespace ( 'basket' );
	}
	
	/**
	 * Enter description here...
	 *
	 * 
	 * @return html
	 */
	public function Basket() {
		$this->init ();
		if (isset ( $this->_basketSession->items )) {

			$items = $this->_basketSession->items;
			if (count($items)>0) {
				$this->_view->items_session = $items;
				$this->_view->tovars = Catalog_Tovar::getInstance ()->find ( array_keys ( $items ) );
                                
						
			}
		}
		
		return $this->_view->render ( 'BasketFront.phtml' );
	}

}