<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_Pagination extends Zend_View_Helper_Abstract
{
   
/**
	 * @var Zend_View
	 */
	protected $_view = null ;
	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {
		
		$this->_view = Registry::getView() ;
		
	}
	
	public function Pagination($url, $total, $onpage=10, $current_page = 1){
		if ($total && $onpage){
	       	$count_pages = ceil($total/$onpage);
	       	$this->init();
	       	$this->_view->count_pages = $count_pages;
	       	$this->_view->url = $url;
	       	$this->_view->current = $current_page;
	       	return $this->_view->render( 'Pagination.phtml' ) ;
		}
		return '';
	}	
}