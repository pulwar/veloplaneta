<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_DateFromDb extends Zend_View_Helper_Abstract
{
   
	
	/**
	 * convert datefrom db
	 */
	public function DateFromDb($date){
	   if ($date!='' && !preg_match('/0000/', $date)){		
	       if(preg_match('/([\d]{4})-([\d]{2})-([\d]{2})/', $date, $matches)){			
		   	return $matches[3].'.'.$matches[2].'.'.$matches[1];
	       }
	   }	
	   return '';
	}	
}