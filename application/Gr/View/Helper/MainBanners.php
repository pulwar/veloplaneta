<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_MainBanners extends Zend_View_Helper_Abstract
{
   
	var $_view = null;	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {
		$this->_view = Registry::getView ();		
	}
	
	/**
	 * Get baners for plase
	 */
	public function MainBanners($place_id){
		$this->init();
		$where = 'place_id='.(int)$place_id.' AND active=1';
		$this->_view->baners = Baners::getInstance()->fetchAll($where, 'ord ASC');		
		return $this->_view->render( 'MainBanners.phtml' ) ;
	}	
}