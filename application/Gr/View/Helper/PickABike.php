<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_PickABike extends Zend_View_Helper_Abstract
{

	/**
	 * @var Zend_View
	 */
	protected $_view = null ;

	/**
	 * Enter description here...
	 *
	 */

    public $bikeBrand = array();
    public $bikeType = array();
    public $humanType = array();

	public function init() {
		$this->bikeType = array (
            1 => "Горный",
            //3 => "Комфортный",
            4 => "Городской",
			//7 => "Складной"
        );

        $this->humanType = array(
            1 => "Мужской",
            2 => "Женский",
            3 => "Подростковый",
            4 => "Детский"
        );
        $this->bikeBrand = Catalog_Division::getInstance()->getCatalogMenu(1, 1, 14);
		$this->_view = Registry::getView() ;
	}
    
    /**
	 * Site menu
	 *
	 * @param string $type
	 * @param string $version
	 * @return phtml
	 */
    public function PickABike()
    {
        $this->init();
        $this->_view->bikeBrand = $this->bikeBrand;
        $this->_view->bikeType = $this->bikeType;
        $this->_view->humanType = $this->humanType;
        return $this->_view->render( 'PickABike.phtml' ) ;
    }


}