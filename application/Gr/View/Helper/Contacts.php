<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_Contacts extends Zend_View_Helper_Abstract
{
	
	/**
	 * @var Zend_View
	 */
	protected $_view = null ;
	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {
		
		$this->_view = Registry::getView() ;
		
	}
	
	/**
	 * Site menu
	 *
	 * @param string $type
	 * @param string $version
	 * @return phtml
	 */
    public function Contacts()
    {
    	$this->init(); 	
        return $this->_view->render( 'Contacts.phtml' ) ;
    }

        
}