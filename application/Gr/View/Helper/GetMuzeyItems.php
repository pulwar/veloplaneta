<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_GetMuzeyItems extends Zend_View_Helper_Abstract
{
    /**
	 * @var Zend_View
	 */
	//protected $_view = null ;
	

	public function init() {
		
		$this->_view = Registry::getView() ;
		
	}
	
	
	public function GetMuzeyItems($count){
      $items = Muzey::getInstance()->fetchAll('pub=1 AND page_id=111',' rand()', $count, 0);
      if (count($items)){
      	return $items;
      }
      return '';
        
	}	
}