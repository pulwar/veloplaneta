<?php

/**
 * Layout Helper
 *
 */
class Gr_View_Helper_Actions extends Zend_View_Helper_Abstract
{

	/**
	 * @var Zend_View
	 */
	protected $_view = null ;

	/**
	 * Enter description here...
	 *
	 */
	public function init() {

		$this->_view = Registry::getView() ;

	}

	/**
	 * Site menu
	 *
	 * @param string $type
	 * @param string $version
	 * @return phtml
	 */
    public function Actions($type)
    {
    	$this->init();
        switch ($type) {
            case 'ActionsMainList':
                $this->_view->actions = News::getInstance()->getMain('actions', 0);
                return $this->_view->render( 'ActionsListMain.phtml' ) ;
                break;
            case 'ActionsPageList':
                return $this->_view->render( 'ActionsPageList.phtml' ) ;
                break;
            case 'ActionsOne':
                return $this->_view->render( 'ActionsOne.phtml' ) ;
                break;
            default:
                break;
        }
    }
}