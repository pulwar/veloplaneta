<?php

class Loader 
{
	public static function load($fullPath)
	{
		require_once(ROOT_DIR . $fullPath . '.php');
	}
	
	/**********************************************
	/* Application root folder
	/**********************************************/

	/**
	 * /application/system
	 */
	public static function loadSystem($fullPath)
	{
		
		require_once(DIR_APPLICATION . 'system' . DS . $fullPath . '.php');
	}

	/**
	 * /application/common
	 */
	public static function loadCommon($path)
	{
		//print DIR_APPLICATION .'common'. DS . $path. '.php'; exit();

		//print DIR_APPLICATION . 'common' . DS . $path . '.php'; exit();
		//require_once('D:\Work\ruchki\application\common\controllers\MainAdminController.php');\
                
		require_once(DIR_APPLICATION . 'common' . DS . $path . '.php');

	}

		
	/**********************************************
	/* Public module
	/*********************************************/
	
	/**
	 * /application/models
	 */
	public static function loadPublicModel($model)
	{	
		$filename = DIR_MODELS . $model . '.php' ;
		if( file_exists($filename) )
			require_once($filename);
		elseif( file_exists( $filename = str_replace("_",DS,$filename) ) )
			require_once( $filename ) ;
	}
	
	/************************************************
	/* Admin module
	/************************************************/
	
	/**
	 * /application/modules/admin/models
	 *
	 */
//	public static function loadAdminModel($model)
//	{
//		require_once(DIR_ADMIN_MODELS . $model . '.php');
//	}
	
}