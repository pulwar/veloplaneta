<?php

/**
 * TODO: production release
 */
error_reporting(E_ALL|E_STRICT);

/**
 * Simply the short way
 */
define('DS', DIRECTORY_SEPARATOR);


require_once(ROOT_DIR . 'application/library/Zend/Loader.php');




Zend_Loader::registerAutoload() ;

//function __autoload($class){
//    Zend_Loader::autoload($class);
//}

class Bootstrap{
	/**
	 * Define most commonly used directories
	 */

	private static function defineDirectoriesConstants()
	{
		define('DIR_APPLICATION', ROOT_DIR . 'application' . DS);
		define('DIR_PUBLIC', ROOT_DIR );
		define('DIR_MODULES', ROOT_DIR . 'application' . DS . 'modules' . DS);
		define('DIR_DEFAULT_CONTROLLERS', DIR_MODULES  . 'default' . DS . 'controllers' . DS);
		define('DIR_LIBRARY', DIR_APPLICATION . 'library' . DS);
		define('DIR_ZEND', DIR_LIBRARY . 'Zend' . DS);
		define('DIR_PEAR', DIR_LIBRARY . 'Pear' . DS);
		define('DIR_COMMON', DIR_APPLICATION . 'common' .  DS);
		define('DIR_MODELS', DIR_APPLICATION . 'models' .  DS);
		define('DIR_ADMIN_MODELS', DIR_MODULES . 'admin' .  DS . 'models' . DS);
		define('DIR_LAYOUTS', DIR_APPLICATION . 'layouts' . DS);
		define('GR', DIR_APPLICATION . 'Gr' . DS);
		define('DIR_HELPER', GR . 'View' . DS . 'Helper' . DS );
		define('DIR_SCRIPTS', GR . 'View' . DS . 'Scripts' . DS );
		define('DIR_FILTERS', GR . 'Filter' . DS );
//		define('DIR_ADMIN_LAYOUTS', DIR_MODULES . 'admin' .  DS . 'layouts' . DS);
	}
	
	/**
	 * Append to existing 'include path' array of paths
	 *
	 * @param array $paths
	 */
	private static function setIncludePath($paths)	{
		$pathString = implode($paths, PATH_SEPARATOR);

		set_include_path($pathString . get_include_path());
	}
	
	public static function start()	{

		Bootstrap::defineDirectoriesConstants();

                
                
		Bootstrap::setIncludePath(array(
			'.',
			ROOT_DIR,
			DIR_LIBRARY,
			DIR_ZEND,
			DIR_PEAR,
			DIR_DEFAULT_CONTROLLERS,
			DIR_MODELS,
			DIR_COMMON,
			DIR_APPLICATION
			
		));
		
    	date_default_timezone_set('Europe/Minsk');
        setlocale(LC_TIME, array("ru_RU.CP1251","rus_RUS.1251"));
	//Zend_Loader::loadClass('Zend_Controller_Plugin_ErrorHandler');
	//Zend_Loader::loadClass('Zend_Controller_Front');
	//Zend_Loader::loadClass('Zend_Layout');
	Loader::loadCommon('controllers/MainAdminController');
	
		
        // setup controller
	$frontController = Zend_Controller_Front::getInstance()
		->addModuleDirectory(DIR_MODULES)
		->setDefaultModule('default')
		->throwExceptions(true)
		->registerPlugin(new Zend_Controller_Plugin_ErrorHandler());
		
	
	
	$router = $frontController->getRouter();
	Configurator::setupDatabase();
	Configurator::setupView();
	Zend_Layout::setup(array('path' => DIR_LAYOUTS));	
	//Registry::setView(Zend_Layout::getView());
	//Loader::loadCommon('Security');
        
        //require_once(DIR_APPLICATION . 'common' . DS . 'Security.php');
	Security::getInstance()->init();		
	Configurator::setupRoutes($router);
	$exeption=0;
	//if ($exeption){
	  	try 
	   	{
	    		$frontController->dispatch();
	   	} 
	   	catch (Exception $e) 
                {
                    header('Location: /');
		}
	//} else {
	//	$frontController->dispatch();
	//}
		
	}
	
	
}