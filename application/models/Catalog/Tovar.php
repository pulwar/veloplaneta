<?php
define('FILE_PATH',DIR_PUBLIC.'pics/catalog/tovar/');
define('DIR_FILES',FILE_PATH.'Files/');
/**
 * Catalog_Tovar
 *
 */
class Catalog_Tovar extends Zend_Db_Table {
	
	
	
	
	/**
	 * The default table name.
	 *
	 * @var string
	 */
	protected $_name = 'catalog_tovar';
	
	/**
	 * The default primary key.
	 *
	 * @var array
	 */
	protected $_primary = array('id');
	
	/**
	 * Whether to use Autoincrement primary key.
	 *
	 * @var boolean
	 */
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом
	
	/**
	 * Singleton instance.
	 *
	 * @var St_Model_Layout_Pages
	 */
	protected static $_instance = null;
	
	/**
	 * Dependent tables.
	 *
	 * @var array
	 */
	protected $_dependentTables = array(
		
		
	) ;

	/**
	 * Reference map.
	 *
	 * @var array
	 */
	protected $_referenceMap = array(
		'Catalog_Division' => array(
            'columns'           => array('division_id'),
            'refTableClass'     => 'Catalog_Division',
            'refColumns'        => array('id'),
            'onDelete'          => self::CASCADE,
            'onUpdate'          => self::RESTRICT
        )
	) ;

    public $_bikeType = array (
              1 => "Горный",
             //2 => "Шоссейный",
            //3 => "Комфортный",
              4 => "Городской",
              //7 => "Складной"
    );


     public $_humanType = array(
              1 => "Мужской",
              2 => "Женский",
              3 => "Подростковый",
              4 => "Детский");


	
	/**
	 * Class to use for rows.
	 * Class to use for rows.
	 *
	 * @var string
	 */
	protected $_rowClass = "Catalog_Tovar_Row" ;
	
	/**
	 * Class to use for row sets.
	 *
	 * @var string
	 */
	protected $_rowsetClass = "Catalog_Tovar_Rowset" ;
	/**
	 * @var string
	 */
	private $_img_preview_x ='263';
	/**
	 * @var string
	 */
	private $_img_preview_y ='155';
	/**
	 * @var string
	 */
	private $_img_big_x ='437';
	/**
	 * @var string
	 */
	private $_img_big_y ='269';
	
	/**
	 * @var string
	 */
	private $_file_path = FILE_PATH;
	
	private $_dir_files = DIR_FILES;
	
	/**
	 * 
	 */
	private $_path ='/catalogtovar/tovar/tovarid/';
	/**
	 * матерьялы товаров
	 * @var array 
	 */
	private $_materials = array('Пластик', 'Метал', 'Пластик-метал');
	
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Tovar
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	/**
	 * @return string
	 */
	public function getTableName() {
		return $this->_name;
	}

	
	/**
	 * Добавление элемента 
	 *
	 * @param array $data
	 *	
	 */
	
	public function addTovar($data){
	
		if (!isset($data['created_at'])){
			$data['created_at'] = new Zend_Db_Expr('NOW()');
		}
		$data['prior']=$this->getMaxPriorInDivision($data['division_id'])+1;
		return $this->insert($data);
		
	}
	/**
	 * @return  string
	 */
	public function getImgPreviewX(){
		return $this->_img_preview_x;
	}
	/**
	 * @return string
	 */
	public function getImgPreviewY(){
		return $this->_img_preview_y;
	}
	/**
	 * @return string
	 */
	public function getImgBigX(){
		return $this->_img_big_x;
	}
	/**
	 * @return string
	 */
	public function getImgBigY(){
		return $this->_img_big_y;
	}
	
	/**
	 * @return unknown
	 */
	public function getDirFiles() {
		return $this->_dir_files;
	}

	
	/**
	 * @return string
	 */
	public function getFilePath(){
		return $this->_file_path;
	}
	/**
	 * @return string
	 */
	public function getPath(){
			return $this->_path;
	
	}	
	/**
	 * gets materials
	 *
	 * @return array
	 */
	public function getMaterials(){
		return $this->_materials;
	}
	/**
	 * Редактирование товара 
	 *
	 * @param array $data
	 * @param int $id	
	 */
	
	public function editTovar($data,$id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->update($data,$where);
		
	}
	
	/**
	 * Удаление tovara
	 *
	 * @param array $data
	 *	
	 */
	
	public function deleteTovar($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->delete($where);
		
	}
	
	/**
	 * Удаление элемента
	 *
	 * @param array $data
	 *	
	 */
	
	public function getTovarById($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->fetchRow($where);
		
	}
	
	public function getTovarToBasket($array){
		$where = $this->getAdapter()->quoteInto('id = ?', $array);
		return $this->fetchAll($where);
	}
	/**
	 * Получение всех элементов  (для админки)
	 *
	 * 
	 *	
	 */
	
	public function getAll(){		
		return $this->fetchAll();
		
	}
	
	
	/**
	 * Получение только опубликованных элементов
	 *
	 * 
	 *	
	 */

	public function getRand(){      
        $db = Zend_Registry::get('db');
        $sql = ("SELECT * FROM catalog_tovar ORDER BY RAND() ASC LIMIT 1");
        $result = $db->query($sql);
        return $result->fetchAll();   
    }


    
	public function getAllPub(){
		$where = $this->getAdapter()->quoteInto('pub = ?', 1);
		return $this->fetchAll($where);
		
	}
	
	/**
	 * Опубликовать элемент 
	 *
	 * @param int $id
	 *	
	 */
	
	public function setPubItem($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$data=array('pub'=>1);
		return $this->update($data,$where);
		
	}
	
	/**
	 * Заблокировать элемент 
	 *
	 * @param int $id
	 *	
	 */
	
	public function unsetPubItem($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$data=array('pub'=>0);
		return $this->update($data,$where);
		
	}
	
	
	/**
	 * проверить есть в разделе с переданным id  товары или нет 
	 *
	 * @param int $division_id
	 * @return	boolean
	 */	
	public function issetTovarInDiv($id){
		$where = $this->getAdapter()->quoteInto('division_id = ?', $id);
		
		$res= $this->fetchRow($where);
		if ($res==null){
			return false;
		}
		else {
			return true;
		
		}
	}
	
	/**
	 * получить все товары раздела 
	 *
	 * @param int $division_id
	 * @return	Catalog_Tovar_Rowset
	 */
	
	public function getAllTovarsByDivId($division_id,$offset=0, $limit=null,$sort=null){
		//$dbAdapter = Zend_Registry::get('db');
		
		$sortText='';
		if ($sort == 'nASC') {
			$sortText = ' name ASC ';
			
		} elseif ($sort == 'nDESC') {
			$sortText = ' name DESC ';
		}
		
		if ($sort == 'pASC') {
			$sortText = ' price ASC ';
			
		} elseif ($sort == 'pDESC') {
			$sortText = ' price DESC ';
		}
		
		if ($sort==null){
			$sortText = '  prior ASC';
		}
		$where = $this->getAdapter()->quoteInto('division_id = ?', $division_id);
		
		return $this->fetchAll($where,$sortText,$limit,$offset);
		
	}
	
	public function getTovarByPrior($prior,$div_id){
		$where = array ($this->getAdapter()->quoteInto('prior = ?', $prior),
						$this->getAdapter()->quoteInto('division_id = ?', $div_id));
		$res =  $this->fetchRow($where);
		if ($res==null) {
			return false; 
		} else {
			return true;
		}
	}
	
	public function getMaxPriorInDivision($div_id){
		$dbAdapter = Zend_Registry::get('db');
   		$sql = $dbAdapter->quoteInto("SELECT MAX(catalog_tovar.prior) AS MAX FROM catalog_tovar WHERE division_id= ? ", $div_id);
   		 $result = $dbAdapter->fetchOne($sql);
   		 
   		 return $result;
	}
	
	public function setFirstPrior($id){
		$data = array('prior' =>1);
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return  $this->update($data,$where);
		
		
	}
	
	public function setPriorByTovarId($prior,$id){
		$data = array('prior' =>$prior);
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return  $this->update($data,$where);
		
		
	}
	
/*
*увеличивает приоритет всех товаров в разделе на 1
*/
	public function incAllTovarsByDivid($div_id){
		$dbAdapter = Zend_Registry::get('db');
   		$sql = $dbAdapter->quoteInto("UPDATE catalog_tovar SET prior=prior+1 WHERE division_id= ".$div_id);
   		return   $dbAdapter->query($sql);
	}
	
	/*
	* увеличивет приоритет товаров с заданным приоритетом в опред разделе  на 1		
		
	*/
	
	public function incPrior($prior,$div_id){
		$dbAdapter = Zend_Registry::get('db');
   		$sql = $dbAdapter->quoteInto("UPDATE catalog_tovar SET prior=prior+1 WHERE prior=".$prior."  AND division_id= ".$div_id);
   		return   $dbAdapter->query($sql);
	}
	
	/*
	* уменьшает приоритет товаров с заданным приоритетом в опред разделе  на 1		
		
	*/
	
	public function decPrior($prior,$div_id){
		$dbAdapter = Zend_Registry::get('db');
   		$sql = $dbAdapter->quoteInto("UPDATE catalog_tovar SET prior=prior-1 WHERE prior=".$prior." AND prior>0  AND division_id= ".$div_id);
   		return   $dbAdapter->query($sql);
	}
	
	public function priorUp($id){
		//$this->getPagesByParam('type', 'page');
		  $dbAdapter = Zend_Registry::get('db');
   		$sql = $dbAdapter->quoteInto("UPDATE catalog_tovar SET prior=prior+1 WHERE id=?",$id);
   		return   $dbAdapter->query($sql);
	}
	
	public function priorDwn($id){
		//$this->getPagesByParam('type', 'page');
		  $dbAdapter = Zend_Registry::get('db');
   		$sql = $dbAdapter->quoteInto("UPDATE catalog_tovar SET prior=prior-1 WHERE id=? AND prior>0 ",$id);
   		return   $dbAdapter->query($sql);
	}
	 
	
	public function search($search, $count=12 , $offset=0){
		$db =$this->getAdapter();
		$limit = '';	
		if ($count && !$offset){
			$limit = 'LIMIT '.$count;				
		}
		if ($count && $offset){
			$limit = 'LIMIT '.$offset.','.$count;	
		}
		//$advansed = "LIMIT ".$offset." , ".$count;
		$sortText = '  name ASC';
		$orderBy = 'ORDER BY'.$sortText;
       /* $sql = ("SELECT id, MATCH(`name`,intro,description,articul) AGAINST ('$search') AS REL 
					FROM {$this->_name}
					HAVING REL>0
				 ".$limit
				);*/
        $sql = "SELECT id
					FROM {$this->_name}
					WHERE 
						name LIKE '%$search%' OR
						intro LIKE '%$search%' OR
						description LIKE '%$search%' OR
						articul LIKE '%$search%'
					
				 ".$limit
				;
       // echo $sql; exit;
        //$db->setFetchMode(Zend_Db::FETCH_OBJ);
        $result = $db->fetchAll($sql);
        return $result;
		
	}
	public function search_more($params, $count=null , $offset=null){		
		$db = $this->getAdapter();		
		$session = new Zend_Session_Namespace('search');		
		$where = '';
        $wherebrand = '';
        $wherehuman = '';
        $wheretype = '';
        $price = '';

		if (isset($params['bikeBrand'])){
            $wherebrand .= '(';
            foreach ($params['bikeBrand'] as $brand) {
                $children = Catalog_Division::getInstance()->getChildDivisions($brand);

                foreach ($children as $child) {
                   $wherebrand .= 'division_id='.$child->id.' OR ';                     
                }
            }
            $wherebrand = substr($wherebrand, 0, strlen($wherebrand) - 4);
            $wherebrand .= ')';
		}

        if (isset($params['bikeType'])){
            $wheretype .= '(';
            foreach ($params['bikeType'] as $type) {
                $wheretype .= 'bike_type='.$type.' OR ';
            }
            $wheretype = substr($wheretype, 0, strlen($wheretype) - 4);
            $wheretype .= ')';
		}

        if (isset($params['humanType'])){
            $wherehuman .= '(';
            foreach ($params['humanType'] as $type) {
                $wherehuman .= 'human_type='.$type.' OR ';
			    //$where[]= $db->quoteInto('human_type=?', $type);
            }
            $wherehuman = substr($wherehuman, 0, strlen($wherehuman) - 4);
            $wherehuman .= ')';
		}

        if (trim($params['priceFrom']) !='' && trim($params['priceTo']) != ''){
            $price .= '(price>' . $params['priceFrom'] . ' AND price<' . $params['priceTo'] . ')';
		}
        elseif (trim($params['priceFrom']) != '') {
             $price .= 'price>' . $params['priceFrom'];
        }
        elseif (trim($params['priceTo']) != '') {
             $price .= 'price<' . $params['priceTo'];
        }

        if ($wherebrand != '') $where .= $wherebrand;
        if ($wheretype != '' && $where != '') $where .= ' AND ' . $wheretype;
        elseif ($wheretype != '') $where .= $wheretype;
        if ($wherehuman != '' && $where != '') $where .= ' AND ' . $wherehuman;
        elseif ($wherehuman != '') $where .= $wherehuman;
        if ($price != '' && $where != '') $where .= ' AND ' . $price;
        elseif ($price != '') $where .= $price;

        
                 
          
/*$where = 'SELECT * FROM ' . $this->_name . ' WHERE ' . $where;

        echo "<pre>";
            print_r($where);
        echo "</pre>";
        exit;  */
        

		return $this->fetchAll($where);
		
		
		
	}
	
	public function getSelectedProducts($brands, $humans, $types, $priceFrom, $priceTo){
	$db = $this->getAdapter();
	$sql = "SELECT * FROM {$this->_name} WHERE 1 ";
		//$select = $this->select();
		//$select->from(array('p'=>'catalog_tovar'));
		if(count($brands)){
		$ids=array();
			foreach($brands as $brand){
				$children = Catalog_Division::getInstance()->getChildDivisions($brand);
				foreach ($children as $child) {
                   $ids[]=$child->id;                     
                }
				
			}
			$maker_id = implode(',', $ids);
			//$select->where('maker_id IN (?)', $maker_id);
			$sql.="AND division_id IN ($maker_id) ";
		}
		if(count($humans)){
			$human_type = implode(',', $humans);
			//$select->where('human_type IN (?)', $human_type);
			$sql.="AND human_type IN ($human_type) ";
		}
		if(count($types)){
			$bike_type = implode(',', $types);
			//$select->where('bike_type IN (?)', $bike_type);
			$sql.="AND bike_type IN ($bike_type) ";
		}
		if($priceFrom){
			$sql.="AND price > $priceFrom" . '0000 ';
			$sql.="AND price !=0 ";
		}
		
		if($priceTo){
			$sql.="AND price <= $priceTo" . '0000 ';
			$sql.="AND price !=0 ";
		}
		//if($onpage)$sql.="LIMIT $offset, $onpage";
		//$select->limit($onpage, $offset);
		//echo $sql;exit;
		return $db->fetchAll($sql);
		
	}
}