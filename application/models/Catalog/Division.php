<?php
/**
 * Catalog_Division 
 *
 */
define('PATH',DIR_PUBLIC."pics".DS."catalog".DS."Division".DS);
define('VEW_PATH',"/pics/catalog/Division/");
class Catalog_Division extends Zend_Db_Table {
	
		
	/**
	 * The default table name.
	 *
	 * @var string
	 */
	protected $_name = 'catalog_division';
	
	/**
	 * The default primary key.
	 *
	 * @var array
	 */
	protected $_primary = array('id');
	
	/**
	 * Whether to use Autoincrement primary key.
	 *
	 * @var boolean
	 */
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом
	
	/**
	 * Singleton instance.
	 *
	 * @var St_Model_Layout_Pages
	 */
	protected static $_instance = null;
	
	/**
	 * Dependent tables.
	 *
	 * @var array
	 */
	protected $_dependentTables = array(
		'Catalog_Tovar'
		
	) ;

	/**
	 * Reference map.
	 *
	 * @var array
	 */
	protected $_referenceMap = array(
	) ;

	
	/**
	 * Class to use for rows.
	 *
	 * @var string
	 */
	protected $_rowClass = "Catalog_Division_Row" ;
	
	/**
	 * Class to use for row sets.
	 *
	 * @var string
	 */
	protected $_rowsetClass = "Catalog_Division_Rowset" ;
	
	/**
	 * Необходима для рекурсивного сохранения
	 * дерева
	 *
	 * @var array
	 */
	private $_tree = null;
	/**
	 * необходима для построения меню каталога
	 *
	 * @var array 
	 */
	private $_div = null;
	
	/**
	 * path to upload images
	 */
	private  $_pictPath = PATH;
	
	/**
	 * path to show images in view
	 * 
	 */
	private  $_view_pictPath = VEW_PATH;
	/**
	 * postfix to img filename
	 *
	 * @var string
	 */
	private $_img_postFix ='_logo.';
	
	/**
	 * Singleton instance
	 *
	 * @return catalog_division
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	
		
	
	/**
	 * Добавление основного раздела (parent_id = 0)
	 *
	 * @param array $data
	 *	
	 */
	
	public function addItem($data){
		
		return $this->insert($data);
		
	}
	/**
	 * gets path to img to front
	 *
	 * @return string
	 */
	public function getViewPictPath(){
		return $this->_view_pictPath;
	}
	
	/**
	 * gets path to img dir on server
	 *
	 * @return string
	 */
	public function getPictPath(){
		return $this->_pictPath;
	}
	
	/** gets postfix to img filename
	 *
	 * @return string
	 */
	public function getImgPostFix(){
		return $this->_img_postFix;
	}
	
	/**
	 * Редактирование раздела, подраздела
	 *
	 * @param array $data
	 * @param int $id	
	 */
	
	public function editItem($data,$id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->update($data,$where);
		
	}
	
	/**
	 * Удвление раздела
	 *
	 * @param array $data
	 *	
	 */
	
	public function deleteItem($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->delete($where);
		
	}
	
	
	
	
	
	/**
	 * Получение всех элементов  (для админки)
	 *
	 * 
	 *	
	 */
	
	public function getAll(){
		
		return $this->fetchAll();
		
	}
	
	
	
	/**
	 * Получение всех элементов  (для админки) с сортировкой по приоритету
	 *
	 * 
	 *	
	 */
	
	public function getAllBySort(){
		
		return $this->fetchAll(null,'sortid ASC', null, null);
		
	}
	
	
	/**
	 * Построение дерева  разделов
	 *
	 * 
	 * @param int $parentId
	 * @param int $level
	 * @return array
	 */
	public function getDivisionsMap($id = 0){
		$where = array(
			$this->getAdapter()->quoteInto('parent_id = ?', $id),
			
		);
		$nodes = array();
		$nodes = $this->fetchAll($where, "sortid");
		
		foreach ($nodes as $data){
			$data->description ='';
			$data->intro ='';
			$this->_tree[] = $data;
			$this->getDivisionsMap($data->id);
		}
		
		return $this->_tree;
	}
	
	public function resetTree(){
		$this->_tree=null;
	}
	
	
	/**
	 * Построение дерева для Меню каталога
	 *
	 * 
	 * @param int $parentId
	 * @param int $level
	 * @param int $id_page
	 * @return array
	 */
	public function getCatalogMenu($parentId = 0, $level = 0, $id_page){
		$return = array();
		$nodes = array();		
		$where = array(
			$this->getAdapter()->quoteInto('level = ?', $level),
			$this->getAdapter()->quoteInto('parent_id = ?', $parentId),			
			$this->getAdapter()->quoteInto('id_page = ?', $id_page)			
		);
		$nodes = $this->fetchAll($where, 'sortid');
		
		foreach ($nodes as $data){
			
			if($data->getCountOfChildren() > 0){
				$data = $data->toArray();
				$data['children']= $this->getCatalogMenu($data['id_page'], $data['id'], $data['level'] + 1, $id_page);
				
				$return[] =$data;
			}
			else{
				$data = $data->toArray();
				$return[] = $data;
								
			}
		}	
		
		return $return;
	}
	
	
	/**
	 * Построение дерева для ExtJS
	 *
	 * 
	 * @param int $parentId
	 * @param int $level
	 * @return array
	 */
	public function getTree($id_page , $season='summer', $parentId = 0, $level = 0){
		$return = array();			
		$where = array(
			$this->getAdapter()->quoteInto('id_page = ?', $id_page),
                        $this->getAdapter()->quoteInto('season = ?', $season),
			$this->getAdapter()->quoteInto('level = ?', $level),
			$this->getAdapter()->quoteInto('parent_id = ?', $parentId)			
		);
		$nodes = $this->fetchAll($where, 'sortid');
		
		foreach ($nodes as $data){
			
			if( $data->getCountOfChildren() > 0){
				$return[] = array(
				'task' => $data->name,
                                'display' => $data->display,
                                'img' => $data->img,
    				'duration' =>$data->getDuration(),
    				'user' => Security::getInstance()->getUser()->username,
    				'id' => $data->id,
    				'uiProvider' => 'col',
    				'cls' => 'master-task',
    				'iconCls' => 'task-folder',													
    				'children' => $this->getTree($data->id_page, $season, $data->id, $data->level + 1)
				);
			}
			else{
				$return[] = array(
				'task' => $data->name,
					'shortname' => $data->shortname==null ? $data->name : $data->shortname, 
					'favicon' => $data->favicon==null ? '' : $data->favicon, 
                    'display' => $data->display,
    				'duration' =>$data->getDuration(),
    				'user' => Security::getInstance()->getUser()->username,
    				'id' => $data->id,
    				'uiProvider' => 'col',
    				'leaf' => 'true',
    				'iconCls' => $data->level==0 ? 'task-folder': 'task'
				);							
			}
		}	

		return $return;
	}
	
	
	/**
	 * Сохранение результата dnd 
	 * в зависимости от типа возвращаемого ExtJS
	 * результата
	 *
	 * @param int $id
	 * @param int $parentId
	 * @param string $point
	 */
	public function replace($id, $parentId, $point){
		if($point == 'above')
			$this->replaceAbove($id, $parentId, $point);
		elseif($point == 'below')
			$this->replaceBelow($id, $parentId, $point);
		elseif($point == 'append')
			$this->replaceAppend($id, $parentId, $point);		
	}
	
	/**
	 * Проверка действительности события dnd
	 *
	 * @param int $id
	 * @param int $parentId
	 * @return boolean
	 */
	public function isReplace($id, $parentId){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$node = $this->fetchRow($where);
		
		return $node->parentId == $parentId ? false : true;
	}
	
	
	
	
	
	/**
	 * Получение раздела по ID
	 *
	 * @param int $id
	 *	
	 */
	
	public function getItemById($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->fetchRow($where);
		
	}
	
	/**
	 * Получение родительских элементов каталога (parent_id = 0)
	 *
	 * @param int $parent_id
	 *	
	 */
	
	public function getMainDivisions($parent_id=0){
		$where = $this->getAdapter()->quoteInto('parent_id = ?', $parent_id);
		return $this->fetchRow($where);		
	}

        public function getChildDivisions($parent_id=0){
		$where = $this->getAdapter()->quoteInto('parent_id = ?', $parent_id);
		return $this->fetchAll($where);
	}
	
	public function getParentDivision($parent_id=0){
		$where = $this->getAdapter()->quoteInto('parent_id = ?', $parent_id);
		return $this->fetchRow($where);
		
	}

        public function getParenDiv($parent_id=0){
		$where = $this->getAdapter()->quoteInto('id = ?', $parent_id);
		return $this->fetchRow($where);

	}
	
	
	
	
	/**
	 * Перемещение страницы через dnd,
	 * при котором страница выше находится 
	 * на том же уровне
	 * 	 
	 * @param int $id
	 * @param int $parentId
	 * @param string $point
	 */
	private function replaceAbove($id, $parentId, $point){
		
		$parent = $this->getItemById($parentId);
		
		$this->getAdapter()->query("UPDATE $this->_name SET parent_id = :parent_id, level = :level, sortid = sortid + 1 WHERE parent_id = :id AND sortid >= :sort", 
			array('parent_id' => $parent->parent_id, 'level' => $parent->level, 'id' => $parent->parent_id, 'sort' => $parent->sortid));
		
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->update(array(
			'parent_id' => $parent->parent_id,
			'level' => $parent->level, 
			'sortid' => $parent->sortid			
			), $where);
	}
	
	/**
	 * Перемещение страницы через dnd,
	 * при котором страница выше является родительской
	 *
	 * @param int $id
	 * @param int $parentId
	 * @param string $point
	 */
	private function replaceBelow($id, $parentId, $point){
	
		$parent = $this->getItemById($parentId);
		
		
		$this->getAdapter()->query("UPDATE $this->_name SET parent_id = :parent_id, level = :level, sortid = sortid + 1 WHERE parent_id = :id AND sortid >= :sort", 
			array('parent_id' => $parent->parent_id, 'level' => $parent->level, 'id' => $parent->parent_id, 'sort' => $parent->sortid));
	
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->update(array(
			'parent_id' => $parent->parent_id,
			'level' => $parent->level, 
			'sortid' => $parent->sortid			
			), $where);
	}
	
	/**
	 * Перемещение страницы через dnd, при котором
	 * она была брошена на страницу, тем самым была добавлена
	 * в конец списка вложенных
	 *
	 * @param int $id
	 * @param int $parentId
	 * @param string $point
	 */
	private function replaceAppend($id, $parentId, $point){		
		/*$parent = $this->getItemById($parentId);
		$max_sort = $this->getMaxSort($parentId);
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->update(array(
			'parent_id' => $parentId,
			'level' => $parent->level + 1, //на 1 больше родительского уровня
			'sortid' => $max_sort + 1 //на 1 больше максимального- добавляется в конец			
			), $where)*/;
	}
	
	
	/**
	 * Получение максимального sortId 
	 * родительской страницы
	 *
	 * @param int $id
	 * @param int $id_page
	 * @return int
	 * 
	 */
	public function getMaxSort($id_page, $id){
		return $this->getSort($id_page, $id, 'DESC');
	}
	
	public function getMainDiv($id){
		$data= $this->getItemById($id);
		while ($data->parent_id!=0){
			$data=$this->getItemById($data->parent_id);
		}
		
		return $data->id;
	}
	
	public function getAkcii(){
		$page_ids = Pages::getInstance()->getPagesByParam('template', 'akcii')->getIdis();
		return News::getInstance()->fetchAll("type='akcii' AND pub=1");
		
	}
	
	public function search($search){
		$db = Zend_Registry::get('db');
        $sql = ("SELECT name, id
				FROM $this->_name
				WHERE name LIKE '%".$search."%'
				OR description LIKE '%".$search."%'
				ORDER BY name ASC "
				);
        $result = $db->fetchAll($sql);
        return $result;
		
	}
	
	/**
	 * Получение sortId 
	 * родительской страницы,
	 * определенным образом сортируя(asc, desc)
	 *
	 * @param int $id
	 * @return int
	 */
	private function getSort($id_page, $id, $type){
		$order = "sortid $type";
		$where = array(
			$this->getAdapter()->quoteInto('parent_id = ?', $id),
			$this->getAdapter()->quoteInto('id_page = ?', $id_page)
		);
		$item = $this->fetchRow($where, $order);
		
		return $item ? $item->sortid : 0;
	}
	
	public function getTovarDivision($tovarId){
            $db = Zend_Registry::get('db');
            $sql = ("SELECT division_id FROM catalog_tovar WHERE id = $tovarId LIMIT 1");
            $result = $db->query($sql);
            return $result->fetchAll();
        }

    public function getAllHumanTypes($div_id){
        $db = Zend_Registry::get('db');
        $sql = ("select distinct(t.`human_type`) from catalog_division p
                join catalog_division d
                on d.parent_id = p.id
                join catalog_tovar t
                on d.id = t.`division_id`
                where p.id = " . $div_id . "
                and t.`human_type`!=0");
        $results = $db->query($sql)->fetchAll();
        $arr =[];
        foreach ($results as $val) {
            $arr[] = $val['human_type'];
        }
        return $arr;
    }


    public function getAllBikeTypes($div_id){
        $db = Zend_Registry::get('db');
        $sql = ("select distinct(t.`bike_type`) from catalog_division p
                join catalog_division d
                on d.parent_id = p.id
                join catalog_tovar t
                on d.id = t.`division_id`
                where p.id = " . $div_id . "
                and t.`bike_type`!=0");
        $results = $db->query($sql)->fetchAll();
        $arr =[];
        foreach ($results as $val) {
            $arr[] = $val['bike_type'];
        }
        return $arr;
    }
	public function getMarks($id) {
		$bike = array(
						'1' => array(
									 'typeId' => '1',									 
									 'name' => 'Горные',
									 'type' => 'type',
									 'icon' => 'mountain-icon',
									 'sort' => '1'
									 ),
						'3' => array(	
									 'typeId' => '3',								 
									 'name' => 'Комфортный',
									 'type' => 'type',
									 'icon' => 'comfort-icon',
									 'sort' => '3'
									 ),
						'4' => array(
									 'typeId' => '4',										 
									 'name' => 'Городские',
									 'type' => 'type',
									 'icon' => 'city-icon',
									 'sort' => '4'
									 ),
						'7' => array(
									 'typeId' => '7',										 
									 'name' => 'Складные',
									 'type' => 'type',
									 'icon' => 'kids-icon',
									 'sort' => '7'
									 ),
						);
		$human = array(
						'1' => array(		
									'typeId' => '1',							 
									 'name' => 'Мужские',
									 'type' => 'purpose',
									 'icon' => 'man-icon',
									 'sort' => '4'
									 ),
						'2' => array(			
									'typeId' => '2',						 
									 'name' => 'Женские',
									 'type' => 'purpose',
									 'icon' => 'woman-icon',
									 'sort' => '5'
									 ),
                        '3' => array(
                                    'typeId' => '3',
                                    'name' => 'Подростковый',
                                    'type' => 'purpose',
                                    'icon' => 'comfort-icon',
                                    'sort' => '6'
                        ),
						'4' => array(			
									'typeId' => '4',						 
									 'name' => 'Детские',
									 'type' => 'purpose',
									 'icon' => 'kids-icon',
									 'sort' => '6'
									 ),
						);
    	$where = $this->getAdapter()->quoteInto('parent_id = ?', $id);
		//все бренды
		$nodes = $this->fetchAll($where, 'sortid');
		$makers = array();
		foreach ($nodes as $key0 => $data){
			$data = $data->toArray();
			//bike_type
            $db = Zend_Registry::get('db');
            $sql = ("SELECT DISTINCT `bike_type` FROM `catalog_tovar` WHERE `division_id` IN (SELECT `id` FROM `catalog_division` WHERE `parent_id` = ".$data['id'].") AND bike_type != 0 ORDER BY `bike_type`");
            $bike_type = $db->query($sql)->fetchAll();
            foreach ($bike_type as $key => $value) {

            	$makers[$data['id']][] = $bike[$value['bike_type']];
            }
			//human_type
			$db = Zend_Registry::get('db');
            $sql = ("SELECT DISTINCT `human_type` FROM `catalog_tovar` WHERE `division_id` IN (SELECT `id` FROM `catalog_division` WHERE `parent_id` = ".$data['id'].") AND human_type != 0 ORDER BY `human_type`");
            $human_type = $db->query($sql)->fetchAll();
            foreach ($human_type as $key => $value) {
            	//var_export($value['human_type']);
            	$makers[$data['id']][] = $human[$value['human_type']];
            }
			
		}
		
		return $makers;
		
    }
	
	
	
}