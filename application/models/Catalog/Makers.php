<?php

class Catalog_Makers extends Zend_Db_Table {
	
	protected $_name = 'catalog_maker';
	protected $_primary = array('id');
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом

	protected static $_instance = null;
	
	/**
	 * Singleton instance
	 *
	 * @return catalog_division
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	
		
	
	/**
	 * Добавление элемента 
	 *
	 * @param array $data
	 *	
	 */
	
	public function addItem($data){
		
		return $this->insert($data);
		
	}
	
	
	/**
	 * Редактирование элемента
	 *
	 * @param array $data
	 * @param int $id	
	 */
	
	public function editItem($data,$id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->update($data,$where);
		
	}
	
	/**
	 * Удаление элемента
	 *
	 * @param array $data
	 *	
	 */
	
	public function deleteItem($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->delete($where);
		
	}
	
	
	
	
	
	/**
	 * Получение всех элементов  (для админки)
	 *
	 * 
	 *	
	 */
	
	public function getAll(){
		
		return $this->fetchAll(null,'name ASC', null, null);
		
	}
	
	
	/**
	 * Получение только опубликованных элементов
	 *
	 * 
	 *	
	 */
	
	public function getAllPub(){
		$where = $this->getAdapter()->quoteInto('pub = ?', 1);
		return $this->fetchAll($where);
		
	}
	
	/**
	 * Опубликовать элемент 
	 *
	 * @param int $id
	 *	
	 */
	
	public function setPubItem($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$data=array('pub'=>1);
		return $this->update($data,$where);
		
	}
	
	/**
	 * Заблокировать элемент 
	 *
	 * @param int $id
	 *	
	 */
	
	public function unsetPubItem($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$data=array('pub'=>0);
		return $this->update($data,$where);
		
	}
	
	
	
	
	
}