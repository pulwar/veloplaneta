<?php

class Catalog_Color extends Zend_Db_Table {
	
	protected $_name = 'catalog_color';
	protected $_primary = array('id');
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом
	protected static $_instance = null;
	
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Color
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	
}