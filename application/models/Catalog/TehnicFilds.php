<?php

class Catalog_TehnicFilds extends Zend_Db_Table {
	
	protected $_name = 'catalog_division_tehnic_filds';
	protected $_primary = array('id');
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом

	protected static $_instance = null;
	
	/**
	 * Singleton instance
	 *
	 * @return catalog_division
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	
		
	
	/**
	 * Добавление элемента 
	 *
	 * @param array $data
	 *	
	 */
	
	public function addItem($field_id,$div_id){
	
		$insert = array('tehnic_fild_id'=>$field_id,'division_id'=>$div_id);
		return $this->insert($insert);
		
	}
	
	
	/**
	 * Редактирование элемента
	 *
	 * @param array $data
	 * @param int $id	
	 */
	
	public function editItem($data,$id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->update($data,$where);
		
	}
	

	public function getFieldById($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->fetchRow($where);
		
	}
	
	/**
	 * Удаление элемента
	 *
	 * @param array $data
	 *	
	 */
	
	public function deleteItem($field_id){
		$where = $this->getAdapter()->quoteInto('tehnic_fild_id = ?', $field_id);
		return $this->delete($where);
		
	}
	
	
	
	
	
	/**
	 * Получение всех элементов  (для админки)
	 *
	 * 
	 *	
	 */
	
	public function getAll(){
		
		return $this->fetchAll();
		
	}
	
	
	/**
	 * Получение только опубликованных элементов
	 *
	 * 
	 *	
	 */
	
	public function getAllPub(){
		$where = $this->getAdapter()->quoteInto('pub = ?', 1);
		return $this->fetchAll($where);
		
	}
	
	/**
	 * Получение элементов принадлежащих определенному разделу division_id
	 *
	 * 
	 *	
	 */
	
	public function getAllByDivId($division_id){
		$where = $this->getAdapter()->quoteInto('division_id = ?', $division_id);
		return $this->fetchAll($where);
		
	}
	
	public function getFieldsByDivId($division_id){
		$db = Zend_Registry::get('db');
		$db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = ("SELECT tehnic_fild_id, division_id FROM catalog_division_tehnic_filds WHERE division_id =?");
       
       $result = $db->fetchCol($sql,$division_id);
        $db->setFetchMode(Zend_Db::FETCH_ASSOC);
        return $result; 
        
		
		
	}
	
	/**
	 * Опубликовать элемент 
	 *
	 * @param int $id
	 *	
	 */
	
	public function setPubItem($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$data=array('pub'=>1);
		return $this->update($data,$where);
		
	}
	
	/**
	 * Заблокировать элемент 
	 *
	 * @param int $id
	 *	
	 */
	
	public function unsetPubItem($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$data=array('pub'=>0);
		return $this->update($data,$where);
		
	}
	
	
	
	
	
}