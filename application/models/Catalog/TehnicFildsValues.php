<?php

class Catalog_TehnicFildsValues extends Zend_Db_Table {
	
	protected $_name = 'catalog_tehnic_filds_values';
	protected $_primary = array('id');
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом

	protected static $_instance = null;
	
	/**
	 * Singleton instance
	 *
	 * @return catalog_division
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	
		
	
	/**
	 * Добавление элемента 
	 *
	 * @param array $data
	 *	
	 */
	
	public function addItem($data){
	
		
		return $this->insert($data);
		
	}
	
	
	/**
	 * Редактирование элемента
	 *
	 * @param array $data
	 * @param int $id	
	 */
	
	public function editFieldValue($data,$teh_field_id, $tovar_id){
		$where =array($this->getAdapter()->quoteInto('tehnic_fild_id  = ?', $teh_field_id),
					$this->getAdapter()->quoteInto('tovar_id  = ?', $tovar_id));
		return $this->update($data,$where);
		
	}
	
	/**
	 * Удаление элемента
	 *
	 * @param int $tovar_id
	 *	
	 */
	
	public function deleteValuesByTovarId($tovar_id){
		$where = $this->getAdapter()->quoteInto('tovar_id = ?', $tovar_id);
		return $this->delete($where);
		
	}
	
	/**
	 * Удаление элемента
	 *
	 * @param int $tovar_id
	 *	
	 */
	
	public function deleteValuesByFieidId($field_id){
		$where = $this->getAdapter()->quoteInto('tehnic_fild_id = ?', $field_id);
		return $this->delete($where);
		
	}
	
	
	
	/**
	 * Получение всех элементов  (для админки)
	 *
	 * 
	 *	
	 */
	
	public function getAll(){
		
		return $this->fetchAll();
		
	}
	
	
	/**
	 * Получение только опубликованных элементов
	 *
	 * 
	 *	
	 */
	
	public function getAllPub(){
		$where = $this->getAdapter()->quoteInto('pub = ?', 1);
		return $this->fetchAll($where);
		
	}
	
	/**
	 * Получение элементов принадлежащих определенному разделу division_id
	 *
	 * 
	 *	
	 */
	
	public function getAllByDivId($division_id){
		$where = $this->getAdapter()->quoteInto('division_id = ?', $division_id);
		return $this->fetchAll($where);
		
	}
	
	public function  getAllByTovarId($id){
		$where = $this->getAdapter()->quoteInto('tovar_id = ?', $id);
		return $this->fetchAll($where);
	}
	
	/**
	 * Опубликовать элемент 
	 *
	 * @param int $id
	 *	
	 */
	
	public function setPubItem($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$data=array('pub'=>1);
		return $this->update($data,$where);
		
	}
	
	/**
	 * Заблокировать элемент 
	 *
	 * @param int $id
	 *	
	 */
	
	public function unsetPubItem($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$data=array('pub'=>0);
		return $this->update($data,$where);
		
	}
	
	public function issetValue($teh_field_id,$tovar_id){
		$where = array(
						$this->getAdapter()->quoteInto('tehnic_fild_id = ?', $teh_field_id),
						$this->getAdapter()->quoteInto('tovar_id = ?', $tovar_id)
						);
		$result = $this->fetchRow($where);
		if ($result!=null) {
			return true;
		} else {
			return false;
		}
		
	}
	
	
	
}