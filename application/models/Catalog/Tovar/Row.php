<?php
class Catalog_Tovar_Row extends Zend_Db_Table_Row {
	/**
	 * @var Zend_View
	 */
	private  $_view = null;
	
	private $_img_path = '';
	
	public function init(){
		$this->_view = Registry::getView();
		$this->_img_path = Catalog_Tovar::getInstance()->getFilePath();
	}
			
	public function toArray(){
		return parent::toArray();
	}
	/**
	 * delete
	 * @see application/library/Zend/Db/Table/Row/Zend_Db_Table_Row_Abstract#delete()
	 */
	public function delete(){
		@unlink($this->_img_path.$this->img_small);
		@unlink($this->_img_path.$this->img_big);
		$meta = PagesOptions::getInstance()->fetchRow("type='tovar' AND item_id=".(int)$this->id);
		if (!is_null($meta)){
			$meta->delete();
		}
		return parent::delete();
	}
	
	public function getPrice(){
		return number_format($this->price, 2, '.', '');
	}

        public function getOldPrice(){
		return number_format($this->oldprice, 2, '.', '');
	}
	
	/**
	 * gets body color
	 * @return string
	 */
	/*public function getBodyColor(){
		$color = Catalog_Color::getInstance()->find($this->color_id_body)->current();
		if (!is_null($color)){
			return $color->name;
		}
		return '';
	}*/
	
	/**
	 * Цвет клипсы
	 * @return string
	 */
	/*public function getClipColor(){
		$color = Catalog_Color::getInstance()->find($this->color_id_clip)->current();
		if (!is_null($color)){
			return $color->name;
		}
		return '';
	}*/
	
	/**
	 * Цветной элемент корпуса
	 * @return string
	 */
	/*public function getElemColor(){
		$color = Catalog_Color::getInstance()->find($this->color_id_elem)->current();
		if (!is_null($color)){
			return $color->name;
		} else {
			return 'Нет';
		}
	}*/
	
	public function getBrandLogo(){
		if ($this->maker_id){
			$brand = Catalog_Division::getInstance()->find($this->maker_id)->current();
			if (!is_null($brand) && $brand->img!=''){
				return $brand->img;
			}
		}
		return '';
	}
	
	/**
	 * @return string
	 */
	public function getPath(){
		return  Catalog_Tovar::getInstance()->getPath();	
	}
	
	/**
	 * gets 3 random item from current division
	 * @param int $num
	 * @return html
	 */
	
	public function getRand($num){  
			$this->init();  
			$catalog_tovar = Catalog_Tovar::getInstance();
			$where = array(
				$catalog_tovar->getAdapter()->quoteInto('id <> ?', $this->id),
				$catalog_tovar->getAdapter()->quoteInto('division_id = ?', $this->division_id)
			);
			
			$this->_view->rand_tovar = $catalog_tovar->fetchAll($where , 'RAND() ASC' , $num, NULL);
	        return $this->_view->render("RandTovar.phtml");
	}
	
	public function rowTotextPlain($count){
		$totalPrice = $this->price*(int)$count;
		$separator =" / ";
		$textPlain =$this->name.$separator;
		$textPlain .= $count." шт.".$separator;
		$textPlain .= $this->price."руб.".$separator;
		$textPlain .="всего ". $totalPrice."руб."."\r\n";
		
		return ($textPlain);
	}
}