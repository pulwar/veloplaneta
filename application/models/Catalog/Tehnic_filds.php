<?php

class Catalog_Tehnic_filds extends Zend_Db_Table {
	
	protected $_name = 'catalog_tehnic_filds';
	protected $_primary = array('id');
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом

	protected static $_instance = null;
	
	/**
	 * Singleton instance
	 *
	 * @return catalog_division
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	
		
	
	/**
	 * Добавление элемента 
	 *
	 * @param array $data
	 *	
	 */
	
	public function addItem($data){
	
		
		return $this->insert($data);
		
	}
	
	
	/**
	 * Редактирование элемента
	 *
	 * @param array $data
	 * @param int $id	
	 */
	
	public function editItem($data,$id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->update($data,$where);
		
	}
	

	public function getFieldById($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->fetchRow($where);
		
	}
	
	/**
	 * Удаление элемента
	 *
	 * @param array $data
	 *	
	 */
	
	public function deleteItem($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->delete($where);
		
	}
	
	
	
	
	
	/**
	 * Получение всех элементов  (для админки)
	 *
	 * 
	 *	
	 */
	
	public function getAll(){
		
		return $this->fetchAll(null,'sortid DESC');
		
	}
	
	
	/**
	 * Получение только опубликованных элементов
	 *
	 * 
	 *	
	 */
	
	public function getAllPub(){
		$where = $this->getAdapter()->quoteInto('pub = ?', 1);
		return $this->fetchAll($where);
		
	}
	
	/**
	 * Получение элементов принадлежащих определенному разделу division_id
	 *
	 * 
	 *	
	 */
	
	public function getAllByDivId($division_id){
		$where = $this->getAdapter()->quoteInto('division_id = ?', $division_id);
		return $this->fetchAll($where);
		
	}
	
	public function getFieldsArrayId($ids){
		$ids = (array)$ids;
		$sIds = "";
		foreach ($ids as $id){
			if ($sIds!=""){$sIds.=",";}
		$sIds .= strval($id);	
		}
		
		//$where = $this->getAdapter()->quoteInto("id IN (?)", $ids);
		//return $this->fetchAll($where, "sortid DESC");
		$db = Zend_Registry::get('db');
		$db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = ("SELECT * FROM catalog_tehnic_filds WHERE id IN (".$sIds.") ORDER BY sortid DESC ");
       
       	//$result = $db->query($sql);
      	$result = $db->fetchAll($sql);
       $db->setFetchMode(Zend_Db::FETCH_ASSOC);
        return $result;
		
	}
	
	/**
	 * Опубликовать элемент 
	 *
	 * @param int $id
	 *	
	 */
	
	public function setPubItem($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$data=array('pub'=>1);
		return $this->update($data,$where);
		
	}
	
	/**
	 * Заблокировать элемент 
	 *
	 * @param int $id
	 *	
	 */
	
	public function unsetPubItem($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$data=array('pub'=>0);
		return $this->update($data,$where);
		
	}
	
	public function issetFieldByName($name){
		$where = $this->getAdapter()->quoteInto('name = ?', $name);
		$result = $this->fetchRow($where);
		if ($result!=null) {
			return true;
		} else {
			return false;
		}
		
		
	}
	
	
	
}