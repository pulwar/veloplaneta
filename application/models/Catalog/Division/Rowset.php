<?php

class Catalog_Division_Rowset extends Zend_Db_Table_Rowset {

	private $_view =null;
	public function init(){
		$this->_view = Registry::getView();
	}
	/**
	 * @return array
	 */
	public function toArray(){
		return parent::toArray();
	}
	/**
	 * gets current row
	 *
	 * @return News_Row
	 */
	public function current(){
		return parent::current();
	}
	
	public function getChildsTpl($template){
		if (count($this)>0){
			$this->init();
			$this->_view->divisions = $this;
			return $this->_view->render($template);
		}
		
	}
}