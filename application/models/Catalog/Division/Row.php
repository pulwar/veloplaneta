<?php

class Catalog_Division_Row extends Zend_Db_Table_Row {
	/** 
	 * @var Zend_view
	 */
	protected  $_view =null;
	
	public function init(){
		 $this->_view = Registry::getView();
	}
	
	public function toArray(){
		return parent::toArray();
	}
	
	public function getPath(){
		return Pages::getInstance()->find($this->id_page)->current()->path;
	}
	/**
	 * каскадное удаление 
	 * удаляет все вложенные разделы вместе с товарами
	 */
	public function delete(){
		$path = Catalog_Division::getInstance()->getPictPath();
		$postFix = Catalog_Division::getInstance()->getImgPostFix();
		if ($this->issetChilds()) {
			$childs =$this->getChilds();
			foreach ($childs as $node) {
				unlink($path . $node->id . $postFix);
				$node->delete();
			}
		}
		$meta = PagesOptions::getInstance()->fetchRow("type='catalog_division' AND item_id=".(int)$this->id);
		if (!is_null($meta)){
			$meta->delete();
		}
		return 	parent::delete();
	}
	
	/**
	 * Получение  дочерних элементов раздела 
	 *
	 *@param int $this->id
	 *@return Catalog_Division_Rowset	
	 */
	
	public function getChilds($count=null, $offset = null){		
		return Catalog_Division::getInstance()->fetchAll('parent_id ='. $this->id , 'sortid', $count, $offset);
		
	}
	
	public function getChildsTpl($template = "Divisions.phtml"){
		$this->init();		
		$this->_view->divisions =$this->getChilds();
		return $this->_view->render($template);
	}
	
	/**
	 * проверяет наличее детей в разделе
	 *
	 * @param int $this->id
	 *	
	 */
	
	public function issetChilds(){
		$res =  Catalog_Division::getInstance()->fetchRow('parent_id ='. $this->id);
		if ($res==null) {
			return false;
		} else {
			return true;
		}
		
	}
	/**
	 * проверяет наличие вложенных разделов
	 * @return bollean
	 */
	public function issetTovarInDiv(){
		return Catalog_Tovar::getInstance()->issetTovarInDiv($this->id);
	}
	/**
	 * gets tovar from this division
	 * return Catalog_Tovar_Rowset
	 */
	public function getTovars($count=null, $offset=null ){
		return Catalog_Tovar::getInstance()->getAllTovarsByDivId($this->id, $offset, $count);		
	}
	
/**
	 * gets tovar from this division
	 * return string
	 */
	public function getTovarsTpl($template = "Tovars.phtml"){
		$this->init();
		$this->_view->tovars = $this->getTovars();
		return $this->_view->render($template);
		
	}
	/**
	 * Получение количества дочерних элементов раздела 
	 *
	 * @param int $this->id
	 *	
	 */
	
	public function getCountOfChildren(){
		
		$count = count($this->getChilds($this->id));
		
		return $count;
		
	}
	
	public function getParent(){
		return Catalog_Division::getInstance()->find($this->parent_id)->current();
	}
	
	public function getLogo(){
		$this->init();
		$Catalog_Division = Catalog_Division::getInstance();
		$logo = glob($Catalog_Division->getPictPath().$this->id.$Catalog_Division->getImgPostFix().'*');
		if ($logo != false){
			$logo = str_replace($Catalog_Division->getPictPath(), '',$logo[0]);
		}
		
		$this->_view->logo =$Catalog_Division->getViewPictPath().$logo;
		$this->_view->logo_server = $Catalog_Division->getPictPath().$logo;
		
		return $this->_view->render('DivLogo.phtml');

	}
	
	
	public function getBread($tovarName = '', $flag=true, $seoFilter = null){
		$this->init();
		$array = array();
		$array[] = $division_row = $this;
		while ($division_row->level != 0){
			$division_row = $division_row->getParent();
			$array[]=$division_row;
		}
		$this->_view->bread = array_reverse($array);
		$this->_view->seoFilter = $seoFilter;
		$this->_view->flag = $flag;
        $this->_view->tovarName = $tovarName;
		return $this->_view->render("DivisionBread.phtml");
	}
	/**
	 * Получение текста для вставки иконок управления 
	 * напротив каждого элемента
	 *
	 * @param array $this
	 * @return string
	 */
	public function getDuration(){
		$module = 'Catalog_Division';
		$lang = 'ru';
		$add_tovar_link ='';
		$add_div_link ='';
		
		
		if  ( !$this->issetChilds($this->id)){
		$add_tovar_link ="<a href ='#' title='добавить товар' style='margin-left: 8px'><img src='/images/plus_a.gif' onclick='javascript:window.location = \"/admin/$lang/Catalog_Tovar/add/parent_id/$this->id/id_page/$this->id_page\" '/></a>".
		"<a href ='#' title='Перейти в раздел' style='margin-left: 8px'><img src='/images/folder.gif' onclick='javascript:window.location = \"/admin/$lang/Catalog_Tovar/items/divid/$this->id/id_page/$this->id_page\" '/></a>" ;	
		} 
		if (!Catalog_Tovar::getInstance()->issetTovarInDiv($this->id)) {
			$add_div_link ="<a href ='#' title='Добавить' style='margin-left: 8px'><img src='/images/plus_krug.gif' onclick='javascript:window.location = \"/admin/$lang/$module/add/parent_id/$this->id/id_page/$this->id_page\" '/></a>" ;
		}
		
		return   
		
		$add_tovar_link.
				
    			"<a href ='#' title='Редактировать' style='margin-left: 8px'><img src='/images/redact.gif' onclick='javascript:window.location = \"/admin/$lang/$module/edit/id/$this->id/id_page/$this->id_page\" '/></a>" . 
    			$add_div_link.
    			"<a href ='#' title='Удалить' style='margin-left: 8px'><img src='/images/delete.gif' onclick='pre_delete( \"/admin/$lang/$module/delete/id/$this->id/id_page/$this->id_page\"); '/></a>";
	
	}
}