<?php

class Lang extends Zend_Db_Table {
	
	protected $_name = 'site_languages';
	protected $_primary = array('id');
	protected static $_instance = null;
	
	/**
	 * Singleton instance
	 *
	 * @return Lang
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	/**
	 * Добавление новой языковой версии
	 *
	 * @param array $data
	 */
	public function addLang($data){
		$this->insert($data);
		Loader::loadPublicModel('Pages');
		Pages::getInstance()->addVersion($data['name']);
	}
		
	/**
	 * Получение языковой версии
	 *
	 * @param string $name
	 * @return object
	 */
	public function getVersion($name){
		$where = $this->getAdapter()->quoteInto('name = ?', $name);
		
		return $this->fetchRow($where);
	}
	
}