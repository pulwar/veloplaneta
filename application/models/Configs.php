<?php

class Configs extends Zend_Db_Table {
	
	protected $_name = 'site_configs';
	protected $_primary = array('id');
	protected static $_instance = null;
	
	/**
	 * Singleton instance
	 *
	 * @return Configs
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	/**
	 * Получение глабольных настроек
	 *
	 * @return array
	 */
	public function getGlobals(){
		$where = $this->getAdapter()->quoteInto("type = ?", 'global');
		
		return $this->fetchAll($where);
	}
	
	/**
	 * Получение e-mail-ов
	 *
	 * @return array
	 */
	public function getMail(){
		$where = $this->getAdapter()->quoteInto("type = ?", 'mail');
		
		return $this->fetchAll($where);
	}
	
	/**
	 * Сохранение изменений
	 *
	 * @param array $array
	 * @return boolean
	 */
	public function save($array){
		foreach ($array as $key => $data){
			$where = $this->getAdapter()->quoteInto("id = ?", $key);
			$value = array('value' => $data);
			$this->update($value, $where);
		}
		
		return true;
	}
	/** get config params by system name
	 * @param string $name
	 * @return Configs_row
	 */
	public function getByName($name){
		$where = $this->getAdapter()->quoteInto('name = ?', $name);
		return $this->fetchRow($where);
	}
	
}