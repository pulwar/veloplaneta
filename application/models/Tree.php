<?php

class Tree extends Zend_Db_Table {
	
	protected $_name = 'site_content_tree';
	protected $_primary = array('id');
	protected static $_instance = null;

	/**
	 * Singleton instance
	 *
	 * @return Modules
	 */
	public static function getInstance()
	{
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	/**
	 * Добавляет один узел в дерево
	 *
	 * @param int $right_key
	 * @param int $level
	 * @return boolean
	 */
	public function addNode($right_key, $level)
	{
		$this->query("INSERT INTO $this->_name SET left_key = :right_key,
    		right_key = :right_key + 1, level = :level + 1", 
			array('right_key' => $right_key, 'level' => $level));
		
		$this->query("UPDATE $this->_name SET right_key = right_key + 2,
    		left_key = IF(left_key > :right_key, left_key + 2, left_key) 
			WHERE right_key >= :right_key ", 
			array('right_key' => $right_key));
		
		return true;
		
	}
	
	/**
	 * Возвращает полное дерево
	 *
	 * @return array
	 */
	public function getTree()
	{
		return $this -> query("SELECT * FROM $this->_name ORDER BY left_key");
	}
	
	/**
	 * Перемещает узел на другое место
	 *
	 * @param int $right_key
	 * @param ште $level
	 * @return boolean
	 */
	//работает покуда не для всех ситуаций
	public function replaceNode($right_key, $level)
	{
		$skew_edit =$right_key_near - $left_key + 1;
		
		$this -> query("UPDATE $this->_name SET right_key = IF(
        	left_key >= $left_key, right_key + $skew_edit, 
            IF(right_key < $left_key, right_key + $skew_tree, 
            right_key)), level = IF(left_key >= $left_key, 
            level + $skew_level, level),
    		left_key = IF(left_key >= $left_key, left_key + $skew_edit, 
    		IF(left_key > $right_key_near, left_key + $skew_tree, left_key))
			WHERE right_key > $right_key_near AND left_key < $right_key",
			array('left_key' => $left_key, 'right_key' => $right_key, 'skew_level' =>$skew_edit));
			
		return true;	
	}
	
	/**
	 * Удаляет узел из дерева
	 *
	 * @param int $right_key
	 * @param int $left_key
	 * @return boolean
	 */
	public function deleteNode($right_key, $left_key)
	{
		$this->query("DELETE FROM $this->_name WHERE left_key >= :left_key
  			AND right_ key <= :right_key ", 
			array('left_key' => $left_key, 'right_key' => $right_key));
		
		$this->query("UPDATE $this->name SET 
			left_key = IF(left_key > $left_key, left_key – ($right_key - $left_key + 1), left_key),
    		right_key = right_key – ($right_key - $left_key + 1) 
			WHERE right_key > $right_key",
			array('left_key' => $left_key, 'right_key' => $right_key));
			
		return true;	
	}
	
	/**
	 * Возвращает все дочернюю ветку
	 *
	 * @param int $right_key
	 * @param int $left_key
	 * @return array
	 */
	public function getChilds($right_key, $left_key)
	{
		return $this->query("SELECT *FROM $this->_name WHERE left_key >= :left_key
  			AND right_key <= :right_key ORDER BY left_key", 
			array('left_key' => $left_key, 'right_key' => $right_key));
	}
	
	/**
	 * Возвращает родительскую ветку
	 *
	 * @param int $right_key
	 * @param int $left_key
	 * @return array
	 */
	public function getParents($right_key, $left_key)
	{
		return $this->query("SELECT * FROM $this->_name WHERE 
    		left_key <= :left_key AND right_key >= :right_key ORDER BY left_key", 
    		array('left_key' => $left_key, 'right_key' => $right_key));
	}
	
	/**
	 * Возвращает родительский элемент
	 *
	 * @return array
	 */
	public function getParent()
	{
		return $this->query("SELECT * FROM $this->_name WHERE left_key <= :left_key
  			AND right_key >= :right_key AND level = :level + 1 ORDER BY left_key", 
			array('right_key' => $right_key, 'left_key' => $left_key));

	}
	
	/**
	 * Выполнение запроса
	 *
	 * @param string $sql
	 * @param array $params
	 * @return array
	 */
	private function query($sql, $params = null)
	{
		$db = $this->getAdapter();
		$result = $db->query($sql, $params);
		
		return $result->fetchAll();
	}
	
}