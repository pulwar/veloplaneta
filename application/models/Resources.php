<?php

class Resources extends Zend_Db_Table {
	
	
	protected static $_instance = null;

	/**
	 * Singleton instance
	 *
	 * @return Resources
	 */
	public static function getInstance()
	{
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	protected $_name = 'resources';
	protected $_primary = array('id');
	protected $_referenceMap = array(
		'content'	=>	array(
			'columns'	=> array('resourceId'),
			'refTableClass' => 'site_content',
			'refColumns' => array('id')
		)
		
	);
	
	
	/**Проверяет существование URL в базе
	 *
	 * @param string $url
	 * @return boolean
	 */
	public function is_resource($url){
		$where  = $this->getAdapter()->quoteInto('url = ?', "$url");
		$row = $this->fetchRow($where);
		
		return $row ? true : false;
	}
	
	/**
	 * Возвращает ресурс, находящийся по указанному URL
	 *
	 * @param string $url
	 * @return object
	 */
	public function getResource($url){
		$where  = $this->getAdapter()->quoteInto('url = ?', "$url");
		$row = $this->fetchRow($where);
		
		return $row;
	}

	public function getType($articleId)
	{
		$row = $this->find($articleId)->current();
		return $row->type;
	}
	
	public function getResourceByUrl($param)
	{
		$where  = $this->getAdapter()->quoteInto('url = ?', "$param");
		return $this->fetchRow($where);
	}
	
	public function deleteArticle($articleId)
	{
		$row = $this->find($articleId)->current();
		$this->delete("id = '$articleId'");
	}
	
	public function checkFind($id)
	{
		$count = count($this->find($id))==0 ? false : true;
	}
	
	
}