<?php

class Consalting extends Zend_Db_Table {
	protected $_name = 'consalting';
	protected $_primary = array('id');
	
	/**
	 * Singleton instance.
	 *
	 * @var St_Model_Layout_Pages
	 */
	protected static $_instance = null;
	
	/**
	 * Singleton instance
	 *
	 * @return Consalting
	 */
	public static function getInstance() {
		if (null === self::$_instance) {
			self::$_instance = new self ( );
		}		
		return self::$_instance;
	}
	
	
	
	/**
	 * get approved items
	 * 
	 */
	public static function getApproved($offset, $limit){
		$dbAdapter = Zend_Registry::get('db');
		$advanced = '';
		if (is_int($offset) && is_int($limit)) {
			$advanced .= " LIMIT " . $offset . ", " . $limit;
		}
		$sql = $dbAdapter->quoteInto(
				"SELECT *, DATE_FORMAT(added, '%d.%m.%Y') as added FROM consalting WHERE approved =? ORDER BY prior DESC, added DESC" . $advanced,
    			1
			);
		
		$result = $dbAdapter->query($sql);
		return $result->fetchAll();
	}
		
}