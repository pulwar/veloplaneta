<?php

class Menu_Row extends Zend_Db_Table_Row {
	/**
	 * gets route to page
	 *
	 * @return string
	 */
	public function getPath(){
		return $this->path;
	}
	
	public function getCatalogFirstLevel(){
		if ($this->template == 'catalog') {
			$db = $this->_getTable()->getAdapter();
			$where =array(
				$db->quoteInto('id_page =?', $this->id),
				$db->quoteInto('parent_id =?', 0),
				$db->quoteInto('level =?', 0)
			);	
			return Catalog_Division::getInstance()->fetchAll( $where );
		} else return '';
	}
}