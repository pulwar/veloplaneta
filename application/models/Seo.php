<?php

class Seo extends Zend_Db_Table
{

    /**
     * The default table name.
     *
     * @var string
     */
    protected $_name = 'site_seo';

    /**
     * The default primary key.
     *
     * @var array
     */
    protected $_primary = array('id');

    /**
     * Whether to use Autoincrement primary key.
     *
     * @var boolean
     */
    protected $_sequence = true; // Использование таблицы с автоинкрементным ключом

    /**
     * Singleton instance.
     *
     * @var Seo
     */
    protected static $_instance = null;

    /**
     * Class to use for rows.
     *
     * @var string
     */
    protected $_rowClass = "Seo_Row";

    /**
     * Singleton instance
     *
     * @return Seo
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getById($id){
        $where = $this->getAdapter()->quoteInto('id = ?',$id);
        return $this->fetchRow($where);
    }

    public function getActiveFilter($div_id, $type, $purpose) {
        if (!$div_id && !$type && !$purpose)
            return null;
        $where = array();
        $where[] = $this->getAdapter()->quoteInto('active = ?', 1);
        if ($div_id)
            $where[] = $this->getAdapter()->quoteInto('div_id = ?', $div_id);
        else
            $where[] = $this->getAdapter()->quoteInto('div_id IS NULL', null);
        if ($type)
            $where[] = $this->getAdapter()->quoteInto('type = ?', $type);
        else
            $where[] = $this->getAdapter()->quoteInto('type IS NULL', null);
        if ($purpose)
            $where[] = $this->getAdapter()->quoteInto('purpose = ?', $purpose);
        else
            $where[] = $this->getAdapter()->quoteInto('purpose IS NULL', null);
        return $this->fetchRow($where);
    }

    public function getAll()
    {
        return $this->fetchAll(null, "id DESC", null, null);
    }

    public function pub($id)
    {
        $array = array('active' => 1);
        $where = $this->getAdapter()->quoteInto('id = ?', $id);
        return $this->update($array, $where);
    }

    public function unpub($id)
    {
        $array = array('active' => 0);
        $where = $this->getAdapter()->quoteInto('id = ?', $id);
        return $this->update($array, $where);
    }

    public function add($data)
    {
        return $this->insert($data);
    }

    public function edit($data, $id)
    {
        $where = $this->getAdapter()->quoteInto('id = ?', $id);
        return $this->update($data, $where);
    }

    public function deleteSeo($id)
    {
        $where = $this->getAdapter()->quoteInto('id = ?', $id);
        return $this->delete($where);
    }
}
