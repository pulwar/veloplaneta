<?php
/**
 * Publications model
 *
 */
class Baners extends Zend_Db_Table {
	
	protected $_name = 'site_places_baners';
	protected $_primary = array('id');
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом

	protected static $_instance = null;
	
	/**
	 * Class to use for rows.
	 *
	 * @var string
	 */
	//protected $_rowClass = "Peoples_Row";
	
	
	/**
	 * Singleton instance
	 *
	 * @return Baners
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	
	
}

?>
