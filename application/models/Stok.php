<?php

class Stok extends Zend_Db_Table {
	
	protected $_name = 'site_stok';
	protected $_primary = array('id');
	protected static $_instance = null;
	
	/**
	 * Singleton instance
	 *
	 * @return News
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	/*	
	*получает все новости для админки
	*с разбивкой по страницам
	*/
	public function getAll($offset, $limit,$sort){
		
		$dbAdapter = Zend_Registry::get('db');
		$advanced = '';
		$sortText='';
		
		if (is_int($offset) && is_int($limit)) {
			$advanced .= " LIMIT " . $offset . ", " . $limit." ";
		}
		
		if ($sort == 'nASC') {
			$sortText = ' ORDER BY site_stok.name ASC';
			
		} elseif ($sort == 'nDESC') {
			$sortText = ' ORDER BY site_stok.name DESC';
		}
		if ($sort == 'sASC') {
			$sortText = ' ORDER BY site_stok.pub     ASC';
		} elseif ($sort == 'sDESC') {
			$sortText = ' ORDER BY site_stok.pub     DESC';
		}
		
		if ($sort==null){
			$sortText = ' ORDER BY site_stok.created_at    DESC';
		}
		
		$sql = $dbAdapter->quoteInto("SELECT * FROM site_stok ".$sortText. $advanced,null);
		$result = $dbAdapter->query($sql);
		 $list = $result->fetchAll();
		 return $list;
	}
	
	/*	
	*Возвращает все опбликованные новости
	*
	*/
	public function getAllPub($ofset,$count){
		$where = $this->getAdapter()->quoteInto('pub= ?',1);
		$order =array( $this->getAdapter()->quoteInto('created_at DESC',null),
						 $this->getAdapter()->quoteInto('name ASC',null)
		);
		return $this->fetchAll($where,$order,$count,$ofset);
	}
	
	
	/*	
	*добавление новости
	*
	*/
	public function addNew($data){
		if (!isset($data['created_at'])) {
			$data['created_at'] =new Zend_Db_Expr('NOW()'); //date("d-m-Y H:i:s");
			
		}
		
		if (isset($data['pub']) && $data['pub']==1) {
			$data['pub_date'] =new Zend_Db_Expr('NOW()'); //date("d-m-Y H:i:s");
			
		}
		
		return $this->insert($data);
	}
	
	/*	
	*получение новости по $id
	*@param $id int
	*
	*/
	public function getNewById($id){
		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		return $this->fetchRow($where);
	}
	
	/*	
	*опубликовать новость
	*@param $id int
	*
	*/
	public function pubNew($id){
		$array = array('pub'=>1,'pub_date'=>new Zend_Db_Expr('NOW()'));
		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		return $this->update($array,$where);
	}
	/*	
	*заблокировать новость
	*@param $id int
	*
	*/
	public function unpubNew($id){
		$array = array('pub'=>0);
		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		return $this->update($array,$where);
	}
	/*	
	*Редактирование новости
	*@param $data array
	*@param $id int
	*
	*/
	public function editNew($data,$id){
		
		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		return $this->update($data,$where);
	}
	
	/*	
	*копирование новости
	*@param $id int
	*
	*/
	public function CopyNew($id){
		
		$new= $this->getNewById($id);
		$new = $new->toArray();
		$new['created_at'] = new Zend_Db_Expr('NOW()');
		if ($new['pub']==1){
			$new['pub_date']=new Zend_Db_Expr('NOW()');
		}
		unset($new['id']);
		$new['name']=$new['name'].'_copy';
		return $this->insert($new);
	}
	
	public function deleteNew($id){		
		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		return $this->delete($where);
	}
	/*	
	*сделать новость главной (для отображения на главной странице)
	*@param $id int
	*
	*/
	public function setMainNew($id){		
		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		$data=array('main'=>1);
		return $this->update($data,$where);
	}
	
	public function unsetMainNew($id){		
		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		$data=array('main'=>0);
		return $this->update($data,$where);
	}
	/*	
	*возвращает все новости устанавленные как главные и опубликованные
	*
	*
	*/
	public function getMainNews(){
		$where =array( $this->getAdapter()->quoteInto('pub= ?',1),
					   $this->getAdapter()->quoteInto('main= ?',1));
		$order = $this->getAdapter()->quoteInto('created_at DESC',null);
		return $this->fetchAll($where,$order);
		
	}
	/*	
	*возвращает одну рандомную новость
	*
	*
	*/
	public function getRandomNews(){
		$where =array( $this->getAdapter()->quoteInto('pub= ?',1),
					   $this->getAdapter()->quoteInto('main= ?',1));
		$order = $this->getAdapter()->quoteInto('RAND()',null);
		
		return $this->fetchAll($where,$order,1 );
		
	}
	/*	
	*возвращает последние 10 новостей с сортировкой по дате
	*
	*
	*/
	public function  getLastTenNews(){
      $order = $this->getAdapter()->quoteInto('created_at DESC',null);
      $where =$this->getAdapter()->quoteInto('pub= ?',1);
      return $this->fetchAll($where,$order,10,0);
      
      
	}
	/*	
	*преобразует поле дата к нужному формату  '%d.%m.%Y'
	*
	*
	*/
	public function convertData($id) {
	
   		$dbAdapter = Zend_Registry::get('db');
   		$sql = $dbAdapter->quoteInto(" SELECT  DATE_FORMAT( created_at, '%d.%m.%Y' ) as data from site_stok WHERE id=?",$id);
   		$result = $dbAdapter->query($sql);
   		$result=$result->fetchAll();
   		
		return $result['0']['data'];
	}

    public function getRandNews(){      
        $db = Zend_Registry::get('db');
        $sql = ("SELECT * FROM site_stok WHERE main=1 and pub=1 ORDER BY RAND() ASC LIMIT 2");
        $result = $db->query($sql);
        return $result->fetchAll();   
    }
}

