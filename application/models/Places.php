<?php
/**
 * Peoples model
 *
 */
class Places extends Zend_Db_Table {
	
	protected $_name = 'site_places';
	protected $_primary = array('id');
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом

	protected static $_instance = null;
	
	/**
	 * Class to use for rows.
	 *
	 * @var string
	 */
	//protected $_rowClass = "Places_Row";
	
	
	/**
	 * Singleton instance
	 *
	 * @return Places
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	
	
}

?>
