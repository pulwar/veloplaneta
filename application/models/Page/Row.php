<?php

class Page_Row extends Zend_Db_Table_Row {
	
 	/**
 	 *  @var Zend_view
	 */
	protected  $_view =null;
	
	public function init(){
		 $this->_view = Registry::getView();
	}
	/**
	 * gets route to page
	 *
	 * @return string
	 */
	public function getPath(){
		return $this->path;
	}
	/**
	 * @return Catalog_Division_Rowset
	 */
	public function getCatalogFirstLevel(){
		if ($this->module == 'catalog') {
			$db = $this->_getTable()->getAdapter();
			$where =array(
				$db->quoteInto('id_page =?', $this->id),
				$db->quoteInto('parent_id =?', 0),
				$db->quoteInto('level =?', 0)
			);	
			return Catalog_Division::getInstance()->fetchAll( $where, 'sortid' );
		} else return '';
	}
	
	public function getInfoblockItems($type){
		if ($this->template == $type){
			return News::getInstance()->getAllPub(null,null,$this->id);
		} else return false;
	}
	
	public function getCatalogMenu(){
		if ($this->module=='catalog'){
			$this->init();
			$this->_view->catalog_divs = $this->getCatalogFirstLevel();
			return $this->_view->render("CatalogMap.phtml");
		}	else return '';
	}
	
	public function getParent(){
		return Pages::getInstance()->find($this->parentId)->current();
	}
	
	public function issetChild(){
		if (count(Pages::getInstance()->fetchAll('parentId='.(int)$this->id. ' AND published=1 '))>0){
			return true;
		} else return false;
	}
	public function getChild($count=0){
		return Pages::getInstance()->fetchAll('parentId='.$this->id. ' AND published=1 ' , 'sortId ASC', $count);
	}
	public function getChildTpl(){
		if ($this->issetChild()){
			$this->init();
			$this->_view->childs = $this->getChild();
			return $this->_view->render("PageChilds.phtml");
		} else return '';
	}
	
	public function getRightMenu(){
		$id_uslugi_page = 172;
		$html = '';
		if ($this->id!=$id_uslugi_page && $this->parentId!=$id_uslugi_page){
			$childs = $this->getChild();			
			if (sizeof($childs)>0){		
				$html .= '<ul class="right_menu">';
	    		foreach ($childs as $child){
	    			$html.= "<li><a href='/$child->path'>$child->name</a></li>\r\n";
	    		}
	    		$html.= '</ul>';
			}
			
		}
		return $html;
	}
	
	public function getBread($flag=true){
		$this->init();
		$array =array();
		$array[] = $page_row =$this;
		while ($page_row->level !=1){
			$page_row = $page_row->getParent();
			$array[]=$page_row;
		}
		$this->_view->bread = array_reverse($array);
		$this->_view->flag = $flag;
		return $this->_view->render("PageBread.phtml");
	}

        public function getBreadArray(){
		$this->init();
		$array =array();
		$array[] = $page_row =$this;
		while ($page_row->level !=1){
			$page_row = $page_row->getParent();
			$array[]=$page_row;
		}
		return array_reverse($array);
	}

        public function getBreadInfoblock($itemName){
		$this->init();
		$array =array();
		$array[] = $page_row =$this;
		while ($page_row->level !=1){
			$page_row = $page_row->getParent();
			$array[]=$page_row;
		}
		$this->_view->bread = array_reverse($array);
		$this->_view->flag = false;
                $this->_view->itemName = $itemName;
		return $this->_view->render("InfoblockBread.phtml");
	}

	/**
	 * @return Portfolio_Divisions_Rowset
	 */
	public function getPortfolioDivisions(){
		//return $this->findDependentRowset('Portfolio_Divisions', 'Divisions');
	  return Portfolio_Divisions::getInstance()->getAll();
	}
	
	
	
	
}