<?php

require_once('Zend/Db/Table.php');

class Content extends Zend_Db_Table {
	
	
	protected static $_instance = null;

	/**
	 * Singleton instance
	 *
	 * @return Content
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	protected $_name = 'site_content';
	protected $_primary = array('id');
	
	/**Проверяет существование URL в базе
	 *
	 * @param string $url
	 * @return boolean
	 */
	public function is_exist($url){
		
		Loader::loadPublicModel('Resources');
		$where  = $this->getAdapter()->quoteInto('url = ?', "$url");
		$row = Resources::getInstance()->fetchRow($where);
		
		if($row){
			$where  = $this->getAdapter()->quoteInto('resourceId = ?', "$row->id");
			$row = $this->fetchRow($where);
		}
		
		return $row ? true : false;
	}
	
	/**
	 * Возвращает страницу, находящуюся по указанному URL
	 *
	 * @param string $url
	 * @return array
	 */
	public function getPage($url){
		
		$resource = Resources::getInstance()->getResourceByUrl($url);
		$where  = $this->getAdapter()->quoteInto('id = ?', "$resource->id");
		$row = $this->fetchRow($where);
		
		return $row;
	}
	
	/**
	/**Возвращает соответствующий ресурс
	 *@return object
	 */
	public function getResource($param){
		Loader::loadPublicModel('Resources');
		$where  = $this->getAdapter()->quoteInto('id = ?', "$param");
		return Resources::getInstance()->fetchRow($where);
	}
	
	/**
	 * Получение типа контента
	 *
	 * @param int $articleId
	 * @return string
	 */
	public function getType($articleId){
		$row = $this->find($articleId)->current();
		
		return $row->type;
	}
	
	/**
	 * Удаление страницы
	 *
	 * @param int $articleId
	 */
	public function deleteArticle($articleId){
		$row = $this->find($articleId)->current();
		$this->delete("id = '$articleId'");
	}
	
	/**
	 * Проверка существования 
	 * страницы с указанным ID
	 *
	 * @param unknown_type $id
	 */
	public function checkFind($id){
		$count = count($this->find($id))==0 ? false : true;
	}
	
}