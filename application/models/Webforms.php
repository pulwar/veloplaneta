<?php

class Webforms extends Zend_Db_Table {
	
	protected $_name = 'site_feedback';
	protected $_primary = array('id');
	protected static $_instance = null;
	
	/**
	 * Singleton instance
	 *
	 * @return Pages
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	/**
	 *
	 * @return array
	 */
	public function getEmails(){
		$all = $this->fetchAll();
		return $all;
	}
	
	public function updateEmails($array,$id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return  $this->update($array,$where);
	}
}