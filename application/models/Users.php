<?php

require_once('Zend/Db/Table.php');

class Users extends Zend_Db_Table 
{
	protected $_name = 'users';
	protected $_primary = array('id');
	
	protected static $_instance = null;
	
	/**
	 * Singleton instance
	 * @return Users
	 */
	public static function getInstance()
	{
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	public function getUsers(){
//		$where = $this->getAdapter()->quoteInto('role = ?', 'admin');
		return $this->fetchAll();
	}
	
	public function setActivity($id, $value){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->update(array('activity' => $value), $where);
		
		return true;
	}
	
	public function deleteUser($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->delete($where);
		
		return true;
	}
	
	public function editUser($id, $data){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
//		print_r($data);exit;
		$data = array(
			'username' => $data['username'],
			'password' => $data['password'],
			'email' => $data['email'],
			'firstName' => $data['first_name'],
			'lastName' => $data['last_name'],
			'activity' => isset($data['activity']) ? '1' : '0'
			);
		$this->update($data, $where);
		
		return true;
	}
	
	public function getUser($id){
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$user = $this->fetchRow($where);
		
		return $user;
	}
	
	public function addUser($data){
		$data = array(
			'username' => $data['username'],
			'password' => $data['password'],
			'email' => $data['email'],
			'firstName' => $data['first_name'],
			'lastName' => $data['last_name'],
			'deletable' => '1',
			'role' => 'admin',
			'activity' => isset($data['activity']) ? '1' : '0'
			);
		$this->insert($data);
		
		return true;
	}
	
}