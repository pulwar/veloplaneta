<?php
/**
 * Actions main class
 * @author Vitaly
 * @version 1.0
 * @copyright www.proweb.by
 * @package roweb
 *
 */
class Actions extends Zend_Db_Table {


	/**
	 * The default table name.
	 *
	 * @var string
	 */
	protected $_name = 'site_news';

	/**
	 * The default primary key.
	 *
	 * @var array
	 */
	protected $_primary = array('id');

	/**
	 * Whether to use Autoincrement primary key.
	 *
	 * @var boolean
	 */
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом

	/**
	 * Singleton instance.
	 *
	 * @var St_Model_Layout_Pages
	 */
	protected static $_instance = null;

	/**
	 * Dependent tables.
	 *
	 * @var array
	 */
	protected $_dependentTables = array(

	) ;

	/**
	 * Reference map.
	 *
	 * @var array
	 */
	protected $_referenceMap = array(

	) ;

	/**
	 * Class to use for rows.
	 *
	 * @var string
	 */
	protected $_rowClass = "News_Row" ;

	/**
	 * Class to use for row sets.
	 *
	 * @var string
	 */
	protected $_rowsetClass = "News_Rowset" ;

	/**
	 * Singleton instance
	 *
	 * @return News
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public function find($id){
		return parent::find($id);
	}

	public function getAll($offset, $limit,$sort, $page_id){

		$dbAdapter = Zend_Registry::get('db');
		$advanced = '';
		$sortText='';

		if (is_int($offset) && is_int($limit)) {
			$advanced .= " LIMIT " . $offset . ", " . $limit." ";
		}

		if ($sort == 'nASC') {
			$sortText = ' ORDER BY site_news.name ASC';

		} elseif ($sort == 'nDESC') {
			$sortText = ' ORDER BY site_news.name DESC';
		}
		if ($sort == 'sASC') {
			$sortText = ' ORDER BY site_news.pub     ASC';
		} elseif ($sort == 'sDESC') {
			$sortText = ' ORDER BY site_news.pub     DESC';
		}

		if ($sort==null){
			$sortText = ' ORDER BY site_news.created_at    DESC';
		}

		$sql = $dbAdapter->quoteInto("SELECT * FROM site_news WHERE page_id=".$page_id.' '.$sortText. $advanced,null);

		$result = $dbAdapter->query($sql);
		 $list = $result->fetchAll();
		 return $list;
	}

	/*
	*Возвращает все опбликованные новости
	*
	*/
	public function getAllPub($page_id, $ofset = null, $count = null){
		$where=array();
		if (is_array($page_id)){
			$where[] =$this->getAdapter()->quoteInto('pub= ?',1);
			foreach ($page_id as $id){
					$where[] = $this->getAdapter()->quoteInto('page_id= ?',$id);
			}
		} else{
		$where =array( $this->getAdapter()->quoteInto('pub= ?',1),
						$this->getAdapter()->quoteInto('page_id= ?',(int)$page_id));
		}

		$order =array( $this->getAdapter()->quoteInto('created_at DESC',null),
						 $this->getAdapter()->quoteInto('name ASC',null)
		);

		return $this->fetchAll($where,$order,$count,$ofset);
	}


	/*
	*добавление новости
	*
	*/
	public function addNew($data){
		if (!isset($data['created_at'])) {
			$data['created_at'] =new Zend_Db_Expr('NOW()'); //date("d-m-Y H:i:s");

		}

		if (isset($data['pub']) && $data['pub']==1) {
			$data['pub_date'] =new Zend_Db_Expr('NOW()'); //date("d-m-Y H:i:s");

		}

		return $this->insert($data);
	}

	/*
	*получение новости по $id
	*@param $id int
	*
	*/
	public function getNewById($id){
		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		return $this->fetchRow($where);
	}
	/**
	 * получение опубликованной новости по id
	 * @return News_Row
	 */
	public function getFrontNew($id){
		$where = array(
			$this->getAdapter()->quoteInto('id= ?',$id),
			$this->getAdapter()->quoteInto('pub= ?',1)
			);
		return $this->fetchRow($where);
	}
	/*
	*опубликовать новость
	*@param $id int
	*
	*/
	public function pubNew($id){
		$array = array('pub'=>1,'pub_date'=>new Zend_Db_Expr('NOW()'));
		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		return $this->update($array,$where);
	}
	/*
	*заблокировать новость
	*@param $id int
	*
	*/
	public function unpubNew($id){
		$array = array('pub'=>0);
		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		return $this->update($array,$where);
	}
	/*
	*Редактирование новости
	*@param $data array
	*@param $id int
	*
	*/
	public function editNew($data,$id){

		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		return $this->update($data,$where);
	}

	/*
	*копирование новости
	*@param $id int
	*
	*/
	public function CopyNew($id){

		$new= $this->getNewById($id);
		$new = $new->toArray();
		$new['url'] = $new['url'].'_copy'.rand(1,20);
		$new['created_at'] = new Zend_Db_Expr('NOW()');
		if ($new['pub']==1){
			$new['pub_date']=new Zend_Db_Expr('NOW()');
		}
		unset($new['id']);

		return $this->insert($new);
	}

	public function deleteNew($id){
		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		return $this->delete($where);
	}
	/*
	*сделать новость главной (для отображения на главной странице)
	*@param $id int
	*
	*/
	public function setMainNew($id){
		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		$data=array('main'=>1);
		return $this->update($data,$where);
	}

	public function unsetMainNew($id){
		$where = $this->getAdapter()->quoteInto('id= ?',$id);
		$data=array('main'=>0);
		return $this->update($data,$where);
	}
	/*
	*возвращает все новости устанавленные как главные и опубликованные
	*
	*
	*/
	public function getMainNews(){
		$where =array(
				$this->getAdapter()->quoteInto('pub= ?',1),
				$this->getAdapter()->quoteInto('main= ?',1),
				$this->getAdapter()->quoteInto('type= ?','news')
				);
		$order = $this->getAdapter()->quoteInto('created_at DESC',null);
		return $this->fetchAll($where,$order);

	}

/*
	*возвращает все новости устанавленные как главные и опубликованные
	* @param string $type
	*
	*/

	public function getMain(){
		$where =array(
				$this->getAdapter()->quoteInto('pub= ?',1),
                                $this->getAdapter()->quoteInto('main= ?',1),
                                $this->getAdapter()->quoteInto('type= ?','actions'),
				);
		return $this->fetchAll($where);

	}

        public function getAction($id, $order = null) {
		$where = $this->getAdapter ()->quoteInto ( 'id = ?', ( int ) $id );
		$page = $this->fetchRow ( $where, $order );

		return $page;
	}

	public function getMainAkciya(){
		$where =array(
				$this->getAdapter()->quoteInto('pub= ?',1),
				$this->getAdapter()->quoteInto('main= ?',1),
				$this->getAdapter()->quoteInto('type= ?','akcii')
				);
		$order = $this->getAdapter()->quoteInto('created_at DESC',null);
		return $this->fetchRow($where,$order);

	}
	/*
	*возвращает одну рандомную новость
	*
	*
	*/
	public function getRandomNews(){
		$where =array( $this->getAdapter()->quoteInto('pub= ?',1),
					   $this->getAdapter()->quoteInto('main= ?',1));
		$order = $this->getAdapter()->quoteInto('RAND()',null);

		return $this->fetchAll($where,$order,1 );

	}

	/*
	*возвращает одну рандомную Акцию
	*
	*
	*/
	public function getRandomStok(){
		$where =array( $this->getAdapter()->quoteInto('pub= ?',1),
					   $this->getAdapter()->quoteInto('type= ?','akcii'),
					   $this->getAdapter()->quoteInto('main= ?',1));
		$order = $this->getAdapter()->quoteInto('RAND()',null);

		return $this->fetchAll($where,$order,1 );

	}

	/*
	*возвращает последние 10 новостей с сортировкой по дате
	*
	*
	*/
	public function  getLastTenNews($type = 'news'){
      $order = $this->getAdapter()->quoteInto('created_at DESC',null);
      $where =array(
      		$this->getAdapter()->quoteInto('pub= ?',1),
      		$this->getAdapter()->quoteInto('type= ?',$type)
      );
      return $this->fetchAll($where,$order,10,0);


	}
	/*
	*преобразует поле дата к нужному формату  '%d.%m.%Y'
	*
	*
	*/
	public function convertData($id, $format = "%d.%m.%Y") {

   		$dbAdapter = Zend_Registry::get('db');
   		$sql = $dbAdapter->quoteInto(" SELECT  DATE_FORMAT( created_at, '$format' ) as data from site_news WHERE id=?",$id);
   		$result = $dbAdapter->query($sql);
   		$result=$result->fetchAll();

		return $result['0']['data'];
	}

    public function getRandNews(){
        $db = Zend_Registry::get('db');
        $sql = ("SELECT * FROM site_news WHERE main=1 and pub=1 ORDER BY RAND() ASC LIMIT 2");
        $result = $db->query($sql);
        return $result->fetchAll();
    }

	public function search($search){
		 $dbAdapter = Zend_Registry::get('db');
		 $sql = $dbAdapter->quoteInto("SELECT DISTINCT *, 'news' AS TYPE FROM site_news WHERE site_news.name LIKE '%".$search."%'
		    OR site_news.intro LIKE '%".$search."%'
		 	OR site_news.content LIKE '%".$search."%'
		    AND site_news.pub =1 ORDER BY site_news.name ; ",null);
		 $result = $dbAdapter->query($sql);
		 return  $result->fetchAll();

	}

}

