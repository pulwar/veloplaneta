<?php

class News_Row extends Zend_Db_Table_Row {
	
	//private $_path ="/pics/portfolio/"; // path to images

	/**
	 * Gets route
	 *
	 * @return string
	 */
	public function getUrl(){
		return Pages::getInstance()->find($this->page_id)->current()->getPath();
	}
	
public function getPath(){
		return Pages::getInstance()->find($this->page_id)->current()->getPath();
	}
	/**
	 * Gets date by format
	 *
	 * @param string $param
	 * @return string
	 */
	public function date($param = 'all'){
		$format='';
		switch ($param) {
			case 'day': $format ="%d";// Day of the month, numeric (0..31)
				
				break;
			case 'month': $format ="%b"; //Abbreviated month name (Jan..Dec)
				
				break;				
		
			default: $format =  "%d.%m.%Y"; //01.01.2008
				break;
		}
			
		$date = News::getInstance()->convertData($this->id , $format);
		/*if (0 ==(int)$date) {
			//return  iconv("Windows-1251", "UTF-8", strftime($format, strtotime($date)));
			return strftime($format, strtotime($date));
		} else {
			return $date;
		}*/
		return iconv("Windows-1251", "UTF-8", strftime('%d %b %Y', strtotime($date)));
		 
	}
	/**
	 * получает автора новости, статьи, ответа
	 *
	 * @return object
	 */
	public function getUser(){
		if ($this->peoples_id!=0){
			$user = Peoples::getInstance()->find($this->peoples_id)->current();
			if ($user!=null) return $user;
		}
		return '';
		
	}
}