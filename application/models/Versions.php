<?php

require_once('Zend/Db/Table.php');

class Versions extends Zend_Db_Table {
	protected $_name = 'site_languages';
	protected $_primary = array('id');
	protected static $_instance = null;

	/**
	 * Singleton instance
	 *
	 * @return Versions
	 */
	public static function getInstance()
	{
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	/**
	 * Получение всех существующих версий
	 *
	 * @return unknown
	 */
	public function getVersions(){
		return $this->fetchAll();
	}
	
	/**
	 * Получение версии
	 *
	 * @param string $param
	 * @param int $id
	 * @return object
	 */
	public function getVersion($param, $id = null){
		if(!$id){
			$where = $this->getAdapter()->quoteInto('name = ?', $param);
		}
		else{
			$where = $this->getAdapter()->quoteInto('id = ?', $id);
		}
		
		return $this->fetchRow($where);
	}
}