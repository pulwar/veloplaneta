<?php

/**
 * IndexController - The default controller class
 *
 * @author
 * @version
 */

class Catalog_IndexController extends Zend_Controller_Action {


    private $_options = null;
    /**
     * Инициализирует класс
     */
    public function init() {
        $this->view->lang = $this->_getParam('lang', 'ru');
        $this->view->options =$page_options = PagesOptions::getInstance()->getPageOptions($this->_getParam('id'));
        $page = Pages::getInstance()->getPage($this->_getParam('id', 0));


        if (!is_null($page)){
            $this->view->options = $this->_options = PagesOptions::getInstance()->getPageOptions($page->id);
            $this->view->page = $page;
        }
    }

    public function indexAction()
    {
        if ($this->_hasParam('tovar')) {
            $this->_forward('tovar');
        }

        $this->view->lang = $this->_getParam('lang', 'ru');
        $this->view->options = $page_options = PagesOptions::getInstance()->getPageOptions($this->_getParam('id'));
        $page = Pages::getInstance()->getPage($this->_getParam('id'));
        $this->view->options = PagesOptions::getInstance()->getPageOptions($page->id);
        $this->setTemplate($page->template);
        $this->view->page = $page;


        $sortSession = new Zend_Session_Namespace('sortSearch');
        $sort = $this->view->sort = $this->getSort($sortSession->sort);
        $lang = $this->getRequest()->getParam('lang', 'ru');
        $this->view->lang = $lang;

        if ($this->_getParam('sort', NULL) == 'nasc') {
            $sort = $this->view->sort = 'nASC';
            $sortSession->sort = 'nASC';
        } elseif ($this->_getParam('sort', NULL) == 'ndesc') {
            $sort = $this->view->sort = 'nDESC';
            $sortSession->sort = 'nDESC';
        }
        if ($this->_getParam('sort', NULL) == 'pasc') {
            $sort = $this->view->sort = 'pASC';
            $sortSession->sort = 'pASC';
        } elseif ($this->_getParam('sort', NULL) == 'pdesc') {
            $sort = $this->view->sort = 'pDESC';
            $sortSession->sort = 'pDESC';
        }
        if ($this->_getParam('sort', NULL) == 'casc') {
            $sort = $this->view->sort = 'cASC';
            $sortSession->sort = 'cASC';
        } elseif ($this->_getParam('sort', NULL) == 'cdesc') {
            $sort = $this->view->sort = 'cDESC';
            $sortSession->sort = 'cDESC';
        }

        if ($this->_hasParam('div_id')) {
            $div_id = $this->_getParam('div_id', 0);
            $division = Catalog_Division::getInstance()->find($div_id)->current();


            if ($division->id == 1 || $division->id == 16000 || $division->id == 43000 || $division->id == 73000 || $division->id == 335000 || $division->id == 399000 || $division->id == 408000 || $division->id == 401000 || $division->id == 415000 || $division->id == 420000 || $division->id == 424000 || $division->id == 428000 || $division->id ==431012) {
                if ($division->id == 1) {
                    $division = Catalog_Division::getInstance()->find('1')->current();
                }
//                $test = Catalog_Division::getInstance()->getAllHumanTypes($division->id);
//
//                var_export($test); die;
                // Страница велосипеды "Производители"
                $this->view->division = $division;
                $parent = Catalog_Division::getInstance()->getParenDiv($division->parent_id);
                $this->view->parent = $parent;

                $options = PagesOptions::getInstance()->getOptions($division->id, 'catalog_division');
                    if (!is_null($options)) {
                        $opt_array = $options->toArray();
                        foreach ($opt_array as $key => $value) {
                            if ($value == '') {
                                $opt_array[$key] = $division->name;
                            }
                        }
                        $options->setFromArray($opt_array)->save();
                        $this->view->options = $options;
                    }
				
                // $where = 'bike_type != 0'; выводит все значения не равные нулю, т.е. все велосипеды
                $params['type'] = $this->view->type = $this->_getParam('type', '');
                $params['purpose'] = $this->view->purpose = $this->_getParam('purpose', '');
                $where = 'bike_type is not null';
                if ($params['type'] != '') {
                    $where .= ' AND bike_type = ' . $params['type'];
                }
                if ($params['purpose'] != '') {
                    $where .= ' AND human_type = ' . $params['purpose'];
                }

                $this->view->onpage = $onpage = 120; // products per page
                $ipage = $this->view->current_page = $this->_getParam('page', 1);
                $offset = ($ipage - 1) * $onpage;
                //echo $offset; exit;
                $sub_divs = Catalog_Division::getInstance()->getChildDivisions($division->id);
                $divs = array();
                foreach ($sub_divs as $div) {
                    $divs[] = $div->id;
                }
                //d.id_division IN (" . $id_div . ")"
                if ($division->id != 1) {
                    $where .= ' AND division_id in (' . implode(',', $divs) . ')';
                }
                //Zend_Debug::dump($where);exit();
                //$this->view->products = $division->getTovars($count = $onpage, $offset);

                $this->view->products = $abw = Catalog_Tovar::getInstance()->fetchAll($where, 'prior ASC', $onpage, $offset);
                //Zend_Debug::dump($abw);exit();
                // $division->getTovars($count = $onpage, $offset);
                $this->view->total = count(Catalog_Tovar::getInstance()->fetchAll($where));
                $this->view->bike = true;
            } else {
                if ($division != null) {
                    $options = PagesOptions::getInstance()->getOptions($division->id, 'catalog_division');
                    if (!is_null($options)) {
                        $opt_array = $options->toArray();
                        foreach ($opt_array as $key => $value) {
                            if ($value == '') {
                                $opt_array[$key] = $division->name;
                            }
                        }
                        $options->setFromArray($opt_array)->save();
                        $this->view->options = $options;
                    }
                }
                Zend_Controller_Front::getInstance()->setParam('div_id', $div_id);
                if ($division->issetChilds()) {
                    $this->view->onpage = $onpage = 100; //divisios per page
                    $ipage = $this->view->current_page = $this->_getParam('page', 1);
                    $offset = ($ipage - 1) * $onpage;
                    $this->view->div_childs = $division->getChilds($count = $onpage, $offset);
                    $this->view->total = $division->getChilds()->count();
                    //Зимние товары каталог
                    if ($division->id == 413001) {
                        $where = 'division_id = 411000 or division_id = 412000 or division_id = 413000 or division_id = 414000';
                    } else {
                        //Велоаксессуары каталог
                        $where = 'division_id = 153000 or division_id = 183000 or division_id = 173000 or division_id = 133000 or division_id = 223000 or division_id = 243000 or division_id = 263000 or division_id = 213000 or division_id = 398000 or division_id = 203000 or division_id = 303000 or division_id = 143000 or division_id = 253000 or division_id = 15000 or division_id = 13000 or division_id = 12000 or division_id = 6000 or division_id = 397000 or division_id = 2000 or division_id = 7000 or division_id = 395000 or division_id = 407000 or division_id = 5000 or division_id = 233000';
                    }
                    $total = count(Catalog_Tovar::getInstance()->fetchAll($where));
                    $this->view->products = $abw = Catalog_Tovar::getInstance()->fetchAll($where, 'prior ASC', $total, $offset);
                    //echo count($this->view->products); die;

                } elseif ($division->issetTovarInDiv()) {
                    $this->view->onpage = $onpage = 33; // products per page
                    $ipage = $this->view->current_page = $this->_getParam('page', 1);
                    $offset = ($ipage - 1) * $onpage;
                    //echo $offset; exit;
                    $this->view->products = $division->getTovars($count = $onpage, $offset);
                    $this->view->total = $division->getTovars()->count();
                }



                $this->view->division = $division;
                $this->view->cur_id = $division->id;
                $parent = Catalog_Division::getInstance()->getParenDiv($division->parent_id);
                $this->view->parent = $parent;

                $this->view->bread = $this->getBread($division->id);
            }

            $type = $this->_getParam('type', '');
            $purpose = $this->_getParam('purpose', '');
            Loader::loadPublicModel('Seo');
            $seoFilter = Seo::getInstance()->getActiveFilter($div_id, $type, $purpose);
            if ($seoFilter) {
                $this->view->options->title = $seoFilter->title;
                $this->view->options->descriptions = $seoFilter->description;
                $this->view->options->h1 = $seoFilter->h1;
                $this->view->seoFilter = $seoFilter;
            }
        } else {
            $onpage = 9;
            $offset = 0;
            $division = Catalog_Division::getInstance()->find(1)->current();
            $this->view->onpage = $onpage = 15; //divisios per page
            $ipage = $this->view->current_page = $this->_getParam('page', 1);
            $offset = ($ipage - 1) * $onpage;
            $where = 'bike_type != 0';
            $this->view->total = count(Catalog_Tovar::getInstance()->fetchAll($where));
            $this->view->products = $abw = Catalog_Tovar::getInstance()->fetchAll($where, 'prior ASC', $this->view->total, $offset);
            $this->view->brands = Catalog_Division::getInstance()->fetchAll('level=0 AND parent_id=0', 'sortid', $count = $onpage, $offset);
        }
    }

    public function tovarAction(){
        $tovar_id = $this->_getParam('tovar');
        $tovar = Catalog_Tovar::getInstance()->find($tovar_id)->current();
        $div_id = Catalog_Division::getInstance()->getTovarDivision($tovar_id);
        $division = Catalog_Division::getInstance()->find($div_id[0]['division_id'])->current();
        if (!is_null($tovar)) {
            $this->view->tovar = $tovar;
            $this->view->bread = $this->getBread($tovar->division_id);
            if($tovar->bike_type != 0){
                $division = Catalog_Division::getInstance()->find(1)->current();
            }
            $this->view->division = $division;

            $division = Catalog_Division::getInstance()->getTovarDivision($tovar_id);
            $this->view->div_id = $division;

            $this->setTemplate('tovar');
            $options  = PagesOptions::getInstance()->getOptions($tovar->id, 'tovar');

            if (!is_null($options) ){

                $opt_array = $options->toArray();
                foreach ($opt_array as $key=>$value){
                    if ($value == ''){
                        $opt_array[$key] = $tovar->name;
                    }
                }
                $options->setFromArray($opt_array)->save();
                $this->view->options = $options;
            }
        }

    }

    public function articulAction(){
        header('Content-Type: text/html; charset=utf-8');

        if($this->_hasParam('q')){
            $q = trim($this->_getParam('q'));
            $base = Catalog_Tovar::getInstance()->fetchAll("articul LIKE '$q%'");
            // print_r($base->toArray());
            if (count($base)){
                foreach ($base as $pen){
                    print $pen->articul."\n";
                }
            }
        }
        exit;
    }

    private function getBread($div_id){
        $bread = array();
        if ($div_id){
            $division = Catalog_Division::getInstance()->find($div_id)->current();
            if (!is_null($division)){
                $bread[]=$division;
                $cur_div = $division;
                while ($cur_div->parent_id !=0){
                    $cur_div = Catalog_Division::getInstance()->getItemById($cur_div->parent_id);
                    if ($cur_div->level==0){
                        Zend_Controller_Front::getInstance()->setParam('div_id', $cur_div->id);
                    }
                    $bread[]=$cur_div;
                }
            }
            return array_reverse($bread);
        }
    }

    private function getSort($sort)
    {
        if (!$sort) {
            return NULL;
        } elseif ($sort == 'nASC') {
            return 'nASC';
        } elseif ($sort == 'nDESC') {
            return 'nDESC';

        } elseif ($sort == 'pASC') {
            return 'pASC';
        } elseif ($sort == 'pDESC') {
            return 'pDESC';

        } elseif ($sort == 'cASC') {
            return 'cASC';
        } elseif ($sort == 'cDESC') {
            return 'cDESC';

        } elseif ($sort == 'gASC') {
            return 'gASC';
        } elseif ($sort == 'gDESC') {
            return 'gDESC';

        } elseif ($sort == 'sASC') {
            return 'sASC';
        } elseif ($sort == 'sDESC') {
            return 'sDESC';

        } elseif ($sort == 'lASC') {
            return 'lASC';
        } elseif ($sort == 'lDESC') {
            return 'lDESC';
        } elseif ($sort == 'newASC') {
            return 'newASC';
        } else {
            return NULL;
        }
    }

    private function getSortParam($sortParam)
    {
        if ($sortParam == 'nasc') {
            return 'nASC';
        } elseif ($sortParam == 'ndesc') {
            return 'nDESC';
        }
        if ($sortParam == 'casc') {
            return 'cASC';
        } elseif ($sortParam == 'cdesc') {
            return 'cDESC';
        }

        if ($sortParam == 'pasc') {
            return 'pASC';
        } elseif ($sortParam == 'pdesc') {
            return 'pDESC';
        }

        if ($sortParam == 'dasc') {
            return 'dASC';
        } elseif ($sortParam == 'ddesc') {
            return 'dDESC';
        }

        if ($sortParam == 'sasc') {
            return 'sASC';
        } elseif ($sortParam == 'sdesc') {
            return 'sDESC';
        }
        if ($sortParam == 'pasc') {
            return 'pASC';
        } elseif ($sortParam == 'pdesc') {
            return 'pDESC';
        }
        if ($sortParam == 'newdesc') {
            return 'newDESC';
        } elseif ($this->_getParam('sort', NULL) == 'prior') {
            return NULL;
        }
    }

    private function setTemplate($template){
        $layoutManager = $this->getHelper('LayoutManager');
        $layoutManager->useLayoutName($template);
        $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('header', 'header', 'template'));
        $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('catalogcontent', 'catalogcontent', 'template'));
        $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('tovarcontent', 'tovarcontent', 'template'));
        $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('cartcontent', 'cartcontent', 'template'));
        $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('leftcollum', 'leftcollum', 'template'));
        $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('rightcolumn', 'rightcolumn', 'template'));
        $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('cart', 'preview', 'cart', 'catalog'));
        $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('footer', 'footer', 'template'));
    }
}