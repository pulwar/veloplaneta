<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

class Catalog_MenuController extends Zend_Controller_Action {

	
	private $_options = null;
	
	private $_menu = array();
	private $_session = array();
	
	
	
	/**
	 * Инициализирует класс
	 */
	public function init() {	
            $this->_session = new Zend_Session_Namespace('catalogmenu');
            if (is_array($this->_session->catalogmenu)){
                    $this->_menu = $this->_session->catalogmenu;
            }
	}

	/**
	 * отображает страницу корзины.
	 */
    public function indexAction() {
    	$id = $this->_getParam('id', 0);
    	if ($this->_getParam('clear')){
    		$this->_menu = array();
    		$this->save_menu();
    	}
    	
    	if ($id){
    		$page = Pages::getInstance()->getPage($id);
    		$page_options = PagesOptions::getInstance()->getPageOptions($id);
    		$this->view->page = $page;
    		$this->view->content = $page->content;
    		$this->view->options = $page_options;
    		$this->view->items = $this->getProducts();
    		$this->setTemplate($page->template);   	
    	}
    }
    
    /**
     * обзор товаров 
     *
     */
    public function previewAction(){    	   		
    	$this->view->items = $this->getProducts();
    	
    }
    /**
     * Добавить товар 
     *
     */
    public function addAction(){
    	$div_id = $this->_getParam('div_id', 0);
        echo in_array($div_id, $this->_menu);
    	if ($div_id!=0){
            $this->_menu[$div_id] = 1;
    		$this->save_menu();
    	}
    	exit;
    }
    
    public function changeAction(){
    	
    }


     public function showAction(){
                print_r($this->_menu);
                exit;


     	//exit;

    }
    /**
     * удалить товар
     *
     */
    public function deleteAction(){
    	$div_id = $this->_getParam('div_id', 0);
        echo $div_id;
    	if (array_key_exists($div_id, $this->_menu)){
    		unset($this->_menu[$div_id]);
    		$this->save_menu();
                echo 'del';
    		
    	}        
    	exit;

      
    }
	/**
	 * очистить 
	 *
	 */
    public function clearAction(){
    	$this->_menu = array();
    	$this->save_menu();
    	exit;
    }
    
	private function save_menu(){
		$this->_session->catalogmenu = $this->_menu;
	}
	
	private function getProducts(){
		if (count($this->_menu)){
			$this->_total_price = 0;
    		foreach ($this->_menu as $id=>$params){
    			$product = Catalog_Tovar::getInstance()->find($id)->current();
    			if (!is_null($product)){
    				$item['name'] = $product->name;
    				$item['id'] = $product->id;
    				$item['price'] = $product->price;
    				$item['count'] = (int)$params['count'];
    				//$item['articul'] = $product->articul;
    				$item['total_price'] = $product->price*$item['count'];
    				$this->_total_price += $item['total_price'];
    				$items[] = $item;
    			} else{
    				unset($this->_menu[$id]);
    			}
    		}
    		return  isset($items) ? $items : '';
    	}
	}
    
    /**
     * 
     */
    
    private function setTemplate($template){
		
		$layoutManager = $this->getHelper('LayoutManager');
		$layoutManager->useLayoutName($template);
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('cart', 'preview', 'cart', 'catalog'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('header', 'header', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('leftcollum', 'leftcollum', 'template'));
                $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('rightcolumn', 'rightcolumn', 'template'));
                $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('cartcontent', 'cartcontent', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('footer', 'footer', 'template'));
		
		
	}
    
}