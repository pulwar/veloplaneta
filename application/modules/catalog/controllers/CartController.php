<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

class Catalog_CartController extends Zend_Controller_Action {

	
	private $_options = null;
	
	private $_cart = null;
	private $_session = array();
	
	private $_table_head = '
		<tr>			
			<td>Наименование</td>
			<td>Кол-во </td>
			<td>Цена</td>
			<td>Стоимость</td>
		</tr>
	
	';
	private $_pen_template = '
		<tr>			
			<td>{name}</td>
			<td>{count} </td>
			<td>{price}$ </td>
			<td>{total_price}$ </td>
		</tr>
		
	';
	
	private $_total_price = 0;
	
	/**
	 * Инициализирует класс
	 */
	public function init() {	
		/*$this->view->lang = $this->_getParam('lang', 'ru');
		$this->view->options =$page_options = PagesOptions::getInstance()->getPageOptions($this->_getParam('id'));
		$page = Pages::getInstance()->getPage($this->_getParam('id'));
		$this->view->options = $this->_options = PagesOptions::getInstance()->getPageOptions($page->id);
		$this->view->page = $page;*/
		
		$this->_session = new Zend_Session_Namespace('cart');
		if (is_array($this->_session->cart)){
			$this->_cart = $this->_session->cart;
		}
	}

	/**
	 * отображает страницу корзины.
	 */
    public function indexAction() {
    	$id = $this->_getParam('id', 0);
    	if ($this->_getParam('clear')){
    		$this->_cart = array();
    		$this->save_cart();
    	}
    	
    	if ($id){
    		$page = Pages::getInstance()->getPage($id);
    		$page_options = PagesOptions::getInstance()->getPageOptions($id);
    		$this->view->page = $page;
    		$this->view->content = $page->content;
    		$this->view->options = $page_options;
    		$this->view->items = $this->getProducts();
    		$this->setTemplate($page->template);   	
    	}


        if (
            $this->_request->isPost() &&
            (
                trim($this->_getParam('name'))=='' ||
                trim($this->_getParam('phone'))=='' 
            )
        )
        {
            $this->view->message = "Заполните поля, отмеченные *";
        }

        elseif ($this->_request->isPost()){
                $fields = $this->_request->getParams();
    		$pens = $this->getProducts();
                //echo "<pre>";
                //print_r($fields);
                //exit;
    		if (count($pens)){
                                $i=0;
                                $total_price = 0;
                                foreach ($pens as &$pen){
                                    $pen['count'] = $fields['textfield'][$i++];
                                    $total_price += $pen['total_price'] = $pen['count']*$pen['price'];
                                }
				$html = '<style>td {padding:5px;}</style><table border="1" cellpadding="0" cellspacing="0">'.$this->_table_head;
				foreach ($pens as $item){                                        
					$pen_tpl = $this->_pen_template;
					foreach ($item as $k=>$v){
						$pen_tpl = str_replace('{'.$k.'}',$v, $pen_tpl );
					}
					$html.=$pen_tpl;
				}
				$html.='<tr><td colspan="4" align="right">Итого: '.($total_price+8).'$</td></tr>';
				$html.='</table><br>';
				$template = file_get_contents(DIR_PUBLIC.'form_template.html');
				
				
				foreach ((array)$fields as $key=>$value){
					$template = @str_replace('{'.$key.'}', $value, $template );
				}
                                
				$html.=$template;	
				//echo  $html;		
				//$email = Webforms::getInstance()->fetchAll()->current();
                                $email = Webforms::getInstance()->fetchAll();
                                
                                $email = $email->toArray();
                                
				$emails=$email[0]['title'];
				$from=$email[0]['description'];
				$recipient = 'New message';			
				$subject = 'Заказ';
                                
				Loader::loadCommon('Mail');
				Mail::send($emails, $html, $from, $subject, $recipient);
				$this->_cart = array();
				$this->save_cart();
				//$this->view->message = $message;				
				$this->setTemplate('default');				
				$this->view->content = 'Спасибо! Ваше письмо было отправлено администратору.';
					//echo $html;
			}
    	}
 
    }
    
    /**
     * обзор товаров 
     *
     */
    public function previewAction(){    	   		
    	$this->view->items = $this->getProducts();
    	
    }
    /**
     * Добавить товар 
     *
     */
    public function addAction(){
    	$product_id = $this->_getParam('prod_id', 0);
    	$count = $this->_getParam('count',0);
    	if ($product_id && $count && !array_key_exists($product_id, $this->_cart)){
    		$this->_cart[$product_id]['count'] = $count;
    		$this->save_cart();
    		echo 'ok';    		
    	}
    	exit;
    }
    
    public function changeAction(){
    	$product_id = $this->_getParam('prod_id', 0);
    	$count = $this->_getParam('count',0);
    	if ($product_id && $count && array_key_exists($product_id, $this->_cart)){
    		$this->_cart[$product_id]['count'] = $count;
    		$this->save_cart();
    		echo 'ok';    		
    	}
    	exit;	
    }
    
    /**
     * удалить товар
     *
     */
    public function deleteAction(){
    	$product_id = $this->_getParam('prod_id', 0);   
        
    	if ($product_id && array_key_exists($product_id, $this->_cart)){
    		unset($this->_cart[$product_id]);
    		$this->save_cart();
    		echo 'ok';
    		
    	}
    	exit;
    }
	/**
	 * очистить 
	 *
	 */
    public function clearAction(){
    	$this->_cart = array();
    	$this->save_cart();
    	echo 'ok';
    	exit;
    }
    
	private function save_cart(){
		$this->_session->cart = $this->_cart;
	}
	
	private function getProducts(){
		if (count($this->_cart)){
			$this->_total_price = 0;
    		foreach ($this->_cart as $id=>$params){
    			$product = Catalog_Tovar::getInstance()->find($id)->current();
    			if (!is_null($product)){
    				$item['name'] = $product->name;
    				$item['id'] = $product->id;
    				$item['price'] = $product->price;
    				$item['count'] = (int)$params['count'];
    				//$item['articul'] = $product->articul;
    				$item['total_price'] = $product->price*$item['count'];
    				$this->_total_price += $item['total_price'];
    				$items[] = $item;
    			} else{
    				unset($this->_cart[$id]);
    			}
    		}
    		return  isset($items) ? $items : '';
    	}
	}
    
    /**
     * 
     */
    
    private function setTemplate($template){
		
		$layoutManager = $this->getHelper('LayoutManager');
		$layoutManager->useLayoutName($template);
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('cart', 'preview', 'cart', 'catalog'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('header', 'header', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('leftcollum', 'leftcollum', 'template'));
                $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('rightcolumn', 'rightcolumn', 'template'));
                $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('cartcontent', 'cartcontent', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('footer', 'footer', 'template'));
		
		
	}
    
}