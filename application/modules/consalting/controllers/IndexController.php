<?php

class Consalting_IndexController extends Zend_Controller_Action
{
	
	
	public function init(){
		Loader::loadPublicModel('Pages');
		Loader::loadPublicModel('PagesOptions');
	}
	//нуждается в рефакторинге!
	public function indexAction(){
		Loader::loadPublicModel('Consalting');
		
		$list = $this->view->list = $this->_getParam('list', 1);
		$onlist= $this->view->onlist = 20;
		
		$this->view->consalting = Consalting::getApproved(($list-1)*$onlist, $onlist);
		$count = $this->view->Count =  sizeof(Consalting::getApproved(null, null));
		$listCount = (int)$this->view->listCount = ceil($count / $onlist);
		
		$this->request = $this->getRequest();
		$this->view->currentlist = '/' . $this->request->getControllerName() . '/' . $this->request->getActionName();
		
		$this->view->options = PagesOptions::getInstance()->getPageOptions($this->_getParam('id'));
		$id = $this->_request->getParam('id');
			$page = Pages::getInstance()->getPage($id);
			
			if($page->published == '0'){
				$this->_redirect('/');
			}
			$this->setTemplate($page->template);
			$this->view->page = $page;
			$this->view->content = $page->content;
					
	
	
	}	
	


		private function setTemplate($template){
		
		$layoutManager = $this->getHelper('LayoutManager');
		$layoutManager->useLayoutName($template);
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('headerDefault', 'headerDefault', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('leftcollum', 'leftcollum', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('subhead', 'subhead', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('footerDefault', 'footerDefault', 'template'));
		
	}
				
}