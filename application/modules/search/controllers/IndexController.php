<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

class Search_IndexController extends Zend_Controller_Action {
	/**
	 * @var Zend_View
	 */
	private  $_view = null;
	
	/**
	 * Инициализирует класс
	 */
	public function init() {	
		$this->initView();
		$this->view->lang = $this->_getParam('lang', 'ru');
		$this->view->options = PagesOptions::getInstance()->getPageOptions($this->_getParam('id'));
		$page = Pages::getInstance()->getPage($this->_getParam('id'));		 
		$this->setTemplate($page->template);
		$this->view->page = $page;
		$this->view->content = $page->content;
		$lang = $this->getRequest()->getParam('lang','ru');
		$this->view->lang = $lang;
		if($this->_hasParam('selection')){
			$this->_forward('selection');
		}
	}

	/**
	 * 
	 */
    public function indexAction() {
		
    	$this->view->search = $search =trim($this->_getParam('keywords',null));
		//echo urldecode($this->_getParam('search',null));		
		//echo $this->_getParam('search',null); exit;
		//$search_session->param = $search;
			$onpage = 12;			
			$this->view->onpage = $onpage;
			$this->view->current_page = $ipage = $this->_getParam('page', 1);
			$offset = ($ipage-1)*$onpage;
		if (isset($search) && $search !=null && strlen(trim($search))>1){
			$search = trim($search);
			$search = str_replace('{','/', $search);
			$search_result_array = array();			
			$res = Catalog_Tovar::getInstance()->search($search, $count=$onpage, $offset);
 
			$this->view->total = count(Catalog_Tovar::getInstance()->search($search, $count=0, $offset =0));
		 
			$ids = '';
			$products = array();	
				
			foreach ((array)$res as $item){
				$ids[]=$item['id'];
			}
			if ($ids){
				$products = Catalog_Tovar::getInstance()->find($ids);
			}
                        //echo "<pre>";
                        //    print_r($products);
                        //echo "</pre>";
                        //exit;

			$this->view->result  = $products;
                        $this->view->request = $search;
			$this->view->search = str_replace('/','{', $search);
		}
			
			/*if (count($res)){
				
				$res = array_slice($res, $offset, $onpage);
			}*/
			
	    	//$search_session->items = $search_result_array;  
			$this->view->lang = $this->_getParam('lang','ru');	
		
		
		
 
    }
  
	public function selectionAction(){
		$onpage = 12;			
			$this->view->onpage = $onpage;
			$this->view->current_page = $ipage = $this->_getParam('page', 1);
			$offset = ($ipage-1)*$onpage;
			$brands = $this->_getParam('bikeBrand');
			$humans = $this->_getParam('humanType');
			$types = $this->_getParam('bikeType');
			$priceFrom = $this->_getParam('priceFrom');
			$priceTo = $this->_getParam('priceTo');
			$this->view->result = Catalog_Tovar::getInstance()->getSelectedProducts($brands, $humans, $types, $priceFrom, $priceTo);
			//$this->view->total = count(Catalog_Tovar::getInstance()->getSelectedProducts($brands, $humans, $types));
			//$this->view->lang = $this->_getParam('lang','ru');
			
	}
	
	public function searchmoreAction(){
		$id = $this->_getParam('id',0);		
		$page = Pages::getInstance()->getPage($id);
		$this->view->options = PagesOptions::getInstance()->getPageOptions($id);	
		$this->view->page = $page;	
		$this->view->materials = Catalog_Tovar::getInstance()->getMaterials();
		$this->view->brands = Catalog_Division::getInstance()->fetchAll('level=0 AND parent_id=0', 'sortid');
		$session = new Zend_Session_Namespace('search');		
		if ($this->_getParam('search_more')){
			$search_params['brand'] = $this->_getParam('brand');	
			$search_params['material'] = $this->_getParam('material');	
			$search_params['price_from'] = $this->_getParam('price_from');	
			$search_params['price_to'] = $this->_getParam('price_to');	
			$search_params['body_color'] = $this->_getParam('body_color');	
			$search_params['clip_color'] = $this->_getParam('clip_color');
			$search_params['elem_color'] = $this->_getParam('elem_color');
			if (!$this->array_empty($search_params)){
				$session->params = $search_params;
			} else{
				$search_params = $session->params;
			}
			$this->view->search_params = $search_params;
			$params = $search_params;
			$onpage = 12;			
			$this->view->onpage = $onpage;
			$this->view->current_page = $ipage = $this->_getParam('page', 1);
			$offset = ($ipage-1)*$onpage;			
			$this->view->result = Catalog_Tovar::getInstance()->search_more($params, $onpage, $offset);
			$this->view->total = count(Catalog_Tovar::getInstance()->search_more($params));
			
			//hide form
			$this->view->hide_advanced_search = 1;
		} else {
			unset($session->params);
			unset($session->query);
		}
		
		
		//$this->setTemplate($page->template);
	}
	
	//--------------------сортировка
	
	
	
	
	
	
	
	
	//----------------------конец сортировке
	private function array_empty($array){
		 foreach ((array)$array as $item){
		 	if ($item){
		 		return false;
		 	}
		 }
		 return true;
	}
    
    /**
     * load templates
     */
    
    private function setTemplate($template){
		
		$layoutManager = $this->getHelper('LayoutManager');
		$layoutManager->useLayoutName($template);
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('header', 'header', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('leftcollum', 'leftcollum', 'template'));
                $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('rightcolumn', 'rightcolumn', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('cart', 'preview', 'cart', 'catalog'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('footer', 'footer', 'template'));
		
		
	}
    
}