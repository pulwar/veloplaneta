<?php

class IndexController extends Zend_Controller_Action
{
    public function indexAction()
    {
        
	  $page = Pages::getInstance()->getPageByParam('type','main');
	  $this->view->page = $page;
	  $this->_setParam('id',$page->id);
	  $this->setTemplate($page->template);
    }

	private function setTemplate($template){
		$layoutManager = $this->getHelper('LayoutManager');
		$layoutManager->useLayoutName($template);
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('header', 'header', 'template'));
                $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('maincontent', 'maincontent', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('leftcollum', 'leftcollum', 'template'));
                $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('rightcolumn', 'rightcolumn', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('footer', 'footer', 'template'));
	}
}