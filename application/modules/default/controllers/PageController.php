<?php
class PageController extends Zend_Controller_Action
{
    public function init(){
    	//подгружаем модель - класс для работы с таблицей БД
    	
    }
    
	public function indexAction(){		
		if($this->_hasParam("id")){
			$this->_forward('page');
		}
		
	}
	
	public function __call($method, $args){
    	$this->_redirect('/');
	}
	
	/**Открывает страницу с переданным ID	 *
	 */
	public function pageAction(){		
		Loader::loadPublicModel('Pages');
		Loader::loadPublicModel('PagesOptions');
		$page = Pages::getInstance()->getPage($this->_getParam('id'));
		$this->view->lang = $page->version;

		if($page->published == '0'){
			$this->_redirect('/');
		}
		$this->setTemplate($page->template, $page);
		$id = $this->_getParam('id');
		$this->view->page = $page;
		
		$this->view->options = PagesOptions::getInstance()->getPageOptions($id);
		$this->view->content = $page->content;
		//$this->view->addHelperPath(DIR_HELPER, 'View_Helper');
		//print_r($this->view->getHelperPaths()); exit;
		//print_r($this->view->getHelperPath('Menu')); exit();
		
	}

        public function custompageAction(){
		Loader::loadPublicModel('Pages');
		$page = Pages::getInstance()->getPage($this->_getParam('pageid'));
		$this->view->lang = $page->version;
		$this->view->data = $page->content;

	}
	
	
	private function setTemplate($template, $page){
            $layoutManager = $this->getHelper('LayoutManager');
            $layoutManager->useLayoutName($template);
            $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('header', 'header', 'template'));
            $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('mainheader', 'mainheader', 'template'));
            $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('cart', 'preview', 'cart', 'catalog'));
            $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('footer', 'footer', 'template'));
            $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('leftcollum', 'leftcollum', 'template'));
            $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('rightcolumn', 'rightcolumn', 'template'));
            $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('maincontent', 'maincontent', 'template'));
            $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('actionscontent', 'actionscontent', 'template'));
            $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('defaultpage', 'defaultpage', 'template'));
	}
}