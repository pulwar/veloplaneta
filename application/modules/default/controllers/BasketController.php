<?php
class BasketController extends Zend_Controller_Action
{	
	/**
	 * Zend_Session_Namespace
	 *
	 * @var Zend_Session_Namespace
	 */
	private  $_basketSession =null;
	/**
	 * Zend_View instanse
	 *
	 * @var Zend_View
	 */
	private $_view = null;
	 /** Controller name
	 *
	 * @var string
	 */
	private  $_current_module =null;
	
	
	public function init(){
		$this->view = Registry::getView();
		$this->_basketSession = new  Zend_Session_Namespace('basket');
		$this->request = $this->getRequest();
		$this->view->currentModul = $this->_current_module = $this->request->getControllerName();
		
		
	}
	
    public function indexAction()
    {
	 	if ($this->_basketSession->items !=NULL) {
	 		$items = $this->_basketSession->items;
	 		$this->view->items_session = $items;
	 		$this->view->tovars = Catalog_Tovar::getInstance()->find(array_keys($items));
	 	}
	 	$this->getResponse()->appendBody(
            $this->view->render("Basket.phtml")
            
        );
	 	
	 		   
    }
    
    public function addAction(){
    	
    	if ($this->_hasParam('id_tovar')){
    		$id_tovar = $this->_getParam('id_tovar');
    		$items =$this->_basketSession->items;
    		if (!isset($items[$id_tovar])) {
    			$items[$id_tovar] =array('id_tovar'=>$id_tovar , 'count'=>1);
    			$this->_basketSession->items = $items;
    		} else {
    			$items[$id_tovar]['count']= $items[$id_tovar]['count']+1;
    			$this->_basketSession->items = $items;
    		}
        }	
        $this->_redirect('/'.$this->_current_module);
    }
    
    public function deleteAction(){
    	if ($this->_hasParam('id_tovar')){
    		$id_tovar = $this->_getParam('id_tovar');
    		$items =$this->_basketSession->items;
    		if (isset($items[$id_tovar])) {
    			unset($items[$id_tovar]);
    			$this->_basketSession->items = $items;
    		}
        }	
        $this->_redirect('/'.$this->_current_module);
    }
    
 	public function deletefrontAction(){
    	if ($this->_hasParam('id_tovar')){
    		$id_tovar = $this->_getParam('id_tovar');
    		$items =$this->_basketSession->items;
    		if (isset($items[$id_tovar])) {
    			unset($items[$id_tovar]);
    			$this->_basketSession->items = $items;
    		}
        }
        if(count($items)>0){
        $this->view->items_session = $items;	
        $this->view->tovars = Catalog_Tovar::getInstance()->find(array_keys($items));
        }	
       $this->getResponse()->appendBody(
            $this->view->render("BasketFront.phtml")
            
        );
    }
    
    public function clearAction(){
    	unset($this->_basketSession->items);
    	$this->_redirect('/'.$this->_current_module);
    }
    
    public function countAction(){
    	if ($this->_hasParam('id_tovar') && $this->_hasParam('count')){
    		$count = $this->_getParam('count');
    		$id_tovar = $this->_getParam('id_tovar');
    		$items =$this->_basketSession->items;
    		if (isset($items[$id_tovar])) {
    			$items[$id_tovar]['count']= $count;
    			$this->_basketSession->items = $items;
    		} 
    	}
    }

    
}