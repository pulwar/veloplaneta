<?php
class CompareController extends Zend_Controller_Action
{
	/**
	 * Zend_Session_Namespace
	 *
	 * @var Zend_Session_Namespace
	 */
	private  $_compareSession =null;
	/**
	 * Zend_View instanse
	 *
	 * @var Zend_View
	 */
	private $_view = null;
	 /** Controller name
	 *
	 * @var string
	 */
	private  $_current_module =null;
	
	public function init(){
		$this->view = Registry::getView();
		$this->_compareSession = new  Zend_Session_Namespace('compare');
		$this->request = $this->getRequest();
		$this->view->currentModul = $this->_current_module = $this->request->getControllerName();
	}
	
    public function indexAction()
    {
	 	if ($this->_compareSession->items != NULL) {
	 		$items = $this->_compareSession->items;	
	 		$limit=false;
    		if (count($items)>=4) {
    			$limit=true;
    		}
    		$this->view->limit = $limit;
	 		$this->view->tovars = Catalog_Tovar::getInstance()->find(array_keys($items));
	 	}
	 	$this->getResponse()->appendBody(
            $this->view->render("Compare.phtml")
            
        ); 
    }
    public function addAction(){
    	
    	if ($this->_hasParam('id_tovar')){
    		$id_tovar = $this->_getParam('id_tovar');
    		$items =$this->_compareSession->items;
    		$limit=false;
    		if (count($items)>=4) {
    			$limit=true;
    		}    		
    		if (!isset($items[$id_tovar]) && !$limit) {
    			$items[$id_tovar] =$id_tovar;
    			$this->_compareSession->items = $items;
    		} else{    			   			
    			//$this->_compareSession->items = $items;
    		}
    		 
        }	
        $this->_redirect('/'.$this->_current_module);
    }
    
    public function deleteAction(){
    	if ($this->_hasParam('id_tovar')){
    		$id_tovar = $this->_getParam('id_tovar');
    		$items =$this->_compareSession->items;
    		if (isset($items[$id_tovar])) {
    			unset($items[$id_tovar]);
    			$this->_compareSession->items = $items;
    		}
        }	
        $this->_redirect('/'.$this->_current_module);
    }
    
    public function clearAction(){
    	unset($this->_compareSession->items);
    	$this->_redirect('/'.$this->_current_module);
    }
    

}