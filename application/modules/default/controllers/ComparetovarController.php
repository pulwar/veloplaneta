<?php

class ComparetovarController extends Zend_Controller_Action
{
    public function indexAction()
    {
	 	//   Loader::loadPublicModel('Catalog_Tovar');
        
		
        if ($this->_hasParam('delete'))
        {
            $iDeleteFromCompare = (int) $this->_getParam('delete');
            $iIndex = array_search($iDeleteFromCompare, $_SESSION['iCompareId']);
            unset($_SESSION['iCompareId'][$iIndex]);
        }     
        $aData = array();
         $teh_fields=array();
       if (key_exists('iCompareId', $_SESSION))
        {
            foreach ($_SESSION['iCompareId'] as $iItem)
            {
                $aData[$iItem]['tovar'] = Catalog_Tovar::getInstance()->getTovarById($iItem);
               // $main_div = Catalog_Division::getInstance()->getMainDiv($aData[$iItem]['tovar']->division_id);
               // $main_div = Catalog_Division::getInstance()->getMainDiv($tovar->division_id);
             // $teh_fields[] = Catalog_Division_tehnic_filds::getInstance()->getFieldsByDivId($main_div);
             // if ( isset($teh_fields) && $teh_fields!=null){
            // $this->view->teh_fields = Catalog_tehnic_filds::getInstance()->getFieldsArrayId($teh_fields);
              //$teh_fields_test[] = Catalog_tehnic_filds::getInstance()->getFieldsArrayId($teh_fields);
             // }
               //$this->view->teh_fields = Catalog_tehnic_filds::getInstance()->getAllByDivId($main_div);
              //  $aData[$iItem]['teh_fields_values'] = Catalog_tehnic_filds_values::getInstance()->getAllByTovarId($iItem);
            }
        }
        $this->view->aCompareTovars = $aData;
    }
    
    public function addtovarAction(){
    	if ($this->_hasParam('compareid') && !$this->in_array($this->_getParam('compareid'))){
    		
			$iCompareId = (int)$this->getRequest()->getParam('compareid');
			$_SESSION['iCompareId'][$iCompareId] = $iCompareId;
			
        }	
        $this->_redirect('/Comparetovar/index/');
    }
    
    public function deleteAction(){
    	if ($this->_hasParam('compareid')){
    		
			$iDeleteFromCompare = (int) $this->_getParam('compareid');
            $iIndex = array_search($iDeleteFromCompare, $_SESSION['iCompareId']);
            unset($_SESSION['iCompareId'][$iIndex]);
			
        }	
        $this->_redirect('/Comparetovar/index/');
    }
    
    private function in_array($array,$compare_id){
    	$flag = false;
    	foreach ($array as $data){
    		if ($data ==$compare_id){
    			$flag=true;
    		}
    	}
    	return $flag;
    }
    

	private function setTemplate($template, $page){
		$layoutManager = $this->getHelper('LayoutManager');
		$layoutManager->useLayoutName($template);
		Loader::loadPublicModel('PagesOptions');
		$options = PagesOptions::getInstance()->getPageOptions($page->id);
		$header = array(
			'h1' => $options->h1,
			'keywords' => $options->keywords,
			'tags' => $options->tags,
			'title' => $options->title
		);
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('header', 'header', 'template'));
		
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('footer', 'footer', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('uslugi', 'uslugi', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('uslugi_main', 'uslugimain', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('form', 'form', 'template'));
	}



}