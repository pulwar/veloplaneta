<?php

class UsersController extends Zend_Controller_Action 
{
	private $_count = 0;
	
	public function init(){
		Loader::loadPublicModel('Users');
	}
	
	public function preDispatch(){
		if(!Security::getInstance()->checkAdminAllow()){
			$this->_redirect('/');
		}
	}
	
	public function indexAction(){
		$this->view->users = Users::getInstance()->getJsonUsers();
		$this->view->list = Users::getInstance()->getList();
		$this->view->count = Users::getInstance()->getUsersCount();
	}
	
	public function deleteAction(){
		if($this->_request->isPost() && $this->_hasParam('id')){
			Users::getInstance()->deleteUser($this->_getParam('id'));
		}
	}
	public function addAction(){
		if($this->_request->isPost() && $this->_hasParam('id')){
			Users::getInstance()->addUser(array('id'=>$this->_getParam('id')));
		}
		
	}
	
	public function editAction(){
		if($this->_request->isPost() && $this->_hasParam('cell')){
			Users::getInstance()->updateUser($this->_request->getParams());
		}
	}
}
