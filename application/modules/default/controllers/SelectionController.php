<?php

class SelectionController extends Zend_Controller_Action
{
		
	public function init()
	{
		//Loader::loadPublicModel('Catalog_Tovar');
	}

public function selectAction(){

    	$page = Pages::getInstance()->getPageByParam('path','selection');

    	$this->view->options = PagesOptions::getInstance()->getPageOptions($page->id);
		$this->setTemplate($page->template);
        
		$this->_setParam('id',$page->id);
		$this->view->page = $page;
		$this->view->search = $search = trim($this->_getParam('search',null));
		if (isset($search) && $search !=null && strlen(trim($search))>4){
			$search = trim($search);
			$search_result_array = array();
			unset($search_session->items);
			$search_result_array['Works'] = Portfolio_Works::getInstance()->search($search,'nASC',null,null);
			$search_result_array['Division'] = Portfolio_Divisions::getInstance()->search($search);
			$search_result_array['Pages'] = Pages::getInstance()->search($search);
			$search_result_array['News'] = News::getInstance()->search($search);
	    	$search_session->items = $search_result_array;  
			$this->view->lang = $this->_getParam('lang','ru');	
		}
		else {
			if(isset($search_session->items)){
				$this->view->search_array = $search_session->items;
				$this->view->search = $search_session->param;
			}
		}
		
	}

	
	
	
private function setTemplate($template){
		
		$layoutManager = $this->getHelper('LayoutManager');
		$layoutManager->useLayoutName($template);
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('headerDefault', 'headerDefault', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('leftcollum', 'leftcollum', 'template'));
        $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('rightcolumn', 'rightcolumn', 'template'));
        $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('selectcontent', 'selectcontent', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('subhead', 'subhead', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('footerDefault', 'footerDefault', 'template'));
		
		
	}
}
