<?php

class CatalogtovarController extends Zend_Controller_Action 
{
		
	public function init()
	{
		//Loader::loadPublicModel('Catalog_Tovar');
	}
	
public function searchAction(){
		require_once 'Zend/Session.php';
    	$search_session = new Zend_Session_Namespace('search');
    	$page = Pages::getInstance()->getPageByParam('path','rezultaty_poiska');
    	$this->view->options = PagesOptions::getInstance()->getPageOptions($page->id);
		$this->setTemplate($page->template);
		$this->_setParam('id',$page->id);
		$this->view->page = $page;
		$this->view->search = $search = trim($this->_getParam('search',null));
		$search_session->param = $search;
		if (isset($search) && $search !=null && strlen(trim($search))>4){
			$search = trim($search);
			$search_result_array = array();
			unset($search_session->items);
			$search_result_array['Tovar'] = Catalog_Tovar::getInstance()->search($search,'nASC',null,null);
			$search_result_array['Division'] = Catalog_Division::getInstance()->search($search);
			$search_result_array['Pages'] = Pages::getInstance()->search($search);
			$search_result_array['News'] = News::getInstance()->search($search);
	    	$search_session->items = $search_result_array;  
			$this->view->lang = $this->_getParam('lang','ru');	
		}
		else {
			if(isset($search_session->items)){
				$this->view->search_array = $search_session->items;
				$this->view->search = $search_session->param;
			}
		}
		
	}

	public function tovarAction(){
		$this->view->tovar_id = $tovar_id =  $this->getRequest()->getParam('tovarid');
		$this->view->tovar = $tovar = Catalog_Tovar::getInstance()->find($tovar_id)->current();
		$division = Catalog_Division::getInstance()->find($tovar->division_id)->current();
		$this->view->division=$division;
		$page = Pages::getInstance()->find($division->id_page)->current();

		if($page->published == '0'){
			$this->_redirect('/');
		}
		$this->setTemplate($page->template, $page);
		$this->_setParam('id',$page->id);
		$this->view->page = $page;
		$this->view->options = PagesOptions::getInstance()->getPageOptions($page->id);
		$this->view->lang = $this->_getParam('lang','ru');
		$this->request = $this->getRequest();			
		
		
		/*if (isset($tovar_id)){
			$sitemap = array();
			$sitemap[]=$division;
			$cur_div = $division;
			if ($division->parent_id !=0){
				while ($cur_div->parent_id !=0){
					$cur_div = Catalog_Division::getInstance()->getItemById($cur_div->parent_id);
					$sitemap[]=$cur_div;
				}
			}*/
		//$this->view->catalog_map = array_reverse($sitemap);
		//$this->view->makers  = Catalog_makers::getInstance()->getAll();
		//$main_div = Catalog_Division::getInstance()->getMainDiv($tovar->division_id);
		/*$fields = Catalog_Division_tehnic_filds::getInstance()->getFieldsByDivId($main_div);
		if ( isset($fields) && $fields!=null){
		$this->view->teh_fields = Catalog_tehnic_filds::getInstance()->getFieldsArrayId($fields);
		}
		//$this->view->teh_fields = Catalog_tehnic_filds::getInstance()->getAllByDivId($main_div);
		$this->view->teh_fields_values = Catalog_tehnic_filds_values::getInstance()->getAllByTovarId($tovar->id);*/
		
		//}
		
		
	}
	
	
private function setTemplate($template){
		
		$layoutManager = $this->getHelper('LayoutManager');
		$layoutManager->useLayoutName($template);
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('headerDefault', 'headerDefault', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('leftcollum', 'leftcollum', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('subhead', 'subhead', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('footerDefault', 'footerDefault', 'template'));
		
		
	}
}
