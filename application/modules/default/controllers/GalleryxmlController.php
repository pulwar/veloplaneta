<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

class GalleryXMLController extends Zend_Controller_Action {

    public function init()
    {
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
      
        Loader::loadPublicModel('Gallery');
        Loader::loadPublicModel('Image');
    }
    
    public function indexAction()
    {
    	$aGallery = array();
    	$gallery_id = (int)$this->_request->getParam('gid', 0);
    	//$oGallery = Gallery::getInstance()->selectMember($aGallery);
    	$sXml = "";
       
       // foreach ($oGallery as $iK => $aI)
       // {
        	$oImages = Image::getInstance()->getAll($gallery_id);
	        foreach ($oImages as $iKey => $aItem)
	        {
	        	$sType = substr($aItem->name, strpos($aItem->name, ".", strpos($aItem->name, ".")+1)+1);
	            $sXml .= "<img>
	    <id>$aItem->id</id>
	    <title>".substr($aItem->name, 0, strpos($aItem->name, '.')).
	            "</title>
	    <preview>".'/pics/galerypics/'.$aItem->small.
	            "</preview>
	    <image>".'/pics/galerypics/'.$aItem->big.
	            "</image>
	</img>";
	        }
       // }    
        //Zend_Debug::dump($sXml);
        $this->view->xml = $sXml;
    }
}