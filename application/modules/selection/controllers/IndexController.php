<?php

class Selection_IndexController extends Zend_Controller_Action
{
		
	public function init()
	{
		//Loader::loadPublicModel('Catalog_Tovar');
	}

public function selectAction(){

    	$page = Pages::getInstance()->getPageByParam('path','selection');
        $this->setTemplate($page->template);
        if ($this->_request->isPost()) {
            $post = $params = $this->_request->getParams();            
            $result = Catalog_Tovar::getInstance()->search_more($post);
            $this->view->result = $result;
      
         }
	}




private function setTemplate($template){

		$layoutManager = $this->getHelper('LayoutManager');
		$layoutManager->useLayoutName($template);
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('header', 'header', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('leftcollum', 'leftcollum', 'template'));
        $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('rightcolumn', 'rightcolumn', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('footer', 'footer', 'template'));


	}
}
