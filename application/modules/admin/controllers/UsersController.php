<?php

class Admin_UsersController extends MainAdminController
{
	public function init(){
		Loader::loadPublicModel('Users');
	}
	
	public function indexAction(){
		$this->_forward('usersListAll');
	}
	
	public function usersListAllAction(){
		$this->view->users = Users::getInstance()->getUsers();
	}
	
	public function activateAction(){
		if($this->_hasParam('id'))
			Users::getInstance()->setActivity($this->_request->getParam('id'), 1);
		
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		$this->_redirect("/admin/$lang/users/users-list-all/");		
	}
	
	public function unactivateAction(){
		if($this->_hasParam('id'))
			Users::getInstance()->setActivity($this->_request->getParam('id'), 0);
			
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		$this->_redirect("/admin/$lang/users/users-list-all/");		
	}
	
	public function deleteAction(){
		if($this->_hasParam('id'))
			Users::getInstance()->deleteUser($this->_request->getParam('id'));
			
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		$this->_redirect("/admin/$lang/users/users-list-all/");		
	}
	
	public function editAction(){
		if($this->_hasParam('id')){
			if ($this->_request->isPost()){
				Users::getInstance()->editUser($this->getParam('id'), $this->_request->getParams());
				$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
				$this->_redirect("/admin/$lang/users/users-list-all/");	
			}
			
			$this->view->user = Users::getInstance()->getUser($this->getParam('id'));
		}
		else{
			$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
			$this->_redirect("/admin/$lang/users/users-list-all/");		
		}
			
	}
	
	public function addAction(){
		if ($this->_request->isPost()){
			Users::getInstance()->addUser($this->_request->getParams());
			$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
			$this->_redirect("/admin/$lang/users/users-list-all/");		
		}
	}
	
}
	