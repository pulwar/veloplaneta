<?php
define('COUNT', 50);

class Admin_ConfigController extends MainAdminController
{

	public function indexAction(){
//		Loader::loadPublicModel('Configs');
//		$this->_forward('profile');
	}
	
	public function globalAction()
	{
		Loader::loadPublicModel('Configs');
		$this->save($this);
		$globals = Configs::getInstance()->getGlobals();
		$this->view->globals = $globals;
	}
	
	public function mailAction(){
		Loader::loadPublicModel('Configs');
		$this->save($this);
		
		$mail = Configs::getInstance()->getMail();
		$this->view->mail = $mail;
	}
	
	public function defaultsAction(){
		
	}
	
	private function save($_this)
	{
		Loader::loadPublicModel('Configs');
		
		if($_this->getRequest()->isPost() && $_this->_hasParam('value')){
			$values = $_this->getRequest()->getParam('value');
			Configs::getInstance()->save($values);
		}
	}
	
}
