<?php
/**
 * Admin_Catalog_TovarController
 *
 */
class Admin_Catalog_TovarController extends MainAdminController
{
	/**
	 * Controller name
	 *
	 * @var string
	 */
	private  $_current_module =null;
	/**
	 * Catalog page id
	 *
	 * @var int
	 */
	private  $_id_page = null;
	/**
	 * @var string
	 */
	private $_img_preview_x = NULL;
	/**
	 * @var string
	 */
	private $_img_preview_y = NULL;
	/**
	 *@var string
	 */
	private $_dir_files =null;




	private $_file_path = NULL;

	public function init()
	{	$this->initView();
		$this->request = $this->getRequest();
		$this->view->lang = $lang = $this->_getParam('lang','ru');
		$this->view->id_page = $this->_id_page = $this->_getParam('id_page',0);
		$this->view->currentModul = $this->_current_module = $lang.'/'. $this->request->getControllerName();
		$catalog_tovar = Catalog_Tovar::getInstance();
		$this->_img_preview_x = $catalog_tovar->getImgPreviewX();
		$this->_img_preview_y = $catalog_tovar->getImgPreviewY();
		$this->_img_big_x = $catalog_tovar->getImgBigX();
		$this->_img_big_y = $catalog_tovar->getImgBigY();

		$this->_file_path = $catalog_tovar->getFilePath();
		$this->view->dir_files = $this->_dir_files = $catalog_tovar->getDirFiles();
		$this->view->img_path = $this->_file_path;
		$this->view->view_img_path = '/pics/catalog/tovar/';

	}


	public function indexAction()
	{

		$main_divs = Catalog_Division::getInstance()->getChilds(0);
		$this->view->main_divs = $main_divs;



	}


	public function addAction()
	{
		$this->view->parent_id = $parent_id =  $this->getRequest()->getParam('parent_id', 0);
		//$makers = array();
		$this->view->division = $division = Catalog_Division::getInstance()->getItemById($parent_id);
		$this->view->page = Pages::getInstance()->find($division->id_page)->current();
		//$this->view->makers  = Catalog_Makers::getInstance()->getAll();

		$main_div = Catalog_Division::getInstance()->getMainDiv($parent_id);


		$fck1 = $this->getFck('add[intro]', '90%', '150','Basic');
		$this->view->fck_intro = $fck1;
		$fck2 = $this->getFck('add[description]', '90%', '300');
		$this->view->fck_content = $fck2;

		if ($this->_request->isPost()){

			//$data = $this->filter($this->getRequest()->getParam('add'));
            $data = $this->getRequest()->getParam('add');

			$data['division_id'] = $parent_id;
			$data['maker_id'] = $main_div;

			if (!empty($data['name'])){
				$data['prior'] = Catalog_Tovar::getInstance()->getMaxPriorInDivision($parent_id);
				$tovar = Catalog_Tovar::getInstance()->createRow($data);
				$tovar_id = $tovar->save();


			$name=$_FILES['img_small']['name'];
			$tempname=$_FILES['img_small']['tmp_name'];

                        if ($name=='' && $_FILES['img_big']['name'] != ''){
                            $name = $_FILES['img_big']['name'];
                            $tempname=$_FILES['img_big']['tmp_name'];
                        }

			if ($name!='' && ($tempname!='')){
				$ext = @end(explode('.', $name));
				//preview
				$this->img_resize(
					$tempname,
					$this->_file_path.$tovar_id.'_img_small.'.$ext ,
					$this->_img_preview_x,
					$this->_img_preview_y,
					$rgb=0xFFFFFF,
					$quality=100
				);
				$tovar->img_small = $tovar_id.'_img_small.'.$ext;
				$tovar->save();
			}

			$name=$_FILES['img_big']['name'];
			$tempname=$_FILES['img_big']['tmp_name'];
			if ($name!='' && ($tempname!='')){
				$ext = @end(explode('.', $name));
                                $size = getimagesize($tempname);
                                if ($size[0]>$this->_img_big_x)
                                {
                                    $this->img_resize(
					$tempname,
					$this->_file_path.$tovar_id.'_img_big.'.$ext ,
                                        $this->_img_big_x,
					$this->_img_big_y,
					$rgb=0xFFFFFF,
					$quality=100
                                    );
                                }
				copy($tempname, $this->_file_path.$tovar_id.'_img_big.'.$ext);
				$tovar->img_big = $tovar_id.'_img_big.'.$ext;
				$tovar->save();
			}


                        $name=$_FILES['img_show']['name'];
			$tempname=$_FILES['img_show']['tmp_name'];

                        if ($name=='' && $_FILES['img_big']['name'] != ''){
                            $name = $_FILES['img_big']['name'];
                            $tempname=$_FILES['img_big']['tmp_name'];
                        }

			if ($name!='' && ($tempname!='')){
				$ext = @end(explode('.', $name));
				copy($tempname, $this->_file_path.$tovar_id.'_img_show.'.$ext);
				$tovar->img_show = $tovar_id.'_img_show.'.$ext;

                $this->view->bikeType = Catalog_Tovar::getInstance()->_bikeType;
                $this->view->bikeType[0] = 'Нет';

                $this->view->humanType = Catalog_Tovar::getInstance()->_humanType;
                $this->view->humanType[0] = 'Нет';
                
				$tovar->save();
			}

			$this->editMeta('tovar', $tovar->id);
			$this->_redirect('/admin/'.$this->_current_module.'/items/divid/'.$parent_id.'/id_page/'.$this->_id_page);
			} else {
				$this->view->message="Ошибка заполните поля помеченные звездочкой";
			}

		}

	}

	public function editAction(){

		$this->view->tovar_id = $tovar_id =  $this->getRequest()->getParam('tovarid');
		$makers = array();
		$tovar = Catalog_Tovar::getInstance()->getTovarById($tovar_id);
		$this->view->division = $division = Catalog_Division::getInstance()->getItemById($tovar->division_id);
		$this->view->page = Pages::getInstance()->find($division->id_page)->current();

		$main_div = Catalog_Division::getInstance()->getMainDiv($tovar->division_id);

		$fck1 = $this->getFck('add[intro]', '90%', '150','Basic');
        
		$this->view->fck_intro = $fck1;
		$fck2 = $this->getFck('add[description]', '90%', '300');
		$this->view->fck_content = $fck2;
		if ($this->_request->isPost()){
			//$data = $this->filter($this->getRequest()->getParam('add'));
            $data = $this->getRequest()->getParam('add');
			/*$data['division_id'] = $parent_id;*/
			$data['maker_id'] = $main_div;
			if (!empty($data['name'])){
				//$data['prior'] =Catalog_Tovar::getInstance()->getMaxPriorInDivision($parent_id);
                                $tovar->setFromArray($data);
				$tovar_id = $tovar->save();

			$name=$_FILES['img_small']['name'];
			$tempname=$_FILES['img_small']['tmp_name'];
			$delete_small = $this->_getParam('delete_img_small', 0);

                        if ($name=='' && $_FILES['img_big']['name'] != ''){
                            $name = $_FILES['img_big']['name'];
                            $tempname=$_FILES['img_big']['tmp_name'];
                        }

			if ($name!='' && $tempname!='' && !$delete_small){
				$ext = @end(explode('.', $name));
                                $size = getimagesize($tempname);

				$this->img_resize(
					$tempname,
					$this->_file_path.$tovar_id.'_img_small.'.$ext ,
                                        $this->_img_preview_x,
					$this->_img_preview_y,
					$rgb=0xFFFFFF,
					$quality=100
				);
				$tovar->img_small = $tovar_id.'_img_small.'.$ext;

				$tovar->save();
                        }
			 elseif ($delete_small){
				@unlink($this->_file_path.$tovar->img_small);
				$tovar->img_small='';
				$tovar->save();
			}


			$name=$_FILES['img_big']['name'];
			$tempname=$_FILES['img_big']['tmp_name'];
			$delete_big = $this->_getParam('delete_img_big', 0);
			if ($name!='' && $tempname!='' && !$delete_big){
				$ext = @end(explode('.', $name));
				copy($tempname, $this->_file_path.$tovar_id.'_img_big.'.$ext);
				$tovar->img_big = $tovar_id.'_img_big.'.$ext;
				$tovar->save();
			} elseif ($delete_big){
				@unlink($this->_file_path.$tovar->img_big);
				$tovar->img_big='';
				$tovar->save();
			}

                        $name=$_FILES['img_show']['name'];
			$tempname=$_FILES['img_show']['tmp_name'];
			$delete_show = $this->_getParam('delete_img_show', 0);
			if ($name!='' && $tempname!='' && !$delete_show){
				$ext = @end(explode('.', $name));
				copy($tempname, $this->_file_path.$tovar_id.'_img_show.'.$ext);
				$tovar->img_show = $tovar_id.'_img_show.'.$ext;
				$tovar->save();
			} elseif ($delete_show){
				@unlink($this->_file_path.$tovar->img_show);
				$tovar->img_show='';
				$tovar->save();
			}

			$this->editMeta('tovar', $tovar->id);
			$this->view->ok = "Данные сохранены";
			//$this->_redirect('/admin/'.$this->_current_module.'/items/divid/'.$tovar->division_id.'/id_page/'.$this->_id_page);
			} else {
				$this->view->message="Ошибка заполните поля помеченные звездочкой";
			}

		}

        $this->view->bikeType = Catalog_Tovar::getInstance()->_bikeType;
        $this->view->bikeType[0] = 'Нет';

        $this->view->humanType = Catalog_Tovar::getInstance()->_humanType;
        $this->view->humanType[0] = 'Нет';
        
		$this->view->tovar = $tovar;
		$this->view->options = $this->getMeta('tovar', $tovar->id);

	}

	public function fieldsAction(){

		$this->view->div_id = $div_id = $this->_getParam('divid');
		if (!isset($div_id)){
			$this->_redirect('/admin/ru/Tehnicfields/index/');
		}
		$fields = Catalog_TehnicFilds::getInstance()->getFieldsByDivId($div_id);
		if ( isset($fields) && $fields!=null){
		$this->view->teh_fields = Catalog_TehnicFilds::getInstance()->getFieldsArrayId($fields);
		}
	}

	public function fieldaddAction(){
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();
		$this->view->currentModul =$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();

		Loader::loadPublicModel('Catalog_TehnicFilds');
		Loader::loadPublicModel('Catalog_Division');

		$this->view->div_id = $div_id = $this->_getParam('divid');
		$this->view->division = Catalog_Division::getInstance()->getItemById($div_id);
		$this->view->all_fields = Catalog_TehnicFilds::getInstance()->getAll();
		if (!isset($div_id)){
			//$this->_redirect('/admin/ru/catalogtovar/fields/');
		}
		if ($this->_request->isPost()){
			Loader::loadPublicModel('Catalog_TehnicFilds');
			if ($this->_hasParam('field_id') && $this->_getParam('field_id')!=0){


			Catalog_TehnicFilds::getInstance()->addItem($this->_getParam('field_id'),$div_id);
			$this->_redirect('/admin/ru/catalogtovar/fields/divid/'.$div_id);
			}
			else {
			$data= $this->_getParam('add');


				if ( isset($data['name']) && $data['name']!=''){
					$data['name']=$this->filter($data['name']);
					if(!Catalog_TehnicFilds::getInstance()->issetFieldByName($data['name'])){
					$fId = Catalog_TehnicFilds::getInstance()->addItem($data);

					Catalog_TehnicFilds::getInstance()->addItem($fId,$div_id);
					$this->_redirect('/admin/ru/catalogtovar/fields/divid/'.$div_id);
					} else {$this->view->message='Поле с таким именем уже существует!!';}
				}
			}
		}
		//$this->view->teh_fields = Catalog_TehnicFilds::getInstance()->getAllByDivId($div_id);

	}

	public function getTovarToBasket($array){

	}

	public function deleteAction(){
		if($this->_hasParam('tovarid')){
			$tovar_id = (int)$this->getRequest()->getParam('tovarid');
			$tovar = Catalog_Tovar::getInstance()->find($tovar_id)->current();
			$division_id = Catalog_Tovar::getInstance()->find($tovar_id)->current()->division_id;
			if (null != $tovar) {
				$tovar->delete();
			}


		}

		$this->_redirect('/admin/'.$this->_current_module.'/items/divid/'.$division_id.'/id_page/'.$this->_id_page);
	}

	public function copyAction(){
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();
		$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
			if($this->_hasParam('tovarid')){
				$tovar_id = (int)$this->getRequest()->getParam('tovarid');
				$tovar = Catalog_Tovar::getInstance()->find($tovar_id)->current()->toArray();
				//$teh_fields_values = Catalog_TehnicFildsValues::getInstance()->getAllByTovarId($tovar_id);
				unset($tovar['id']);
				//$tovar['created_at'] = new Zend_Db_Expr('CURDATE()');
				$tovar['name'].='_'.'copy';
				$tovar['img_small']='';
				$tovar['img_big']='';
				$tovar['is_new']=0;
				$new_row = Catalog_Tovar::getInstance()->createRow()->setFromArray($tovar);
				$new_row->save();

				/*if (isset($tovar)){
					$new_tovar_id = Catalog_Tovar::getInstance()->addTovar($tovar);
					foreach ($teh_fields_values as $value){
					$insert_value = Array('tehnic_fild_id'=>$value->tehnic_fild_id,
					                     'tovar_id'=>$new_tovar_id,
					                     'value'=>$value->value);
					Catalog_TehnicFildsValues::getInstance()->addItem($insert_value);
				}
			}*/
		}
		$this->_redirect('/admin/'.$currentModul.'/items/divid/'.$tovar['division_id'].'/id_page/'.$this->_id_page);
	}

	public function itemsAction(){
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();
		$this->view->currentModul =$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
		if($this->_hasParam('divid')){

		$sortSession = new Zend_Session_Namespace('sortSearch');
		$sort = $this->view->sort = $this->getSort($sortSession->sort);
		$lang = $this->getRequest()->getParam('lang','ru');

		if ($this->_getParam('sort', NULL) == 'nasc') {
			$sort = $this->view->sort = 'nASC';
			$sortSession->sort = 'nASC';
		} elseif ($this->_getParam('sort', NULL) == 'ndesc') {
			$sort = $this->view->sort = 'nDESC';
			$sortSession->sort = 'nDESC';
		}

		$this->view->div_id=	$div_id= $this->_getParam('divid');
		$this->view->division = Catalog_Division::getInstance()->getItemById($div_id);

		$list = $this->view->list = $this->_getParam('list', 1);
				$onlist= $this->view->onlist = 15;
				$this->view->tovars=Catalog_Tovar::getInstance()->getAllTovarsByDivId($div_id,($list-1)*$onlist, $onlist, $sort)->toArray();
				$count = $this->view->Count =  sizeof(Catalog_Tovar::getInstance()->getAllTovarsByDivId($div_id,null, null));
				$listCount = (int)$this->view->listCount = ceil($count / $onlist);
				$this->request = $this->getRequest();



		}

	}

	public function editfieldAction(){

		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();
		$this->view->currentModul =$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();

		Loader::loadPublicModel('Catalog_TehnicFilds');
		$this->view->div_id = $div_id = $this->_getParam('divid');
		$this->view->field_id = $field_id = $this->_getParam('fid');
		if (isset($div_id) && isset($field_id)){
			$this->view->field = Catalog_TehnicFilds::getInstance()->getFieldById($field_id);
		}
		if ($this->_request->isPost()){
			$data= $this->_getParam('add');
			$data['name']=$this->filter($data['name']);
			if ($data['name']!=''){
				Catalog_TehnicFilds::getInstance()->editItem($data,$field_id);
				$this->_redirect('/admin/ru/catalogtovar/fields/divid/'.$div_id);
			}

		}

	}

	public function delfieldAction(){

		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();
		$this->view->currentModul =$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();

		Loader::loadPublicModel('Catalog_TehnicFilds');
		Loader::loadPublicModel('Catalog_TehnicFildsValues');
		Loader::loadPublicModel('Catalog_TehnicFilds');
		$this->view->div_id = $div_id = $this->_getParam('divid');
		$this->view->field_id = $field_id = $this->_getParam('fid');
		if (isset($div_id) && isset($field_id)){
			Catalog_TehnicFilds::getInstance()->deleteItem($field_id);
			$this->_redirect('/admin/'.$currentModul.'/fields/divid/'.$div_id);
		}


	}


    //prior------------------------------------

    public  function priordwnAction(){
    	$this->view->lang = $lang = $this->getParam('lang','ru');
    	$this->request = $this->getRequest();
		$currentModul = $lang.'/'. $this->request->getControllerName();
    	if ($this->_hasParam('tovarid')){
    		$id = $this->_getParam('tovarid');
    		$div_id = $this->_getParam('divid');
    	$tovar = Catalog_Tovar::getInstance()->getTovarById($id);
    	$prior = $tovar->prior;
    	$prior++;

	    	if (Catalog_Tovar::getInstance()->getTovarByPrior($prior,$div_id)){
	    		Catalog_Tovar::getInstance()->decPrior($prior,$div_id)	;
	    	}
	    	Catalog_Tovar::getInstance()->priorUp($id);

    	}
    	$this->_redirect('/admin/'.$currentModul.'/items/divid/'.$div_id);
    }

    public  function priorupAction(){
    	$this->view->lang = $lang = $this->getParam('lang','ru');
    	$this->request = $this->getRequest();
		$currentModul = $lang.'/'. $this->request->getControllerName();
    	if ($this->_hasParam('tovarid')){
    		$id = $this->_getParam('tovarid');
    		$div_id = $this->_getParam('divid');
	    	$tovar = Catalog_Tovar::getInstance()->getTovarById($id);
	    	$prior = $tovar->prior;
	    	$prior--;
	    	if ($prior>0){
		    	if (Catalog_Tovar::getInstance()->getTovarByPrior($prior,$div_id)){
		    		Catalog_Tovar::getInstance()->incPrior($prior,$div_id)	;

		    	}
		    }
		    Catalog_Tovar::getInstance()->priorDwn($id);

    	}
    	$this->_redirect('/admin/'.$currentModul.'/items/divid/'.$div_id);
    }

    public function setfirstAction(){
    	$this->view->lang = $lang = $this->getParam('lang','ru');
    	$this->request = $this->getRequest();
		$currentModul = $lang.'/'. $this->request->getControllerName();

    	if ($this->_hasParam('tovarid')){
    		$id = $this->_getParam('tovarid');
    		$div_id = $this->_getParam('divid');
	    		Catalog_Tovar::getInstance()->incAllTovarsByDivid($div_id);
	    		Catalog_Tovar::getInstance()->setFirstPrior($id);

    	}
    	$this->_redirect('/admin/'.$currentModul.'/items/divid/'.$div_id);

    }

     public function setlastAction(){
    	$this->view->lang = $lang = $this->getParam('lang','ru');
    	$this->request = $this->getRequest();
		$currentModul = $lang.'/'. $this->request->getControllerName();

    	if ($this->_hasParam('tovarid')){
    		$id = $this->_getParam('tovarid');
    		$div_id = $this->_getParam('divid');

	    		$max =Catalog_Tovar::getInstance()->getMaxPriorInDivision($div_id);
	    		Catalog_Tovar::getInstance()->setPriorByTovarId($max+1,$id);

    	}
    	$this->_redirect('/admin/'.$currentModul.'/items/divid/'.$div_id);

    }


	 /**
     * закачка файла
     *
     * @param string $file_name
     * @param string $tempfile_name
     * @return
     */
    private  function upload($sTempFileName, $sType,$path ,$fileName) //закачка файла на сервер
    {

        if (isset($sTempFileName) && $sType == 'jpeg')
        {
            @chmod($sTempFileName, 0777);
           return move_uploaded_file($sTempFileName, $path . $fileName);

        }
    }


	/**
	 * @param array $array
	 * @return array	 *
	 */
	private function filter($array){
		$filter = new Zend_Filter();
		$filter->addFilter(new Gr_Filter_Quote());
		foreach ($array as $key=> $value){
			$array[$key] =$filter->filter($value);
		}

			return parent::trimFilter($array);
	}
}
