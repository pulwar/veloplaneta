<?php
class Admin_Catalog_CsvController extends MainAdminController 
{
	/**
	 * Controller name
	 *
	 * @var string
	 */
	private  $_current_module =null;
	
	public function indexAction()
	{
		
	}
	
	public function importIndexAction()
	{
		$this->view->lang = $lang = $this->_getParam('lang','ru');
		
		if($this->_hasParam('id'))
		{
			$id = (int)$this->getRequest()->getParam('id');
			$item = Catalog_Division::getInstance()->find($id)->current();
			if (!isset($item))
				$this->_redirect('/');
		}
		
		$this->view->id_page = $this->_id_page = $this->_getParam('id_page',0);
		
		$this->view->item = $item;
		$this->view->options = $this->getMeta('catalog_division', $item->id);
			
		
		if ($this->_request->isPost() === true)
		{
			$file = $_FILES['csvfile'];
			if (0 == $file['size'])
			{
				$this->view->ok = 0;
				return;
			}
			if (!empty($file['name']))
			{
				$file_name = $_FILES['csvfile']['name'];
				$file_source = $_FILES['csvfile']['tmp_name'];
				if ('' == $file_name && '' == $file_source )
					$this->view->ok=0;
					
				$ext = @end(explode('.', $file_name));
				if ('csv' != $ext)
				{
					$this->view->ok = 2;
					return;
				}
				
				file_put_contents($file_source, iconv('windows-1251', 'utf-8', file_get_contents($file_source)));
				
				$file = fopen($file_source, "r");
				$line = fgetcsv($file, 40960, ';');
				
				while (! feof($file))
				{
					$fields = fgetcsv($file, 40960, ';');
					if (count($fields) == 1)
						break;
						
					$data['division_id'] = $fields[2];
					$item = Catalog_Division::getInstance()->find($data['division_id'])->current();
					if (!isset($item) || $item == null)
					{
						$this->view->ok = 2;
						fclose($file);
						return;
					}
					$trans = array('"' => '&quot;');
					$data['name'] = $fields[3];
					$data['name'] = strtr($data['name'], $trans);
					$data['intro'] = $fields[9];
					$data['intro'] = strtr($data['intro'], $trans);
					$data['description'] = $fields[10];
//					$data['description'] = html_entity_decode($data['description']);
					$data['description'] = strtr($data['description'], $trans);
					$data['price'] = $fields[4];
					$data['articul'] = $fields[5];
					$data['is_new'] = $fields[6];
					$data['prior'] = $fields[8];
					if ($data['prior'] == '')
					{
						$data['prior'] = 0;
					}
					$data['price'] = str_replace(',', '.', $data['price']);
					$data['material'] = $fields[7];
					$data['material'] = $fields[7] == '/Металл' ? 'Пластик/Металл' : $fields[7];
					if ($data['material'] == '')
					{	
						$material = explode('->', $fields[0]);
						$material = $material[1];
						$data['material'] = $material;
					}
					$data['id'] = $fields[1];
					if ($data['division_id'] == '')
					{
						$this->view->ok = 2;
						return;
					}
					
					$product_row = Catalog_Tovar::getInstance()->find($data['id'])->current();;
					if (!isset($product_row) || ($data['id'] == ''))
					{
						$product_row = Catalog_Tovar::getInstance()->createRow();
						unset($data['id']);
						$data['color_id_body'] = 2; //we need to export color
						$data['color_id_clip'] = 2; //we need to export color
						$data['color_id_elem'] = 0; //we need to export color
					}
					
					$product_row->setFromArray($data);
					$product_row->save();
					
				}
		
				fclose($file);
				
				$this->view->ok=1;
				
			}
			else 
				$this->view->ok=0;
		}
	}

	private function generateRow($path, $product_id, $div_id, $product_name, $price, $articul, $is_new, $material, $prior, $intro, $description)
	{
		$result_csv_string = $path . ';';
		$result_csv_string .= $product_id . ';';
		$result_csv_string .= $div_id . ';';
		$result_csv_string .= $product_name .  ';';
		$result_csv_string .= $price . ';';
		$result_csv_string .= $articul . ';';
		$result_csv_string .= $is_new . ';';
		$result_csv_string .= $material . ';';
		$result_csv_string .= $prior . ';';
		$result_csv_string .= $intro . ';';
		$result_csv_string .= $description;
		$result_csv_string .= "\n";
		
		return $result_csv_string;
	}
	
	public function exportAction()
	{
		$result_csv_string = 'Путь;id товара;id раздела;Название товара;Стоимость;Артикул;Новинка(0/1);' . 
									'Материал;Приоритет;Краткое описание;Полное описание' . "\n";
		$items = Catalog_Division::getInstance()->fetchAll('id=' . $this->_getParam('id'));
		
		if($this->_hasParam('id'))
		{
			$root_id = (int)$this->getRequest()->getParam('id');
			$root_item = Catalog_Division::getInstance()->find($root_id)->current();
		}
		
		if (isset($items) && count($items) > 0 && $items->current()->level < 2)
		{
			$item_name = $items->current()->name;
			$items = Catalog_Division::getInstance()->fetchAll('parent_id=' . $this->_getParam('id'));
			if (isset($items) && count($items) > 0)
			{
				if ($items->current()->level == 1)
				{
					$items = Catalog_Division::getInstance()->fetchAll('parent_id=' . $this->_getParam('id') . ' AND level=1');
					if (count($items) > 0)
					{
						foreach ($items as $item)
						{
							$models = Catalog_Division::getInstance()->fetchAll('parent_id=' . $item->id . ' AND level=2');
							if (count($models) > 0)
							{
								foreach ($models as $model)
								{
									$products = Catalog_Tovar::getInstance()->fetchAll('division_id=' . $model->id);
									if (count($products) > 0)
									{
										foreach ($products as $product)
										{
											//generate row
											$result_csv_string .= $this->generateRow($item_name . '->' . $item->name . '->' . $model->name,
																			$product->id,
																			$product->division_id,
																			'"' . str_replace('"', '""', $product->name) . '"',
																			$product->price,
																			$product->articul,
																			$product->is_new,
																			$product->material,
																			$product->prior,
																			'"' . str_replace('"', '""', $product->intro) . '"',
																			'"' . str_replace('"', '""', $product->description) . '"');
										}
									}
								}
							}
						}
					}
				}
				else if (isset($items) && count($items) > 0 && $items->current()->level == 2)
				{
					$models = $items;	
					foreach ($models as $model)
					{
						$products = Catalog_Tovar::getInstance()->fetchAll('division_id=' . $model->id);
						if (count($products) > 0)
						{
							$items_level_1 = Catalog_Division::getInstance()->fetchAll('id=' . $model->parent_id);
							$items_level_0 = Catalog_Division::getInstance()->fetchAll('id=' . $items_level_1->current()->parent_id);
							foreach ($products as $product)
							{
								//generate row
								$result_csv_string .= $this->generateRow($items_level_0->current()->name . '->' . $items_level_1->current()->name . '->' . $model->name,
																			$product->id,
																			$product->division_id,
																			'"' . str_replace('"', '""', $product->name) . '"',
																			$product->price,
																			$product->articul,
																			$product->is_new,
																			$product->material,
																			$product->prior,
																			'"' . str_replace('"', '""', $product->intro) . '"',
																			'"' . str_replace('"', '""', $product->description) . '"');
							}
						}
					}
				} 
				else
				{
					if (isset($root_item) && isset($root_id))
					{
						$result_csv_string .= $this->generateRow($root_item->name,
																				$root_id,
																				'',
																				'',
																				'',
																				'',
																				'',
																				'',
																				'',
																				'');
					}
				}
			}
			else if (count($items) == 0)
			{
				if (isset($root_item) && isset($root_id))
				{
					$result_csv_string .= $this->generateRow($root_item->name . ';',
																			$root_id,
																			'',
																			'',
																			'',
																			'',
																			'',
																			'',
																			'',
																			'');
				} 
			}
		}
		else if (isset($items) && count($items) > 0 && $items->current()->level == 2)
		{
			$products = Catalog_Tovar::getInstance()->fetchAll('division_id=' . $this->_getParam('id'));
			if (count($products) > 0)
			{
				$items_level_2 = Catalog_Division::getInstance()->fetchAll('id=' . $products->current()->division_id);
				$items_level_1 = Catalog_Division::getInstance()->fetchAll('id=' . $items_level_2->current()->parent_id);
				$items_level_0 = Catalog_Division::getInstance()->fetchAll('id=' . $items_level_1->current()->parent_id);
				foreach ($products as $product)
				{
					//generate row
					$result_csv_string .= $this->generateRow($items_level_0->current()->name . '->' . $items_level_1->current()->name . '->' . $items_level_2->current()->name,
																$product->id,
																$product->division_id,
																'"' . str_replace('"', '""', $product->name) . '"',
																$product->price,
																$product->articul,
																$product->is_new,
																$product->material,
																$product->prior,
																'"' . str_replace('"', '""', $product->intro) . '"',
																'"' . str_replace('"', '""', $product->description) . '"');
				}
			}
			else
			{
				if (isset($root_item) && isset($root_id))
				{
					$items_level_1 = Catalog_Division::getInstance()->fetchAll('id=' . $root_item->parent_id);
					$items_level_0 = Catalog_Division::getInstance()->fetchAll('id=' . $items_level_1->current()->parent_id);
					$result_csv_string .= $this->generateRow($items_level_0->current()->name . '->' . $items_level_1->current()->name . '->' . $root_item->name . ';',
																			$root_id,
																			'',
																			'',
																			'',
																			'',
																			'',
																			'',
																			'',
																			'');
				}
			}
		}
		
		//utf-8 -> windows-1251
		$result_csv_string = iconv("utf-8", "windows-1251", $result_csv_string);
		
		//give file
		header('Content-type: application/csv');
		header('Content-Disposition: inline; filename=export.csv');
		echo $result_csv_string;
		exit();
	}
}