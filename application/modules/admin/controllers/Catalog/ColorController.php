<?php
define('SP','/') ;
/**
 * Admin_WorksController
 * 
 * @author Grover
 * @version 1.0
 */

class Admin_Catalog_ColorController extends MainAdminController  {
	
	/**
 	 * @var string
 	 */
	private $_curModule = null;	
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_onpage = 20;
	
	private $_page = null;
	/**
	 * offset
	 *
	 * @var int
	 */	
	private $_offset = null;	
	
	public function init(){
		$this->initView();
		$this->view->strictVars(false);
		$this->view->addScriptPath(DIR_SCRIPTS) ;
		$this->view->addHelperPath(DIR_HELPER , 'Gr_View_Helper') ;		
		$this->view->caption = 'Дела';
		$this->view->peoples_id = $this->_getParam('peoples_id');		
		$lang = $this->_getParam('lang','ru');
		//var_dump($this->view->peoples_id);
		$this->view->currentModule = $this->_curModule = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		$this->_page = $this->_getParam('page', 1);	
		$this->_offset =($this->_page-1)*$this->_onpage;	
		$this->view->current_page = $this->_page;
		$this->view->onpage = $this->_onpage;
	
		
	}
	
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		
		$this->view->child_name = 'Палитра Цветов';
		$this->view->currentModule = $this->_curModule;		
		$where = null;
		$this->view->total = count(Catalog_Color::getInstance()->fetchAll($where));		
		$this->view->all = $peoples =  Catalog_Color::getInstance()->fetchAll($where, 'priority DESC', (int)$this->_onpage, (int)$this->_offset);
		
		
		
	
	}
	
	public function editAction(){
		$id = $this->_getParam('id', '');
		if ($id){
			$item = Catalog_Color::getInstance()->find($id)->current();
		} else{
			$item = Catalog_Color::getInstance()->createRow();
		}
			
		if ($this->_request->isPost()){						
			$data = $this->trimFilter($this->_getParam('edit'));			
			if ($data['name']!=''){
				$item->setFromArray($data);
				$id =  $item->save();				
				$this->view->ok=1;
				
			} else{
				$this->view->err=1;				
			}
			
		}
		
		$this->view->item = $item;
		if ($item->id){			
			$this->view->child_name = 'Редактировать';
		} else {
			$this->view->child_name = 'Добавить';
		}	
		
	}

	
	public function deleteAction(){
		$id = $this->_getParam('id');
		if ($id ){
			$item = Catalog_Color::getInstance()->find($id)->current();			
			$item->delete();			
			$this->_redirect($this->_curModule.$path);
		}
	}
}