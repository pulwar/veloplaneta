<?php
class Admin_Catalog_DivisionController extends MainAdminController 
{
	/**
	 * Controller name
	 *
	 * @var string
	 */
	private  $_current_module =null;
	/**
	 * Catalog page id
	 *
	 * @var int
	 */
	private  $_id_page = null;
	/**
	 * path to upload images
	 */
	private  $_pictPath = null;
	
	/**
	 * path to show images in view
	 * 
	 */
	private  $_view_pictPath = null;
	/**
	 * postfix to img filename
	 *
	 * @var string
	 */
	private $_img_postFix ='_logo.';
	
	public function init()
	{	$this->initView();
		$this->request = $this->getRequest();
		$this->view->lang = $lang = $this->_getParam('lang','ru');
		$this->view->id_page = $this->_id_page = $this->_getParam('id_page',0);	
		$division = new Catalog_Division();
		$this->view->viewPictPath = $this->_view_pictPath = $division->getViewPictPath();	
		$this->view->pictPath = $this->_pictPath = $division->getPictPath();				
		$this->view->currentModul = $this->_current_module = $lang.'/'. $this->request->getControllerName();
		$this->view->img_postFix = $this->_img_postFix =$division->getImgPostFix();
		
	}
	
	
	public function indexAction()
	{
		$this->view->lang = $lang = $this->getParam('lang','ru');
		if ($this->_id_page > 0) {
			$tree = Catalog_Division::getInstance()->getTree($this->_id_page);
			$catalog_page = Pages::getInstance()->find($this->_id_page)->current();
			$return[] = array(
					'task' => $catalog_page->name,
	    				'duration' =>"<a href ='#' title='Добавить' style='margin-left: 128px'><img src='/images/plus_krug.gif' onclick='javascript:window.location=\"/admin/$lang/Catalog_Division/add/id_page/$catalog_page->id\"'/></a>",
	    				'user' => Security::getInstance()->getUser()->username,
	    				'id' => $catalog_page->id,
	    				'uiProvider' => 'col',
	    				'cls' => 'master-task',
	    				'iconCls' => 'task-folder',
	    				'children' => $tree
					);
			$this->view->ch=Zend_Json::encode($return);		
		}
		if($this->_hasParam('node') && $this->_hasParam('target') && $this->_hasParam('point')){
			
			$node = (int)$this->_getParam('node');
			$target = (int)$this->_getParam('target');
			$point = (string)$this->_getParam('point');
			$catalog = Catalog_Division::getInstance();
			$catalog->replace($node, $target, $point);
		}
	}
	
	
	public function addAction()
	{	
		$this->view->level= $this->_getParam('level',0);
		$this->view->parent_id = $parent_id =  $this->_getParam('parent_id',0);
		$fck1 = $this->getFck('add[intro]', '90%', '150','Basic');	
		$this->view->fck_intro = $fck1;	
		$fck2 = $this->getFck('add[description]', '90%', '300');
		$this->view->fck_content = $fck2;
		
		/*$fck1 = $this->getFck('add[background]', '90%', '80','Basic');	
		$this->view->fck_background = $fck1;	
		
		$fck1 = $this->getFck('add[flash]', '90%', '80','Basic');	
		$this->view->fck_flash = $fck1;	*/
		
		if ($this->_request->isPost()){
			$data = $this->getRequest()->getParam('add');

			$data = $this->trimFilter($data);
			$data['level'] =  $this->getRequest()->getParam('level',0);
			$data['parent_id'] =  $this->getRequest()->getParam('parent_id',0);	
			$data['id_page'] = $this->_id_page;
                        $data['season'] = 'summer';
			$item=null;
			if ($data['parent_id'] >0) {
				$item = Catalog_Division::getInstance()->find($data['parent_id'])->current();
				$data['level'] = $item->level+1;
			}
			$data['sortid'] = Catalog_Division::getInstance()->getMaxSort($this->_id_page, $parent_id)+1;
			$new_division = Catalog_Division::getInstance()->createRow();
			if (!empty($data['name'])){
				$new_division->setFromArray($data);
				$id = $new_division->save();	
				$img_name = $_FILES['img']['name'];
				$img_source = $_FILES['img']['tmp_name'];
								
				if ($img_name!='' && $img_source!=''){
					$ext = @end(explode('.', $img_name));
					$small_img = DIR_PUBLIC.'pics/catalog/division/'.$id.'_img.'.$ext;					
					if(copy($img_source, $small_img)){
						$new_division->img = $id.'_img.'.$ext;
						$new_division->save();
					}
					
				} 		
											
			}
			$this->editMeta('catalog_division', $new_division->id);
			$this->_redirect('/admin/'.$this->_current_module.'/index/id_page/'.$this->_id_page);
		}
	}
	
	public function editAction(){
		
		$fck1 = $this->getFck('add[intro]', '90%', '150','Basic');	
		$this->view->fck_intro = $fck1;	
		
		/*$fck1 = $this->getFck('add[background]', '90%', '80','Basic');	
		$this->view->fck_background = $fck1;	
		
		$fck1 = $this->getFck('add[flash]', '90%', '80','Basic');	
		$this->view->fck_flash = $fck1;	*/
		
		$fck2 = $this->getFck('add[description]', '90%', '300');
		$this->view->fck_content = $fck2;		
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			$item = Catalog_Division::getInstance()->find($id)->current();
		}
		if ($this->_request->isPost()){
			$data = $this->getRequest()->getParam('add');
			$id = $this->getRequest()->getParam('id');
			if (!isset($id)){
				$this->_redirect('/admin/'.$this->_current_module.'/index/id_page/'.$this->_id_page);
			}
			if (!empty($data['name'])){				
				$item->setFromArray($this->trimFilter($data));
				$item->save();
					//$this->upload($this->_pictPath , $division_row->id.$this->_img_postFix);
					$img_name = $_FILES['img']['name'];
					$img_source = $_FILES['img']['tmp_name'];
					$delete_img = $this->_getParam('delete_img');				
					if ($img_name!='' && $img_source!='' && !$delete_img){
						$ext = @end(explode('.', $img_name));
						$small_img = DIR_PUBLIC.'pics/catalog/division/'.$id.'_img.'.$ext;	
								
						if(copy($img_source, $small_img)){
							$item->img = $id.'_img.'.$ext;
							$item->save();
						}
						
					} else if ($delete_img){
						@unlink(DIR_PUBLIC.'pics/catalog/division/'.$item->img);					
						$item->img='';					
						$item->save();
					}		
				$this->view->ok = 1;
				$this->editMeta('catalog_division', $item->id);		
				//$this->_redirect('/admin/'.$this->_current_module.'/index/id_page/'.$this->_id_page);
			} else $this->view->ok=0;
		}
		$this->view->item =$item;
		$this->view->options = $this->getMeta('catalog_division', $item->id);
		
	}
	
	
	
	public function deleteAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			$division_row = Catalog_Division::getInstance()->find($id)->current();
			if (null != $division_row) {
				unlink($this->_pictPath.$division_row->id.$this->_img_postFix);
				$division_row->delete();
			}
		}
		$this->_redirect("/admin/".$this->_current_module."/index/id_page/$this->_id_page/");
	}
	
	public function dellogoAction(){
		if($this->_hasParam('id')){
			$this->geleteFiles($this->_getParam('id'));
		}
		$this->_redirect("/admin/".$this->_current_module."/index/id_page/$this->_id_page/");
		
	}
	
	
	/**
	 * StringTrim filter
	 *
	 * @param Array $array
	 * @return Array
	 */
	public function trimFilter($array){
		return parent::trimFilter($array);
	}
	
	private function geleteFiles($id){
			$division_row = Catalog_Division::getInstance()->find($id)->current();
			if (null != $division_row) {
				$logo = glob($this->_pictPath.$division_row->id.$this->_img_postFix.'*');
				if ($logo){
					foreach ($logo as $item){
						unlink($item);
					}
				}
			}
	}
	
	public function upload($path,$fileName){
	    $sTempFileName = $_FILES['logo']['tmp_name'];
            if (@getImageSize($sTempFileName))
            {   $sType = substr($_FILES['logo']['type'], strpos($_FILES['logo']['type'], '/')+1);
               // if ($sType =='jpeg') {
               		$logo = glob($path.$fileName.'*');
               		if ($logo){
               			unset($logo[0]);
               		}
					return parent::upload_file($sTempFileName,$path,$fileName.$sType);
				/*} else {
					return FALSE;
				}*/
            }	
	}
}
