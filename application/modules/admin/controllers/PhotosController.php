<?php

class Admin_PhotosController extends MainAdminController 
{
	
	
	public function init()
	{
		Loader::loadPublicModel('Photos');
		require_once 'Zend/Session.php';
	}
	
	
	public function indexAction()
	{
		
		
	}
	
	
	public function addAction()
	{
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();			
		$this->view->currentModul =$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
		$this->view->muzey_id = (int)$this->getRequest()->getParam('parent_id');
		
		if ($this->_request->isPost()){
			$data = $this->getRequest()->getParam('add');	
			if ($data['name']==''){
				$data['name']=$_FILES['img_sourse']['name']	;			
			}
			if ( isset($data['pub']) && $data['pub']=='on') {
				$data['pub']=1;
			} else $data['pub']=0;
			
			$data['name'] = trim($data['name']);
			$fname = '';
			$name=$_FILES['img_sourse']['name'];
			$tempname=$_FILES['img_sourse']['tmp_name'];
		if (($name!='') && ($tempname!='')){
			$type =  strtolower(substr($_FILES['img_sourse']['type'], strpos($_FILES['img_sourse']['type'],'/')+1));
			$data['type']= $type;
			$fname =$this->upload_file($name,$tempname,$type);
			if( $fname!='' ){
			$id = Photos::getInstance()->addItem($data);			
			$this->img_resize(DIR_PUBLIC.'pics/muzey/'.$fname,DIR_PUBLIC.'pics/muzey/'.$id.'_small.'.$type,123,93,$rgb=0xFFFFFF, $quality=80);
			$this->img_resize(DIR_PUBLIC.'pics/muzey/'.$fname,DIR_PUBLIC.'pics/muzey/'.$id.'_preview.'.$type,300,200,$rgb=0xFFFFFF, $quality=100);
			$this->copyImg(DIR_PUBLIC.'pics/muzey/'.$fname,DIR_PUBLIC.'pics/muzey/'.$id.'_full.'.$type,DIR_PUBLIC.'pics/muzey/logo.png');
			unlink(DIR_PUBLIC.'pics/muzey/'.$fname);
			
			
			}
			
		}
	
			
					
			
				
        $pageNamespace = new Zend_Session_Namespace('page');
        $url='';
        if (isset($pageNamespace->page_id)) {
        	$url = "/page/id/".$pageNamespace->page_id;
        }
        
		$this->_redirect("/admin/$lang/muzey".$url);
			}
			
			
		//}
		
	}
	
	public function editAction(){
		
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();			
		$this->view->currentModul =$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
		
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			$data= $this->view->item = Photos::getInstance()->getItemById($id);
			
		}
		if ($this->_request->isPost()){
			$data = $this->getRequest()->getParam('add');	
			$id = (int)$this->getRequest()->getParam('id');
			
			if (isset($data['name']) && !empty($data['name'])){
				
				if ( $data['pub']=='on') {$data['pub']=1;
					
				}
				 else $data['pub']=0;
				 
			$data['name'] = trim($data['name']);
			
			
			Photos::getInstance()->editItem($data,$id);
			
        $pageNamespace = new Zend_Session_Namespace('page');
        $url='';
        if (isset($pageNamespace->page_id)) {
        	$url = "/page/id/".$pageNamespace->page_id;
        }
        
		$this->_redirect("/admin/$lang/muzey".$url);
			}
			
			
		}
		
		
	}
	
	
	public function activateAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Photos::getInstance()->setPubItem($id);
		}
		
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';	
				
		
        $pageNamespace = new Zend_Session_Namespace('page');
        $url='';
        if (isset($pageNamespace->page_id)) {
        	$url = "/page/id/".$pageNamespace->page_id;
        }
        
		$this->_redirect("/admin/$lang/muzey".$url);
	}
	
	public function unactivateAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Photos::getInstance()->unsetPubItem($id);
		}
		
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		
				
		
        $pageNamespace = new Zend_Session_Namespace('page');
        $url='';
        if (isset($pageNamespace->page_id)) {
        	$url = "/page/id/".$pageNamespace->page_id;
        }
        
		$this->_redirect("/admin/$lang/muzey".$url);
	}
	
	public function deleteAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Photos::getInstance()->delImg($id);
			Photos::getInstance()->deleteItem($id);
			
		}
		
		$lang = $this->getParam('lang','ru');
		
		
	
        $pageNamespace = new Zend_Session_Namespace('page');
        $url='';
        if (isset($pageNamespace->page_id)) {
        	$url = "/page/id/".$pageNamespace->page_id;
        }
        
		$this->_redirect("/admin/$lang/muzey".$url);
	}
	
	public function setmainAction(){
		
			$id = (int)$this->getRequest()->getParam('id');
			if (isset($id)){
			Photos::getInstance()->setMainItem($id);
		}
		
		$lang = $this->getParam('lang','ru');
		
		
        $pageNamespace = new Zend_Session_Namespace('page');
        $url='';
        if (isset($pageNamespace->page_id)) {
        	$url = "/page/id/".$pageNamespace->page_id;
        }
        
		$this->_redirect("/admin/$lang/muzey".$url);
	}
	
	public function unsetmainAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Photos::getInstance()->unsetMainItem($id);
		}
		
		$lang = $this->getParam('lang','ru');
		
		
		
        $pageNamespace = new Zend_Session_Namespace('page');
        $url='';
        if (isset($pageNamespace->page_id)) {
        	$url = "/page/id/".$pageNamespace->page_id;
        }
        
		$this->_redirect("/admin/$lang/muzey".$url);
	}
	
	
	/**
	 * закачка файла 
	 *
	 * @param string $file_name
	 * @param string $tempfile_name
	 * @return 
	 */	
	 
   public	function upload_file($file_name,$tempfile_name,$type) //закачка файла на сервер
	{
		if(isset($file_name)&isset($tempfile_name)){
			
		$date=date("dmyhi");// определяем дату,	   
	     $fn=$date.'.'.$type;  
                       
                     
           // записываем файл в виде "дата.расширение 
           copy($tempfile_name,DIR_PUBLIC.'pics/muzey/'.$fn);
                
              return $fn;
		}
	}
	
	
	
	/***********************************************************************************
Функция img_resize(): генерация thumbnails
Параметры:
  $src             - имя исходного файла
  $dest            - имя генерируемого файла
  $width, $height  - ширина и высота генерируемого изображения, в пикселях
Необязательные параметры:
  $rgb             - цвет фона, по умолчанию - белый
  $quality         - качество генерируемого JPEG, по умолчанию - максимальное (100)
***********************************************************************************/

  public   function img_resize($src, $dest, $width, $height, $rgb=0xFFFFFF, $quality=100)
{
  if (!file_exists($src)) return false;

  $size = getimagesize($src);
  
 

  if ($size === false) return false;

  // Определяем исходный формат по MIME-информации, предоставленной
  // функцией getimagesize, и выбираем соответствующую формату
  // imagecreatefrom-функцию.
  $format = strtolower(substr($size['mime'], strpos($size['mime'],'/')+1));
  $icfunc = "imagecreatefrom" . $format;
  if (!function_exists($icfunc)) return false;

  $x_ratio = $width / $size[0];
  $y_ratio = $height / $size[1];

  
  
  
  $ratio       = min($x_ratio, $y_ratio);
  $use_x_ratio = ($x_ratio == $ratio);

  $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
  $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
  
  $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
  //$new_left    = $use_x_ratio  ? 0 : 0;
  $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);
  //$new_top     = !$use_x_ratio ? 0 : 0;

  $isrc = $icfunc($src);
  $idest = imagecreatetruecolor($width, $height);
  //$idest = imagecreatetruecolor($new_width, $new_height);

  imagefill($idest, 0, 0, $rgb);
  imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0, 
    $new_width, $new_height, $size[0], $size[1]);

  imagejpeg($idest, $dest, $quality);

  imagedestroy($isrc);
  imagedestroy($idest);

  return true;

}	

 private function copyImg($img,$img_dest,$logo){
	
	global $IMGDIR;
    $IMGDIR='public/images/';
      $size = getimagesize($img);
      
      
       switch(strtolower(substr($size['mime'], strpos($size['mime'],'/')+1)))
    {
        case "jpeg":
            $srcImage = ImageCreateFromJPEG($img);                
        break;
            
        case "gif":
            $srcImage = ImageCreateFromGIF($img);
        break;

        case "png":
            $srcImage = ImageCreateFromPNG($img);
        break;
        
        default:
            return -1;
        break;
    }
  
  
  //	$photoImage=ImageCreateFromJPEG($img);
	ImageAlphaBlending($srcImage, true);
	 //$IMGDIR='public/images/';
	 
	 
	$logoImage=ImageCreateFromPNG($logo);
	$logoW=ImageSX($logoImage);
	$logoH=ImageSY($logoImage);
	
	$srcWidth  = ImageSX($srcImage);
    $srcHeight = ImageSY($srcImage);
	
	$trcolor = ImageColorAllocate($logoImage, 83,239, 71);
	//$trcolor = ImageColorAllocate($logoImage, 255,255, 255);
    ImageColorTransparent($logoImage , $trcolor);
	//ImageCopy($srcImage, $logoImage, 0, 0, 0, 0, $logoW, $logoH);
	imagecopymerge ($srcImage, $logoImage, $srcWidth - $logoW-20, $srcHeight - $logoH-20, 0, 0, $logoW, $logoH,50);
	
	ImageJPEG($srcImage,$img_dest,100);   // вывод в браузер 
	
	ImageDestroy($srcImage);
	ImageDestroy($logoImage);
  }
    

	
}
