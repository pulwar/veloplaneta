<?php
/**
 * Основной класс управления содержимым сайта
 *
 */
class Admin_PagesController extends MainAdminController 
{
	/**
	 * Список всех страниц
	 * 
	 */
	
	public function indexAction()
	{
		$lang = $this->_hasParam('lang') ? $this->_getParam('lang') : 'ru';
		Loader::loadPublicModel('Lang');
		$version = Lang::getInstance()->getVersion($lang);
		$this->view->version = $version;
		$root = Pages::getInstance()->getRoot($version->name);
		$tree = Pages::getInstance()->getTree($version->name, $root->parentId);
		
		if($this->_hasParam('node') && $this->_hasParam('target') && $this->_hasParam('point')){
			Loader::loadPublicModel('Pages');
			$node = (int)$this->_getParam('node');
			$target = (int)$this->_getParam('target');
			$point = (string)$this->_getParam('point');
			$pages = Pages::getInstance();
			$pages->replace($node, $target, $point);
		}
		
		if(!empty($tree)){
			$id = $tree[0]['id'];
			$tree[0]['duration'] = "<a href ='#' title='Добавить' style='margin-left: 128px'><img src='/images/plus_krug.gif' onclick='javascript:window.location=\"/admin/$lang/pages/add/parent_id/$id/\"'/></a>";
			$this->view->ch=Zend_Json::encode($tree);
			$this->view->pageName = 'Модуль управления содержимым сайта';
//			$this->view->help = 'Модуль управления содержимым сайта';
			$this->view->lang = $lang;
		}
	}
	
	
	/**
	 * Добавление новой страницы
	 *
	 */
	public function addAction(){
		$lang = $this->getParam('lang');
		
		if(!$this->_hasParam('parent_id')){
			$this->_redirect("/admin/$lang/pages/");
		}

		$error = '';
		
		if($this->getRequest()->isPost()){
			$data = $this->getRequest()->getParams();
			$id = Pages::getInstance()->addPage($data);
			$page = Pages::getInstance()->find($id)->current();
			$img_name = $_FILES['image']['name'];
			$img_source = $_FILES['image']['tmp_name'];				
			$delete_img = $this->_getParam('delete_img');
			if ($img_name!='' && $img_source!='' && !$delete_img){
				$ext = @end(explode('.', $img_name));
				$img = DIR_PUBLIC.'pics/default/'.$id.'_img.'.$ext;					
				if($this->img_resize($img_source, $img, $width = 38, $heiht = 38 )){
					$page->img = $id.'_img.'.$ext;
					$page->save();
				}					
			} else if ($delete_img){
				@unlink(DIR_PUBLIC.'pics/default/'.$item->img);					
				$page->img='';					
				$page->save();
			}
			$data['id'] = $id;
			
			if($error = $this->addRoute($data) == ''){
				$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
				$this->_redirect("/admin/$lang/pages/");
			}
			
		}
		
		$this->view->error = $error;
		Loader::loadPublicModel('MenuTypes');
		$fck = $this->getFck('content', '90%', '400');
		$this->view->fck = $fck;
		$this->view->introText = $this->getFck('introText', '90%', '150','Basic');
		$parentId = (int)$this->getRequest()->getParam('parent_id');
		$this->view->parentId = $parentId;
		if ($parentId==1){// в меню добавляется только первый уровень страниц.
			$this->view->menu = MenuTypes::getInstance()->getAll();
		}
		$this->view->url = Pages::getInstance()->generateStringPath($parentId, '/');
		$this->view->pageName = 'Добавить страницу';
		Loader::loadPublicModel('Templates');
		$this->view->templates = Templates::getInstance()->getAll();
		$this->view->lang = $lang;
		
	}
	
	/**
	 * Удаление страницы
	 *
	 */
	public function deleteAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Loader::loadPublicModel('Basket');
			Basket::getInstance()->add($id);
		}
		
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		$this->_redirect("/admin/$lang/pages/");
	}
	
	public function pubAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Pages::getInstance()->pubPage($id);
		}
		
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		$this->_redirect("/admin/$lang/pages/");
	}
	
	public function unpubAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Pages::getInstance()->unpubPage($id);
		}
		
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		$this->_redirect("/admin/$lang/pages/");
	}
	
	public function editAction(){
		$lang = $this->getParam('lang');
		$id = (int)$this->getRequest()->getParam('id');
		if(!$this->_hasParam('id')){
			$this->_redirect("/admin/$lang/pages/");
		}
		
		if($this->getRequest()->isPost()){
			$data = $this->getRequest()->getParams();
			$error = $this->editRoute($data);
			Pages::getInstance()->editPage($data);
			$page = Pages::getInstance()->find($id)->current();
			$img_name = $_FILES['image']['name'];
			$img_source = $_FILES['image']['tmp_name'];				
			$delete_img = $this->_getParam('delete_img');
			if ($img_name!='' && $img_source!='' && !$delete_img){
				$ext = @end(explode('.', $img_name));
				$img = DIR_PUBLIC.'pics/default/'.$id.'_img.'.$ext;					
				if($this->img_resize($img_source, $img, $width = 38, $heiht = 38 )){
					$page->img = $id.'_img.'.$ext;
					$page->save();
				}					
			} else if ($delete_img){
				@unlink(DIR_PUBLIC.'pics/default/'.$item->img);					
				$page->img='';					
				$page->save();
			}
			$this->_redirect("/admin/$lang/pages/");
		}
		
		
		$this->view->page = $page = Pages::getInstance()->getPage($id);
		Loader::loadPublicModel('PagesOptions');
		$this->view->options = PagesOptions::getInstance()->getPageOptions($id);
		Loader::loadPublicModel('Menu');
		Loader::loadPublicModel('MenuTypes');
		if ($page->level<2){// в меню добавляется только первый уровень страниц.
			$this->view->menu = MenuTypes::getInstance()->getAll();
			$this->view->pageMenu = Menu::getInstance()->getMenuPage($id);
		}
		
		$fck = $this->getFck('content', '90%', '400');
		$this->view->fck = $fck;
		$this->view->introText = $this->getFck('introText', '90%', '150','Basic');
		$this->view->pageName = 'Редактировать страницу';
		if ($page->deleteble==1){
			$this->view->templates = Templates::getInstance()->getAll();
		} else $this->view->templates = Templates::getInstance()->getTemplateName($page->template);		
		$this->view->lang = $lang;
	}
	
	public function basketAction(){
		$tree = Pages::getInstance()->getTree();
		$id = $tree[0]['id'];
		$tree[0]['duration'] = "<a href ='#' style='margin-left: 125px'><img src='/images/plus_krug.gif' onclick='javascript:window.location=\"/admin/pages/add/parent_id/$id/\"'/></a>";
		$this->view->ch = Zend_Json::encode($tree);
		$this->view->pageName = 'Модуль управления содержимым сайта';
	}
	
	public function copyAction(){
//		Zend_Debug::dump($this->_request->getParams());exit;
		if($this->_hasParam('id')){
			Pages::getInstance()->copyPage($this->_getParam('id'));
		}
			
//		exit;
		$lang = $this->_hasParam('lang') ? $this->_getParam('lang') : 'ru';
		$this->_redirect("/admin/$lang/pages/");
	}
	
	public function copyAllAction(){
		if($this->_hasParam('id')){
			Pages::getInstance()->copyPage($this->_getParam('id'));
		}
			
		$lang = $this->_hasParam('lang') ? $this->_getParam('lang') : 'ru';
		$this->_redirect("/admin/$lang/pages/");
	}
	
	public function newlangAction(){
		if($this->_request->isPost()){
			$data['name'] = $this->_getParam('_name');
			$data['title'] = $this->_getParam('_title');
			Loader::loadPublicModel('Lang');
			Lang::getInstance()->addLang($data);
			$this->_redirect("/admin/$data[name]/pages/");
		}
	}
	
	
	public function getVersion($lang){
		Loader::loadPublicModel('Versions');
		$version = Versions::getInstance()->getVersion($lang);
		
		return $version;
	}
	
	private function editRoute($data){
		Loader::loadCommon('Router');
		Loader::loadPublicModel('Templates');
//		echo $data['template'] . "<br />";
		$comments = Templates::getInstance()->getComments($data['template']);
//		Zend_Debug::dump($comments);

		$controller = $comments->module == 'default' ? 'page' : 'index';
		
		
		// фикс для модуля сотрудников
		if ($data['template']=='searchmore'){
			$comments->module = 'search';
			$action = 'searchmore';
		}
		if ($data['template']=='search'){
			$comments->module = 'search';
		}
		if ($data['template']=='cart'){
			$comments->module = 'catalog';
			$controller = 'cart';
		}
		$action = isset($action) ? $action : 'index';
//		
//		exit;
		if(!Router::getInstance()->replaceRoute($data, $action, $controller, $comments->module)){
			return "Такой URL уже существует!";
		}
		
		return '';
	}
	
	private function inLanguages($param){
		$version = $this->getVersion($param);
		
		return empty($version) ? false : true;
	}
	
}
