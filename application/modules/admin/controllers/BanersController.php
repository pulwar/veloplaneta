<?php
define('SP','/') ;
/**
 * Admin_PublicationsController
 * 
 * @author Grover
 * @version 1.0
 */

class Admin_BanersController extends MainAdminController  {
	
	/**
 	 * @var string
 	 */
	private $_curModule = null;	
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_onpage = 20;
	
	private $_page = null;
	/**
	 * offset
	 *
	 * @var int
	 */	
	private $_offset = null;	
	
	public function init(){
		$this->initView();
		$this->view->strictVars(false);
		$this->view->addScriptPath(DIR_SCRIPTS) ;
		$this->view->addHelperPath(DIR_HELPER , 'Gr_View_Helper') ;		
		$this->view->caption = 'Слайдеры';
		$this->view->place_id = $this->_getParam('place_id');		
		$lang = $this->_getParam('lang','ru');
		//var_dump($this->view->places_id);
		$this->view->currentModule = $this->_curModule = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		$this->_page = $this->_getParam('page', 1);	
		$this->_offset =($this->_page-1)*$this->_onpage;	
		$this->view->current_page = $this->_page;
		$this->view->onpage = $this->_onpage;
	
		
	}
	
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		
		$this->view->child_name = 'Все слайды';
		$this->view->currentModule = $this->_curModule;
		$place_id = $this->_getParam('place_id');		
				
		if ($place_id){
			$where = 'place_id='.(int)$place_id;
			$this->view->place_id = $place_id;
			$place = Places::getInstance()->find($place_id)->current();
			if (isset($place->name) && $place->name!='') {
				$this->view->child_name = 'Банеры в зоне '.$place->name;
			}
			$this->view->place_id = $place_id;
	
		} else $where = null;
		
		$this->view->total = count(Baners::getInstance()->fetchAll($where));		
		$this->view->all = Baners::getInstance()->fetchAll($where, 'ord ASC');
		
		
		
	
	}
	
	public function editAction(){
		
		if ($this->getParam('id')){
			$item = Baners::getInstance()->find($this->getParam('id'))->current();
		} else{
			$item = Baners::getInstance()->createRow();
		}
			
		$this->view->code = $this->getFck('edit[code]', '100%','200','Basic');
		//$this->view->description = $this->getFck('edit[description]', '100%','300');
		if ($this->_request->isPost()){						
			$data = $this->trimFilter($this->_getParam('edit'));
			if ($data['type']!='' && $data['place_id']){
				$item->setFromArray($data);
				$id =  $item->save();
				$img_name = $_FILES['image']['name'];
				$img_source = $_FILES['image']['tmp_name'];				
				$delete_img = $this->_getParam('delete_img');
				//$data['name'] = $img_name;
				if ($img_name!='' && $img_source!='' && !$delete_img){
					$ext = @end(explode('.', $img_name));
					$img = DIR_PUBLIC.'img/slider/'.$id.'_img.'.$ext;
					if($this->upload_file($img_source, $img )){
						$item->img = $id.'_img.'.$ext;
						$item->save();	
					}
									
				} else if ($delete_img){
					@unlink(DIR_PUBLIC.'img/slider/'.$item->img);					
					$item->img='';					
					$item->save();
				}
				$this->view->ok=1;
			} else{
				$this->view->err=1;				
			}
			
		}		
		$this->view->item = $item;	
		$this->view->child_name = 'Редактировать';		
		$this->view->places = Places::getInstance()->fetchAll(null, 'priority DESC');	
		
	}

	
	public function deleteAction(){
		$id = $this->_getParam('id');
		if ($id ){
			$item = Baners::getInstance()->find($id)->current();		
			@unlink(DIR_PUBLIC.'img/slider/'.$item->img);	
			$item->delete();
			//$path = $this->view->places_id ? '/index/places_id/'.$this->view->places_id : '';
			$this->_redirect($this->_curModule);
		}
	}
}