<?php

class Admin_WebformsController extends MainAdminController
{

	public function init(){
		Loader::loadPublicModel('Webforms');
		Loader::loadPublicModel('Pages');
	}
	
	public function indexAction()
	{
		$lang = $this->_getParam('lang');
		$this->view->lang = $lang;
		$all = Pages::getInstance()->getVersionPages($lang, 'webforms');
		$this->view->all = $all;
	}
	
	public function settingsAction(){
            $this->view->lang = $this->_getParam('lang');
            if($this->getRequest()->isPost()) {
                $array = $this->getRequest()->getParams();
                foreach ($array['add'] as $a) {
                    //echo "<pre>";
                    //print_r($a);
                       $arr = array(
                            'name'=>$a['name'],
                            'title'=>$a['title'],
                            'description'=>$a['description']
                        );
                    if (!empty($arr['title']) && isset($a['mail_id']) ) {
                        Webforms::getInstance()->updateEmails($arr,$a['mail_id']);
                        $this->view->message = "Данные сохранены";
                        
                        
                    }
                }
                
                
            }
            $this->view->settings = Webforms::getInstance()->getEmails();
        }
	
	public function addAction(){
		$error = '';	
		if($this->getRequest()->isPost()){
//			Zend_Debug::dump($this->_request->getParams());exit;
			$data = $this->getRequest()->getParams();
			$data['parent_id'] = $data['parent_id'] == 0 ? '1' : $data['parent_id'];
			$id = Pages::getInstance()->addPage($data, 'webforms');
			$data['id'] = $id;
			$error = $this->addRoute($data, 'index', 'index', 'webforms');
			
			if($error == ''){
				$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
				$this->_redirect("/admin/$lang/webforms/");
			}
			
		}
		
		$this->view->error = $error;
		$fck = $this->getFck('content', '90%', '400');
		$this->view->fck = $fck;
		$parentId = (int)$this->getRequest()->getParam('parent_id');
		$this->view->parent_id = $parentId;
		Loader::loadPublicModel('MenuTypes');
		$this->view->menu = MenuTypes::getInstance()->getAll();
		$this->view->url = Pages::getInstance()->generateStringPath(1, '/');
		Loader::loadPublicModel('Templates');
		$this->view->templates = Templates::getInstance()->getAll('webforms');
		$this->view->lang = $this->_getParam('lang');
	}
	
	public function editAction(){
		if(!$this->_hasParam('id')){
			$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
			$this->_redirect("/admin/$lang/webforms/");
		}
		
		if($this->getRequest()->isPost()){
			$data = $this->getRequest()->getParams();
			$error = $this->editRoute($data);
			Pages::getInstance()->editPage($data);
			$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
			$this->_redirect("/admin/$lang/webforms/");
		}
		
		$id = (int)$this->getRequest()->getParam('id');
		$this->view->page = Pages::getInstance()->getPage($id);
		Loader::loadPublicModel('PagesOptions');
		$this->view->options = PagesOptions::getInstance()->getPageOptions($id);
		Loader::loadPublicModel('Menu');
		Loader::loadPublicModel('MenuTypes');
		$this->view->menu = MenuTypes::getInstance()->getAll();
		$this->view->pageMenu = Menu::getInstance()->getMenuPage($id);
		
		$fck = $this->getFck('content', '90%', '400');
		$this->view->fck = $fck;
		Loader::loadPublicModel('Templates');
		$this->view->templates = Templates::getInstance()->getAll('webforms');
		$this->view->lang = $this->_getParam('lang');
	}
	
	public function activateAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Pages::getInstance()->pubPage($id);
		}
		$this->view->lang = $this->_getParam('lang');
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		$this->_redirect("/admin/$lang/webforms/");
	}
	
	public function unactivateAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Pages::getInstance()->unpubPage($id);
		}
		$lang = $this->_getParam('lang');
		$this->view->lang = $lang;
		$this->_redirect("/admin/$lang/webforms/");
	}
	
	public function deleteAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Loader::loadPublicModel('Basket');
			Basket::getInstance()->add($id);
		}
		
		$lang = $this->_getParam('lang');
		$this->view->lang = $lang;
		$this->_redirect("/admin/$lang/webforms/");
	}
	
	private function editRoute($data){
		Loader::loadCommon('Router');
		if ($data['template'] !='feedback' && $data['template'] !='zakaz' ){
			$action = $data['template'];
		}
			else $action = "index";
		if(!Router::getInstance()->replaceRoute($data, $action,'index' , 'webforms')){
			return "Такой URL уже существует!";
		}
		
		return '';
	}
	
}
