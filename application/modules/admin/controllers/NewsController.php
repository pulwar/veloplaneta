<?php
define('SP','/') ;
class Admin_NewsController extends MainAdminController
{

	public function init(){
		Loader::loadPublicModel('News');
		Loader::loadPublicModel('Pages');

	}

	public function indexAction(){
		$lang = $this->_getParam('lang','ru');
		$this->view->currentModul = $curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();

		$this->view->lang = $lang;
		$all = Pages::getInstance()->getVersionPages($lang, 'news');
		$this->view->all = $all;

	}

	public function itemsAction(){

		$sortSession = new Zend_Session_Namespace('sortSearch');
		$sort = $this->view->sort = $this->getSort($sortSession->sort);
		$lang = $this->getRequest()->getParam('lang','ru');

		/*$user_id = $this->_getParam('peoples_id');
		if ($user_id){
			$where = 'peoples_id='.(int)$user_id;
			$this->view->peoples_id = $user_id;
			$user = Peoples::getInstance()->find($user_id)->current();
			if (isset($user->name) && $user->name!='') {
				$this->view->child_name = 'Публикации сотрудника '.$user->name;
			}
		} else $where = null;*/

		if ($this->_getParam('sort', NULL) == 'nasc') {
			$sort = $this->view->sort = 'nASC';
			$sortSession->sort = 'nASC';
		} elseif ($this->_getParam('sort', NULL) == 'ndesc') {
			$sort = $this->view->sort = 'nDESC';
			$sortSession->sort = 'nDESC';
		}
		if ($this->_getParam('sort', NULL) == 'sasc') {
			$sort = $this->view->sort = 'sASC';
			$sortSession->sort = 'sASC';
		} elseif ($this->_getParam('sort', NULL) == 'sdesc') {
			$sort = $this->view->sort = 'sDESC';
			$sortSession->sort = 'sDESC';
		}
				$page = $this->view->page = $this->_getParam('page', 1);
				$onPage = $this->view->onPage = 15;
			$pageSession = new Zend_Session_Namespace('pageSearch');
			$pageSession->page = $page;

			$this->view->counter=($page-1)*$onPage;
	  $page_id = $this->_getParam('pageid');
	   $this->view->modul_page = Pages::getInstance()->getPage($page_id);
      $this->view->news = News::getInstance()->getAll(($page-1)*$onPage, $onPage,$sort,$page_id);
	  $newsCount = $this->view->newsCount = sizeof(News::getInstance()->getAll(null,null, null,$page_id));
	  $pagesCount = (int)$this->view->pagesCount = ceil($newsCount / $onPage);
      $this->view->currentModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
      $this->view->lang = $lang;

	}

        public function addAction(){
		$lang = $this->getRequest()->getParam('lang','ru');
		$this->view->currentModul = $curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
     	 $this->view->lang = $lang;
     	//  $this->view->peoples = Peoples::getInstance()->fetchAll(null, 'priority DESC');
     	  $page_id = $this->_getParam('pageid');
			$this->view->modul_page = $page = Pages::getInstance()->getPage($page_id);
			$this->view->page = $page;
		if ($this->_request->isPost()){

			$data = $this->getRequest()->getParams();


			$new = $data['new'];
			if ($new['created_at']!=''){
				$adate = preg_match('/([\d]{2}).+([\d]{2}).+([\d]{4})/is', $new['created_at'], $matches);
				$new['created_at'] = $matches[3].'-'.$matches[2].'-'.$matches[1];
			} else {
				$new['created_at'] = date('Y-m-d H:i:s');

			}
			$new['name']=trim($new['name']);
			if (isset($new['pub']) && $new['pub']=='on') {
				$new['pub'] = 1;
			}
			else {
				$new['pub'] = 0;
			}
			(isset($new['main']) && $new['main']=='on' ?	$new['main'] = 1 :$new['main'] = 0);
			if (isset($page)){
				$new['page_id']=$page->id;
				$new['type']=$page->template;
                                switch ($page->id){
                                    case 4: {$new['type'] = 'news'; break;}
                                    case 5: {$new['type'] = 'articles';break;}
                                    case 13: {$new['type'] = 'actions';break;}
                                }
			}
			News::getInstance()->addNew($new);
			$this->_redirect($curModul."/items/pageid/".$page_id);
		}

		$fck1 = $this->getFck('new[intro]', '90%', '200');
		$this->view->fck_intro = $fck1;
		$fck2 = $this->getFck('new[content]', '90%', '300');
		$this->view->fck_content = $fck2;



	}

	public function editpAction(){
		$id = (int)$this->getRequest()->getParam('id');
		$lang = $this->getRequest()->getParam('lang','ru');
		$this->view->currentModul = $curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
     	 $this->view->lang = $lang;
     	 $novost = News::getInstance()->getNewById($id);
     	// $this->view->peoples = Peoples::getInstance()->fetchAll(null, 'priority DESC');
     	 //print_r($novost->toArray());
     	 $page_id = $this->_getParam('pageid');
			$this->view->modul_page =$page = Pages::getInstance()->getPage($page_id);
		if ($this->_request->isPost()){
			$id = (int)$this->getRequest()->getParam('id');
			$data = $this->getRequest()->getParams();
			if ($data['new']['name']!='' &&  $data['new']['url']!='' ){
				$new = $data['new'];
				if ($new['created_at']!=''){
					$adate = preg_match('/([\d]{2}).+([\d]{2}).+([\d]{4})/is', $new['created_at'], $matches);
					$new['created_at'] = $matches[3].'-'.$matches[2].'-'.$matches[1];
				} else {
					$new['created_at'] = date('Y-m-d H:i:s');

				}
				$new['name']=trim($new['name']);
				if (isset($new['pub']) && $new['pub']=='on') {
					$new['pub'] = 1;
				}
				else {
					$new['pub'] = 0;
				}
				(isset($new['main']) && $new['main']=='on' ?	$new['main'] = 1 :$new['main'] = 0);
				News::getInstance()->editNew($new,$id);
				$this->editMeta('news', $id);
				$sortSession = new Zend_Session_Namespace('sortSearch');
				$pageSession = new Zend_Session_Namespace('pageSearch');

				if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";

				if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";

				$this->_redirect($curModul."/items/pageid/".$novost->page_id.$page_link.$sort_link);
			} else $this->view->err=1;
		}

		$fck1 = $this->getFck('new[intro]', '90%', '200');
		$this->view->fck_intro = $fck1;
		$fck2 = $this->getFck('new[content]', '90%', '300');
		$this->view->fck_content = $fck2;
		$this->view->options = $this->getMeta('news', $id);
		$novost->created_at = $this->dateFromDb($novost->created_at);
		$this->view->new = $novost;


	}

	public function pubAction(){

		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		if(!$this->_hasParam('id')) {
			$this->_redirect($curModul."/items/pageid/".$this->_getParam('pageid'));
		}
		else {
			$id = (int)$this->getRequest()->getParam('id');
			News::getInstance()->pubNew($id);

			$sortSession = new Zend_Session_Namespace('sortSearch');
			$pageSession = new Zend_Session_Namespace('pageSearch');

			if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";

			if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";

			$this->_redirect($curModul."/items/pageid/".$this->_getParam('pageid').$page_link.$sort_link);

		}

	}

	public function unpubAction(){

		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		if(!$this->_hasParam('id')) {
			$this->_redirect($curModul."/items/");
		}
		else {
			$id = (int)$this->getRequest()->getParam('id');
			News::getInstance()->unpubNew($id);
			$sortSession = new Zend_Session_Namespace('sortSearch');
			$pageSession = new Zend_Session_Namespace('pageSearch');

			if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";

			if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";

			$this->_redirect($curModul."/items/pageid/".$this->_getParam('pageid').$page_link.$sort_link);
		}
	}

	public function copyAction(){
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		if(!$this->_hasParam('id')) {
			$this->_redirect($curModul."/items/");
		}
		else {
			$id = (int)$this->getRequest()->getParam('id');
			News::getInstance()->CopyNew($id);
			$sortSession = new Zend_Session_Namespace('sortSearch');
			$pageSession = new Zend_Session_Namespace('pageSearch');

			if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";

			if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";

			$this->_redirect($curModul."/items/pageid/".$this->_getParam('pageid').$page_link.$sort_link);
		}

	}

	public function checkurlAction(){
		$url = $this->_getParam('url', '');
		$id = $this->_getParam('id', '');
		if ($url){
			$item = News::getInstance()->fetchRow("url='$url' AND id!='$id'");
			if ($item!=null){
				echo 'err';
			} else echo 'ok';

		}
		exit;
	}

	public function deleteAction(){
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		if(!$this->_hasParam('id')) {
			$this->_redirect($curModul."/items/");
		}
		else {
			$id = (int)$this->getRequest()->getParam('id');
			News::getInstance()->deleteNew($id);
			$sortSession = new Zend_Session_Namespace('sortSearch');
			$pageSession = new Zend_Session_Namespace('pageSearch');

			if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";

			if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";

			$this->_redirect($curModul."/items/pageid/".$this->_getParam('pageid').$page_link.$sort_link);
		}

	}

	public function setmainAction(){
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		if(!$this->_hasParam('id')) {
			$this->_redirect($curModul."/items/");
		}
		else {
			$id = (int)$this->getRequest()->getParam('id');
			News::getInstance()->setMainNew($id);
			$sortSession = new Zend_Session_Namespace('sortSearch');
			$pageSession = new Zend_Session_Namespace('pageSearch');

			if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";

			if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";

			$this->_redirect($curModul."/items/pageid/".$this->_getParam('pageid').$page_link.$sort_link);
		}
	}


	public function unsetmainAction(){
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		if(!$this->_hasParam('id')) {
			$this->_redirect($curModul."/items/");
		}
		else {
			$id = (int)$this->getRequest()->getParam('id');
			News::getInstance()->unsetMainNew($id);
			$sortSession = new Zend_Session_Namespace('sortSearch');
			$pageSession = new Zend_Session_Namespace('pageSearch');

			if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";

			if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";

			$this->_redirect($curModul."/items/pageid/".$this->_getParam('pageid').$page_link.$sort_link);
		}
	}
	//======= pages===================================================


	public function addpAction(){
		$error = '';
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		$this->view->currentModul = $curModul;

		if($this->getRequest()->isPost()){
//			Zend_Debug::dump($this->_request->getParams());exit;
			$data = $this->getRequest()->getParams();
			$data['parent_id'] = $data['parent_id'] == 0 ? '1' : $data['parent_id'];
			$id = Pages::getInstance()->addPage($data, 'news');
			$data['id'] = $id;
			$error = $this->addRoute($data, 'index', 'index', 'news');

			if($error == ''){
				$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
				$this->_redirect("$curModul");
			}

		}

		$this->view->error = $error;
		$fck = $this->getFck('content', '90%', '400');
		$this->view->fck = $fck;
		$this->view->introText = $this->getFck('introText', '90%', '150','Basic');
		$parentId = (int)$this->getRequest()->getParam('parent_id');
		$this->view->parent_id = $parentId;
		Loader::loadPublicModel('MenuTypes');
		$this->view->menu = MenuTypes::getInstance()->getAll();
		$this->view->url = Pages::getInstance()->generateStringPath(1, '/');
		Loader::loadPublicModel('Templates');
		$this->view->templates =$tpl = Templates::getInstance()->getAll('news');

		$this->view->lang = $this->_getParam('lang');
	}


	public function editAction(){
		if(!$this->_hasParam('id')){
			$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
			$this->_redirect("/admin/$lang/news/");
		}

		if($this->getRequest()->isPost()){
			$data = $this->getRequest()->getParams();
			$error = $this->editRoute($data);
			Pages::getInstance()->editPage($data);
			$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
			$this->_redirect("/admin/$lang/news/");
		}

		$id = (int)$this->getRequest()->getParam('id');
		$this->view->page = $page = Pages::getInstance()->getPage($id);
		Loader::loadPublicModel('PagesOptions');
		$this->view->options = PagesOptions::getInstance()->getPageOptions($id);
		Loader::loadPublicModel('Menu');
		Loader::loadPublicModel('MenuTypes');
		$this->view->menu = MenuTypes::getInstance()->getAll();
		$this->view->pageMenu = Menu::getInstance()->getMenuPage($id);

		$fck = $this->getFck('content', '90%', '400');
		$this->view->fck = $fck;
		$this->view->introText = $this->getFck('introText', '90%', '150','Basic');
		Loader::loadPublicModel('Templates');
		if ($page->deleteble==1){
			$this->view->templates = Templates::getInstance()->getAll('news');
		} else $this->view->templates = Templates::getInstance()->getTemplateName($page->template, 'news');
		//$this->view->templates = Templates::getInstance()->getAll('news');
		$this->view->lang = $this->_getParam('lang');
	}

	public function activateAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Pages::getInstance()->pubPage($id);
		}
		$this->view->lang = $this->_getParam('lang');
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		$this->_redirect("/admin/$lang/news/");
	}

	public function unactivateAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Pages::getInstance()->unpubPage($id);
		}
		$lang = $this->_getParam('lang');
		$this->view->lang = $lang;
		$this->_redirect("/admin/$lang/news/");
	}

	public function deletepAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Loader::loadPublicModel('Basket');
			Basket::getInstance()->add($id);
		}

		$lang = $this->_getParam('lang');
		$this->view->lang = $lang;
		$this->_redirect("/admin/$lang/news/");
	}

	private function getSort($sort)
	{
		if (!$sort) {
			return  NULL;
		} elseif ($sort == 'nASC') {
			return 'nASC';
		} elseif ($sort == 'nDESC') {
			return 'nDESC';

		} elseif ($sort == 'fASC') {
			return 'fASC';
		} elseif ($sort == 'fDESC') {
			return 'fDESC';

		} elseif ($sort == 'eASC') {
			return 'eASC';
		} elseif ($sort == 'eDESC') {
			return 'eDESC';

		} elseif ($sort == 'gASC') {
			return 'gASC';
		} elseif ($sort == 'gDESC') {
			return 'gDESC';

		} elseif ($sort == 'sASC') {
			return 'sASC';
		} elseif ($sort == 'sDESC') {
			return 'sDESC';

		} elseif ($sort == 'lASC') {
			return 'lASC';
		} elseif ($sort == 'lDESC') {
			return 'lDESC';
		}  elseif ($sort == 'newASC') {
			return 'newASC';
		}



		else {
			return NULL;
		}
	}


	private function getSortParam($sortParam)
	{
		if ($sortParam == 'nasc') {
			return  'nASC';
		} elseif ($sortParam == 'ndesc') {
			return 'nDESC';
		}
		if ($sortParam == 'casc') {
			return 'cASC';
		} elseif ($sortParam == 'cdesc') {
			return 'cDESC';
		}

		if ($sortParam == 'easc') {
			return 'eASC';
		} elseif ($sortParam == 'edesc') {
			return 'eDESC';
		}

		if ($sortParam == 'dasc') {
			return 'dASC';
		} elseif ($sortParam == 'ddesc') {
			return 'dDESC';
		}

		if ($sortParam == 'sasc') {
			return 'sASC';
		} elseif ($sortParam == 'sdesc') {
			return 'sDESC';
		}
		if ($sortParam == 'pasc') {
			return 'pASC';
		} elseif ($sortParam == 'pdesc') {
			return 'pDESC';
		} if ($sortParam == 'newdesc') {
			return 'newDESC';
		}



		elseif ($this->_getParam('sort', NULL) == 'prior') {
			return NULL;
		}
	}



	private function editRoute($data){
		Loader::loadCommon('Router');

		if(!Router::getInstance()->replaceRoute($data, 'index', 'index', 'news')){
			return "Такой URL уже существует!";
		}

		return '';
	}


}
