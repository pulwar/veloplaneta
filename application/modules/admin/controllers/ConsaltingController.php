<?php

class Admin_ConsaltingController extends MainAdminController
{

	public function init(){
		Loader::loadPublicModel('Webforms');
		Loader::loadPublicModel('Pages');
	}
	
	public function indexAction()
	{
		$lang = $this->_getParam('lang');
		$this->view->lang = $lang;
		$all = Pages::getInstance()->getVersionPages($lang, 'consalting');
		$this->view->all = $all;
	}
	
	public function settingsAction(){
		$this->view->settings = Webforms::getInstance()->getEmails();
		$this->view->lang = $this->_getParam('lang');
	}
	
	public function addAction(){
		$error = '';
		
		
		if($this->getRequest()->isPost()){
//			Zend_Debug::dump($this->_request->getParams());exit;
			$data = $this->getRequest()->getParams();
			$data['parent_id'] = $data['parent_id'] == 0 ? '1' : $data['parent_id'];
			$id = Pages::getInstance()->addPage($data, 'consalting');
			$data['id'] = $id;
			$error = $this->addRoute($data, 'index', 'index', 'consalting');
			
			if($error == ''){
				$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
				$this->_redirect("/admin/$lang/consalting/");
			}
			
		}
		
		$this->view->error = $error;
		$fck = $this->getFck('content', '90%', '400');
		$this->view->fck = $fck;
		$parentId = (int)$this->getRequest()->getParam('parent_id');
		$this->view->parent_id = $parentId;
		Loader::loadPublicModel('MenuTypes');
		$this->view->menu = MenuTypes::getInstance()->getAll();
		$this->view->url = Pages::getInstance()->generateStringPath(1, '/');
		Loader::loadPublicModel('Templates');
		$this->view->templates = Templates::getInstance()->getAll('consalting');
		$this->view->lang = $this->_getParam('lang');
	}
	
	public function editAction(){
		if(!$this->_hasParam('id')){
			$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
			$this->_redirect("/admin/$lang/consalting/");
		}
		
		if($this->getRequest()->isPost()){
			$data = $this->getRequest()->getParams();
			$error = $this->editRoute($data);
			Pages::getInstance()->editPage($data);
			$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
			$this->_redirect("/admin/$lang/consalting/");
		}
		
		$id = (int)$this->getRequest()->getParam('id');
		$this->view->page = Pages::getInstance()->getPage($id);
		Loader::loadPublicModel('PagesOptions');
		$this->view->options = PagesOptions::getInstance()->getPageOptions($id);
		Loader::loadPublicModel('Menu');
		Loader::loadPublicModel('MenuTypes');
		$this->view->menu = MenuTypes::getInstance()->getAll();
		$this->view->pageMenu = Menu::getInstance()->getMenuPage($id);
		
		$fck = $this->getFck('content', '90%', '400');
		$this->view->fck = $fck;
		Loader::loadPublicModel('Templates');
		$this->view->templates = Templates::getInstance()->getAll('consalting');
		$this->view->lang = $this->_getParam('lang');
	}
	
	public function activateAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Pages::getInstance()->pubPage($id);
		}
		$this->view->lang = $this->_getParam('lang');
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		$this->_redirect("/admin/$lang/consalting/");
	}
	
	public function unactivateAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Pages::getInstance()->unpubPage($id);
		}
		$lang = $this->_getParam('lang');
		$this->view->lang = $lang;
		$this->_redirect("/admin/$lang/consalting/");
	}
	
	public function deleteAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Loader::loadPublicModel('Basket');
			Basket::getInstance()->add($id);
		}
		
		$lang = $this->_getParam('lang');
		$this->view->lang = $lang;
		$this->_redirect("/admin/$lang/consalting/");
	}
	
	
	
	
	
	public function allvAction(){
		Loader::loadPublicModel('Consalting');
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		$this->view->lang = $lang;
		$this->view->all = Consalting::getInstance()->fetchAll(null, 'prior DESC');
				
	}
	
	public function addvoprosAction(){
		
		$fck = $this->getFck('add[otvet]', '100%', '300', 'Basic');
		$this->view->fck = $fck;	
		if($this->getRequest()->isPost()){
			$error =0;				
			$data =$this->getRequest()->getParam('add');
			$data = $this->trimFilter($data);
			$data['approved']=1;
			if (!$data['vopros']){
					$error =1;
					$this->view->error = $error;
			}				
			if (!$error){
				Consalting::getInstance()->createRow($data)->save();								        
		        $this->_redirect("/admin/all/consalting/allv");
	        
			}
	     
		} 
		
		
		
	}
	
	public function editvAction(){
		
		$id =$this->getRequest()->getParam('id');
		if (isset($id)) {
			Loader::loadPublicModel('Consalting');
			$vopros = Consalting::getInstance()->find($id)->current();
			
			$fck = $this->getFck('add[otvet]', '100%', '300','Basic');
		    $this->view->fck = $fck;
			
			if($this->getRequest()->isPost()){
				$error =0;			
				$data =$this->getRequest()->getParam('add');
				$data = $this->trimFilter($data);	
				$data['added'] = $this->dateToDb($data['added']);	
				//print_r($data['added']);		
				if (!$data['vopros']){
					$error =1;
					$this->view->error = $error;
				}
				
				if (!$error){
					$vopros->setFromArray($data)->save();			        
			       // $this->_redirect("/admin/all/consalting/allv");
		        
				}
			 }
			 $vopros->added = $this->dateFromDb($vopros->added);						
			 $this->view->vopros =$vopros;	
				
		}	else $this->_redirect("/admin/all/consalting/allv");	
		
		
	}
	
	public function deletevAction(){
		
		$id =$this->getRequest()->getParam('id');
		if (isset($id)) {			
			$item = Consalting::getInstance()->find($id)->current();
			if ($item->id){
				$item->delete();
			}
			$this->_redirect("/admin/all/consalting/allv");
		}
	
	}
	
	public function activatevAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			$item = Consalting::getInstance()->find($id)->current();
			if ($item->id){
				$item->approved=1;
				$item->save();
			}
		}
		$this->_redirect("/admin/all/consalting/allv");
		
	}
	
	public function unactivatevAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			$item = Consalting::getInstance()->find($id)->current();
			if ($item->id){
				$item->approved=0;
				$item->save();
			}
		}
		$this->_redirect("/admin/all/consalting/allv");
		
	}
	
	private function editRoute($data){
		Loader::loadCommon('Router');		
		if(!Router::getInstance()->replaceRoute($data, 'index', 'index', 'consalting')){
			return "Такой URL уже существует!";
		}
		
		return '';
	}
	
}
