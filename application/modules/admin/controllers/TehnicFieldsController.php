<?php

class Admin_TehnicfieldsController extends MainAdminController 
{
	
	
	public function init()
	{
		Loader::loadPublicModel('Catalog_tehnic_filds');
		
	}
	
	
	public function indexAction()
	{
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();			
		$this->view->currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
		$this->view->teh_fields = Catalog_tehnic_filds::getInstance()->getAll();
		//Loader::loadPublicModel('Catalog_Division');
		//$main_divs = Catalog_Division::getInstance()->getChilds(0);
		//$this->view->fields = $main_divs;
		//print_r(Catalog_tehnic_filds::getInstance()->getAllByDivId())
		
		
	}
	
	
	public function addAction(){
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();			
		$this->view->currentModul =$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();		
		
		
		if ($this->_request->isPost()){
			$data= $this->_getParam('add');
			$data['name']=$this->filter($data['name']);
			if ($data['name']!=''){
				Catalog_tehnic_filds::getInstance()->addItem($data);
				$this->_redirect('/admin/'.$currentModul.'/index/');
			}
			
		}
		//$this->view->teh_fields = Catalog_tehnic_filds::getInstance()->getAllByDivId($div_id);
		
	}
	
	
	
	public function fieldsAction(){
		
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();			
		$this->view->currentModul =$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
		
		Loader::loadPublicModel('Catalog_tehnic_filds');
		
		$this->view->div_id = $div_id = $this->_getParam('divid');
		if (!isset($div_id)){
			$this->_redirect('/admin/ru/catalogtovar/fields/');
		}
		
		$this->view->teh_fields = Catalog_tehnic_filds::getInstance()->getAllByDivId($div_id);
		
	}
	
	public function fieldaddAction(){
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();			
		$this->view->currentModul =$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
		
		Loader::loadPublicModel('Catalog_tehnic_filds');
		$this->view->div_id = $div_id = $this->_getParam('divid');
		if (!isset($div_id)){
			//$this->_redirect('/admin/ru/catalogtovar/fields/');
		}
		if ($this->_request->isPost()){
			$data= $this->_getParam('add');
			$data['name']=$this->filter($data['name']);
			if ($data['name']!=''){
				Catalog_tehnic_filds::getInstance()->addItem($data);
				$this->_redirect('/admin/ru/catalogtovar/fields/divid/'.$div_id);
			}
			
		}
		//$this->view->teh_fields = Catalog_tehnic_filds::getInstance()->getAllByDivId($div_id);
		
	}
	
	
	public function activateAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Muzey::getInstance()->setPubItem($id);
		}
		
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';	
		// eсли запрос пришел от аякса
		if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
		{
 		 $this->_redirect("/admin/$lang/muzey/index/tpl/no/");
		}	
		
		$this->_redirect("/admin/$lang/muzey/");
	}
	
	public function unactivateAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Muzey::getInstance()->unsetPubItem($id);
		}
		
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		
		// eсли запрос пришел от аякса
		if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
		{
 		 $this->_redirect("/admin/$lang/muzey/index/tpl/no/");
		}	
		
		$this->_redirect("/admin/$lang/muzey/");
	}
	
	public function deleteAction(){
		 $this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();			
		$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
		
		Loader::loadPublicModel('Catalog_tehnic_filds_values');
		
		if($this->_hasParam('tovarid')){
			$tovar_id = (int)$this->getRequest()->getParam('tovarid');
			$tovar = Catalog_Tovar::getInstance()->getTovarById($tovar_id);
			
			if (isset($tovar)){
			Catalog_Tovar::getInstance()->deleteTovar($tovar_id);
			Catalog_tehnic_filds_values::getInstance()->deleteValuesByTovarId($tovar_id);
			}
		}
		
		
		
		
		$this->_redirect("/admin/".$currentModul."/items/divid/".$tovar->division_id);
	}
	
	public function copyAction(){
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();			
		$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
		
		Loader::loadPublicModel('Catalog_tehnic_filds_values');
		
		if($this->_hasParam('tovarid')){
			$tovar_id = (int)$this->getRequest()->getParam('tovarid');
			$tovar = Catalog_Tovar::getInstance()->getTovarById($tovar_id)->toArray();
			$teh_fields_values = Catalog_tehnic_filds_values::getInstance()->getAllByTovarId($tovar_id);
			unset($tovar['id']);
			unset($tovar['created_at']);
			$tovar['name'].='_copy';
			
			if (isset($tovar)){
				$new_tovar_id = Catalog_Tovar::getInstance()->addTovar($tovar);
				foreach ($teh_fields_values as $value){
				$insert_value = Array('tehnic_fild_id'=>$value->tehnic_fild_id,
				                     'tovar_id'=>$new_tovar_id,
				                     'value'=>$value->value);
				Catalog_tehnic_filds_values::getInstance()->addItem($insert_value);
				}
			}
		}
		
		
		
		
		$this->_redirect("/admin/".$currentModul."/items/divid/".$tovar['division_id']);
	
		
	}
	
	public function itemsAction(){
		Loader::loadPublicModel('Catalog_Division');
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();			
		$this->view->currentModul =$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
		if($this->_hasParam('divid')){
		$this->view->div_id=	$div_id= $this->_getParam('divid');
		$this->view->division = Catalog_Division::getInstance()->getItemById($div_id);
		$this->view->tovars= $tovars = Catalog_Tovar::getInstance()->getAllTovarsByDivId($div_id);
			
		}
		
	}
	
	public function editfieldAction(){
		
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();			
		$this->view->currentModul =$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
		print_r($this->request);
		Loader::loadPublicModel('Catalog_tehnic_filds');
		$this->view->div_id = $div_id = $this->_getParam('divid');
		$this->view->field_id = $field_id = $this->_getParam('fid');
		
			$this->view->field = Catalog_tehnic_filds::getInstance()->getFieldById($field_id);
	
		if ($this->_request->isPost()){
			$data= $this->_getParam('add');
			$data['name']=$this->filter($data['name']);
			if ($data['name']!=''){
				Catalog_tehnic_filds::getInstance()->editItem($data,$field_id);
				$this->_redirect('/admin/'.$currentModul.'/index/');
			}
			
		}
		
	}
	
	public function delfieldAction(){
		
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();			
		$this->view->currentModul =$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
		
		Loader::loadPublicModel('Catalog_tehnic_filds');
		Loader::loadPublicModel('Catalog_tehnic_filds_values');
		
		$this->view->field_id = $field_id = $this->_getParam('fid');
		if (isset($field_id)){
			Catalog_tehnic_filds::getInstance()->deleteItem($field_id);
			Catalog_tehnic_filds_values::getInstance()->deleteValuesByFieidId($field_id);
			
			$this->_redirect('/admin/'.$currentModul.'/index/');
		}
		
		
	}
	

	private function filter($param){
			// Предоставляет возможность создания цепочек фильтров
			require_once 'Zend/Filter.php';

			// Фильтры, нужные для примера
			require_once 'Zend/Filter/StringTrim.php';
			require_once 'Zend/Filter/StripTags.php';
			//require_once 'Zend/Filter/Alnum.php';
			
			

			// Создание цепочки фильтров и добавление в нее фильтров
			$filter = new Zend_Filter();
			$filter->addFilter(new Zend_Filter_StringTrim())
					//->addFilter(new Zend_Filter_Alnum())	
			         ->addFilter(new Zend_Filter_StripTags());
			
			// Фильтрация 
			return $filter->filter($param);
	}
}
