<?php
class Admin_TestController extends Zend_Controller_Action
{
    public function indexAction(){
    	
		print_r($this->_request->getParams());

		if($this->_hasParam('node') && $this->_hasParam('target') && $this->_hasParam('point')){
			Loader::loadPublicModel('Pages');
			$node = (int)$this->_getParam('node');
			$target = (int)$this->_getParam('target');
			$point = (string)$this->_getParam('point');
			$pages = Pages::getInstance();
			
//			if($pages->isReplace($node, $target)){
				$pages->replace($node, $target, $point);
//			}
		}
    }
    
    
	private function formatBytes($val, $digits = 3, $mode = "SI", $bB = "B"){ //$mode == "SI"|"IEC", $bB == "b"|"B"
	   $si = array("", "K", "M", "G", "T", "P", "E", "Z", "Y");
	   $iec = array("", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi", "Yi");
	   switch(strtoupper($mode)) {
	       case "SI" : $factor = 1000; $symbols = $si; break;
	       case "IEC" : $factor = 1024; $symbols = $iec; break;
	       default : $factor = 1000; $symbols = $si; break;
	   }
	   switch($bB) {
	       case "b" : $val *= 8; break;
	       default : $bB = "B"; break;
	   }
	   for($i=0;$i<count($symbols)-1 && $val>=$factor;$i++)
	       $val /= $factor;
	   $p = strpos($val, ".");
	   if($p !== false && $p > $digits) $val = round($val);
	   elseif($p !== false) $val = round($val, $digits-$p);
	   return round($val, $digits) . " " . $symbols[$i] . $bB;
	}
	
	public function testAction(){
		Loader::loadPublicModel('Pages');
		$tree = Pages::getInstance()->getTree();
//		Zend_Debug::dump($tree[0]);exit;
		$this->view->root = $tree[0];
		
		$tree = $tree[0]['children'];
		
//		$ch = array(array('cls' => 'file', 'text' => 'qwe', 'id'=>'user_idsq', 'leaf' => 'true'));
//		$nodes = array(array('cls' => 'file', 'text' => 'file1', 'id'=>'user_ids', 'leaf' => 'true'),
//			array('cls' => 'folder', 'text' => 'folder', 'id'=>'user_id', 'leaf' => 0, 'children' => $ch));
		$this->view->ch=Zend_Json::encode($tree);

	}
	
	public function test2Action(){
		Loader::loadPublicModel('Pages');
		$tree = Pages::getInstance()->getTree2();
//		Zend_Debug::dump($tree[0]);exit;
		$this->view->root = $tree[0];
		
		$tree[0]['duration'] = "<a href ='#' style='margin-left: 125px'><img src='/test/resources/images/default/tree/actions/plus_krug.gif' onclick='javascript:window.location()'/></a>";
//		$ch = array(array('cls' => 'file', 'text' => 'qwe', 'id'=>'user_idsq', 'leaf' => 'true'));
//		$nodes = array(array('cls' => 'file', 'text' => 'file1', 'id'=>'user_ids', 'leaf' => 'true'),
//			array('cls' => 'folder', 'text' => 'folder', 'id'=>'user_id', 'leaf' => 0, 'children' => $ch));
		$this->view->ch=Zend_Json::encode($tree);

	}
	
	public function test3Action(){
		Loader::loadPublicModel('Pages');
		$tree = Pages::getInstance()->getTree();
//		Zend_Debug::dump($tree[0]);exit;
		$this->view->root = $tree[0];
		
		$tree = $tree[0]['children'];
		
//		$ch = array(array('cls' => 'file', 'text' => 'qwe', 'id'=>'user_idsq', 'leaf' => 'true'));
//		$nodes = array(array('cls' => 'file', 'text' => 'file1', 'id'=>'user_ids', 'leaf' => 'true'),
//			array('cls' => 'folder', 'text' => 'folder', 'id'=>'user_id', 'leaf' => 0, 'children' => $ch));
		$this->view->ch=Zend_Json::encode($tree);

	}

}