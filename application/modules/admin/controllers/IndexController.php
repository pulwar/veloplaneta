<?php
define('COUNT', 50);

class Admin_IndexController extends MainAdminController 
{
	/**
	 * Аутентификация
	 *
	 */
	public function indexAction(){
		$this->getHelper('LayoutManager')->disableLayouts();

		if(Security::getInstance()->checkAdminAllow()){
			$this->_redirect('/admin/ru/pages/');	//отправляем на управление содержимым
		}
		elseif ($this->_request->isPost()){
			$username = $this -> getRequest()->getParam('login');
			$password = $this -> getRequest()->getParam('pwd');

			if(!$this->view->error = Security::getInstance()->checkAdminLogin($username, $password)){
				$this->_redirect('/admin/ru/pages/');	//отправляем на управление содержимым
			}
		}
	}
}
