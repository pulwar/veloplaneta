<?php
define('SP','/') ;
/**
 * Admin_PublicationsController
 * 
 * @author Grover
 * @version 1.0
 */

class Admin_PlacesController extends MainAdminController  {
	
	/**
 	 * @var string
 	 */
	private $_curModule = null;	
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_onpage = 20;
	
	private $_page = null;
	/**
	 * offset
	 *
	 * @var int
	 */	
	private $_offset = null;	
	
	public function init(){
		$this->initView();
		$this->view->strictVars(false);
		$this->view->addScriptPath(DIR_SCRIPTS) ;
		$this->view->addHelperPath(DIR_HELPER , 'Gr_View_Helper') ;		
		$this->view->caption = 'Зоны банеров';
		$this->view->peoples_id = $this->_getParam('peoples_id');		
		$lang = $this->_getParam('lang','ru');
		//var_dump($this->view->peoples_id);
		$this->view->currentModule = $this->_curModule = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		$this->_page = $this->_getParam('page', 1);	
		$this->_offset =($this->_page-1)*$this->_onpage;	
		$this->view->current_page = $this->_page;
		$this->view->onpage = $this->_onpage;
	
		
	}
	
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		
		$this->view->child_name = 'Все зоны';
		$this->view->currentModule = $this->_curModule;
		$user_id = $this->_getParam('peoples_id');		
				
		/*if ($user_id){
			$where = 'peoples_id='.(int)$user_id;
			$this->view->peoples_id = $user_id;
			$user = Peoples::getInstance()->find($user_id)->current();
			if (isset($user->name) && $user->name!='') {
				$this->view->child_name = 'Публикации сотрудника '.$user->name;
			}
		} else $where = null;*/
		//$this->view->total = count(Places::getInstance()->fetchAll($where));		
		$this->view->all = Places::getInstance()->fetchAll(null, 'priority DESC');
		
		
		
	
	}
	
	public function editAction(){
		
		if ($this->getParam('id')){
			$item = Places::getInstance()->find($this->getParam('id'))->current();
		} else{
			$item = Places::getInstance()->createRow();
		}
			
		//$this->view->intro = $this->getFck('edit[intro]', '100%','150','Basic');
		//$this->view->description = $this->getFck('edit[description]', '100%','300');
		if ($this->_request->isPost()){						
			$data = $this->trimFilter($this->_getParam('edit'));
			if ($data['name']!=''){
				$item->setFromArray($data);
				$id =  $item->save();
				$this->view->ok=1;
			} else{
				$this->view->err=1;				
			}
			
		}
		
		$this->view->item = $item;	
		$this->view->child_name = 'Редактировать';			
		
	}

	
	public function deleteAction(){
		$id = $this->_getParam('id');
		if ($id ){
			$item = Places::getInstance()->find($id)->current();
			$baners = Baners::getInstance()->fetchAll('place_id='.(int)$item->id);
			if ($baners){
				foreach ($baners as $baner){
					@unlink(DIR_PUBLIC.'pics/baners/'.$baner->img);
					$baner->delete();
				}
			}
			$item->delete();
			//$path = $this->view->peoples_id ? '/index/peoples_id/'.$this->view->peoples_id : '';
			$this->_redirect($this->_curModule);
		}
	}
}