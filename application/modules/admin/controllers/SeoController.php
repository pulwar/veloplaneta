<?php
define('SP','/') ;
class Admin_SeoController extends MainAdminController
{

    public function init()
    {
        Loader::loadPublicModel('Seo');
    }

    public function indexAction()
    {
        $lang = $this->_getParam('lang', 'ru');
        $this->view->currentModul = $curModul = SP . 'admin' . SP . $lang . SP . $this->getRequest()->getControllerName();

        $this->view->lang = $lang;
        $all = Seo::getInstance()->getAll();
        $this->view->all = $all;
    }

    public function addAction()
    {
        $lang = $this->getRequest()->getParam('lang', 'ru');
        $this->view->currentModul = $curModul = SP . 'admin' . SP . $lang . SP . $this->getRequest()->getControllerName();
        $this->view->lang = $lang;
        if ($this->_request->isPost()) {
            $data = $this->getRequest()->getParams();

            $new = $data['seo'];
            $new['name'] = trim($data['name']);
            $new['title'] = trim($data['title']);
            $new['h1'] = trim($data['h1']);
            $new['description'] = trim($data['description']);
            if (isset($data['active']) && $data['active'] == 'on') {
                $new['active'] = 1;
            } else {
                $new['active'] = 0;
            }
            if ($data['div_id'] != '0')
                $new['div_id'] = $data['div_id'];
            if ($data['bike_type'] != '0')
                $new['type'] = $data['bike_type'];
            if ($data['purpose'] != '0')
                $new['purpose'] = $data['purpose'];
            Seo::getInstance()->add($new);
            $this->_redirect($curModul);
        }

        Loader::loadPublicModel('Catalog_Division');
        $this->view->divs = Catalog_Division::getInstance()->getAllBySort('name');
        $this->view->bikeType = Catalog_Tovar::getInstance()->_bikeType;
        $this->view->humanType = Catalog_Tovar::getInstance()->_humanType;

        $fck1 = $this->getFck('seo[text_upper]', '90%', '300');
        $this->view->text_upper = $fck1;
        $fck2 = $this->getFck('seo[text_bottom]', '90%', '300');
        $this->view->text_bottom = $fck2;
    }

    public function editAction()
    {
        $id = (int)$this->getRequest()->getParam('id');
        $lang = $this->getRequest()->getParam('lang', 'ru');
        $this->view->currentModul = $curModul = SP . 'admin' . SP . $lang . SP . $this->getRequest()->getControllerName();
        $this->view->lang = $lang;
        $item = Seo::getInstance()->getById($id);
        if ($this->_request->isPost()) {
            $id = (int)$this->getRequest()->getParam('id');
            $data = $this->getRequest()->getParams();
            if ($data['name'] != '') {
                $new = $data['seo'];
                $new['name'] = trim($data['name']);
                $new['title'] = trim($data['title']);
                $new['h1'] = trim($data['h1']);
                $new['description'] = trim($data['description']);
                if (isset($data['active']) && $data['active'] == 'on') {
                    $new['active'] = 1;
                } else {
                    $new['active'] = 0;
                }
                if ($data['div_id'] != '0')
                    $new['div_id'] = $data['div_id'];
                else
                    $new['div_id'] = null;
                if ($data['bike_type'] != '0')
                    $new['type'] = $data['bike_type'];
                else
                    $new['type'] = null;
                if ($data['purpose'] != '0')
                    $new['purpose'] = $data['purpose'];
                else
                    $new['purpose'] = null;
                Seo::getInstance()->edit($new, $id);

                $this->_redirect($curModul);
            } else $this->view->err = 1;
        }

        Loader::loadPublicModel('Catalog_Division');
        $this->view->divs = Catalog_Division::getInstance()->getAllBySort('name');
        $this->view->bikeType = Catalog_Tovar::getInstance()->_bikeType;
        $this->view->humanType = Catalog_Tovar::getInstance()->_humanType;

        $fck1 = $this->getFck('seo[text_upper]', '90%', '300');
        $this->view->text_upper = $fck1;
        $fck2 = $this->getFck('seo[text_bottom]', '90%', '300');
        $this->view->text_bottom = $fck2;

        $this->view->item = $item;
    }

    public function pubAction()
    {
        $lang = $this->getRequest()->getParam('lang', 'ru');
        $curModul = SP . 'admin' . SP . $lang . SP . $this->getRequest()->getControllerName();
        if ($this->_hasParam('id')) {
            $id = (int)$this->getRequest()->getParam('id');
            Seo::getInstance()->pub($id);
        }
        $this->_redirect($curModul);
    }

    public function unpubAction()
    {
        $lang = $this->getRequest()->getParam('lang', 'ru');
        $curModul = SP . 'admin' . SP . $lang . SP . $this->getRequest()->getControllerName();
        if ($this->_hasParam('id')) {
            $id = (int)$this->getRequest()->getParam('id');
            Seo::getInstance()->unpub($id);
        }
        $this->_redirect($curModul);
    }

    public function deleteAction()
    {
        $lang = $this->getRequest()->getParam('lang', 'ru');
        $curModul = SP . 'admin' . SP . $lang . SP . $this->getRequest()->getControllerName();
        if ($this->_hasParam('id')) {
            $id = (int)$this->getRequest()->getParam('id');
            Seo::getInstance()->deleteSeo($id);
        }
        $this->_redirect($curModul);
    }
}
