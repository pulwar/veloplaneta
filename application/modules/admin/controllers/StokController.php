<?php
define('SP','/') ;
class Admin_StokController extends MainAdminController
{

	public function init(){
		Loader::loadPublicModel('Stok');
		Loader::loadPublicModel('Pages');
		
	}
	
	public function indexAction(){
		$lang = $this->_getParam('lang','ru');
		$this->view->currentModul = $curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
     	
		$this->view->lang = $lang;
		$all = Pages::getInstance()->getVersionPages($lang, 'stok');
		$this->view->all = $all;
	  
	}
	
	public function itemsAction(){
		
		$sortSession = new Zend_Session_Namespace('sortSearch');
		$sort = $this->view->sort = $this->getSort($sortSession->sort);		
		$lang = $this->getRequest()->getParam('lang','ru');

		if ($this->_getParam('sort', NULL) == 'nasc') {
			$sort = $this->view->sort = 'nASC';
			$sortSession->sort = 'nASC';
		} elseif ($this->_getParam('sort', NULL) == 'ndesc') {
			$sort = $this->view->sort = 'nDESC';
			$sortSession->sort = 'nDESC';
		} 
		if ($this->_getParam('sort', NULL) == 'sasc') {
			$sort = $this->view->sort = 'sASC';
			$sortSession->sort = 'sASC';
		} elseif ($this->_getParam('sort', NULL) == 'sdesc') {
			$sort = $this->view->sort = 'sDESC';
			$sortSession->sort = 'sDESC';
		} 
				$page = $this->view->page = $this->_getParam('page', 1);
				$onPage = $this->view->onPage = 15;
			$pageSession = new Zend_Session_Namespace('pageSearch');
			$pageSession->page = $page;
			
			$this->view->counter=($page-1)*$onPage;		
		
      $this->view->news = Stok::getInstance()->getAll(($page-1)*$onPage, $onPage,$sort);
	  $newsCount = $this->view->newsCount = sizeof(Stok::getInstance()->getAll(null,null, null));
	  $pagesCount = (int)$this->view->pagesCount = ceil($newsCount / $onPage);	
      $this->view->currentModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
      $this->view->lang = $lang;
		
	}
	public function addAction(){
		$lang = $this->getRequest()->getParam('lang','ru');
		$this->view->currentModul = $curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
     	 $this->view->lang = $lang;	
		if ($this->_request->isPost()){
		
			$data = $this->getRequest()->getParams();
			$new = $data['new'];
			$new['name']=trim($new['name']);
			if (isset($new['pub']) && $new['pub']=='on') {
				$new['pub'] = 1;
			}
			else {
				$new['pub'] = 0; 
			}
			(isset($new['main']) && $new['main']=='on' ?	$new['main'] = 1 :$new['main'] = 0);
			Stok::getInstance()->addNew($new);
			$this->_redirect($curModul."/items/");
		}	

		$fck1 = $this->getFck('new[intro]', '90%', '200','Basic');
		//$fck1->Config['ToolbarSets']= "Basic";
		$this->view->fck_intro = $fck1;	
		$fck2 = $this->getFck('new[content]', '90%', '300');
		$this->view->fck_content = $fck2;
		 
		
	
	}
	
	public function editpAction(){
		$id = (int)$this->getRequest()->getParam('id');
		$lang = $this->getRequest()->getParam('lang','ru');
		$this->view->currentModul = $curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
     	 $this->view->lang = $lang;	
     	 $this->view->new = Stok::getInstance()->getNewById($id);
		if ($this->_request->isPost()){
			$id = (int)$this->getRequest()->getParam('id');
			$data = $this->getRequest()->getParams();
			$new = $data['new'];
			$new['name']=trim($new['name']);
			if (isset($new['pub']) && $new['pub']=='on') {
				$new['pub'] = 1;
			}
			else {
				$new['pub'] = 0; 
			}
			(isset($new['main']) && $new['main']=='on' ?	$new['main'] = 1 :$new['main'] = 0);
			Stok::getInstance()->editNew($new,$id);
			$sortSession = new Zend_Session_Namespace('sortSearch');
			$pageSession = new Zend_Session_Namespace('pageSearch');			
		
			if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";
			
			if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";
			
			$this->_redirect($curModul."/items".$page_link.$sort_link);
		}	

		$fck1 = $this->getFck('new[intro]', '90%', '200', 'Basic');
		$this->view->fck_intro = $fck1;	
		$fck2 = $this->getFck('new[content]', '90%', '300');
		$this->view->fck_content = $fck2;
		 
		
	
	}
	
	public function pubAction(){
		
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		if(!$this->_hasParam('id')) {
			$this->_redirect($curModul."/items/");
		}
		else {
			$id = (int)$this->getRequest()->getParam('id');	
			Stok::getInstance()->pubNew($id);
			
			$sortSession = new Zend_Session_Namespace('sortSearch');
			$pageSession = new Zend_Session_Namespace('pageSearch');			
		
			if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";
			
			if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";
			
			$this->_redirect($curModul."/items".$page_link.$sort_link);
						
		}	
		
	}
	
	public function unpubAction(){
		
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		if(!$this->_hasParam('id')) {
			$this->_redirect($curModul."/items/");
		}
		else {
			$id = (int)$this->getRequest()->getParam('id');	
			Stok::getInstance()->unpubNew($id);
			$sortSession = new Zend_Session_Namespace('sortSearch');
			$pageSession = new Zend_Session_Namespace('pageSearch');			
		
			if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";
			
			if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";
			
			$this->_redirect($curModul."/items".$page_link.$sort_link);		
		}		
	}
	
	public function copyAction(){
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		if(!$this->_hasParam('id')) {
			$this->_redirect($curModul."/items/");
		}
		else {
			$id = (int)$this->getRequest()->getParam('id');	
			Stok::getInstance()->CopyNew($id);
			$sortSession = new Zend_Session_Namespace('sortSearch');
			$pageSession = new Zend_Session_Namespace('pageSearch');			
		
			if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";
			
			if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";
			
			$this->_redirect($curModul."/items".$page_link.$sort_link);			
		}		
		
	}
	
	public function deleteAction(){
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		if(!$this->_hasParam('id')) {
			$this->_redirect($curModul."/items/");
		}
		else {
			$id = (int)$this->getRequest()->getParam('id');	
			Stok::getInstance()->deleteNew($id);
			$sortSession = new Zend_Session_Namespace('sortSearch');
			$pageSession = new Zend_Session_Namespace('pageSearch');			
		
			if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";
			
			if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";
			
			$this->_redirect($curModul."/items".$page_link.$sort_link);			
		}		
		
	}
	
	public function setmainAction(){
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		if(!$this->_hasParam('id')) {
			$this->_redirect($curModul."/items/");
		}
		else {
			$id = (int)$this->getRequest()->getParam('id');	
			Stok::getInstance()->setMainNew($id);
			$sortSession = new Zend_Session_Namespace('sortSearch');
			$pageSession = new Zend_Session_Namespace('pageSearch');			
		
			if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";
			
			if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";
			
			$this->_redirect($curModul."/items".$page_link.$sort_link);			
		}		
	}
	
	
	public function unsetmainAction(){
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		if(!$this->_hasParam('id')) {
			$this->_redirect($curModul."/items/");
		}
		else {
			$id = (int)$this->getRequest()->getParam('id');	
			Stok::getInstance()->unsetMainNew($id);
			$sortSession = new Zend_Session_Namespace('sortSearch');
			$pageSession = new Zend_Session_Namespace('pageSearch');			
		
			if (isset($pageSession->page)) {$page_link="/page/".$pageSession->page;} else $page_link="";
			
			if (isset($sortSession->sort)){ $sort_link="/sort/".strtolower($sortSession->sort);} else $sort_link="";
			
			$this->_redirect($curModul."/items".$page_link.$sort_link);		
		}		
	}
	//======= pages===================================================
	
	
	public function addpAction(){
		$error = '';
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		$this->view->currentModul = $curModul;
		
		if($this->getRequest()->isPost()){
//			Zend_Debug::dump($this->_request->getParams());exit;
			$data = $this->getRequest()->getParams();
			$data['parent_id'] = $data['parent_id'] == 0 ? '1' : $data['parent_id'];
			
			$id = Pages::getInstance()->addPage($data, 'stok');
			$data['id'] = $id;
			$error = $this->addRoute($data, 'index', 'index', 'stok');
			
			if($error == ''){
				$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
				$this->_redirect("$curModul");
			}
			
		}
		
		$this->view->error = $error;
		$fck = $this->getFck('content', '90%', '400');
		$this->view->fck = $fck;
		$parentId = (int)$this->getRequest()->getParam('parent_id');
		$this->view->parent_id = $parentId;
		Loader::loadPublicModel('MenuTypes');
		$this->view->menu = MenuTypes::getInstance()->getAll();
		$this->view->url = Pages::getInstance()->generateStringPath(1, '/');
		Loader::loadPublicModel('Templates');
		$this->view->templates = Templates::getInstance()->getAll('stok');
		$this->view->lang = $this->_getParam('lang');
	}
	
	
	public function editAction(){
		if(!$this->_hasParam('id')){
			$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
			$this->_redirect("/admin/$lang/stok/");
		}
		
		if($this->getRequest()->isPost()){
			$data = $this->getRequest()->getParams();
			$error = $this->editRoute($data);
			Pages::getInstance()->editPage($data);
			$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
			$this->_redirect("/admin/$lang/stok/");
		}
		
		$id = (int)$this->getRequest()->getParam('id');
		$this->view->page = Pages::getInstance()->getPage($id);
		Loader::loadPublicModel('PagesOptions');
		$this->view->options = PagesOptions::getInstance()->getPageOptions($id);
		Loader::loadPublicModel('Menu');
		Loader::loadPublicModel('MenuTypes');
		$this->view->menu = MenuTypes::getInstance()->getAll();
		$this->view->pageMenu = Menu::getInstance()->getMenuPage($id);
		
		$fck = $this->getFck('content', '90%', '400');
		$this->view->fck = $fck;
		Loader::loadPublicModel('Templates');
		$this->view->templates = Templates::getInstance()->getAll('stok');
		$this->view->lang = $this->_getParam('lang');
	}
	
	public function activateAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Pages::getInstance()->pubPage($id);
		}
		$this->view->lang = $this->_getParam('lang');
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		$this->_redirect("/admin/$lang/news/");
	}
	
	public function unactivateAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Pages::getInstance()->unpubPage($id);
		}
		$lang = $this->_getParam('lang');
		$this->view->lang = $lang;
		$this->_redirect("/admin/$lang/news/");
	}
	
	public function deletepAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Loader::loadPublicModel('Basket');
			Basket::getInstance()->add($id);
		}
		
		$lang = $this->_getParam('lang');
		$this->view->lang = $lang;
		$this->_redirect("/admin/$lang/news/");
	}
	
	private function getSort($sort)
	{
		if (!$sort) {
			return  NULL;
		} elseif ($sort == 'nASC') {
			return 'nASC';
		} elseif ($sort == 'nDESC') {
			return 'nDESC';
			
		} elseif ($sort == 'fASC') {
			return 'fASC';
		} elseif ($sort == 'fDESC') {
			return 'fDESC';
			
		} elseif ($sort == 'eASC') {
			return 'eASC';
		} elseif ($sort == 'eDESC') {
			return 'eDESC';
			
		} elseif ($sort == 'gASC') {
			return 'gASC';
		} elseif ($sort == 'gDESC') {
			return 'gDESC';
			
		} elseif ($sort == 'sASC') {
			return 'sASC';
		} elseif ($sort == 'sDESC') {
			return 'sDESC';
			
		} elseif ($sort == 'lASC') {
			return 'lASC';
		} elseif ($sort == 'lDESC') {
			return 'lDESC';
		}  elseif ($sort == 'newASC') {
			return 'newASC';
		} 
		
		
		
		else {
			return NULL;
		}
	}
	
	
	private function getSortParam($sortParam)
	{
		if ($sortParam == 'nasc') {
			return  'nASC';
		} elseif ($sortParam == 'ndesc') {
			return 'nDESC';
		} 
		if ($sortParam == 'casc') {
			return 'cASC';
		} elseif ($sortParam == 'cdesc') {
			return 'cDESC';
		} 
		
		if ($sortParam == 'easc') {
			return 'eASC';
		} elseif ($sortParam == 'edesc') {
			return 'eDESC';
		} 
		
		if ($sortParam == 'dasc') {
			return 'dASC';
		} elseif ($sortParam == 'ddesc') {
			return 'dDESC';
		} 
		
		if ($sortParam == 'sasc') {
			return 'sASC';
		} elseif ($sortParam == 'sdesc') {
			return 'sDESC';
		} 
		if ($sortParam == 'pasc') {
			return 'pASC';
		} elseif ($sortParam == 'pdesc') {
			return 'pDESC';
		} if ($sortParam == 'newdesc') {
			return 'newDESC';
		}
		
		
		
		elseif ($this->_getParam('sort', NULL) == 'prior') {
			return NULL;
		}
	}
	
	
	
	private function editRoute($data){
		Loader::loadCommon('Router');
		
		if(!Router::getInstance()->replaceRoute($data, 'index', 'index', 'stok')){
			return "Такой URL уже существует!";
		}
		
		return '';
	}
	
	
}
