<?php
define('COUNT', 50);

class Admin_BasketController extends MainAdminController
{
	
	public function indexAction(){
		Loader::loadPublicModel('Basket');
		$pages = Basket::getInstance()->getPages('unpubDate DESC');
		$this->view->pages = $pages;
	}
	
	public function restoreAction(){
		Loader::loadPublicModel('Basket');
		
		if($this->_hasParam('id')){
			Basket::getInstance()->restore($this->getRequest()->getParam('id'));
			$this->_redirect('/admin/all/basket/');
		}
	}
	
	public function deleteAction(){
		Loader::loadPublicModel('Pages');
		
		if($this->_hasParam('id')){
			Loader::loadPublicModel('Pages');
			Loader::loadCommon('Router');
			$page = Pages::getInstance()->getPage($this->getRequest()->getParam('id'));
			Router::getInstance()->deleteRoute($page->name, $page->id);
			Pages::getInstance()->remove($page->id);
			//$page->delete();			
			$this->_redirect('/admin/all/basket/');
		}
	}
	
	public function clearAction(){
		Loader::loadPublicModel('Basket');
		Basket::getInstance()->clear();
			$this->_redirect('/admin/all/basket/');
	}
}
