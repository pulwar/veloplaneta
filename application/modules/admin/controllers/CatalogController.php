<?php
define('SP','/') ;

class Admin_CatalogController extends MainAdminController  {

    public function init()
    {
     /*   $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        if (!$iGalleryId = (int)$this->_request->getParam('galid', 0))
        {
        	$this->_redirect('/gallerymanagmentadmin');
        }
        else
        {
        	$this->view->gal_id  = $iGalleryId;
        }*/
         //Loader::loadPublicModel('Image');
       //  Loader::loadPublicModel('ImageSetup');
       //  Loader::loadPublicModel('Gallery');
        // Loader::loadPublicModel('Pages');
         
    }
    
    public function indexAction(){
    	$this->_redirect('/admin/ru/Catalog_Division/index/id_page/14/');
		$lang = $this->_getParam('lang','ru');
		$this->view->currentModul = $curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
     	
		$this->view->lang = $lang;
		$all = Pages::getInstance()->getVersionPages($lang, 'catalog');
		$this->view->all = $all;
	  
	}
        
    public function pageAction()
    {
        //$this->view->title = "Image Managment.";
        //$this->view->image = Image::getInstance()->getAll((int)$this->_request->getParam('galid', 0));
        /*Zend_Debug::dump(Image::getInstance()->getAll());
        exit();*/
        require_once 'Zend/Session.php';

        $pageNamespace = new Zend_Session_Namespace('page');
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$id = (int)$this->_getParam('id');
		$pageNamespace->page_id = $id;
		
		$this->request = $this->getRequest();			
		$this->view->currentModul= $curModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
		if (!isset($id)){
			$this->_redirect($curModul);
		}
		
		$page = Pages::getInstance()->getPage($id);
		
		$tree = Gallery::getInstance()->getTreeByPageId($id);
		//print_r($tree);
		$muzey[] = array(
					'task' => $page->name,
    				'duration' =>'',
    				'user' => Security::getInstance()->getUser()->username,
    				//'id' => $data->id,
    				'id' => $page->id,
    				'uiProvider' => 'col',
    				'cls' => 'master-task',
    				'iconCls' => 'task-folder',
    				'children' =>''
				);
		
		
		$muzey[0]['duration'] = "<a href ='#' title='Добавить' style='margin-left: 128px'><img src='/images/plus_krug.gif' onclick='javascript:window.location=\"/admin/$lang/gallery/add/pid/$page->id\"'/></a>";
		$muzey[0]['children'] = $tree;
		
		$this->view->ch=Zend_Json::encode($muzey);
    }

    public function addpAction(){
		$error = '';
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		$this->view->currentModul = $curModul;
		
		if($this->getRequest()->isPost()){
//			Zend_Debug::dump($this->_request->getParams());exit;
			$data = $this->getRequest()->getParams();
			$data['parent_id'] = $data['parent_id'] == 0 ? '1' : $data['parent_id'];
			$id = Pages::getInstance()->addPage($data, 'catalog');
			$data['id'] = $id;
			$error = $this->addRoute($data, 'index', 'index', 'catalog');
			
			if($error == ''){
				$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
				$this->_redirect("$curModul");
			}
			
		}
		
		$this->view->error = $error;
		$fck = $this->getFck('content', '90%', '400');
		$this->view->fck = $fck;
		$parentId = (int)$this->getRequest()->getParam('parent_id');
		$this->view->parent_id = $parentId;
		Loader::loadPublicModel('MenuTypes');
		$this->view->menu = MenuTypes::getInstance()->getAll();
		$this->view->url = Pages::getInstance()->generateStringPath(1, '/');
		Loader::loadPublicModel('Templates');
		$this->view->templates = Templates::getInstance()->getAll('catalog');
		$this->view->lang = $this->_getParam('lang');
	}
	
	
	public function editAction(){
		
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		$this->view->currentModul = $curModul;
		
		
		if(!$this->_hasParam('id')){
			$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
			$this->_redirect($curModul);
		}
		
		if($this->getRequest()->isPost()){
			$data = $this->getRequest()->getParams();
			$error = $this->editRoute($data);
			Pages::getInstance()->editPage($data);
			$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
			$this->_redirect($curModul);
		}
		
		$id = (int)$this->getRequest()->getParam('id');
		$this->view->page = Pages::getInstance()->getPage($id);
		Loader::loadPublicModel('PagesOptions');
		$this->view->options = PagesOptions::getInstance()->getPageOptions($id);
		Loader::loadPublicModel('Menu');
		Loader::loadPublicModel('MenuTypes');
		$this->view->menu = MenuTypes::getInstance()->getAll();
		$this->view->pageMenu = Menu::getInstance()->getMenuPage($id);
		
		$fck = $this->getFck('content', '90%', '400');
		$this->view->fck = $fck;
		Loader::loadPublicModel('Templates');
		$this->view->templates = Templates::getInstance()->getAll('catalog');
		$this->view->lang = $this->_getParam('lang');
	}
	
	public function activatepAction(){
		
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Pages::getInstance()->pubPage($id);
		}
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		
		$this->view->lang = $this->_getParam('lang');
		$lang = $this->_hasParam('lang') ? $this->getParam('lang') : 'ru';
		$this->_redirect($curModul);
	}
	
	public function unactivatepAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Pages::getInstance()->unpubPage($id);
		}
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		
		$lang = $this->_getParam('lang');
		$this->view->lang = $lang;
		$this->_redirect($curModul);
	}
	
	public function deletepAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			Loader::loadPublicModel('Basket');
			Basket::getInstance()->add($id);
		}
		$lang = $this->getRequest()->getParam('lang','ru');
		$curModul = SP.'admin'.SP.$lang.SP.$this->getRequest()->getControllerName();
		
		$lang = $this->_getParam('lang');
		$this->view->lang = $lang;
		$this->_redirect($curModul);
	}
	
	public function edititemAction(){
		
		$this->view->lang = $lang = $this->getParam('lang','ru');
		$this->request = $this->getRequest();			
		$this->view->currentModul =$currentModul = $lang.'/'. $this->request->getControllerName(); //. $this->request->getActionName();
		
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			$data= $this->view->item = Gallery::getInstance()->getItemById($id);
			
		}
		if ($this->_request->isPost()){
			$data = $this->getRequest()->getParam('add');	
			$id = (int)$this->getRequest()->getParam('id');
			
			if (isset($data['name']) && !empty($data['name'])){
				
				if ( $data['pub']=='on') {$data['pub']=1;
					
				}
				 else $data['pub']=0;
				 
			$data['name'] = trim($data['name']);
			$data['content'] = trim($data['content']);
			$data['intro'] = trim($data['intro']);
			
			Gallery::getInstance()->editItem($data,$id);
			
			require_once 'Zend/Session.php';
			$pageNamespace = new Zend_Session_Namespace('page');
			$url='';
			if (isset($pageNamespace->page_id)) {
        	$url = "/page/id/".$pageNamespace->page_id;
        	}
        
		$this->_redirect("/admin/".$currentModul.$url);
			}
			
			
		}
		
		
	}
	

	
	public function trimFilter($array){
		return parent::trimFilter($array);
	}

   	
	private function editRoute($data){
		Loader::loadCommon('Router');	
			
		if(!Router::getInstance()->replaceRoute($data, 'index','index' , 'catalog')){
			return "Такой URL уже существует!";
		}
		
		return '';
	}
    
}