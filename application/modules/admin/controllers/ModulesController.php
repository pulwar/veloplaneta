<?php

class Admin_ModulesController extends Zend_Controller_Action
{

	public function init(){
		Loader::loadPublicModel('Modules');
		Loader::loadPublicModel('Pages');
		Loader::loadPublicModel('Versions');
	}
	
	/** Возвращает список модулей и их описание
	 *
	 *  @return array
	 * 
	 */
	public function indexAction()
	{
		$modules = Modules::getInstance()->getAllModules();          
		$this->view->modulesList = $modules;
		$this->view->versions = Versions::getInstance()->getVersions();
		$this->view->lang = ($this->_getParam('lang') == 'all') ? 'ru' : $this->_getParam('lang');
	}
	
	public function versionAction(){
		if($this->_hasParam('lang')){
			$lang = $this->_getParam('lang');
		}
		elseif(Zend_Registry::isRegistered('version')){
			$lang = Zend_Registry::get('version');
		}
		
		$version = Versions::getInstance()->getVersion($lang);
		$this->view->version = $version;
	}
	
}
