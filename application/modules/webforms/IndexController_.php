<?php

class Webforms_IndexController extends Zend_Controller_Action
{
	private $_filters = array('text', 'number', 'word', 'email', 'site');
	
	public function init(){
		Loader::loadPublicModel('Pages');
		Loader::loadPublicModel('PagesOptions');
	}
	//нуждается в рефакторинге!
	public function indexAction(){
		Loader::loadCommon('Captcha');
		$captcha = new Captcha;
		$message = '';
		$params = $this->_request->getParams();
		$message = $this->isValidateParams($params);
		$this->view->form = $this->_request->getParams();
		$this->view->options = PagesOptions::getInstance()->getPageOptions($this->_getParam('id'));
		//ошибка валидации	
		if($this->_request->isPost() && !empty($message)){
			$this->view->message = $message;
			$id = $this->_request->getParam('id');
			$page = Pages::getInstance()->getPage($id);
			
			if($page->published == '0'){
				$this->_redirect('/');
			}
			
			$this->setTemplate($page->template);
			$this->view->page = $page;
			$this->view->content = $page->content;
		}
		//проверка каптчи
		elseif($this->_request->isPost() && $this->_hasParam('captcha') && !$captcha->isValidate($this->_getParam('captcha'))){
			$this->view->message = 'Неправильно подсчитано выражение!';
			$id = $this->_request->getParam('id');
			$page = Pages::getInstance()->getPage($id);
			
			if($page->published == '0'){
				$this->_redirect('/');
			}
			
			$this->setTemplate($page->template);
			$this->view->page = $page;
			$this->view->content = $page->content;
			
		}
		//валидация прошла - отправляем сообщение
		elseif($this->_request->isPost()){	
			$params = $this->getNormalParams($params);
			$this->view->params = $params;
			
			Loader::loadCommon('Mail');
			$body = "<table>";
			
			$p = $this->_request->getPost();
			
			foreach($p as $key => $data){
        if((int)$data == 0)
          $body .= "<tr><td>$data</td></tr>";
			}
			
			$body .= "</table>";
		
			$email = "prokurat@icetrade.by";
			$from = "info@proweb.by";
			$recipient = 'New message';
			$subject = 'New message';
			Mail::send($email, $body, $from, $subject, $recipient);
			$this->view->message = $message;
			$id = $this->_request->getParam('id');
			$page = Pages::getInstance()->getPage($id);
			$this->setTemplate('default');
			$this->view->page = $page;
			$this->view->content = "<h3><span style='color:red;'>Спасибо! Ваше письмо было отправлено администратору.<span></h3>";
		}
		//первое открытие страницы
		else{
			$this->view->message = $message;
			$id = $this->_request->getParam('id');
			$page = Pages::getInstance()->getPage($id);
			
			if($page->published == '0'){
				$this->_redirect('/');
			}
			
			$this->setTemplate($page->template);
			$this->view->page = $page;
			$this->view->content = $page->content;
		}
		
	}
	
	public function __call($method, $args){
//    	$this->_redirect('/webforms/');
	}
		
	private function filterText($param){
		$validator = new Zend_Validate_Alnum(true);
		$return = '';
		
		if ($validator->isValid($param)) {
		} 
		else{
		   $return = 'Неправильный формат текста';
		}
		
		return $return;
	}
	
	private function filterNumber($param){
		$validator = new Zend_Validate_Digits();
		$return = '';
		
		if ($validator->isValid($param)) {
		} else {
		    $return = 'Неправильный формат числа';
		}
		return $return;
	}
	
	private function filterWord($param){
		$validator = new Zend_Validate_Alpha();
		$return = '';
		
		if ($validator->isValid($param)) {
		} else {
		    $return = 'Неправильный формат слова';
		}
		
		return $return;
	}
	
	private function filterEmail($param){
		$validator = new Zend_Validate_EmailAddress();
		$return = '';
		
		if ($validator->isValid($param)) {
		} else {
		    $return = 'Неправильный формат e-mail';
		}
		return $return;
	}
	
	private function filterSite($param){
		$validator = new Zend_Validate_Hostname();
		$return = '';
		
		if ($validator->isValid($param)) {
		} else {
		    $return = 'Неправильный формат сайта';
		}
		return $return;
	}
	
	private function isValidateParams($params){
		foreach ($params as $key => $data){
			$param = explode('_', $key);
			
			if(count($param) > 1 && in_array($param[0], $this->getFilters())){
				
				$val = $this->validateField($param[0], $data);
					
				if(!empty($val)){
//					print_r($params);
//					echo $key;
//					exit;
					return $val;
				}
			}
		}
		
		return '';
	}
	
	private function getFilters(){
		return $this->_filters;
	}
	
	private function validateField($type, $value){
		switch ($type){
			case 'text':
				$error = $this->filterText($value);
				
				if(!empty($error)){
					return $error . "($value)";
				}

				break;	
			case 'number':
				$error = $this->filterNumber($value);
				
				if(!empty($error))
					return $error . "($value)";
					
				break;	
			case 'word':
				$error = $this->filterWord($value);
				
				if(!empty($error))
					return $error . "($value)";
					
				break;	
			case 'email':
				$error = $this->filterEmail($value);
				
				if(!empty($error))
					return $error . "($value)";
					
				break;	
			case 'site':
				$error = $this->filterSite($value);
				
				if(!empty($error))
					return $error . "($value)";
					
				break;	
		}
		
		return '';
																			
	}
	
	private function getNormalParams($params){
		$return = array();
		
		foreach ($params as $key => $data){
			$param = explode('_', $key);
			
			if(count($param) > 1 && in_array($param[0], $this->getFilters())){
				$name = $param[1];
				$return[$name] = $data;
			}
		}
		
		return $return;
	}
	
	private function setTemplate($template){
		
		$layoutManager = $this->getHelper('LayoutManager');
		$layoutManager->useLayoutName($template);
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('header', 'header', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('footer', 'footer', 'template'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('uslugi', 'uslugi', 'template'));

		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('captcha', 'get', 'captcha', 'webforms'));
		
	}
	
}