<?php

class Webforms_IndexController extends Zend_Controller_Action
{
	private $_filters = array('text', 'number', 'word', 'email', 'site');
	private $_fields = array('name'=>'Имя','phone'=>'Телефон','company'=>'Компания', 'email'=>'email', 'body'=>'Текст сообщения');
	
	private $_table_head = '
		<tr>			
			<td>Наименование</td>
			<td>Тираж </td>
			<td>Цена</td>
			<td>Стоимость</td>
		</tr>
	
	';
	private $_pen_template = '
		<tr>			
			<td>{name} (Артикул №{articul})</td>
			<td>{count} </td>
			<td>{price}$ </td>
			<td>{total_price}$ </td>
		</tr>
		
	';
	
	public function init(){
		
	}

	//нуждается в рефакторинге!
	public function indexAction(){
		$id = $this->_request->getParam('id');
		$page = Pages::getInstance()->getPage($id);
		$this->view->lang=$page->version;
		$iId = (int)$this->_request->getParam('tid');
		if ($iId > 0)
		{
                    $this->view->tovar  = Catalog_Tovar::getInstance()->getTovarById($iId);
		}               
                
		$message = '';
                $this->view->message = $message;
		$params = $this->_request->getParams();
		
		$this->view->params = $params;
		$message = $this->isValidateParams($params);
		$this->view->form = $this->_request->getParams();
		$this->view->options = PagesOptions::getInstance()->getPageOptions($this->_getParam('id'));
		
		$emails='';
		$from='';
		
		//ошибка валидации	
		

                
		if($this->_request->isPost() && (trim($this->_getParam('body'))=='' || trim($this->_getParam('email'))=='' ))
                {
 			$this->view->message = 'Вы не ввели текст сообщения или e-mail';
			$id = $this->_request->getParam('id');
			$page = Pages::getInstance()->getPage($id);
			
			if($page->published == '0'){
				$this->_redirect('/');
			}
			
			$this->setTemplate($page->template);
			$this->view->page = $page;
			$this->view->content = $page->content;                        
		}
		//проверка каптчи
		elseif($this->_request->isPost() && $this->_request->getParam('captcha') != $_SESSION['answer']){
			$this->view->message = 'Неправильно подсчитано выражение!';
			$id = $this->_request->getParam('id');
			$page = Pages::getInstance()->getPage($id);
			$this->view->lang=$page->version;
			
			if($page->published == '0'){
				$this->_redirect('/');
			}			
			$this->setTemplate($page->template);
			$this->view->page = $page;
			$this->view->content = $page->content;
			
		}
		//валидация прошла - отправляем сообщение
		elseif($this->_request->isPost()){
			$params = $this->getNormalParams($params);
			$email = Webforms::getInstance()->fetchAll();
                        $email = $email->toArray();
                        $emails=$email[1]['title'];
			$from=$email[1]['description'];
			
			
			$recipient = 'New message';			
			$subject = $this->_getParam('subject','feedback');
			$body= ($subject=="zakaz") ? $this->getBasket() :'';
			$fields = $this->_getAllParams();
			$template = file_get_contents(DIR_PUBLIC.'form_template.html');
			
			foreach ($fields as $key=>$param){
				$template = str_replace('{'.$key.'}', $param, $template);
			}
			$template = preg_replace('/{.+}/Usi', '', $template);
			//$body.=$this->createMessage($params);
			$body = $template;			
			Loader::loadCommon('Mail');
			Mail::send($emails, $body, $from, $subject, $recipient);
			$this->view->message = $message;
			$id = $this->_request->getParam('id');
			$page = Pages::getInstance()->getPage($id);
			//$this->setTemplate('default');
			$this->setTemplate($page->template);
			$this->view->page = $page;
			$this->view->content = $page->content;
			$this->view->message = 'Ваше сообщение успешно отправлено!';
		}
		//первое открытие страницы
		else{

                        Loader::loadCommon('Captcha');
                        $captcha = new Captcha();
                        $this->view->captcha = $captcha->getOperation();
                        $this->view->message = $message;
			$id = $this->_request->getParam('id');
			$page = Pages::getInstance()->getPage($id);
			
			if($page->published == '0'){
				$this->_redirect('/');
			}
			
			$this->setTemplate($page->template);
			$this->view->page = $page;
			$this->view->content = $page->content;
		}
		
	}
	
	
	public function stoimostAction(){
		$fields = array(
			//место отправки
			'country_from'=>'Страна',
			'city_from'=>'Город',
			'zip_from'=>'Почтовый индекс',
					
		    // место доставки
			'country_to'=>'Страна',
			'city_to'=>'Город',
			'zip_to'=>'Почтовый индекс',
		
			// параметры груза
			'gruz_name'=>'Наименование груза',
			'pack_type'=>'Вид упаковки',
			'ves'=>'Вес',
			'gruz_type'=>'Тип Груза',
			'date_zagruz'=>'Дата загрузки',
			'request'=>'Запрос/предложение',
			'other_data'=>'Прочие данные',
		
			// данные заказчика
			'customer_name'=>'Ф.И.О.',
			'telephone'=>'Телефон',
			'email'=>'email',
			'firm_name'=>'Название организации'	
			
		);
		$countries = file(DIR_PUBLIC.'country.txt');
		$countries_from = '';
		$countries_to = '';
		foreach ((array)$countries as $country){
			$data = trim($country);
			if ($data!=''){
				$countries_from[]=$data;
				$countries_to[]=$data;
			}
		}
		if (count($countries_from)){
			$this->view->countries_from = $countries_from;
			$this->view->countries_to = $countries_to;
		}
		$id = $this->_request->getParam('id');
		$page = Pages::getInstance()->getPage($id);
		$this->view->lang=$page->version;
		//print_r($_REQUEST); 		
		$this->setTemplate($page->template);
		$this->view->page = $page;
		$this->view->lang = $page->version;
		$this->view->content = $page->content;
		$this->view->options = PagesOptions::getInstance()->getPageOptions($id);
		$params = $this->_request->getParams();
		//print_r($params);
		Loader::loadCommon('Captcha');
		//$captcha = new Captcha;
		if ($this->_request->isPost() && $params['captcha'] && !$captcha->isValidate($params['captcha'])){
			$this->view->message = 'Неправильно подсчитано выражение!';
			
		}elseif ($this->_request->isPost() && file_exists(DIR_PUBLIC.'form_template.html')){			
			$template = file_get_contents(DIR_PUBLIC.'form_template.html');
			foreach ($fields as $key=>$name){
				$template = str_replace('{'.$key.'}', $params[$key], $template );
			}			
			$email = Webforms::getInstance()->fetchAll()->current();
			$emails=$email->title;
			$from=$email->description;
			$recipient = 'New message';			
			$subject = 'Расчитать стоимость';			
			Loader::loadCommon('Mail');
			Mail::send($emails, $template, $from, $subject, $recipient);
			//$this->view->message = $message;
			$id = $this->_request->getParam('id');
			$page = Pages::getInstance()->getPage($id);
			$this->setTemplate('default');
			$this->view->page = $page;
			$this->view->content = 'Спасибо! Ваше письмо было отправлено администратору.';
		}
		
		
		
		
		if ($params){		
				$this->view->params = $params;		
				foreach ($fields as $k=>$v){
					if (isset($params[$k])){
						$this->view->$k = $params[$k];
						//echo $this->view->$k;
					} 	
				}
		}
	
		
	}
	
	
	public function calcAction(){
		$id = $this->_request->getParam('id');
		$page = Pages::getInstance()->getPage($id);
		$this->view->page = $page;
		$this->view->options= PagesOptions::getInstance()->getPageOptions($page->id);
		$this->setTemplate($page->template);
		$pens = array();
		if($this->_request->isPost()){
			$articuls = $this->_getParam('articul');
			$counts = $this->_getParam('count');
			$total = 0;
			foreach ($articuls as $key=>$value){
				if ($value!='' && isset($counts[$key]) && $counts[$key]){
					$pen_row = Catalog_Tovar::getInstance()->fetchRow("articul='$value'");
					if (!is_null($pen_row)){												
						$pens[$key]['articul'] = $value;
						$pens[$key]['count'] = $counts[$key];
						//$pens[$key]['price_format'] = $pen_row->getPrice();
						$pens[$key]['price'] = $pen_row->price;
						$pens[$key]['total_price'] = $pen_row->price*$counts[$key];						
						$pens[$key]['name'] = $pen_row->name;
						$pens[$key]['id'] = $pen_row->id;						
						$pens[$key]['img_small'] = $pen_row->img_small;
						$total+=$counts[$key]*$pen_row->price;
					}
				}
				
			}
			$send = $this->_getParam('send');
			if ($send && count($pens)){				
				$html = '<table border="1" cellpadding="0" cellspacing="0">'.$this->_table_head;
				foreach ($pens as $item){
					$pen_tpl = $this->_pen_template;
					foreach ($item as $k=>$v){
						$pen_tpl = str_replace('{'.$k.'}',$v, $pen_tpl );
					}
					$html.=$pen_tpl;
				}
				$html.='<tr><td colspan="4" align="right">Итого: '.$total.'$</td></tr>';
				$html.='</table>';
				$template = file_get_contents(DIR_PUBLIC.'form_template.html');
				$fields = $this->_getAllParams();				
				foreach ($fields as $key=>$value){
					
					$template = @str_replace('{'.$key.'}', $value, $template );
				}
				$html.=$template;			
				$email = Webforms::getInstance()->fetchAll()->current();
				$emails=$email->title;
				$from=$email->description;
				$recipient = 'New message';			
				$subject = 'Расчитать стоимость';			
				Loader::loadCommon('Mail');
				Mail::send($emails, $html, $from, $subject, $recipient);
				//$this->view->message = $message;
				$id = $this->_request->getParam('id');
				$page = Pages::getInstance()->getPage($id);
				$this->setTemplate('default');
				$this->view->page = $page;
				$this->view->content = 'Спасибо! Ваше письмо было отправлено администратору.';
					//echo $html;
			} else {
				$this->view->pens = isset($pens) ? $pens : array(); 
			}
			
			
			
		}
	}
	
	
	
	
	
	public function __call($method, $args){
//    	$this->_redirect('/webforms/');
	}
	/**
	 * Gets textPlain data
	 * return string
	 */
	private function  getBasket(){
		$basketSession = new  Zend_Session_Namespace('basket');
		$text='';
		$sumPrice=0;
		if (isset($basketSession->items)){
			$tovars = Catalog_Tovar::getInstance()->find(array_keys($basketSession->items));
			foreach ($tovars as $tovar){
				$text.=$tovar->rowTotextPlain($basketSession->items[$tovar->id]['count']);
				$sumPrice = $sumPrice+$tovar->price*$basketSession->items[$tovar->id]['count'];
			}
			$text .="Итог ".$sumPrice."руб.\r\n";
		}
		return $text;
	}
	
	private function createMessage($array)
	{
		$message ='';
		foreach ($array as $key=>$data) {
			$message.=$this->_fields[$key]." / ".$data."\r\n";
		}
		return $message;
	}
	
		
	private function filterText($param){
		$validator = new Zend_Validate_Alnum(true);
		$return = '';
		echo "<br>mass param:";
		
		if ($validator->isValid($param)) {
		} 
		else{
		   $return = 'Неправильный формат текста';
		}
		
		return $return;
	}
	
	private function filterNumber($param){
		$validator = new Zend_Validate_Digits();
		$return = '';
		
		if ($validator->isValid($param)) {
		} else {
		    $return = 'Неправильный формат числа';
		}
		return $return;
	}
	
	private function filterWord($param){
		$validator = new Zend_Validate_Alpha();
		$return = '';
		
		if ($validator->isValid($param)) {
		} else {
		    $return = 'Неправильный формат слова';
		}
		
		return $return;
	}
	
	private function filterEmail($param){
		$validator = new Zend_Validate_EmailAddress();
		$return = '';
		
		if ($validator->isValid($param)) {
		} else {
		    $return = 'Неправильный формат e-mail';
		}
		return $return;
	}
	
	private function filterSite($param){
		$validator = new Zend_Validate_Hostname();
		$return = '';
		
		if ($validator->isValid($param)) {
		} else {
		    $return = 'Неправильный формат сайта';
		}
		return $return;
	}
	
	private function isValidateParams($params){
		foreach ($params as $key => $data){
			$param = explode('_', $key);
			
			if(count($param) > 1 && in_array($param[0], $this->getFilters())){
				
				$val = $this->validateField($param[0], $data);
					
				if(!empty($val)){
					return $val;
				}
			}
		}
		
		return '';
	}
	
	private function getFilters(){
		return $this->_filters;
	}
	
	private function validateField($type, $value){
		switch ($type){
		/*	case 'text':
				$error = $this->filterText($value);
				
				if(!empty($error)){
					return $error . "($value)";
				}

				break;	
				*/
			case 'number':
				$error = $this->filterNumber($value);
				
				if(!empty($error))
					return $error . "($value)";
					
				break;	
			case 'word':
				$error = $this->filterWord($value);
				
				if(!empty($error))
					return $error . "($value)";
					
				break;	
			case 'email':
				$error = $this->filterEmail($value);
				
				if(!empty($error))
					return $error . "($value)";
					
				break;	
			case 'site':
				$error = $this->filterSite($value);
				
				if(!empty($error))
					return $error . "($value)";
					
				break;	
		}
		
		return '';
																			
	}
	
	private function getNormalParams($params){
		$return = array();
		
		foreach ($params as $key => $data){
			$param = explode('_', $key);
			
			if(count($param) > 1 && in_array($param[0], $this->getFilters())){
				$name = $param[1];
				$return[$name] = $data;
			}
		}
		
		return $return;
	}
	
	
private function setTemplate($template){
		
		$layoutManager = $this->getHelper('LayoutManager');
		$layoutManager->useLayoutName($template);
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('header', 'header', 'template'));
		//$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('headerDefault', 'headerDefault', 'template'));
		//$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('inner', 'inner', 'template'));		
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('cart', 'preview', 'cart', 'catalog'));		
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('footer', 'footer', 'template'));
		//$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('captcha', 'get', 'captcha', 'webforms'));
		$layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('leftcollum', 'leftcollum', 'template'));
                $layoutManager->getLayout($template)->addRequest(new Zend_Layout_Request('rightcolumn', 'rightcolumn', 'template'));
	
	}
	
}