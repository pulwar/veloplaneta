<?php

/**
 * Performs app configuration steps
 *
 */
class Configurator
{
	/**
	 * Установка соединения с БД
	 *
	 */
	public static function setupDatabase(){
		$config = Configurator::getConfig('db');
		$db = Zend_Db::factory($config->db->type, $config->db->config->toArray());
		$sql = "SET NAMES UTF8; SET @@auto_increment_increment=1; SET @@auto_increment_offset=1;";
		$db->query($sql);
		Zend_Db_Table::setDefaultAdapter($db);
		Zend_Registry::set('db', $db);
	}
	
	/**
	 * Установка парамаетров View
	 *
	 */
	public static function setupView(){
		Loader::loadCommon('Registry');
		$view = new Zend_View();
		$view->strictVars(false);
		$view->addScriptPath(DIR_SCRIPTS) ;
		$view->addHelperPath(DIR_HELPER , 'Gr_View_Helper') ;
		//print_r($view->getHelperPaths()); exit;
		Registry::setView($view);
	}
	
	/**
	 * Уставновка роутов
	 *
	 * @param object $router
	 */
	public static function setupRoutes($router){
		$routes = Configurator::getRoute('routes');
		//print_r($routes->toArray());
		$router->addConfig($routes, 'routes');
	}
	
	/**
	 * Получение блока конфигурации
	 * из ini-файла
	 *
	 * @param string $blockName
	 * @return unknown
	 */
	public static function getConfig($blockName){
		$file_name =DIR_APPLICATION . 'conf/config.ini';
		if (preg_match('/sklad/', $_SERVER['HTTP_HOST'])){
			$file_name =DIR_APPLICATION . 'conf/config_alfa.ini';
		}
	 	return new Zend_Config_Ini($file_name, $blockName);
	}
	
	/**
	 * Получение имени файла конфигурации
	 *
	 * @return string
	 */
	public static function getConfigFileName(){
		return DIR_APPLICATION . 'conf/config.ini';
	}
	
	/**
	 * Получение конфигурации роутов
	 *
	 * @param string $name
	 * @return object
	 */
	public static function getRoute($name){
		return new Zend_Config_Ini(DIR_APPLICATION . 'conf/routes.ini', $name);
	}
	
	/**
	 * Получение имени файла роутов
	 *
	 * @return string
	 */
	public static function getConfigRoutesFileName(){
		$file_name =DIR_APPLICATION . 'conf/routes.ini';
		if (strpos('sklad', $_SERVER['HTTP_HOST'])!==false){
			$file_name =DIR_APPLICATION . 'conf/routes_alfa.ini';
		}
		return $file_name;
	}
}