<?php

require_once('Zend/Controller/Action.php');


class PropertyController extends PublicController 
{
	public function indexAction()
	{
		$this->_redirect('/');
	}
	
	public function init()
	{
		$this->view->title = "Get Moved";
		$this->_defaultCountry = 197;
		
		Loader::loadPublicModel('CategoryTable');
		Loader::loadPublicModel('TypeTable');
		Loader::loadPublicModel('SubTypeTable');

		Loader::loadPublicModel('CategoryTypeTable');
		Loader::loadPublicModel('TypeSubTypeTable');

		Loader::loadPublicModel('PropertyTable');

		Loader::loadPublicModel('CountryTable');
		Loader::loadPublicModel('RegionTable');
		Loader::loadPublicModel('PlaceTable');

		Loader::loadPublicModel('CountryRegionTable');
		Loader::loadPublicModel('RegionPlaceTable');

		Loader::loadPublicModel('Uploader');
		Loader::loadPublicModel('Property');

		Loader::loadPublicModel('PhotoTable');
		Loader::loadPublicModel('Property');

		Zend_Loader::loadClass('Zend_Session_Namespace');

		Zend_Loader::loadClass('Zend_Filter');
		Zend_Loader::loadClass('Zend_Filter_StripTags');
		Zend_Loader::loadClass('Zend_Filter_StringTrim');
		Zend_Loader::loadClass('Zend_Filter_Int');
	}	
	
	/**
	 * Using for authentication
	 *
	 */
	public function preDispatch()
	{
		if (!Security::getInstance()->checkAllow())
		{
			$this->_redirect('/common/');
		}		
	}
	/**
	 * Script to enter category, type & subtype for property
	 *
	 */
	public function categoryAction()
	{
		// show tables with categories {Residential, Commercial, Others}
		$this->view->categories = CategoryTable::getInstance()->fetchAll();

		$session = new Zend_Session_Namespace('propertyInsertSession');
		// then... if you choose type or subType, permit update to get user see subcategories
		// & finally when user press 'next' button, go to next page
		if ($this->getRequest()->isPost())
		{
			$postArray = $this->_getParam('selectCategory');

			// data about categories, types & subtypes
			$session->page1['categoryId'] = $postArray['level1Category'];
			$session->page1['typeId'] = $postArray['typeList'];
			$session->page1['subTypeId'] = $postArray['level2Category'];

			if ($this->_hasParam('__EVENTTARGET'))
			{
				$eventParam = $this->_getParam('__EVENTTARGET');
				list(, $eventTarget) = explode(':', $eventParam);
			}

			if ($eventTarget == 'level1Category' || $eventTarget == 'typeList')
			{
				// fetching from db appropriate categories, types & subTypes
				$this->view->types = CategoryTable::getInstance()->getTypesById($session->page1['categoryId']);
				$this->view->subTypes = TypeTable::getInstance()->getSubTypesById($session->page1['typeId']);				
				$session->page1['subTypeId'] = null;			
			}
			
			if ($eventTarget == 'level1Category')
			{
				$session->page1['typeId'] = null;
			}

			$this->view->row = $session->page1;
			// check if user made his choice, & only then go further
			if (isset($postArray['btnNext']) && $session->page1['categoryId'] && $session->page1['typeId'])
			{
				$this->_redirect('/'.$this->_controllerPath.'/sell-property');
			}
		}
		else
		{
			// GET: first time
			unset($session->page1);

			$this->view->types = array();
			$this->view->subTypes = array();
		}
	}
	/**
	 * Script to enter misc data for property
	 *
	 */
	public function sellPropertyAction()
	{
		if ($this->_hasParam('propertyId'))
		{
			$propertyId = $this->_getParam('propertyId');
		}

		// Again... We are saving data from page2 into session first.
		$session = new Zend_Session_Namespace('propertyInsertSession');

		// show tables with countries
		$this->view->countries = CountryTable::getInstance()->fetchAll();

		// GET or POST
		if ($this->_request->isPost())
		{
			$postArray = $this->_request->getPost('addPropertyDetails');

			$floor = isset($session->page2['floor']) ? $session->page2['floor'] : null;
			$brochure = isset($session->page2['brochure']) ? $session->page2['brochure'] : null;
			$propertyId = isset($session->page2['propertyId']) ? $session->page2['propertyId'] : null;

			$session->page2 = array(
			'houseNo'		=>	$postArray['txtHouseNo'],
			'street'		=>	$postArray['txtStreet'],
			'add1'			=>	$postArray['txtAdd1'],
			'add2'			=>	$postArray['txtAdd2'],
			'city'			=>	$postArray['txtCity'],
			'post1'			=>	$postArray['txtPost1'],
			'post2'			=>	$postArray['txtPost2'],
			'countryId'		=>	$postArray['countryList'],
			'regionId'		=>	$postArray['regionList'],
			'placeId'		=>	$postArray['subRegionList'],
			'bedRooms'		=>	$postArray['bedRoomsList'],
			'bathRooms'		=>	$postArray['bathRoomsList'],
			'reception'		=>	$postArray['receptionList'],
			'price'			=>	$postArray['txtPrice'],
			'buildType'		=>	$postArray['buildTypeList'],
			'footage'		=>	$postArray['txtFootage'],
			'garage'		=>	$postArray['garageList'],
			'year'			=>	$postArray['txtYear'],
			'description'	=>	$postArray['txtDescription'],
			'primary'		=>	$postArray['txtPrimary'],
			'secondary'		=>	$postArray['txtSecondary'],
			'college'		=>	$postArray['txtCollege'],
			'url'			=>	$postArray['txtURL'],
			'vTour'			=>	$postArray['txtVTour'],
			'propertyStatus'=>	$postArray['propertyStatusList'],
			'status'		=>	$postArray['statusList'],
			'floor'			=>	$floor,
			'brochure'		=>	$brochure,
			'propertyId'	=>	$propertyId,
			'lat'			=>	$postArray['txtLatitude'],
			'lng'			=>	$postArray['txtLongtitude'],
			);

			if (!empty($postArray['features']))
			{
				$session->page2['features'] = $postArray['features'];
				$session->page2['featureArray'] = explode("\n", $postArray['features']);
			}
			else
			{
				$session->page2['features'] = null;
				$session->page2['featureArray'] = null;
			}
			if (!empty($postArray['dimensions']))
			{
				$session->page2['dimensions'] = $postArray['dimensions'];
				$session->page2['dimensionArray'] = explode("\n", $postArray['dimensions']);
			}
			else
			{
				$session->page2['dimensions'] = null;
				$session->page2['dimensionArray'] = null;
			}

			$session->page2['radFeat'] = $postArray['radFeat'] == "YES" ? 1 : 0;

			$this->view->outdoorValues = isset($postArray['outdoorList']) ? $postArray['outdoorList'] : array();
			$this->view->parkingValues = isset($postArray['parkingList']) ? $postArray['parkingList'] : array();
			$this->view->tenureValues = isset($postArray['tenureList']) ? $postArray['tenureList'] : array();
			$this->view->ofValues = isset($postArray['otherList']) ? $postArray['otherList'] : array();

			$session->page2['outdoor'] = $this->convertIntoSetValue($this->view->outdoorValues);
			$session->page2['parking'] = $this->convertIntoSetValue($this->view->parkingValues);
			$session->page2['tenure'] = $this->convertIntoSetValue($this->view->tenureValues);
			$session->page2['otherFeatures'] = $this->convertIntoSetValue($this->view->ofValues);

			if ($this->_hasParam('__EVENTTARGET'))
			{
				$eventParam = $this->_getParam('__EVENTTARGET');
				list(, $eventTarget) = explode(':', $eventParam);
			}

			if ($eventTarget == 'countryList')
			{
				// fetching from db appropriate countries, regions & subRegions for selected country
				$this->view->regions = CountryTable::getInstance()->getRegionsById($postArray['countryList']);
				$this->view->places = array();

				$session->page2['regionId'] = null;
				$session->page2['placeId'] = null;
			}

			if ($eventTarget == 'regionList')
			{
				// fetching from db appropriate countries, regions & subRegions for selected country
				$this->view->regions = CountryTable::getInstance()->getRegionsById($postArray['countryList']);
				$this->view->places = RegionTable::getInstance()->getPlacesById($postArray['regionList']);

				$session->page2['placeId'] = null;
			}

			// To the next page, so if propertyId is set, then we need updating, else inserting
			if (isset($postArray['btnNext']))
			{
				// uploading floor plan & brochure
				$uploadAgent = new Uploader('txtFloor');
				$avTypes = array('image/jpeg', 'image/pjpeg', 'image/gif', 'image/png');
				$uploadAgent->setAvailableTypes($avTypes);
				$uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/web/images/uploaded/files/';
				$uploadAgent->setUploadDir($uploadDir);
				if ($uploadAgent->uploadFile())
				{
					if (isset($session->page2['propertyId']) && isset($session->page2['floor']))
					{
						// delete file downloaded before
						$fileName = $uploadDir . $session->page2['floor'];
						if (file_exists($fileName))
							unlink($fileName);
					}
					$session->page2['floor'] = $uploadAgent->getUniqueFileName();
				}

				$uploadAgent = new Uploader('txtBrochure');
				$avTypes = array('image/jpeg', 'image/pjpeg', 'image/gif', 'image/png');
				$uploadAgent->setAvailableTypes($avTypes);
				$uploadDir = $_SERVER['DOCUMENT_ROOT'] .'/web/images/uploaded/files/';
				$uploadAgent->setUploadDir($uploadDir);

				if($uploadAgent->uploadFile())
				{
					if (isset($session->page2['propertyId']) && isset($session->page2['brochure']))
					{
						// delete file downloaded before
						$fileName = $uploadDir . $session->page2['brochure'];
						if (file_exists($fileName))
							unlink($fileName);
					}
					$session->page2['brochure'] = $uploadAgent->getUniqueFileName();
				}

				if (isset($propertyId))	// Update ad with selected id
				{
					$session->page2['propertyId'] = PropertyTable::getInstance()->saveData($propertyId);
				}
				else	// Insert new ad
				{
					$session->page2['propertyId'] = PropertyTable::getInstance()->saveData();
				}
				$this->_redirect('/'.$this->_controllerPath.'/sell-property-photo');
			}
			else
			{
				// if inner POST then updating field
				$this->view->row = $session->page2;
			}
		}
		else
		{
			unset($session->page2);

			if (isset($propertyId))	// Edit ad with selected id
			{
				$session->page2 = PropertyTable::getInstance()->find($propertyId)->current()->toArray();
				$session->page2['propertyId'] = $propertyId;

				$session->page1['categoryId'] = $session->page2['categoryId'];
				$session->page1['typeId'] = $session->page2['typeId'];
				$session->page1['subTypeId'] = $session->page2['subTypeId'];

				// fetching from db appropriate countries, regions & subRegions for selected country

				// fetching from db appropriate rows
				$this->view->regions = CountryTable::getInstance()->getRegionsById($session->page2['countryId']);
				$session->page2['regionId'] = empty($this->view->regions) ? null : $session->page2['regionId'];

				// fetching from db appropriate rows
				$this->view->places = RegionTable::getInstance()->getPlacesById($session->page2['regionId']);
				$session->page2['placeId'] = empty($this->view->places) ? null : $session->page2['placeId'];

				// other fields (select tags)
				$session->page2['featureArray'] = explode("\n", $session->page2['features']);
				$session->page2['dimensionArray'] = explode("\n", $session->page2['dimensions']);

				// String like: Balcony,Communal Garden
				// needs: 0,1
				$this->view->outdoorValues = explode(',', $session->page2['outdoor']);
				$this->view->parkingValues = explode(',', $session->page2['parking']);
				$this->view->tenureValues = explode(',', $session->page2['tenure']);
				$this->view->ofValues = explode(',', $session->page2['otherFeatures']);
				$this->view->propertyId = $propertyId;
				$this->view->row = $session->page2;
			}
			else	// First time
			{
				// GET without parameters (1st time)
				// set by default United Kingdom
				$session->page2 = PropertyTable::getInstance()->getDefaultValues($this->_defaultCountry, $this->_defaultPropertyStatus);
				$this->view->regions = CountryTable::getInstance()->getRegionsById($session->page2['countryId']);
				$this->view->row = $session->page2;
			}
		}

		$this->view->outdoorArray = PropertyTable::getInstance()->fetchEnumValues('outdoor');
		$this->view->parkingArray = PropertyTable::getInstance()->fetchEnumValues('parking');
		$this->view->tenureArray = PropertyTable::getInstance()->fetchEnumValues('tenure');
		$this->view->ofArray = PropertyTable::getInstance()->fetchEnumValues('otherFeatures');
		$this->view->buildTypes = PropertyTable::getInstance()->fetchEnumValues('buildType');
		$this->view->bedRoomArray = PropertyTable::getInstance()->fetchEnumValues('bedRooms');
		$this->view->bathRoomArray = PropertyTable::getInstance()->fetchEnumValues('bathRooms');
		$this->view->receptionArray = PropertyTable::getInstance()->fetchEnumValues('reception');
		$this->view->garageArray = PropertyTable::getInstance()->fetchEnumValues('garage');
		$this->view->psArray = PropertyTable::getInstance()->fetchEnumValues('propertyStatus');
		$this->view->statusArray = PropertyTable::getInstance()->fetchEnumValues('status');
	}

	/**
	 * Script to add some photos for specified property
	 *
	 */
	public function sellPropertyPhotoAction()
	{
		$session = new Zend_Session_Namespace('propertyInsertSession');

		if ($this->_hasParam('propertyId')) // GET method
		{
			$propertyId = $this->_getParam('propertyId');
		}
		elseif (isset($session->page2['propertyId'])) // POST method
		{
			$propertyId = $session->page2['propertyId'];
		}
		else // if we can not receive propertyId from GET or POST then exit
		{
			$this->_redirect('/');
		}

		if ( $this->getRequest()->isPost() )
		{
			$postArray = $this->_getParam('addPropertyPhoto');

			if (isset($postArray['btnNext']))
			{
				$this->_redirect('/'.$this->_controllerPath.'/sell-review-property');
			}
			
			// Uploading image...
			if (isset($postArray['btnAdd']))
			{
				if ($postArray['txtTitle'] == '')
				{
					$postArray['txtTitle'] = 'No description';
				}
				// uploading file, if ok, saving filename into gm_photo & adding record into gm_Photo
				$uploadAgent = new Uploader('txtFile');
				$avTypes = array('image/jpeg', 'image/pjpeg', 'image/gif', 'image/png');
				$uploadAgent->setAvailableTypes($avTypes);
				$uploadDir = $_SERVER['DOCUMENT_ROOT']. '/web/images/uploaded/photos/';
				$uploadAgent->setUploadDir($uploadDir);

				if($uploadAgent->uploadFile())
				{
					$rowToInsert = array(
					'title' => $postArray['txtTitle'],
					'file' => $uploadAgent->getUniqueFileName(),
					'byDef' => 0,
					'propertyId' => $propertyId,
					);
					// check if it is the first image then set it default image byDef = 1
					$hasImages = count(PhotoTable::getInstance()->fetchAll('propertyId = '.$propertyId));
					if (!$hasImages)
						$rowToInsert['byDef'] = 1;
						
					$photoId = PhotoTable::getInstance()->insert($rowToInsert);
				}
			}

			// set default or remove
			$needsEditing = $this->_getParam('__EVENTTARGET');
			if (!empty($needsEditing))
			{
				$param = $this->_getParam('__EVENTTARGET');
				// format -- :24:btnDefault
				list(, $photoId, $action) = split(':', $param);

				switch ($action)
				{
					case 'removeImage':
						$where = PhotoTable::getInstance()->getAdapter()->quoteInto('id = ?', $photoId);
						PhotoTable::getInstance()->delete($where);

						break;
					case 'setDefault':
						// update if propertyId == ours && byDef == 1
						$data = array('byDef'=>0);
						$where = PhotoTable::getInstance()->getAdapter()->quoteInto('byDef = 1 AND propertyId = ?', $propertyId);
						$photo = PhotoTable::getInstance()->fetchRow($where);

						if (!empty($photo))
						{
							$where = PhotoTable::getInstance()->getAdapter()->quoteInto('id = ?', $photo->id);
							PhotoTable::getInstance()->update($data, $where);
						}

						$data = array('byDef'=>1);
						$where = PhotoTable::getInstance()->getAdapter()->quoteInto('id = ?', $photoId);
						PhotoTable::getInstance()->update($data, $where);

						break;
					default:
						break;
				}
			}

			$where = PhotoTable::getInstance()->getAdapter()->quoteInto('propertyId = ?', $propertyId);
			$this->view->photos = PhotoTable::getInstance()->fetchAll($where)->toArray();
		}
		// GET request
		else
		{
			$where = PhotoTable::getInstance()->getAdapter()->quoteInto('propertyId = ?', $propertyId);
			$this->view->photos = PhotoTable::getInstance()->fetchAll($where)->toArray();
		}
	}
	
	/**
	 * Script to review all data saved before
	 *
	 */
	public function sellReviewPropertyAction()
	{
		if ( $this->getRequest()->isPost() )
		{
			$postArray = $this->_getParam('viewProperty');

			if (isset($postArray['btnNext']))
			{
				$this->_redirect('/'.$this->_controllerPath.'/confirmation');
			}
		}
		
		Property::getInstance()->doReview($this->view);
	}

	/**
	 * Script to confirm all changes
	 *
	 */
	public function confirmationAction()
	{
		if ($this->_request->isPost())
		{
			$postArray = $this->_getParam('confirmation');

			if (isset($postArray['btnConfirm']))
			{
				$this->_redirect('/common/');
			}
		}
	}

	/**
	 * Utils to convert posted array of selected fields into integer value to insert it later into db
	 *
	 */
	private function convertIntoSetValue($arr)
	{
		$outdoor = 0;
		foreach ($arr as $value)
		{
			$outdoor += pow(2, $value);
		}
		return $outdoor;
	}
	
	protected $_defaultPropertyStatus; // property status set by default
	protected $_defaultCountry; // country set by default
	protected $_controllerPath; // path to scripts
}
