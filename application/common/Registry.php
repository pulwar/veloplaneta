<?php

define('REGISTRY_DATABASE', 'db');
//define('REGISTRY_ACL', 'acl');
define('REGISTRY_USER', 'user');
define('REGISTRY_VIEW', 'view');
//...

/**
 * Convenient access to application registry
 *
 */
class Registry extends Zend_Registry {
	
	/**
	 * Получение соединения с БД
	 *
	 * @return object
	 */
	public static function getDatabase(){
		return  Zend_Registry::get(REGISTRY_DATABASE);
	}
	
	/**
	 *Запись установленного соединения
	 * в регистр
	 *
	 * @param object $db
	 */
	public static function setDatabase($db){
		  Zend_Registry::set(REGISTRY_DATABASE, $db);
	}
	
	/**
	 * Получение объекта вида
	 *
	 * @return object
	 */
	public static function getView(){
		return  Zend_Registry::get(REGISTRY_VIEW);
	}
	
	/**
	 * Запись объекта вида в регистр
	 * 
	 * @param object $view
	 */
	public static function setView($view){
		 Zend_Registry::set(REGISTRY_VIEW, $view);
	}
	
}