/*
 * Ext JS Library 2.2
 * Copyright(c) 2006-2008, Ext JS, LLC.
 * licensing@extjs.com
 * 
 * http://extjs.com/license
 */
var mega_price=0;
Ext.onReady(function(){
	
	
 	//slider site type --------------------------------------------------------------
	
	
   var siteTip = new Ext.ux.SliderTip({
        getText: function(sliderSiteType){
        var value=sliderSiteType.getValue();		 
        var text=swich_value_site(value);       
            return String.format("<b class=site_type>"+text[0] +"</b>", sliderSiteType.getValue());            
        }
    });

  var sliderSiteType=  new Ext.Slider({
        renderTo: 'site_type',
        width: 426,
		clickToChange: false,		
        increment: 100,
        minValue: 600,		
        maxValue: 1000,        
        plugins: siteTip
    });
    

      
  sliderSiteType.on('changecomplete', function(){	
	var value = sliderSiteType.getValue();
	 div = document.getElementById('site_cost'); 
	 $("#site_cost").html("+"+value + "<b>$</b>");
	 var info = swich_value_site(value);
	 $("#site_name").html(info[1]);  
      calc_prise(sliderSiteType,sliderDesignType, flashType, searchType ); 
	  $("#ext-gen19").remove();
	});  
	
// slyder design type-------------------------------------------------------------------------------

var designTip = new Ext.ux.SliderTip({
        getText: function(sliderDesignType){
			var value=sliderDesignType.getValue();
			 var name=swich_value_design(value);	
            return String.format("<b class=site_type>"+name[0] +"</b>", sliderDesignType.getValue());
        }
    });

  var sliderDesignType=  new Ext.Slider({
        renderTo: 'design_type',
        width: 424,
        clickToChange: false,
        increment: 100,
        minValue: 200,
        value: 200,
        maxValue: 700,        
        plugins: designTip
    });
    

      
  sliderDesignType.on('changecomplete', function(){  
	  	var value = sliderDesignType.getValue();  
	 $("#design_cost").html("+"+value+"<b>$</b>"); 
		var info = swich_value_design(value);
		$("#design_name").html(info[1]);
        calc_prise(sliderSiteType,sliderDesignType, flashType, searchType );  
		$("#ext-gen34").remove();
	});  
	
	
//--- slider flash-----------------------------------------------	
	

var flashTip = new Ext.ux.SliderTip({
        getText: function(flashType){
			var value=flashType.getValue();
			 var name=swich_value_flash(value);	
            return String.format("<b class=site_type>"+name[0] +"</b>", flashType.getValue());
        }
    });

  var flashType=  new Ext.Slider({
        renderTo: 'flash_type',
        width: 424,
        clickToChange: false,
        increment: 50,
        minValue: 0,
        value: 0,
        maxValue: 150,        
        plugins: flashTip
    });
    

      
  flashType.on('changecomplete', function(){  
	  	var value = flashType.getValue();  
	    $("#flash_cost").html("+"+value+"<b>$</b>"); 
		var info = swich_value_flash(value);		
		$("#flash_name").html(info[1]);
        calc_prise(sliderSiteType,sliderDesignType, flashType, searchType );  
		$("#ext-gen48").remove();
	});  
	
// search type--------------------------------------
	var searchTip = new Ext.ux.SliderTip({
        getText: function(searchType){
			var value=searchType.getValue();
			 var name=swich_value_search(value);	
            return String.format("<b class=site_type>"+name[0] +"</b>", searchType.getValue());
        }
    });

  var searchType=  new Ext.Slider({
        renderTo: 'search_type',
        width: 424,
        clickToChange: false,
        increment: 50,
        minValue: 0,
        value: 0,
        maxValue: 150,        
        plugins: searchTip
    });
    

      
  searchType.on('changecomplete', function(){  
	  	var value = searchType.getValue();  
	    $("#search_cost").html("+"+value+"<b>$</b>"); 
		var info = swich_value_search(value);		
		$("#search_name").html(info[1]);
        calc_prise(sliderSiteType,sliderDesignType, flashType, searchType );  
		$("#ext-gen62").remove();
	});  
	
  // end sliders -----------------------------------  
	//загрузка шкалы лоя слайдеров
	$("#site_type").css("background","url(/img/site_type.gif) no-repeat 0 15px");
    $("#design_type").css("background","url(/img/design_type.gif) no-repeat 0 15px");
    $("#flash_type").css("background","url(/img/flash_type.gif) no-repeat 0 15px");
    $("#search_type").css("background","url(/img/search_type.gif) no-repeat 0 15px");
	
	 $("input[block]").click(function () { 
		   var price = 	calc_blocks();
		  if (price){	
			$("#total_blocks").html(price);				  
		  } else {
			$("#total_blocks").html("0");	
		  }
		  $("#total_price").html(price+mega_price);
    });  	
	 calc_prise(sliderSiteType,sliderDesignType, flashType, searchType );
	 
	 $("#ext-gen19").remove();
	 $("#ext-gen34").remove();
	 $("#ext-gen48").remove();
	 $("#ext-gen62").remove();
	 
});

function calc_prise(sliderSiteType,sliderDesignType, flashType, searchType ){
	var price1 =sliderSiteType.getValue();
	var price2 =sliderDesignType.getValue();	
	var price3 =flashType.getValue();	
	var price4 =searchType.getValue();	
	mega_price = parseInt(price1)+parseInt(price2)+parseInt(price3)+parseInt(price4);
	var total_price =mega_price +calc_blocks();
    $("#total_price").html(total_price); 
	$("#sliders_price").attr("value",total_price);

}

function calc_blocks(){
	var price =0;
	$("input[block]").each(function (i) {					
		if (this.checked) {
		  price= price+parseInt(this.value);
		} 
	});
	return price;
}	

function swich_value_site(value){
	var info =new Array(2);	
		switch (value){
			case 600:
			  info[0]=" Сайт визитка";
			  info[1]=" Небольшой представительский веб-сайт";
			  break;
			case 700:
			   info[0]=" Корпаративный";
			   info[1]=" Информационный ресурс способствующий  продвижению компании";
			  break;  
			case 800:
			   info[0]=" Промо-сайт";
			   info[1]=" Используется для продвижения торговой марки, товаров или услуг";
			  break;
			case 900:
			  info[0]=" Магазин";
			  info[1]=" Сайт с системой управления, каталогом товаров и обработкой заказов";
			  break;	
			case 1000:
			  info[0]=" Портал";
			  info[1]=" Специализированный ресурс с большим объемам информации";
			  break; 
			default:  
			  info[0]=" ";
			  info[1]=" ";
			   break;  
		}
	return info;
}

function swich_value_design(value){
	var info = new Array(2);
		switch (value){
			case 200:
			  info[0]=" Мини"	
			  info[1]=" Простой дизайн с минимумом графики";
			  break;
			case 300:
			 info[0]=" Стандарт";
			 info[1]=" Дизайн на базе фотоматериалов и клипартов";
			  break;
			case 400:
			  info[0]=" Премиум";
			  info[1]=" Дизайн с применением прорисовки отдельных элементов";
			  break;
			case 700:
			  info[0]=" Элит";
			  info[1]=" Дизайн на базе авторской художественной работы";
			  break;
			default:  
			  info[0]=" ";
			  info[1]=" ";
			   break;
		}
	return info;
}	

function swich_value_flash(value){
	var info = new Array(2);
		switch (value){
			case 0:
			  info[0]="Нет"	
			  info[1]=" Описание...";
			  break;
			case 50:
			 info[0]=" Небольшая";
			 info[1]=" Описание...";
			  break;
			case 100:
			  info[0]=" Крутая";
			  info[1]=" Описание...";
			  break;
			case 150:
			  info[0]=" Мега крутая";
			  info[1]=" Описание...";
			  break; 
			default:  
			  info[0]=" ";
			  info[1]=" ";
			   break;  
		}
	return info;
}	


function swich_value_search(value){
	var info = new Array(2);
		switch (value){
			case 0:
			  info[0]="Нет"	
			  info[1]=" Описание...";
			  break;
			case 50:
			 info[0]=" Простой";
			 info[1]=" Поиск по содержимому сайта с выводом списка ссылок на найденные страницы...";
			  break;
			case 100:
			  info[0]=" Крутая";
			  info[1]=" Описание...";
			  break;
			case 150:
			  info[0]=" Мега крутая";
			  info[1]=" Описание...";
			  break;  
			default:  
			  info[0]=" ";
			  info[1]=" ";
			   break;  
		}
	return info;
}	
