<?php
ini_set('display_errors', 0);
$DB_HOST = "cabinet.awagro.by";
$DB_NAME = "veloplan_veloplaneta";
$DB_USER = "veloplan_vel";
$DB_PASS = "Frktk65l5l5T";

$dir = dirname(__FILE__);
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$razdel_pic = array(
		'����c����� GT' => 'logo_gt.png',
		'���������� CANNONDALE' => 'logo_cannondale.png',
		'���������� LTD' => 'logo_ltd.png',
		'���������� Orbea' => 'logo_orbea.png',
		'���������� Aist' => 'logo_aist.png',
		'���������� Rizer' => 'logo_rizer.png',
		'���������� MERIDA' => 'logo_merida.png',
		'���������� FORD by Dahon' => 'logo_ford.png',
		'���������� CENTURION' => 'logo_centurion.png'

);

if (file_exists($dir . '/export.txt')) {
	//$fl = fopen($dir . '/export.txt', 'r+');
	//$ser_struct = fread($fl, filesize($dir . '/export.txt'));
	$ser_struct = file_get_contents($dir . '/export.txt');
	
	$structura = unserialize($ser_struct);
	
	$catalog = $structura['CATALOG'];
	unset($structura['CATALOG']);
	
	$accesories = $structura['ACCESORIES'];
	unset($structura['ACCESORIES']);
	
	$winter = $structura['WINTER'];
	unset($structura['WINTER']);
	
	$news = $structura['NEWS'];
	unset($structura['NEWS']);
	
	//fclose($fl);
	
	$conn = new PDO("mysql:host=" . $DB_HOST . ";dbname=" . $DB_NAME, $DB_USER, $DB_PASS);
	
	$conn->exec('SET NAMES cp1251');
	try {
		$conn->beginTransaction();
		$conn->exec('DELETE FROM catalog_division WHERE level > 0');
		foreach ($catalog as $razdel) {
			if ($razdel['id'] != 19000) {
				$query = "INSERT INTO catalog_division(
        						id,
        						name, 
        						parent_id, 
        						id_page, 
        						level, 
        						sortid,
                                img
                               ) 
        				  VALUES(
        				  	" . $razdel['id'] . ", 
        				  	'" . $razdel['name'] . "',
        				  	 1, 
        				  	 14, 
        				  	 1, 
        				  	 " . $razdel['sortid'] . ",
                             '" . $razdel_pic[$razdel['name']] . "'    
        		 )";
				
				$conn->exec($query);
				foreach ($razdel['elements'] as $element) {
					
					$query_element = "INSERT INTO catalog_division (
            								id,
            								name, 
            								description, 
            								level, 
            								parent_id, 
            								id_page, 
            								sortid
            								
            								) 
            							VALUES (" . $element['id'] . ",
            								'" . $element['name'] . "', 
            								'" . $element['description'] . "',
            								 2, 
            								 " . $element['parent_id'] . ", 
            								 14, 
            								 " . $element['sortid'] . "
            								   
            )";
					
					$conn->exec($query_element);
					foreach ($element['tovar'] as $tovar) {
						if(isset($tovar['property']['three_d_image'])){
                            $show3Dmodule = 1;//echo '<pre>';print_r($tovar);exit;
                        }else{
                            $show3Dmodule = 0;
                        }
						$tovar['intro'] = parseLiks($tovar['intro']);
						$tovar['description'] = parseLiks($tovar['description']);
						
						$is_new_property = 'ishit';
						$is_new = getTovarProperty($tovar['property'], $is_new_property);
						//print_r($tovar['property']);
						unset($tovar['property'][$is_new_property]);
						
						$descriprion = $tovar['description'] . ' ' . getHtmlParams($tovar['property']);
						$bykeType = getBykeType($tovar['property']);
						$humanType = getHumanType($tovar['property']);
						$query_tovar = "INSERT INTO catalog_tovar (
                								id, 
                								division_id, 
                								name, 
                								intro,  
                								description, 
                								prior, 
                								price,
                								pricerub,
            									oldprice,
            									img_small,
            									img_show,
            									img_big,
												is_new,
												bike_type,
												human_type,
												show_3dmodel											
                								) 
                							VALUES (
                								" . $tovar['id'] . ", 
                								" . $tovar['division_id'] . ", 
                								'" . $tovar['name'] . "', 
                								'" . $tovar['intro'] . "', 
                								'" . $descriprion . "', 
                								" . $tovar['prior'] . ",
                								" . $tovar['price'] . ",
                								" . 0 . ",
            								 	" . $tovar['oldprice'] . ",
            								 	'" . $tovar['img_small_path'] . "',
            								 	'" . $tovar['img_show_path'] . "',
            								 	'" . $tovar['property']['img_verybig']['property_value'] . "',
												" . (int) $is_new . ",
												" . (int) $bykeType . ",
												" . (int) $humanType . ",
												" . $show3Dmodule . "
                )";
						//  echo  $query_tovar; exit;               
						$conn->exec($query_tovar);
					}
				}
			}
		}
		
		// ������ �� ����������
		

		foreach ($accesories as $element) {
			
			$query_element = "INSERT INTO catalog_division (
            								id,
            								name, 
            								description, 
            								level, 
            								parent_id, 
            								id_page, 
            								sortid
            								
            								) 
            							VALUES (" . $element['id'] . ",
            								'" . $element['name'] . "', 
            								'" . $element['description'] . "',
            								 1, 
            								 2, 
            								 14, 
            								 " . $element['sortid'] . "
            								   
            )";
			
			$conn->exec($query_element);
			foreach ($element['tovar'] as $tovar) {
				$is_new_property = 'ishit';
				$is_new = getTovarProperty($tovar['property'], $is_new_property);
				//print_r($tovar['property']);
				unset($tovar['property'][$is_new_property]);
				
				$descriprion = $tovar['description'] . ' ' . getHtmlParams($tovar['property']);
				$tovar['intro'] = parseLiks($tovar['intro']);
				$tovar['description'] = parseLiks($tovar['description']);
				$query_tovar = "INSERT INTO catalog_tovar (
                								id, 
                								division_id, 
                								name, 
                								intro,  
                								description, 
                								prior, 
                								price,
                								pricerub,
            									oldprice,
            									img_small,
            									img_show,
            									img_big,
												is_new								
								) VALUES (
                								" . $tovar['id'] . ", 
                								" . $tovar['division_id'] . ", 
                								'" . $tovar['name'] . "', 
                								'" . $tovar['intro'] . "', 
                								'" . $descriprion . "', 
                								" . $tovar['prior'] . ",
                								" . $tovar['price'] . ",
                								" . 0 . ",
            								 	" . $tovar['oldprice'] . ",
            								 	'" . $tovar['img_small_path'] . "',
            								 	'" . $tovar['img_show_path'] . "',
            								 	'" . $tovar['property']['img_verybig']['property_value'] . "',
												" . (int) $is_new . "
                )";
				//  echo  $query_tovar; exit;               
				$conn->exec($query_tovar);
			}
		}
		
		
		// ������ �� ������ �������
		

		foreach ($winter as $element) {
			
			$query_element = "INSERT INTO catalog_division (
            								id,
            								name, 
            								description, 
            								level, 
            								parent_id, 
            								id_page, 
            								sortid
            								
            								) 
            							VALUES (" . $element['id'] . ",
            								'" . $element['name'] . "', 
            								'" . $element['description'] . "',
            								 1, 
            								 413001, 
            								 14, 
            								 " . $element['sortid'] . "
            								   
            )";
			
			$conn->exec($query_element);
			foreach ($element['tovar'] as $tovar) {
				$is_new_property = 'ishit';
				$is_new = getTovarProperty($tovar['property'], $is_new_property);
				unset($tovar['property'][$is_new_property]);
				
				$descriprion = $tovar['description'] . ' ' . getHtmlParams($tovar['property']);
				$tovar['intro'] = parseLiks($tovar['intro']);
				$tovar['description'] = parseLiks($tovar['description']);
				$query_tovar = "INSERT INTO catalog_tovar (
                								id, 
                								division_id, 
                								name, 
                								intro,  
                								description, 
                								prior, 
                								price,
                								pricerub,
            									oldprice,
            									img_small,
            									img_show,
            									img_big,
												is_new
								) VALUES (
                								" . $tovar['id'] . ", 
                								" . $tovar['division_id'] . ", 
                								'" . $tovar['name'] . "', 
                								'" . $tovar['intro'] . "', 
                								'" . $descriprion . "', 
                								" . $tovar['prior'] . ",
                								" . $tovar['price'] . ",
                								" . 0 . ",
            								 	" . $tovar['oldprice'] . ",
            								 	'" . $tovar['img_small_path'] . "',
            								 	'" . $tovar['img_show_path'] . "',
            								 	'" . $tovar['property']['img_verybig']['property_value'] . "',
												" . (int) $is_new . "
                )";
			              
				$conn->exec($query_tovar);
			}
		}		
		
		

		
		$query_news = "DELETE FROM site_news WHERE intro like '%http://velozona.by/news%'";
		$conn->exec($query_news);
		$query_id = "SELECT MAX(id) as max_id FROM site_news";
		$result_id = $conn->query($query_id)->fetchAll();
		$news_id = $result_id[0]['max_id'];
		foreach ($news as $newsid => $newsitem) {
			$news_id++;
			$query_new = "INSERT INTO site_news (
					id,
					page_id,
					peoples_id,
					type,
					name,
					url,
					intro,
					content,
					pub,
					main,
					created_at,
					pub_date,
					number
				) VALUES (
					" . $news_id . ",
					4,
					0,
					'news',
					'" . $newsitem['name'] . "',
					'',
					'<p><a href=\"http://velozona.by/news/" . $newsitem['id'] . ".html\">" . $newsitem['name'] . "</a></p>',
					'',
					1,
					1,
					'" . $newsitem['date'] . "',
					'" . $newsitem['date'] . "',
					0
				)";
			$conn->exec($query_new);
		}
		
		$conn->exec("UPDATE `catalog_division` SET season ='summer' WHERE level>0");
		$conn->commit();
		@unlink($dir . '/export.txt');
		$conn = null;
	} catch (PDOException $e) {
		$conn->rollBack();
		$conn = null;
	}
} else {
	echo ' ���� export.txt �� ����������';
}

function getHtmlParams($params) {
	$html = '<table>';
	foreach ($params as $key => $param) {
		$row = array_values($param);
		if ($key != 'img_big_2' && $key != 'img_big_3' && $key != 'img_verybig_2' && $key != 'img_verybig' && $key != 'img_verybig_3' && $key != 'where_buy' && $row[1] != '')
			
			$html .= '<tr>
					<td>' . replaceQuotes($row['0']) . '</td>' . '<td>' . replaceQuotes($row['1']) . '</td>' . '</tr>';
	}
	$html .= '</table>';
	return $html;
}

function getTovarProperty($params, $is_new_property) {
	if (isset($params[$is_new_property])) {
		$row = array_values($params[$is_new_property]);
		if ($row[1] == '��') {
			return 1;
		}
	}
	return 0;
}

function getBykeType($params) {
	$bikeType = array(
			1 => "������",
			2 => "���������",
			3 => "����������",
			4 => "���������",
			5 => "Dirt Street",
			6 => "BMX", 
			7 => "��������"
	);
	$type = array_flip($bikeType);
	if (isset($params['type']) && isset($type[$params['type']['property_value']])) {
		return $type[$params['type']['property_value']];
	}
	return 0;
}

function getHumanType($params) {
	$humanType = array(
			1 => "�������",
			2 => "�������",
			4 => "�������" 
	);
	$type = array_flip($humanType);
	
	if (isset($params['purpose']) && isset($type[$params['purpose']['property_value']])) {
		return $type[$params['purpose']['property_value']];
	}
	return 0;
}

function parseLiks($text) {
	$text = str_replace('ffff99', '000000', $text);
	$text = str_replace('href="/articles', 'href="http://velozona.by/articles', $text);
	//return preg_replace('/href=\"(\/articles\/)(.+)\"/si', 'href="http://velozona.by\\1\\2"', $text, -1);
	return $text;
}

function replaceQuotes($text) {
	$to_replace = array(
			'\'' 
	);
	return str_replace($to_replace, '&quot;', $text);
}

?>
