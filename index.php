<?php
include_once 'start.inc.php';
define('ROOT_DIR', (dirname( __FILE__ )) . DIRECTORY_SEPARATOR);

require_once(ROOT_DIR . 'application/system/Loader.php');
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
//ini_set('Error_Reporting', 'E_ALL & ~E_NOTICE');
Loader::load('application/system/Bootstrap');

ini_set("magic_quotes_gpc", "0");
ini_set('magic_quotes_runtime', "0");
//echo get_magic_quotes_gpc();         exit;


Bootstrap::start();
include_once 'end.inc.php';