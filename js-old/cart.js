$(document).ready(function(){
        $(".hidden").toggle();
        $('.subitem').children('a').click(function(){
            $(this).parent('.subitem').children(".catalogLastitem").slideToggle("slow");
            if ($(this).parent('.subitem').hasClass('open')) $(this).parent('.subitem').removeClass('open')
            else $(this).parent('.subitem').addClass('open');
        });
        $('.catalogMainItem').children('.catalogItem').click(function(){
            $(this).parent('.catalogMainItem').children(".catalogSubitem").slideToggle("slow");
        });


	// add product to cart	 
	   $("a[add_to_cart='true']").click( function() {
			var id = $(this).attr("prod_id")*1;
   
		//alert(id);
			if(id>0){                                
				$.get(
					"/catalog/cart/add/",
					{prod_id: id, count:1},
					 function(data){
					 //alert("Data Loaded: " + data);
					   // reload cart
					   $("#cart_preview").load("/catalog/cart/preview/");
                                                if(data=='ok'){
                                                        alert('Товар добавлен в корзину.');
                                                        $("#added").fadeIn(1000);
                                                        $("#added").fadeOut(1000);
                                                }
					   } 					   
				);
			}	
		return false;
	  });
	
	// delete product from cart
	$("a[delete_from_cart='true']").click(function(){
		var id = $(this).attr("prod_id")*1;
		//alert(id);
		if(id>0){
			$.get(
				"/catalog/cart/delete/",
				{prod_id: id},
				 function(data){
				   //alert("Data Loaded: " + data);				  
				   // reload cart
				   $("#cart_preview").load("/catalog/cart/preview/");
				    /* if(data=='ok'){
						$("#removed").slideDown(1000, function(){
							$("#removed").fadeOut(700);
							
						});
				   } */
				  
				 }
			);
		}
		 return false;
	});
	
	// clear cart 
	
	 $("a[clear_cart='true']").click(function() {
		$.get(
			"/catalog/cart/clear/",				
			 function(data){
			  // alert("Data Loaded: " + data);
			   // reload cart			    
			   $("#cart_preview").load("/catalog/cart/preview/");
			  /*  if(data=='ok'){
						$("#clear").slideDown(1000, function(){
							$("#clear").fadeOut(700);
							
						});
				   } */
				   
			 }
				   
		);
	return false; 	
	});

});

