//send request with params
	function SendRequest(Url , Params, MethodType, ClassName){
			var myAjax = new Ajax.Request( Url, {
				method: MethodType, parameters: Params, onComplete: function(originalRequest) {
					response= originalRequest.responseText;
					if(ClassName){
					$(ClassName).innerHTML = originalRequest.responseText;
					}
				}
			});
			if(ClassName){
				$(ClassName).innerHTML ='<div align="center"><img src="/img/loading.gif"></div>'+ $(ClassName).innerHTML;
			}
			return false;
	
	}

//basket
	function addTovarToBasket(id_tovar, ClassName) {
	
			var Url = '/Basket/add/';
			var Params = 'id_tovar=' + id_tovar ;
			var MethodType = 'get';
			SendRequest(Url , Params, MethodType, ClassName);
			return false;
		}

	function deleteTovarFromBasket(id_tovar, ClassName) {
			var Url = '/Basket/delete/';
			var Params = 'id_tovar=' + id_tovar ;
			var MethodType = 'get';
			SendRequest(Url , Params, MethodType, ClassName);			
	}
	
	function deleteTovarFromBasketFront(id_tovar, ClassName) {
			var Url = '/Basket/deletefront/';
			var Params = 'id_tovar=' + id_tovar ;
			var MethodType = 'get';
			SendRequest(Url , Params, MethodType, ClassName);			
	}

	function clearBasket(ClassName){
		var Url = '/Basket/clear/';
		var Params = '' ;
		var MethodType = 'get';
		SendRequest(Url , Params, MethodType, ClassName);
		return false;
	}
	
	function chengeCount(id_tovar, count){
		var Url = '/Basket/count/';
		var Params = 'id_tovar=' + id_tovar+'&count='+count;
		var MethodType = 'get';
		SendRequest(Url , Params, MethodType, null);
		return false;
	}	
	
	
	
	
//compare
	function addTovarToCompare(id_tovar, ClassName) {
	
			var Url = '/Compare/add/';
			var Params = 'id_tovar=' + id_tovar ;
			var MethodType = 'get';
			SendRequest(Url , Params, MethodType, ClassName);
			return false;
		}

	function deleteTovarFromCompare(id_tovar, ClassName) {
			var Url = '/Compare/delete/';
			var Params = 'id_tovar=' + id_tovar ;
			var MethodType = 'get';
			SendRequest(Url , Params, MethodType, ClassName);			
	}

	function clearCompare(ClassName){
		var Url = '/Compare/clear/';
		var Params = '' ;
		var MethodType = 'get';
		SendRequest(Url , Params, MethodType, ClassName);
		return false;
	}
